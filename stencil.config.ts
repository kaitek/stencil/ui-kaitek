import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

// https://stenciljs.com/docs/config

export const config: Config = {
  namespace: "ui-kaitek",
  enableCache: true,
  hashFileNames: false,
  minifyJs: false,
  buildDist: true,
  globalStyle: 'src/global/app.css',
  globalScript: 'src/global/app.ts',
  sourceMap: false,
  taskQueue: 'async',
  transformAliasedImportPaths: false,
  //watchIgnoredRegex: /\.@mescius*$/,
  outputTargets: [
    {
      type: 'www',
      dir: 'www',
      // comment the following line to disable service workers in production
      serviceWorker: null,
      baseUrl: 'https://myapp.local/',
    },
    //{
    //  type: 'dist',
    //  dir: 'dist',
    //  transformAliasedImportPathsInCollection: true,
    //  esmLoaderPath: '../loader'
    //},
    //{
    //  type: 'dist-custom-elements',
    //  dir: 'dist_single',
    //  customElementsExportBehavior: 'default',
    //},
  ],
  plugins: [
    //nodePolyfills(),
    sass({
      injectGlobalPaths: [
        'src/global/variables.scss'
      ]
    })
  ],
  devServer: {
    reloadStrategy: 'pageReload',
    port: 4446
  }
};
