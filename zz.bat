@echo off
SET loc=%~dp0
set CURRENT_DATE=%date:~10,4%%date:~7,2%%date:~4,2%
for /d %%X in (%loc%) do "C:\Program Files\7-Zip\7z.exe" a "%loc%/ui-kaitek_%CURRENT_DATE%.7z" -xr!.git -xr!.stencil -xr!loader -xr!node_modules -xr!www "%%X\\"