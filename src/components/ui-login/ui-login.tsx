import { Component, Element, Event, EventEmitter, h, Method } from '@stencil/core';
import '@vaadin/horizontal-layout';
import '@vaadin/login';
import '@vaadin/login/vaadin-login-form.js';
import '@vaadin/notification';
import '@vaadin/vertical-layout';
//import type { Button } from '@vaadin/button';
import type { LoginForm } from '@vaadin/login/vaadin-login-form.js';
import type { LoginI18n } from '@vaadin/login';
import type { PasswordField } from '@vaadin/password-field';
import { Notification } from '@vaadin/notification/vaadin-notification';

@Component({
  tag: 'ui-login',
  styleUrl: 'ui-login.scss',
  shadow: false// theme swicher çalışmıyor
})
export class UILogin {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'uiLoginEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uiLoginEvent: EventEmitter;

  container: HTMLDivElement;
  loginForm: LoginForm;
  //btnSubmit: Button;
  btnTheme: HTMLButtonElement;
  storageKey: string = 'theme-preference';
  currentTheme: any;
  //notification: Notification;
  notificationPos: 'top-stretch' | 'top-start' | 'top-center' | 'top-end' | 'middle' | 'bottom-start' | 'bottom-center' | 'bottom-end' | 'bottom-stretch' = 'top-stretch';

  private i18n: LoginI18n = {
    form: {
      title: '',
      username: 'E Posta',
      password: 'Şifre',
      submit: 'Giriş',
      forgotPassword: 'Şifremi Unuttum',
    },
    errorMessage: {
      title: 'Giriş Hatası',
      message: 'Bilgilerinizi kontrol ederek tekrar deneyiniz.',
      username: 'E Posta girmelisiniz',
      password: 'Şifre girmelisiniz',
    },
  };

  async componentWillLoad() {
    let t = this;
    t.currentTheme = { value: await t.getColorPreference() };
  }

  componentDidLoad() {
    let t = this;
    //t.btnSubmit = t.loginForm.querySelector('vaadin-button[slot=submit]');
    //const usernameInput: HTMLInputElement = t.loginForm.querySelector('input[name=username]');
    //usernameInput.focus();
    //usernameInput.blur();
    t.reflectPreference();

    window
      .matchMedia('(prefers-color-scheme: dark)')
      .addEventListener('change', ({ matches: isDark }) => {
        t.currentTheme.value = isDark ? 'dark' : 'light';
        t.setPreference();
      });
  }

  async getColorPreference() {
    let t = this;
    if (localStorage.getItem(t.storageKey)) {
      return localStorage.getItem(t.storageKey);
    } else {
      return window.matchMedia('(prefers-color-scheme: dark)').matches
        ? 'dark'
        : 'light';
    }
  }

  async reflectPreference() {
    let t = this;
    document.firstElementChild
      .setAttribute('theme', t.currentTheme.value);
    t.btnTheme.setAttribute('aria-label', t.currentTheme.value);
  }

  async setPreference() {
    let t = this;
    localStorage.setItem(t.storageKey, t.currentTheme.value);
    t.reflectPreference();
  }

  //@Method()
  async setSubmitButtonStatus(isEnable: boolean) {
    let t = this;
    if (isEnable) {
      t.loginForm.disabled = false;
      //t.btnSubmit.disabled = false;
    } else {
      t.loginForm.disabled = true;
      //t.btnSubmit.disabled = true;
    }
  }

  @Method()
  async showNotification(message: string) {
    let t = this;
    //if (t.notification.opened) {
    //  t.notification.opened = false;
    //}
    setTimeout(() => {
      //t.notification.opened = true;
      //t.notification = Notification.show(message, { position: t.notificationPos, duration: 900, theme: "error" });
      Notification.show(message, { position: t.notificationPos, duration: 5000, theme: "error" });
      //console.log('t.notification', t.notification);
      setTimeout(() => {
        if (message === 'Girmiş olduğunuz bilgiler hatalıdır.') {
          const passwordInput: PasswordField = t.loginForm.querySelector('vaadin-password-field[name=password]');
          passwordInput.value = '';
        }
        t.setSubmitButtonStatus(true);
      }, 1000);
    }, 100);
  }

  async buttonClick(data: any) {
    let t = this;
    //t.notification = Notification.show('Oturum açılıyor...', { position: t.notificationPos, duration: 900, theme: "contrast" });
    Notification.show('Oturum açılıyor...', { position: t.notificationPos, duration: 900, theme: "contrast" });
    setTimeout(() => {
      t.uiLoginEvent.emit({ type: 'submit', data });//element: t.btnSubmit
    }, 1000);
  }

  async toggleTheme() {
    let t = this;
    t.currentTheme.value = t.currentTheme.value === 'light'
      ? 'dark'
      : 'light';
    t.setPreference();
  }

  render() {
    let t = this;
    const theme: string = t.currentTheme.value;
    return (
      <div ref={(el) => t.container = el as HTMLDivElement} style={{ height: "98vh", width: "100%" }} class="login-container">
        <vaadin-horizontal-layout theme="spacing padding" style={{ justifyContent: "center", height: "100%", width: "100%" }}>
          <vaadin-vertical-layout theme="spacing padding" style={{ justifyContent: "center" }}>
            <vaadin-login-form ref={(el) => t.loginForm = el as LoginForm} i18n={this.i18n}
              class="login-form"
              //style={{background:"var(--lumo-base-color) linear-gradient(var(--lumo-tint-5pct), var(--lumo-tint-5pct))"}}
              no-autofocus
              no-forgot-password
              on-login={(e: CustomEvent) => t.buttonClick(e.detail)}
            ></vaadin-login-form>
            <vaadin-horizontal-layout theme="spacing padding" style={{ justifyContent: "center", height: "30px", width: "100%" }}>
              <button ref={(el) => t.btnTheme = el as HTMLButtonElement} class="theme-toggle"
                id="theme-toggle" title="Tema Değiştirici açık &amp; koyu" aria-label={theme} aria-live="polite"
                on-click={() => t.toggleTheme()}
              >
                <svg class="sun-and-moon" aria-hidden="true" width="24" height="24" viewBox="0 0 24 24">
                  <mask class="moon" id="moon-mask">
                    <rect x="0" y="0" width="100%" height="100%" fill="white"></rect>
                    <circle cx="24" cy="10" r="6" fill="black"></circle>
                  </mask>
                  <circle class="sun" cx="12" cy="12" r="6" mask="url(#moon-mask)" fill="currentColor"></circle>
                  <g class="sun-beams" stroke="currentColor">
                    <line x1="12" y1="1" x2="12" y2="3"></line>
                    <line x1="12" y1="21" x2="12" y2="23"></line>
                    <line x1="4.22" y1="4.22" x2="5.64" y2="5.64"></line>
                    <line x1="18.36" y1="18.36" x2="19.78" y2="19.78"></line>
                    <line x1="1" y1="12" x2="3" y2="12"></line>
                    <line x1="21" y1="12" x2="23" y2="12"></line>
                    <line x1="4.22" y1="19.78" x2="5.64" y2="18.36"></line>
                    <line x1="18.36" y1="5.64" x2="19.78" y2="4.22"></line>
                  </g>
                </svg>
              </button>
            </vaadin-horizontal-layout>
          </vaadin-vertical-layout>
        </vaadin-horizontal-layout>
      </div>
    );
  }
}
