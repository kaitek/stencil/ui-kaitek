import { Component, Element, Event, EventEmitter, h, Listen, Prop } from '@stencil/core';

@Component({
  tag: 'px-toggle',
  styleUrls: ['./px-toggle.scss'],
  shadow: true
})
export class PxToggle {
  @Element() myElement: HTMLElement;
  @Prop() size: 'regular' | 'small' | 'large' | 'huge' = 'large';
  @Prop({ reflect: true }) disabled: boolean = false;
  @Prop({ reflect: true }) option1: string = 'Hayır';
  @Prop({ reflect: true }) option2: string = 'Evet';
  @Prop({ reflect: true }) optionVisible: boolean = false;
  @Prop({ reflect: true }) isChecked: boolean = false;
  @Prop({ reflect: true }) isThirdState: boolean = false;

  @Event({
    eventName: 'pxToggleCheckedChanged',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxToggleCheckedChanged: EventEmitter;

  @Listen('click')
  clickHandler(/*event: CustomEvent*/) {
    let t = this;
    if (!t.disabled) {
      //    t.isChecked = !t.isChecked;
      //    if (t.isThirdState) {
      //      t.isThirdState = false;
      //    }
      //    if (t.isChecked) {
      //      t.myElement.classList.add('checked');
      //    } else {
      //      t.myElement.classList.remove('checked');
      //    }
      //  t.pxToggleCheckedChanged.emit({ value: t.isChecked });
      t.pxToggleCheckedChanged.emit({ value: !t.isChecked });
    }
  }
  @Listen('blur')
  _onBlur() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('_onBlur');
    //}
    t.myElement.removeAttribute('focused');
  }

  @Listen('focus', { target: 'document' })
  _onFocus() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('_onFocus');
    //}
    t.myElement.setAttribute('focused', '');
  }

  //@Method()
  //async setState(state: boolean, emit: boolean = false) {
  //  let t = this;
  //  t.isChecked = state;
  //  if (t.isChecked) {
  //    t.myElement.classList.add('checked');
  //  } else {
  //    t.myElement.classList.remove('checked');
  //  }
  //  if (emit) {
  //    t.pxToggleCheckedChanged.emit({ value: t.isChecked });
  //  }
  //}
  //
  //@Method()
  //async setThirdState(state: boolean) {
  //  let t = this;
  //  t.isThirdState = state;
  //}

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('Render|', t.myElement.tagName.toLowerCase());
    //}
    let _disabled_label: string = '';
    if (t.disabled || t.isThirdState) {
      _disabled_label = ' toggle--disabled ';
    }
    let _cls_label: string = 'toggle__label--' + t.size;
    let _size_label: string = 'toggle__label toggle__label--' + t.size + _disabled_label;
    let _size_input: string = 'toggle__input toggle__input--' + t.size;
    /*return(
    <label class={_size_label}>
        <input type="checkbox" class={_size_input} disabled={t.disabled}></input>
    </label>
    );*/
    if (t.optionVisible) {
      return (
        <div class="flex flex--middle" style={{}}>
          <label class={_cls_label} style={{ marginTop: "6px", fontSize: "xx-large", width: "100px" }} >{t.option1}</label>
          <input type="checkbox" class={_size_input} min={0} max={1} step={1} disabled={t.disabled} checked={t.isChecked} aria-checked={t.isChecked} value={t.isChecked ? 1 : 0}></input>
          <label class={_size_label}></label>
          <label class={_cls_label} style={{ marginTop: "6px", fontSize: "xx-large", width: "100px" }} >{t.option2}</label>
        </div>
      );
    } else {
      return ([
        <input type="checkbox" class={_size_input} min={0} max={1} step={1} disabled={t.disabled} checked={t.isChecked} aria-checked={t.isChecked} value={t.isChecked ? 1 : 0}></input>,
        <label class={_size_label}></label>
      ]);
    }
  }
}
