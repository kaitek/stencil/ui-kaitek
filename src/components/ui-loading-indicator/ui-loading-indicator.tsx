import { Component, Element, h, Method, Prop } from '@stencil/core';

import '@vaadin/progress-bar';
import '@vaadin/vertical-layout';

@Component({
  tag: 'ui-loading-indicator',
  styleUrl: 'ui-loading-indicator.scss',
  shadow: true
})
export class UILoadingIndicator {
  @Element() myElement: HTMLElement;

  @Prop({ reflect: true }) fitToParent: boolean = true;
  @Prop() text: string = 'Lütfen Bekleyiniz...';

  elDivModal!: HTMLDivElement;
  fitInto: object = window;
  opened: boolean = false;
  sizingTarget: HTMLElement = this as unknown as HTMLElement;
  horizontalAlign: string;
  verticalAlign: string;
  positionTarget: Element;
  __shouldPosition: boolean = false;
  _fitInfo: any;
  _localeHorizontalAlign: string;

  componentWillLoad() {
    let t = this;
    t.setVisible(false);
  }

  componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    if (t.fitToParent) {
      t.sizingTarget = t.myElement;
      t.fitInto = t.sizingTarget.parentElement;
      t.refit();
    }
  }

  @Method()
  async getVisible() {
    return this.opened;
  }
  @Method()
  async setVisible(isVisible: boolean) {
    let t = this;
    if (isVisible) {
      t.refit();
      this.myElement.setAttribute('opened', 'true');
      //this.elDivBox.style['display'] = 'flex';
      this.opened = true;
    } else if (!isVisible) {
      this.myElement.removeAttribute('opened');
      //this.elDivBox.style['display'] = 'none';
      this.opened = false;
    }
  }

  @Method()
  async setStateMask(isMask: boolean) {
    let t = this;
    if (isMask) {
      t.elDivModal.innerHTML = '';
      t.elDivModal.classList.add("modal-mask");
      t.myElement.style.zIndex = "5";
      //t.elDivModal.style.zIndex = "3";
    }
  }

  get_localeHorizontalAlign() {
    return this.horizontalAlign;
  }
  _discoverInfo() {
    if (this._fitInfo) {
      return;
    }
    try {
      var target = window.getComputedStyle(this.fitInto as Element);
      var sizer = window.getComputedStyle(this.sizingTarget);

      this._fitInfo = {
        inlineStyle: {
          top: this.sizingTarget.style.top || '',
          left: this.sizingTarget.style.left || '',
          position: this.sizingTarget.style.position || ''
        },
        sizerInlineStyle: {
          maxWidth: this.sizingTarget.style.maxWidth || '',
          maxHeight: this.sizingTarget.style.maxHeight || '',
          boxSizing: this.sizingTarget.style.boxSizing || ''
        },
        positionedBy: {
          vertically: target.top !== 'auto' ?
            'top' :
            (target.bottom !== 'auto' ? 'bottom' : null),
          horizontally: target.left !== 'auto' ?
            'left' :
            (target.right !== 'auto' ? 'right' : null)
        },
        sizedBy: {
          height: sizer.maxHeight !== 'none',
          width: sizer.maxWidth !== 'none',
          minWidth: parseInt(sizer.minWidth, 10) || 0,
          minHeight: parseInt(sizer.minHeight, 10) || 0
        },
        margin: {
          top: parseInt(target.marginTop, 10) || 0,
          right: parseInt(target.marginRight, 10) || 0,
          bottom: parseInt(target.marginBottom, 10) || 0,
          left: parseInt(target.marginLeft, 10) || 0
        }
      };
    } catch (error) {
      console.log(error);
    }
  }

  @Method()
  async refit() {
    var scrollLeft = this.sizingTarget.scrollLeft;
    var scrollTop = this.sizingTarget.scrollTop;
    this.resetFit();
    this.fit();
    this.sizingTarget.scrollLeft = scrollLeft;
    this.sizingTarget.scrollTop = scrollTop;
    return;
  }

  resetFit() {
    if (this.sizingTarget.style) {
      this.sizingTarget.style["boxSizing"] = "";
      this.sizingTarget.style["left"] = "";
      this.sizingTarget.style["maxHeight"] = "";
      this.sizingTarget.style["maxWidth"] = "";
      this.sizingTarget.style["position"] = "";
      this.sizingTarget.style["top"] = "";
    }
  }
  fit() {
    //this.position();
    this.constrain();
    this.center();
  }
  constrain() {
    if (this.__shouldPosition) {
      return;
    }
    this._discoverInfo();

    var info = this._fitInfo;
    // position at (0px, 0px) if not already positioned, so we can measure the
    // natural size.
    if (!info.positionedBy.vertically) {
      this.sizingTarget.style.position = 'fixed';
      this.sizingTarget.style.top = '0px';
    }
    if (!info.positionedBy.horizontally) {
      this.sizingTarget.style.position = 'fixed';
      this.sizingTarget.style.left = '0px';
    }

    // need border-box for margin/padding
    this.sizingTarget.style.boxSizing = 'border-box';
    // constrain the width and height if not already set
    var rect = (this.sizingTarget as Element).getBoundingClientRect();
    if (!info.sizedBy.height) {
      this.__sizeDimension(
        rect, info.positionedBy.vertically, 'top', 'bottom', 'Height');
    }
    if (!info.sizedBy.width) {
      this.__sizeDimension(
        rect, info.positionedBy.horizontally, 'left', 'right', 'Width');
    }
  }
  __getNormalizedRect(target: Element | Window) {
    if (target === document.documentElement || target === window) {
      return {
        top: 0,
        left: 0,
        width: window.innerWidth,
        height: window.innerHeight,
        right: window.innerWidth,
        bottom: window.innerHeight
      };
    }
    return (target as unknown as Element).getBoundingClientRect();
  }
  __sizeDimension(rect, positionedBy, start, end, extent) {
    var info = this._fitInfo;
    var fitRect = this.__getNormalizedRect(this.fitInto as Element);

    var max = extent === 'Width' ? fitRect.width : fitRect.height;
    var flip = (positionedBy === end);
    var offset = flip ? max - rect[end] : rect[start];
    var margin = info.margin[flip ? start : end];
    //var offsetExtent = 'offset' + extent;
    var sizingOffset = 0;//this[offsetExtent] - this.sizingTarget[offsetExtent];
    this.sizingTarget.style['max' + extent] =
      (max - margin - offset - sizingOffset) + 'px';
  }
  center() {
    if (this.__shouldPosition) {
      return;
    }
    this._discoverInfo();
    var positionedBy = this._fitInfo.positionedBy;
    if (positionedBy.vertically && positionedBy.horizontally) {
      // Already positioned.
      return;
    }
    // Need position:fixed to center
    this.sizingTarget.style.position = 'fixed';
    // Take into account the offset caused by parents that create stacking
    // contexts (e.g. with transform: translate3d). Translate to 0,0 and
    // measure the bounding rect.
    if (!positionedBy.vertically) {
      this.sizingTarget.style.top = '0px';
    }
    if (!positionedBy.horizontally) {
      this.sizingTarget.style.left = '0px';
    }
    // It will take in consideration margins and transforms
    var rect = (this.sizingTarget as Element).getBoundingClientRect();
    var fitRect = this.__getNormalizedRect(this.fitInto as Element);
    if (!positionedBy.vertically) {
      var top = fitRect.top - rect.top + (fitRect.height - rect.height) / 2;
      this.sizingTarget.style.top = top + 'px';
    }
    if (!positionedBy.horizontally) {
      var left = fitRect.left - rect.left + (fitRect.width - rect.width) / 2;
      this.sizingTarget.style.left = left + 'px';
    }
  }

  _handleTransitionEnd(evt: TransitionEvent) {
    let t = this;
    //console.log('_handleTransitionEnd:', evt);
    if (evt.currentTarget === t.elDivModal && evt.propertyName === 'visibility') {
      if (t.opened) {
        t.refit();
      } else {

      }
    }
  }

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('Render|', t.myElement.tagName.toLowerCase());
    //}
    return (
      <div class="modal" id="modal" on-transitionend={(event) => t._handleTransitionEnd(event)}
        ref={(el) => t.elDivModal = el as HTMLDivElement}
      >
        <vaadin-vertical-layout theme="spacing padding" style={{ alignItems: "center" }}>
          <h3 class="m-0 text-l indicator-text">{t.text}</h3>
          <vaadin-progress-bar indeterminate style={{ alignSelf: "center" }}></vaadin-progress-bar>
        </vaadin-vertical-layout>
      </div>
    );
  }
}
