/**
 * @license
 * Copyright (c) 2013 - 2024 Kaitek Ltd.
 * This program is available under Apache License Version 2.0, available at https://www.kaitek.com.tr/license/
 */
  
import fa__0 from './fa/0.svg';
import fa__1 from './fa/1.svg';
import fa__2 from './fa/2.svg';
import fa__3 from './fa/3.svg';
import fa__4 from './fa/4.svg';
import fa__5 from './fa/5.svg';
import fa__6 from './fa/6.svg';
import fa__7 from './fa/7.svg';
import fa__8 from './fa/8.svg';
import fa__9 from './fa/9.svg';
import fa__a from './fa/a.svg';
import fa__address_book from './fa/address-book.svg';
import fa__address_card from './fa/address-card.svg';
import fa__align_center from './fa/align-center.svg';
import fa__align_justify from './fa/align-justify.svg';
import fa__align_left from './fa/align-left.svg';
import fa__align_right from './fa/align-right.svg';
import fa__anchor_circle_check from './fa/anchor-circle-check.svg';
import fa__anchor_circle_exclamation from './fa/anchor-circle-exclamation.svg';
import fa__anchor_circle_xmark from './fa/anchor-circle-xmark.svg';
import fa__anchor_lock from './fa/anchor-lock.svg';
import fa__anchor from './fa/anchor.svg';
import fa__angle_down from './fa/angle-down.svg';
import fa__angle_left from './fa/angle-left.svg';
import fa__angle_right from './fa/angle-right.svg';
import fa__angle_up from './fa/angle-up.svg';
import fa__angles_down from './fa/angles-down.svg';
import fa__angles_left from './fa/angles-left.svg';
import fa__angles_right from './fa/angles-right.svg';
import fa__angles_up from './fa/angles-up.svg';
import fa__ankh from './fa/ankh.svg';
import fa__apple_whole from './fa/apple-whole.svg';
import fa__archway from './fa/archway.svg';
import fa__arrow_down_1_9 from './fa/arrow-down-1-9.svg';
import fa__arrow_down_9_1 from './fa/arrow-down-9-1.svg';
import fa__arrow_down_a_z from './fa/arrow-down-a-z.svg';
import fa__arrow_down_long from './fa/arrow-down-long.svg';
import fa__arrow_down_short_wide from './fa/arrow-down-short-wide.svg';
import fa__arrow_down_up_across_line from './fa/arrow-down-up-across-line.svg';
import fa__arrow_down_up_lock from './fa/arrow-down-up-lock.svg';
import fa__arrow_down_wide_short from './fa/arrow-down-wide-short.svg';
import fa__arrow_down_z_a from './fa/arrow-down-z-a.svg';
import fa__arrow_down from './fa/arrow-down.svg';
import fa__arrow_left_long from './fa/arrow-left-long.svg';
import fa__arrow_left from './fa/arrow-left.svg';
import fa__arrow_pointer from './fa/arrow-pointer.svg';
import fa__arrow_right_arrow_left from './fa/arrow-right-arrow-left.svg';
import fa__arrow_right_from_bracket from './fa/arrow-right-from-bracket.svg';
import fa__arrow_right_long from './fa/arrow-right-long.svg';
import fa__arrow_right_to_bracket from './fa/arrow-right-to-bracket.svg';
import fa__arrow_right_to_city from './fa/arrow-right-to-city.svg';
import fa__arrow_right from './fa/arrow-right.svg';
import fa__arrow_rotate_left from './fa/arrow-rotate-left.svg';
import fa__arrow_rotate_right from './fa/arrow-rotate-right.svg';
import fa__arrow_trend_down from './fa/arrow-trend-down.svg';
import fa__arrow_trend_up from './fa/arrow-trend-up.svg';
import fa__arrow_turn_down from './fa/arrow-turn-down.svg';
import fa__arrow_turn_up from './fa/arrow-turn-up.svg';
import fa__arrow_up_1_9 from './fa/arrow-up-1-9.svg';
import fa__arrow_up_9_1 from './fa/arrow-up-9-1.svg';
import fa__arrow_up_a_z from './fa/arrow-up-a-z.svg';
import fa__arrow_up_from_bracket from './fa/arrow-up-from-bracket.svg';
import fa__arrow_up_from_ground_water from './fa/arrow-up-from-ground-water.svg';
import fa__arrow_up_from_water_pump from './fa/arrow-up-from-water-pump.svg';
import fa__arrow_up_long from './fa/arrow-up-long.svg';
import fa__arrow_up_right_dots from './fa/arrow-up-right-dots.svg';
import fa__arrow_up_right_from_square from './fa/arrow-up-right-from-square.svg';
import fa__arrow_up_short_wide from './fa/arrow-up-short-wide.svg';
import fa__arrow_up_wide_short from './fa/arrow-up-wide-short.svg';
import fa__arrow_up_z_a from './fa/arrow-up-z-a.svg';
import fa__arrow_up from './fa/arrow-up.svg';
import fa__arrows_down_to_line from './fa/arrows-down-to-line.svg';
import fa__arrows_down_to_people from './fa/arrows-down-to-people.svg';
import fa__arrows_left_right_to_line from './fa/arrows-left-right-to-line.svg';
import fa__arrows_left_right from './fa/arrows-left-right.svg';
import fa__arrows_rotate from './fa/arrows-rotate.svg';
import fa__arrows_spin from './fa/arrows-spin.svg';
import fa__arrows_split_up_and_left from './fa/arrows-split-up-and-left.svg';
import fa__arrows_to_circle from './fa/arrows-to-circle.svg';
import fa__arrows_to_dot from './fa/arrows-to-dot.svg';
import fa__arrows_to_eye from './fa/arrows-to-eye.svg';
import fa__arrows_turn_right from './fa/arrows-turn-right.svg';
import fa__arrows_turn_to_dots from './fa/arrows-turn-to-dots.svg';
import fa__arrows_up_down_left_right from './fa/arrows-up-down-left-right.svg';
import fa__arrows_up_down from './fa/arrows-up-down.svg';
import fa__arrows_up_to_line from './fa/arrows-up-to-line.svg';
import fa__asterisk from './fa/asterisk.svg';
import fa__at from './fa/at.svg';
import fa__atom from './fa/atom.svg';
import fa__audio_description from './fa/audio-description.svg';
import fa__austral_sign from './fa/austral-sign.svg';
import fa__award from './fa/award.svg';
import fa__b from './fa/b.svg';
import fa__baby_carriage from './fa/baby-carriage.svg';
import fa__baby from './fa/baby.svg';
import fa__backward_fast from './fa/backward-fast.svg';
import fa__backward_step from './fa/backward-step.svg';
import fa__backward from './fa/backward.svg';
import fa__bacon from './fa/bacon.svg';
import fa__bacteria from './fa/bacteria.svg';
import fa__bacterium from './fa/bacterium.svg';
import fa__bag_shopping from './fa/bag-shopping.svg';
import fa__bahai from './fa/bahai.svg';
import fa__baht_sign from './fa/baht-sign.svg';
import fa__ban_smoking from './fa/ban-smoking.svg';
import fa__ban from './fa/ban.svg';
import fa__bandage from './fa/bandage.svg';
import fa__bangladeshi_taka_sign from './fa/bangladeshi-taka-sign.svg';
import fa__barcode from './fa/barcode.svg';
import fa__bars_progress from './fa/bars-progress.svg';
import fa__bars_staggered from './fa/bars-staggered.svg';
import fa__bars from './fa/bars.svg';
import fa__baseball_bat_ball from './fa/baseball-bat-ball.svg';
import fa__baseball from './fa/baseball.svg';
import fa__basket_shopping from './fa/basket-shopping.svg';
import fa__basketball from './fa/basketball.svg';
import fa__bath from './fa/bath.svg';
import fa__battery_empty from './fa/battery-empty.svg';
import fa__battery_full from './fa/battery-full.svg';
import fa__battery_half from './fa/battery-half.svg';
import fa__battery_quarter from './fa/battery-quarter.svg';
import fa__battery_three_quarters from './fa/battery-three-quarters.svg';
import fa__bed_pulse from './fa/bed-pulse.svg';
import fa__bed from './fa/bed.svg';
import fa__beer_mug_empty from './fa/beer-mug-empty.svg';
import fa__bell_concierge from './fa/bell-concierge.svg';
import fa__bell_slash from './fa/bell-slash.svg';
import fa__bell from './fa/bell.svg';
import fa__bezier_curve from './fa/bezier-curve.svg';
import fa__bicycle from './fa/bicycle.svg';
import fa__binoculars from './fa/binoculars.svg';
import fa__biohazard from './fa/biohazard.svg';
import fa__bitcoin_sign from './fa/bitcoin-sign.svg';
import fa__blender_phone from './fa/blender-phone.svg';
import fa__blender from './fa/blender.svg';
import fa__blog from './fa/blog.svg';
import fa__bold from './fa/bold.svg';
import fa__bolt_lightning from './fa/bolt-lightning.svg';
import fa__bolt from './fa/bolt.svg';
import fa__bomb from './fa/bomb.svg';
import fa__bone from './fa/bone.svg';
import fa__bong from './fa/bong.svg';
import fa__book_atlas from './fa/book-atlas.svg';
import fa__book_bible from './fa/book-bible.svg';
import fa__book_bookmark from './fa/book-bookmark.svg';
import fa__book_journal_whills from './fa/book-journal-whills.svg';
import fa__book_medical from './fa/book-medical.svg';
import fa__book_open_reader from './fa/book-open-reader.svg';
import fa__book_open from './fa/book-open.svg';
import fa__book_quran from './fa/book-quran.svg';
import fa__book_skull from './fa/book-skull.svg';
import fa__book_tanakh from './fa/book-tanakh.svg';
import fa__book from './fa/book.svg';
import fa__bookmark from './fa/bookmark.svg';
import fa__border_all from './fa/border-all.svg';
import fa__border_none from './fa/border-none.svg';
import fa__border_top_left from './fa/border-top-left.svg';
import fa__bore_hole from './fa/bore-hole.svg';
import fa__bottle_droplet from './fa/bottle-droplet.svg';
import fa__bottle_water from './fa/bottle-water.svg';
import fa__bowl_food from './fa/bowl-food.svg';
import fa__bowl_rice from './fa/bowl-rice.svg';
import fa__bowling_ball from './fa/bowling-ball.svg';
import fa__box_archive from './fa/box-archive.svg';
import fa__box_open from './fa/box-open.svg';
import fa__box_tissue from './fa/box-tissue.svg';
import fa__box from './fa/box.svg';
import fa__boxes_packing from './fa/boxes-packing.svg';
import fa__boxes_stacked from './fa/boxes-stacked.svg';
import fa__braille from './fa/braille.svg';
import fa__brain from './fa/brain.svg';
import fa__brazilian_real_sign from './fa/brazilian-real-sign.svg';
import fa__bread_slice from './fa/bread-slice.svg';
import fa__bridge_circle_check from './fa/bridge-circle-check.svg';
import fa__bridge_circle_exclamation from './fa/bridge-circle-exclamation.svg';
import fa__bridge_circle_xmark from './fa/bridge-circle-xmark.svg';
import fa__bridge_lock from './fa/bridge-lock.svg';
import fa__bridge_water from './fa/bridge-water.svg';
import fa__bridge from './fa/bridge.svg';
import fa__briefcase_medical from './fa/briefcase-medical.svg';
import fa__briefcase from './fa/briefcase.svg';
import fa__broom_ball from './fa/broom-ball.svg';
import fa__broom from './fa/broom.svg';
import fa__brush from './fa/brush.svg';
import fa__bucket from './fa/bucket.svg';
import fa__bug_slash from './fa/bug-slash.svg';
import fa__bug from './fa/bug.svg';
import fa__bugs from './fa/bugs.svg';
import fa__building_circle_arrow_right from './fa/building-circle-arrow-right.svg';
import fa__building_circle_check from './fa/building-circle-check.svg';
import fa__building_circle_exclamation from './fa/building-circle-exclamation.svg';
import fa__building_circle_xmark from './fa/building-circle-xmark.svg';
import fa__building_columns from './fa/building-columns.svg';
import fa__building_flag from './fa/building-flag.svg';
import fa__building_lock from './fa/building-lock.svg';
import fa__building_ngo from './fa/building-ngo.svg';
import fa__building_shield from './fa/building-shield.svg';
import fa__building_un from './fa/building-un.svg';
import fa__building_user from './fa/building-user.svg';
import fa__building_wheat from './fa/building-wheat.svg';
import fa__building from './fa/building.svg';
import fa__bullhorn from './fa/bullhorn.svg';
import fa__bullseye from './fa/bullseye.svg';
import fa__burger from './fa/burger.svg';
import fa__burst from './fa/burst.svg';
import fa__bus_simple from './fa/bus-simple.svg';
import fa__bus from './fa/bus.svg';
import fa__business_time from './fa/business-time.svg';
import fa__c from './fa/c.svg';
import fa__cable_car from './fa/cable-car.svg';
import fa__cake_candles from './fa/cake-candles.svg';
import fa__calculator from './fa/calculator.svg';
import fa__calendar_check from './fa/calendar-check.svg';
import fa__calendar_day from './fa/calendar-day.svg';
import fa__calendar_days from './fa/calendar-days.svg';
import fa__calendar_minus from './fa/calendar-minus.svg';
import fa__calendar_plus from './fa/calendar-plus.svg';
import fa__calendar_week from './fa/calendar-week.svg';
import fa__calendar_xmark from './fa/calendar-xmark.svg';
import fa__calendar from './fa/calendar.svg';
import fa__camera_retro from './fa/camera-retro.svg';
import fa__camera_rotate from './fa/camera-rotate.svg';
import fa__camera from './fa/camera.svg';
import fa__campground from './fa/campground.svg';
import fa__candy_cane from './fa/candy-cane.svg';
import fa__cannabis from './fa/cannabis.svg';
import fa__capsules from './fa/capsules.svg';
import fa__car_battery from './fa/car-battery.svg';
import fa__car_burst from './fa/car-burst.svg';
import fa__car_on from './fa/car-on.svg';
import fa__car_rear from './fa/car-rear.svg';
import fa__car_side from './fa/car-side.svg';
import fa__car_tunnel from './fa/car-tunnel.svg';
import fa__car from './fa/car.svg';
import fa__caravan from './fa/caravan.svg';
import fa__caret_down from './fa/caret-down.svg';
import fa__caret_left from './fa/caret-left.svg';
import fa__caret_right from './fa/caret-right.svg';
import fa__caret_up from './fa/caret-up.svg';
import fa__carrot from './fa/carrot.svg';
import fa__cart_arrow_down from './fa/cart-arrow-down.svg';
import fa__cart_flatbed_suitcase from './fa/cart-flatbed-suitcase.svg';
import fa__cart_flatbed from './fa/cart-flatbed.svg';
import fa__cart_plus from './fa/cart-plus.svg';
import fa__cart_shopping from './fa/cart-shopping.svg';
import fa__cash_register from './fa/cash-register.svg';
import fa__cat from './fa/cat.svg';
import fa__cedi_sign from './fa/cedi-sign.svg';
import fa__cent_sign from './fa/cent-sign.svg';
import fa__certificate from './fa/certificate.svg';
import fa__chair from './fa/chair.svg';
import fa__chalkboard_user from './fa/chalkboard-user.svg';
import fa__chalkboard from './fa/chalkboard.svg';
import fa__champagne_glasses from './fa/champagne-glasses.svg';
import fa__charging_station from './fa/charging-station.svg';
import fa__chart_area from './fa/chart-area.svg';
import fa__chart_bar from './fa/chart-bar.svg';
import fa__chart_column from './fa/chart-column.svg';
import fa__chart_gantt from './fa/chart-gantt.svg';
import fa__chart_line from './fa/chart-line.svg';
import fa__chart_pie from './fa/chart-pie.svg';
import fa__chart_simple from './fa/chart-simple.svg';
import fa__check_double from './fa/check-double.svg';
import fa__check_to_slot from './fa/check-to-slot.svg';
import fa__check from './fa/check.svg';
import fa__cheese from './fa/cheese.svg';
import fa__chess_bishop from './fa/chess-bishop.svg';
import fa__chess_board from './fa/chess-board.svg';
import fa__chess_king from './fa/chess-king.svg';
import fa__chess_knight from './fa/chess-knight.svg';
import fa__chess_pawn from './fa/chess-pawn.svg';
import fa__chess_queen from './fa/chess-queen.svg';
import fa__chess_rook from './fa/chess-rook.svg';
import fa__chess from './fa/chess.svg';
import fa__chevron_down from './fa/chevron-down.svg';
import fa__chevron_left from './fa/chevron-left.svg';
import fa__chevron_right from './fa/chevron-right.svg';
import fa__chevron_up from './fa/chevron-up.svg';
import fa__child_combatant from './fa/child-combatant.svg';
import fa__child_dress from './fa/child-dress.svg';
import fa__child_reaching from './fa/child-reaching.svg';
import fa__child from './fa/child.svg';
import fa__children from './fa/children.svg';
import fa__church from './fa/church.svg';
import fa__circle_arrow_down from './fa/circle-arrow-down.svg';
import fa__circle_arrow_left from './fa/circle-arrow-left.svg';
import fa__circle_arrow_right from './fa/circle-arrow-right.svg';
import fa__circle_arrow_up from './fa/circle-arrow-up.svg';
import fa__circle_check from './fa/circle-check.svg';
import fa__circle_chevron_down from './fa/circle-chevron-down.svg';
import fa__circle_chevron_left from './fa/circle-chevron-left.svg';
import fa__circle_chevron_right from './fa/circle-chevron-right.svg';
import fa__circle_chevron_up from './fa/circle-chevron-up.svg';
import fa__circle_dollar_to_slot from './fa/circle-dollar-to-slot.svg';
import fa__circle_dot from './fa/circle-dot.svg';
import fa__circle_down from './fa/circle-down.svg';
import fa__circle_exclamation from './fa/circle-exclamation.svg';
import fa__circle_h from './fa/circle-h.svg';
import fa__circle_half_stroke from './fa/circle-half-stroke.svg';
import fa__circle_info from './fa/circle-info.svg';
import fa__circle_left from './fa/circle-left.svg';
import fa__circle_minus from './fa/circle-minus.svg';
import fa__circle_nodes from './fa/circle-nodes.svg';
import fa__circle_notch from './fa/circle-notch.svg';
import fa__circle_pause from './fa/circle-pause.svg';
import fa__circle_play from './fa/circle-play.svg';
import fa__circle_plus from './fa/circle-plus.svg';
import fa__circle_question from './fa/circle-question.svg';
import fa__circle_radiation from './fa/circle-radiation.svg';
import fa__circle_right from './fa/circle-right.svg';
import fa__circle_stop from './fa/circle-stop.svg';
import fa__circle_up from './fa/circle-up.svg';
import fa__circle_user from './fa/circle-user.svg';
import fa__circle_xmark from './fa/circle-xmark.svg';
import fa__circle from './fa/circle.svg';
import fa__city from './fa/city.svg';
import fa__clapperboard from './fa/clapperboard.svg';
import fa__clipboard_check from './fa/clipboard-check.svg';
import fa__clipboard_list from './fa/clipboard-list.svg';
import fa__clipboard_question from './fa/clipboard-question.svg';
import fa__clipboard_user from './fa/clipboard-user.svg';
import fa__clipboard from './fa/clipboard.svg';
import fa__clock_rotate_left from './fa/clock-rotate-left.svg';
import fa__clock from './fa/clock.svg';
import fa__clone from './fa/clone.svg';
import fa__closed_captioning from './fa/closed-captioning.svg';
import fa__cloud_arrow_down from './fa/cloud-arrow-down.svg';
import fa__cloud_arrow_up from './fa/cloud-arrow-up.svg';
import fa__cloud_bolt from './fa/cloud-bolt.svg';
import fa__cloud_meatball from './fa/cloud-meatball.svg';
import fa__cloud_moon_rain from './fa/cloud-moon-rain.svg';
import fa__cloud_moon from './fa/cloud-moon.svg';
import fa__cloud_rain from './fa/cloud-rain.svg';
import fa__cloud_showers_heavy from './fa/cloud-showers-heavy.svg';
import fa__cloud_showers_water from './fa/cloud-showers-water.svg';
import fa__cloud_sun_rain from './fa/cloud-sun-rain.svg';
import fa__cloud_sun from './fa/cloud-sun.svg';
import fa__cloud from './fa/cloud.svg';
import fa__clover from './fa/clover.svg';
import fa__code_branch from './fa/code-branch.svg';
import fa__code_commit from './fa/code-commit.svg';
import fa__code_compare from './fa/code-compare.svg';
import fa__code_fork from './fa/code-fork.svg';
import fa__code_merge from './fa/code-merge.svg';
import fa__code_pull_request from './fa/code-pull-request.svg';
import fa__code from './fa/code.svg';
import fa__coins from './fa/coins.svg';
import fa__colon_sign from './fa/colon-sign.svg';
import fa__comment_dollar from './fa/comment-dollar.svg';
import fa__comment_dots from './fa/comment-dots.svg';
import fa__comment_medical from './fa/comment-medical.svg';
import fa__comment_slash from './fa/comment-slash.svg';
import fa__comment_sms from './fa/comment-sms.svg';
import fa__comment from './fa/comment.svg';
import fa__comments_dollar from './fa/comments-dollar.svg';
import fa__comments from './fa/comments.svg';
import fa__compact_disc from './fa/compact-disc.svg';
import fa__compass_drafting from './fa/compass-drafting.svg';
import fa__compass from './fa/compass.svg';
import fa__compress from './fa/compress.svg';
import fa__computer_mouse from './fa/computer-mouse.svg';
import fa__computer from './fa/computer.svg';
import fa__cookie_bite from './fa/cookie-bite.svg';
import fa__cookie from './fa/cookie.svg';
import fa__copy from './fa/copy.svg';
import fa__copyright from './fa/copyright.svg';
import fa__couch from './fa/couch.svg';
import fa__cow from './fa/cow.svg';
import fa__credit_card from './fa/credit-card.svg';
import fa__crop_simple from './fa/crop-simple.svg';
import fa__crop from './fa/crop.svg';
import fa__cross from './fa/cross.svg';
import fa__crosshairs from './fa/crosshairs.svg';
import fa__crow from './fa/crow.svg';
import fa__crown from './fa/crown.svg';
import fa__crutch from './fa/crutch.svg';
import fa__cruzeiro_sign from './fa/cruzeiro-sign.svg';
import fa__cube from './fa/cube.svg';
import fa__cubes_stacked from './fa/cubes-stacked.svg';
import fa__cubes from './fa/cubes.svg';
import fa__d from './fa/d.svg';
import fa__database from './fa/database.svg';
import fa__delete_left from './fa/delete-left.svg';
import fa__democrat from './fa/democrat.svg';
import fa__desktop from './fa/desktop.svg';
import fa__dharmachakra from './fa/dharmachakra.svg';
import fa__diagram_next from './fa/diagram-next.svg';
import fa__diagram_predecessor from './fa/diagram-predecessor.svg';
import fa__diagram_project from './fa/diagram-project.svg';
import fa__diagram_successor from './fa/diagram-successor.svg';
import fa__diamond_turn_right from './fa/diamond-turn-right.svg';
import fa__diamond from './fa/diamond.svg';
import fa__dice_d20 from './fa/dice-d20.svg';
import fa__dice_d6 from './fa/dice-d6.svg';
import fa__dice_five from './fa/dice-five.svg';
import fa__dice_four from './fa/dice-four.svg';
import fa__dice_one from './fa/dice-one.svg';
import fa__dice_six from './fa/dice-six.svg';
import fa__dice_three from './fa/dice-three.svg';
import fa__dice_two from './fa/dice-two.svg';
import fa__dice from './fa/dice.svg';
import fa__disease from './fa/disease.svg';
import fa__display from './fa/display.svg';
import fa__divide from './fa/divide.svg';
import fa__dna from './fa/dna.svg';
import fa__dog from './fa/dog.svg';
import fa__dollar_sign from './fa/dollar-sign.svg';
import fa__dolly from './fa/dolly.svg';
import fa__dong_sign from './fa/dong-sign.svg';
import fa__door_closed from './fa/door-closed.svg';
import fa__door_open from './fa/door-open.svg';
import fa__dove from './fa/dove.svg';
import fa__down_left_and_up_right_to_center from './fa/down-left-and-up-right-to-center.svg';
import fa__down_long from './fa/down-long.svg';
import fa__download from './fa/download.svg';
import fa__dragon from './fa/dragon.svg';
import fa__draw_polygon from './fa/draw-polygon.svg';
import fa__droplet_slash from './fa/droplet-slash.svg';
import fa__droplet from './fa/droplet.svg';
import fa__drum_steelpan from './fa/drum-steelpan.svg';
import fa__drum from './fa/drum.svg';
import fa__drumstick_bite from './fa/drumstick-bite.svg';
import fa__dumbbell from './fa/dumbbell.svg';
import fa__dumpster_fire from './fa/dumpster-fire.svg';
import fa__dumpster from './fa/dumpster.svg';
import fa__dungeon from './fa/dungeon.svg';
import fa__e from './fa/e.svg';
import fa__ear_deaf from './fa/ear-deaf.svg';
import fa__ear_listen from './fa/ear-listen.svg';
import fa__earth_africa from './fa/earth-africa.svg';
import fa__earth_americas from './fa/earth-americas.svg';
import fa__earth_asia from './fa/earth-asia.svg';
import fa__earth_europe from './fa/earth-europe.svg';
import fa__earth_oceania from './fa/earth-oceania.svg';
import fa__egg from './fa/egg.svg';
import fa__eject from './fa/eject.svg';
import fa__elevator from './fa/elevator.svg';
import fa__ellipsis_vertical from './fa/ellipsis-vertical.svg';
import fa__ellipsis from './fa/ellipsis.svg';
import fa__envelope_circle_check from './fa/envelope-circle-check.svg';
import fa__envelope_open_text from './fa/envelope-open-text.svg';
import fa__envelope_open from './fa/envelope-open.svg';
import fa__envelope from './fa/envelope.svg';
import fa__envelopes_bulk from './fa/envelopes-bulk.svg';
import fa__equals from './fa/equals.svg';
import fa__eraser from './fa/eraser.svg';
import fa__ethernet from './fa/ethernet.svg';
import fa__euro_sign from './fa/euro-sign.svg';
import fa__exclamation from './fa/exclamation.svg';
import fa__expand from './fa/expand.svg';
import fa__explosion from './fa/explosion.svg';
import fa__eye_dropper from './fa/eye-dropper.svg';
import fa__eye_low_vision from './fa/eye-low-vision.svg';
import fa__eye_slash from './fa/eye-slash.svg';
import fa__eye from './fa/eye.svg';
import fa__f from './fa/f.svg';
import fa__face_angry from './fa/face-angry.svg';
import fa__face_dizzy from './fa/face-dizzy.svg';
import fa__face_flushed from './fa/face-flushed.svg';
import fa__face_frown_open from './fa/face-frown-open.svg';
import fa__face_frown from './fa/face-frown.svg';
import fa__face_grimace from './fa/face-grimace.svg';
import fa__face_grin_beam_sweat from './fa/face-grin-beam-sweat.svg';
import fa__face_grin_beam from './fa/face-grin-beam.svg';
import fa__face_grin_hearts from './fa/face-grin-hearts.svg';
import fa__face_grin_squint_tears from './fa/face-grin-squint-tears.svg';
import fa__face_grin_squint from './fa/face-grin-squint.svg';
import fa__face_grin_stars from './fa/face-grin-stars.svg';
import fa__face_grin_tears from './fa/face-grin-tears.svg';
import fa__face_grin_tongue_squint from './fa/face-grin-tongue-squint.svg';
import fa__face_grin_tongue_wink from './fa/face-grin-tongue-wink.svg';
import fa__face_grin_tongue from './fa/face-grin-tongue.svg';
import fa__face_grin_wide from './fa/face-grin-wide.svg';
import fa__face_grin_wink from './fa/face-grin-wink.svg';
import fa__face_grin from './fa/face-grin.svg';
import fa__face_kiss_beam from './fa/face-kiss-beam.svg';
import fa__face_kiss_wink_heart from './fa/face-kiss-wink-heart.svg';
import fa__face_kiss from './fa/face-kiss.svg';
import fa__face_laugh_beam from './fa/face-laugh-beam.svg';
import fa__face_laugh_squint from './fa/face-laugh-squint.svg';
import fa__face_laugh_wink from './fa/face-laugh-wink.svg';
import fa__face_laugh from './fa/face-laugh.svg';
import fa__face_meh_blank from './fa/face-meh-blank.svg';
import fa__face_meh from './fa/face-meh.svg';
import fa__face_rolling_eyes from './fa/face-rolling-eyes.svg';
import fa__face_sad_cry from './fa/face-sad-cry.svg';
import fa__face_sad_tear from './fa/face-sad-tear.svg';
import fa__face_smile_beam from './fa/face-smile-beam.svg';
import fa__face_smile_wink from './fa/face-smile-wink.svg';
import fa__face_smile from './fa/face-smile.svg';
import fa__face_surprise from './fa/face-surprise.svg';
import fa__face_tired from './fa/face-tired.svg';
import fa__fan from './fa/fan.svg';
import fa__faucet_drip from './fa/faucet-drip.svg';
import fa__faucet from './fa/faucet.svg';
import fa__fax from './fa/fax.svg';
import fa__feather_pointed from './fa/feather-pointed.svg';
import fa__feather from './fa/feather.svg';
import fa__ferry from './fa/ferry.svg';
import fa__file_arrow_down from './fa/file-arrow-down.svg';
import fa__file_arrow_up from './fa/file-arrow-up.svg';
import fa__file_audio from './fa/file-audio.svg';
import fa__file_circle_check from './fa/file-circle-check.svg';
import fa__file_circle_exclamation from './fa/file-circle-exclamation.svg';
import fa__file_circle_minus from './fa/file-circle-minus.svg';
import fa__file_circle_plus from './fa/file-circle-plus.svg';
import fa__file_circle_question from './fa/file-circle-question.svg';
import fa__file_circle_xmark from './fa/file-circle-xmark.svg';
import fa__file_code from './fa/file-code.svg';
import fa__file_contract from './fa/file-contract.svg';
import fa__file_csv from './fa/file-csv.svg';
import fa__file_excel from './fa/file-excel.svg';
import fa__file_export from './fa/file-export.svg';
import fa__file_image from './fa/file-image.svg';
import fa__file_import from './fa/file-import.svg';
import fa__file_invoice_dollar from './fa/file-invoice-dollar.svg';
import fa__file_invoice from './fa/file-invoice.svg';
import fa__file_lines from './fa/file-lines.svg';
import fa__file_medical from './fa/file-medical.svg';
import fa__file_pdf from './fa/file-pdf.svg';
import fa__file_pen from './fa/file-pen.svg';
import fa__file_powerpoint from './fa/file-powerpoint.svg';
import fa__file_prescription from './fa/file-prescription.svg';
import fa__file_shield from './fa/file-shield.svg';
import fa__file_signature from './fa/file-signature.svg';
import fa__file_video from './fa/file-video.svg';
import fa__file_waveform from './fa/file-waveform.svg';
import fa__file_word from './fa/file-word.svg';
import fa__file_zipper from './fa/file-zipper.svg';
import fa__file from './fa/file.svg';
import fa__fill_drip from './fa/fill-drip.svg';
import fa__fill from './fa/fill.svg';
import fa__film from './fa/film.svg';
import fa__filter_circle_dollar from './fa/filter-circle-dollar.svg';
import fa__filter_circle_xmark from './fa/filter-circle-xmark.svg';
import fa__filter from './fa/filter.svg';
import fa__fingerprint from './fa/fingerprint.svg';
import fa__fire_burner from './fa/fire-burner.svg';
import fa__fire_extinguisher from './fa/fire-extinguisher.svg';
import fa__fire_flame_curved from './fa/fire-flame-curved.svg';
import fa__fire_flame_simple from './fa/fire-flame-simple.svg';
import fa__fire from './fa/fire.svg';
import fa__fish_fins from './fa/fish-fins.svg';
import fa__fish from './fa/fish.svg';
import fa__flag_checkered from './fa/flag-checkered.svg';
import fa__flag_usa from './fa/flag-usa.svg';
import fa__flag from './fa/flag.svg';
import fa__flask_vial from './fa/flask-vial.svg';
import fa__flask from './fa/flask.svg';
import fa__floppy_disk from './fa/floppy-disk.svg';
import fa__florin_sign from './fa/florin-sign.svg';
import fa__folder_closed from './fa/folder-closed.svg';
import fa__folder_minus from './fa/folder-minus.svg';
import fa__folder_open from './fa/folder-open.svg';
import fa__folder_plus from './fa/folder-plus.svg';
import fa__folder_tree from './fa/folder-tree.svg';
import fa__folder from './fa/folder.svg';
import fa__font_awesome from './fa/font-awesome.svg';
import fa__font from './fa/font.svg';
import fa__football from './fa/football.svg';
import fa__forward_fast from './fa/forward-fast.svg';
import fa__forward_step from './fa/forward-step.svg';
import fa__forward from './fa/forward.svg';
import fa__franc_sign from './fa/franc-sign.svg';
import fa__frog from './fa/frog.svg';
import fa__futbol from './fa/futbol.svg';
import fa__g from './fa/g.svg';
import fa__gamepad from './fa/gamepad.svg';
import fa__gas_pump from './fa/gas-pump.svg';
import fa__gauge_high from './fa/gauge-high.svg';
import fa__gauge_simple_high from './fa/gauge-simple-high.svg';
import fa__gauge_simple from './fa/gauge-simple.svg';
import fa__gauge from './fa/gauge.svg';
import fa__gavel from './fa/gavel.svg';
import fa__gear from './fa/gear.svg';
import fa__gears from './fa/gears.svg';
import fa__gem from './fa/gem.svg';
import fa__genderless from './fa/genderless.svg';
import fa__ghost from './fa/ghost.svg';
import fa__gift from './fa/gift.svg';
import fa__gifts from './fa/gifts.svg';
import fa__glass_water_droplet from './fa/glass-water-droplet.svg';
import fa__glass_water from './fa/glass-water.svg';
import fa__glasses from './fa/glasses.svg';
import fa__globe from './fa/globe.svg';
import fa__golf_ball_tee from './fa/golf-ball-tee.svg';
import fa__gopuram from './fa/gopuram.svg';
import fa__graduation_cap from './fa/graduation-cap.svg';
import fa__greater_than_equal from './fa/greater-than-equal.svg';
import fa__greater_than from './fa/greater-than.svg';
import fa__grip_lines_vertical from './fa/grip-lines-vertical.svg';
import fa__grip_lines from './fa/grip-lines.svg';
import fa__grip_vertical from './fa/grip-vertical.svg';
import fa__grip from './fa/grip.svg';
import fa__group_arrows_rotate from './fa/group-arrows-rotate.svg';
import fa__guarani_sign from './fa/guarani-sign.svg';
import fa__guitar from './fa/guitar.svg';
import fa__gun from './fa/gun.svg';
import fa__h from './fa/h.svg';
import fa__hammer from './fa/hammer.svg';
import fa__hamsa from './fa/hamsa.svg';
import fa__hand_back_fist from './fa/hand-back-fist.svg';
import fa__hand_dots from './fa/hand-dots.svg';
import fa__hand_fist from './fa/hand-fist.svg';
import fa__hand_holding_dollar from './fa/hand-holding-dollar.svg';
import fa__hand_holding_droplet from './fa/hand-holding-droplet.svg';
import fa__hand_holding_hand from './fa/hand-holding-hand.svg';
import fa__hand_holding_heart from './fa/hand-holding-heart.svg';
import fa__hand_holding_medical from './fa/hand-holding-medical.svg';
import fa__hand_holding from './fa/hand-holding.svg';
import fa__hand_lizard from './fa/hand-lizard.svg';
import fa__hand_middle_finger from './fa/hand-middle-finger.svg';
import fa__hand_peace from './fa/hand-peace.svg';
import fa__hand_point_down from './fa/hand-point-down.svg';
import fa__hand_point_left from './fa/hand-point-left.svg';
import fa__hand_point_right from './fa/hand-point-right.svg';
import fa__hand_point_up from './fa/hand-point-up.svg';
import fa__hand_pointer from './fa/hand-pointer.svg';
import fa__hand_scissors from './fa/hand-scissors.svg';
import fa__hand_sparkles from './fa/hand-sparkles.svg';
import fa__hand_spock from './fa/hand-spock.svg';
import fa__hand from './fa/hand.svg';
import fa__handcuffs from './fa/handcuffs.svg';
import fa__hands_asl_interpreting from './fa/hands-asl-interpreting.svg';
import fa__hands_bound from './fa/hands-bound.svg';
import fa__hands_bubbles from './fa/hands-bubbles.svg';
import fa__hands_clapping from './fa/hands-clapping.svg';
import fa__hands_holding_child from './fa/hands-holding-child.svg';
import fa__hands_holding_circle from './fa/hands-holding-circle.svg';
import fa__hands_holding from './fa/hands-holding.svg';
import fa__hands_praying from './fa/hands-praying.svg';
import fa__hands from './fa/hands.svg';
import fa__handshake_angle from './fa/handshake-angle.svg';
import fa__handshake_simple_slash from './fa/handshake-simple-slash.svg';
import fa__handshake_simple from './fa/handshake-simple.svg';
import fa__handshake_slash from './fa/handshake-slash.svg';
import fa__handshake from './fa/handshake.svg';
import fa__hanukiah from './fa/hanukiah.svg';
import fa__hard_drive from './fa/hard-drive.svg';
import fa__hashtag from './fa/hashtag.svg';
import fa__hat_cowboy_side from './fa/hat-cowboy-side.svg';
import fa__hat_cowboy from './fa/hat-cowboy.svg';
import fa__hat_wizard from './fa/hat-wizard.svg';
import fa__head_side_cough_slash from './fa/head-side-cough-slash.svg';
import fa__head_side_cough from './fa/head-side-cough.svg';
import fa__head_side_mask from './fa/head-side-mask.svg';
import fa__head_side_virus from './fa/head-side-virus.svg';
import fa__heading from './fa/heading.svg';
import fa__headphones_simple from './fa/headphones-simple.svg';
import fa__headphones from './fa/headphones.svg';
import fa__headset from './fa/headset.svg';
import fa__heart_circle_bolt from './fa/heart-circle-bolt.svg';
import fa__heart_circle_check from './fa/heart-circle-check.svg';
import fa__heart_circle_exclamation from './fa/heart-circle-exclamation.svg';
import fa__heart_circle_minus from './fa/heart-circle-minus.svg';
import fa__heart_circle_plus from './fa/heart-circle-plus.svg';
import fa__heart_circle_xmark from './fa/heart-circle-xmark.svg';
import fa__heart_crack from './fa/heart-crack.svg';
import fa__heart_pulse from './fa/heart-pulse.svg';
import fa__heart from './fa/heart.svg';
import fa__helicopter_symbol from './fa/helicopter-symbol.svg';
import fa__helicopter from './fa/helicopter.svg';
import fa__helmet_safety from './fa/helmet-safety.svg';
import fa__helmet_un from './fa/helmet-un.svg';
import fa__highlighter from './fa/highlighter.svg';
import fa__hill_avalanche from './fa/hill-avalanche.svg';
import fa__hill_rockslide from './fa/hill-rockslide.svg';
import fa__hippo from './fa/hippo.svg';
import fa__hockey_puck from './fa/hockey-puck.svg';
import fa__holly_berry from './fa/holly-berry.svg';
import fa__horse_head from './fa/horse-head.svg';
import fa__horse from './fa/horse.svg';
import fa__hospital_user from './fa/hospital-user.svg';
import fa__hospital from './fa/hospital.svg';
import fa__hot_tub_person from './fa/hot-tub-person.svg';
import fa__hotdog from './fa/hotdog.svg';
import fa__hotel from './fa/hotel.svg';
import fa__hourglass_end from './fa/hourglass-end.svg';
import fa__hourglass_half from './fa/hourglass-half.svg';
import fa__hourglass_start from './fa/hourglass-start.svg';
import fa__hourglass from './fa/hourglass.svg';
import fa__house_chimney_crack from './fa/house-chimney-crack.svg';
import fa__house_chimney_medical from './fa/house-chimney-medical.svg';
import fa__house_chimney_user from './fa/house-chimney-user.svg';
import fa__house_chimney_window from './fa/house-chimney-window.svg';
import fa__house_chimney from './fa/house-chimney.svg';
import fa__house_circle_check from './fa/house-circle-check.svg';
import fa__house_circle_exclamation from './fa/house-circle-exclamation.svg';
import fa__house_circle_xmark from './fa/house-circle-xmark.svg';
import fa__house_crack from './fa/house-crack.svg';
import fa__house_fire from './fa/house-fire.svg';
import fa__house_flag from './fa/house-flag.svg';
import fa__house_flood_water_circle_arrow_right from './fa/house-flood-water-circle-arrow-right.svg';
import fa__house_flood_water from './fa/house-flood-water.svg';
import fa__house_laptop from './fa/house-laptop.svg';
import fa__house_lock from './fa/house-lock.svg';
import fa__house_medical_circle_check from './fa/house-medical-circle-check.svg';
import fa__house_medical_circle_exclamation from './fa/house-medical-circle-exclamation.svg';
import fa__house_medical_circle_xmark from './fa/house-medical-circle-xmark.svg';
import fa__house_medical_flag from './fa/house-medical-flag.svg';
import fa__house_medical from './fa/house-medical.svg';
import fa__house_signal from './fa/house-signal.svg';
import fa__house_tsunami from './fa/house-tsunami.svg';
import fa__house_user from './fa/house-user.svg';
import fa__house from './fa/house.svg';
import fa__hryvnia_sign from './fa/hryvnia-sign.svg';
import fa__hurricane from './fa/hurricane.svg';
import fa__i_cursor from './fa/i-cursor.svg';
import fa__i from './fa/i.svg';
import fa__ice_cream from './fa/ice-cream.svg';
import fa__icicles from './fa/icicles.svg';
import fa__icons from './fa/icons.svg';
import fa__id_badge from './fa/id-badge.svg';
import fa__id_card_clip from './fa/id-card-clip.svg';
import fa__id_card from './fa/id-card.svg';
import fa__igloo from './fa/igloo.svg';
import fa__image_portrait from './fa/image-portrait.svg';
import fa__image from './fa/image.svg';
import fa__images from './fa/images.svg';
import fa__inbox from './fa/inbox.svg';
import fa__indent from './fa/indent.svg';
import fa__indian_rupee_sign from './fa/indian-rupee-sign.svg';
import fa__industry from './fa/industry.svg';
import fa__infinity from './fa/infinity.svg';
import fa__info from './fa/info.svg';
import fa__italic from './fa/italic.svg';
import fa__j from './fa/j.svg';
import fa__jar_wheat from './fa/jar-wheat.svg';
import fa__jar from './fa/jar.svg';
import fa__jedi from './fa/jedi.svg';
import fa__jet_fighter_up from './fa/jet-fighter-up.svg';
import fa__jet_fighter from './fa/jet-fighter.svg';
import fa__joint from './fa/joint.svg';
import fa__jug_detergent from './fa/jug-detergent.svg';
import fa__k from './fa/k.svg';
import fa__kaaba from './fa/kaaba.svg';
import fa__key from './fa/key.svg';
import fa__keyboard from './fa/keyboard.svg';
import fa__khanda from './fa/khanda.svg';
import fa__kip_sign from './fa/kip-sign.svg';
import fa__kit_medical from './fa/kit-medical.svg';
import fa__kitchen_set from './fa/kitchen-set.svg';
import fa__kiwi_bird from './fa/kiwi-bird.svg';
import fa__l from './fa/l.svg';
import fa__land_mine_on from './fa/land-mine-on.svg';
import fa__landmark_dome from './fa/landmark-dome.svg';
import fa__landmark_flag from './fa/landmark-flag.svg';
import fa__landmark from './fa/landmark.svg';
import fa__language from './fa/language.svg';
import fa__laptop_code from './fa/laptop-code.svg';
import fa__laptop_file from './fa/laptop-file.svg';
import fa__laptop_medical from './fa/laptop-medical.svg';
import fa__laptop from './fa/laptop.svg';
import fa__lari_sign from './fa/lari-sign.svg';
import fa__layer_group from './fa/layer-group.svg';
import fa__leaf from './fa/leaf.svg';
import fa__left_long from './fa/left-long.svg';
import fa__left_right from './fa/left-right.svg';
import fa__lemon from './fa/lemon.svg';
import fa__less_than_equal from './fa/less-than-equal.svg';
import fa__less_than from './fa/less-than.svg';
import fa__life_ring from './fa/life-ring.svg';
import fa__lightbulb from './fa/lightbulb.svg';
import fa__lines_leaning from './fa/lines-leaning.svg';
import fa__link_slash from './fa/link-slash.svg';
import fa__link from './fa/link.svg';
import fa__lira_sign from './fa/lira-sign.svg';
import fa__list_check from './fa/list-check.svg';
import fa__list_ol from './fa/list-ol.svg';
import fa__list_ul from './fa/list-ul.svg';
import fa__list from './fa/list.svg';
import fa__litecoin_sign from './fa/litecoin-sign.svg';
import fa__location_arrow from './fa/location-arrow.svg';
import fa__location_crosshairs from './fa/location-crosshairs.svg';
import fa__location_dot from './fa/location-dot.svg';
import fa__location_pin_lock from './fa/location-pin-lock.svg';
import fa__location_pin from './fa/location-pin.svg';
import fa__lock_open from './fa/lock-open.svg';
import fa__lock from './fa/lock.svg';
import fa__locust from './fa/locust.svg';
import fa__lungs_virus from './fa/lungs-virus.svg';
import fa__lungs from './fa/lungs.svg';
import fa__m from './fa/m.svg';
import fa__magnet from './fa/magnet.svg';
import fa__magnifying_glass_arrow_right from './fa/magnifying-glass-arrow-right.svg';
import fa__magnifying_glass_chart from './fa/magnifying-glass-chart.svg';
import fa__magnifying_glass_dollar from './fa/magnifying-glass-dollar.svg';
import fa__magnifying_glass_location from './fa/magnifying-glass-location.svg';
import fa__magnifying_glass_minus from './fa/magnifying-glass-minus.svg';
import fa__magnifying_glass_plus from './fa/magnifying-glass-plus.svg';
import fa__magnifying_glass from './fa/magnifying-glass.svg';
import fa__manat_sign from './fa/manat-sign.svg';
import fa__map_location_dot from './fa/map-location-dot.svg';
import fa__map_location from './fa/map-location.svg';
import fa__map_pin from './fa/map-pin.svg';
import fa__map from './fa/map.svg';
import fa__marker from './fa/marker.svg';
import fa__mars_and_venus_burst from './fa/mars-and-venus-burst.svg';
import fa__mars_and_venus from './fa/mars-and-venus.svg';
import fa__mars_double from './fa/mars-double.svg';
import fa__mars_stroke_right from './fa/mars-stroke-right.svg';
import fa__mars_stroke_up from './fa/mars-stroke-up.svg';
import fa__mars_stroke from './fa/mars-stroke.svg';
import fa__mars from './fa/mars.svg';
import fa__martini_glass_citrus from './fa/martini-glass-citrus.svg';
import fa__martini_glass_empty from './fa/martini-glass-empty.svg';
import fa__martini_glass from './fa/martini-glass.svg';
import fa__mask_face from './fa/mask-face.svg';
import fa__mask_ventilator from './fa/mask-ventilator.svg';
import fa__mask from './fa/mask.svg';
import fa__masks_theater from './fa/masks-theater.svg';
import fa__mattress_pillow from './fa/mattress-pillow.svg';
import fa__maximize from './fa/maximize.svg';
import fa__medal from './fa/medal.svg';
import fa__memory from './fa/memory.svg';
import fa__menorah from './fa/menorah.svg';
import fa__mercury from './fa/mercury.svg';
import fa__message from './fa/message.svg';
import fa__meteor from './fa/meteor.svg';
import fa__microchip from './fa/microchip.svg';
import fa__microphone_lines_slash from './fa/microphone-lines-slash.svg';
import fa__microphone_lines from './fa/microphone-lines.svg';
import fa__microphone_slash from './fa/microphone-slash.svg';
import fa__microphone from './fa/microphone.svg';
import fa__microscope from './fa/microscope.svg';
import fa__mill_sign from './fa/mill-sign.svg';
import fa__minimize from './fa/minimize.svg';
import fa__minus from './fa/minus.svg';
import fa__mitten from './fa/mitten.svg';
import fa__mobile_button from './fa/mobile-button.svg';
import fa__mobile_retro from './fa/mobile-retro.svg';
import fa__mobile_screen_button from './fa/mobile-screen-button.svg';
import fa__mobile_screen from './fa/mobile-screen.svg';
import fa__mobile from './fa/mobile.svg';
import fa__money_bill_1_wave from './fa/money-bill-1-wave.svg';
import fa__money_bill_1 from './fa/money-bill-1.svg';
import fa__money_bill_transfer from './fa/money-bill-transfer.svg';
import fa__money_bill_trend_up from './fa/money-bill-trend-up.svg';
import fa__money_bill_wave from './fa/money-bill-wave.svg';
import fa__money_bill_wheat from './fa/money-bill-wheat.svg';
import fa__money_bill from './fa/money-bill.svg';
import fa__money_bills from './fa/money-bills.svg';
import fa__money_check_dollar from './fa/money-check-dollar.svg';
import fa__money_check from './fa/money-check.svg';
import fa__monument from './fa/monument.svg';
import fa__moon from './fa/moon.svg';
import fa__mortar_pestle from './fa/mortar-pestle.svg';
import fa__mosque from './fa/mosque.svg';
import fa__mosquito_net from './fa/mosquito-net.svg';
import fa__mosquito from './fa/mosquito.svg';
import fa__motorcycle from './fa/motorcycle.svg';
import fa__mound from './fa/mound.svg';
import fa__mountain_city from './fa/mountain-city.svg';
import fa__mountain_sun from './fa/mountain-sun.svg';
import fa__mountain from './fa/mountain.svg';
import fa__mug_hot from './fa/mug-hot.svg';
import fa__mug_saucer from './fa/mug-saucer.svg';
import fa__music from './fa/music.svg';
import fa__n from './fa/n.svg';
import fa__naira_sign from './fa/naira-sign.svg';
import fa__network_wired from './fa/network-wired.svg';
import fa__neuter from './fa/neuter.svg';
import fa__newspaper from './fa/newspaper.svg';
import fa__not_equal from './fa/not-equal.svg';
import fa__notdef from './fa/notdef.svg';
import fa__note_sticky from './fa/note-sticky.svg';
import fa__notes_medical from './fa/notes-medical.svg';
import fa__o from './fa/o.svg';
import fa__object_group from './fa/object-group.svg';
import fa__object_ungroup from './fa/object-ungroup.svg';
import fa__oil_can from './fa/oil-can.svg';
import fa__oil_well from './fa/oil-well.svg';
import fa__om from './fa/om.svg';
import fa__otter from './fa/otter.svg';
import fa__outdent from './fa/outdent.svg';
import fa__p from './fa/p.svg';
import fa__pager from './fa/pager.svg';
import fa__paint_roller from './fa/paint-roller.svg';
import fa__paintbrush from './fa/paintbrush.svg';
import fa__palette from './fa/palette.svg';
import fa__pallet from './fa/pallet.svg';
import fa__panorama from './fa/panorama.svg';
import fa__paper_plane from './fa/paper-plane.svg';
import fa__paperclip from './fa/paperclip.svg';
import fa__parachute_box from './fa/parachute-box.svg';
import fa__paragraph from './fa/paragraph.svg';
import fa__passport from './fa/passport.svg';
import fa__paste from './fa/paste.svg';
import fa__pause from './fa/pause.svg';
import fa__paw from './fa/paw.svg';
import fa__peace from './fa/peace.svg';
import fa__pen_clip from './fa/pen-clip.svg';
import fa__pen_fancy from './fa/pen-fancy.svg';
import fa__pen_nib from './fa/pen-nib.svg';
import fa__pen_ruler from './fa/pen-ruler.svg';
import fa__pen_to_square from './fa/pen-to-square.svg';
import fa__pen from './fa/pen.svg';
import fa__pencil from './fa/pencil.svg';
import fa__people_arrows from './fa/people-arrows.svg';
import fa__people_carry_box from './fa/people-carry-box.svg';
import fa__people_group from './fa/people-group.svg';
import fa__people_line from './fa/people-line.svg';
import fa__people_pulling from './fa/people-pulling.svg';
import fa__people_robbery from './fa/people-robbery.svg';
import fa__people_roof from './fa/people-roof.svg';
import fa__pepper_hot from './fa/pepper-hot.svg';
import fa__percent from './fa/percent.svg';
import fa__person_arrow_down_to_line from './fa/person-arrow-down-to-line.svg';
import fa__person_arrow_up_from_line from './fa/person-arrow-up-from-line.svg';
import fa__person_biking from './fa/person-biking.svg';
import fa__person_booth from './fa/person-booth.svg';
import fa__person_breastfeeding from './fa/person-breastfeeding.svg';
import fa__person_burst from './fa/person-burst.svg';
import fa__person_cane from './fa/person-cane.svg';
import fa__person_chalkboard from './fa/person-chalkboard.svg';
import fa__person_circle_check from './fa/person-circle-check.svg';
import fa__person_circle_exclamation from './fa/person-circle-exclamation.svg';
import fa__person_circle_minus from './fa/person-circle-minus.svg';
import fa__person_circle_plus from './fa/person-circle-plus.svg';
import fa__person_circle_question from './fa/person-circle-question.svg';
import fa__person_circle_xmark from './fa/person-circle-xmark.svg';
import fa__person_digging from './fa/person-digging.svg';
import fa__person_dots_from_line from './fa/person-dots-from-line.svg';
import fa__person_dress_burst from './fa/person-dress-burst.svg';
import fa__person_dress from './fa/person-dress.svg';
import fa__person_drowning from './fa/person-drowning.svg';
import fa__person_falling_burst from './fa/person-falling-burst.svg';
import fa__person_falling from './fa/person-falling.svg';
import fa__person_half_dress from './fa/person-half-dress.svg';
import fa__person_harassing from './fa/person-harassing.svg';
import fa__person_hiking from './fa/person-hiking.svg';
import fa__person_military_pointing from './fa/person-military-pointing.svg';
import fa__person_military_rifle from './fa/person-military-rifle.svg';
import fa__person_military_to_person from './fa/person-military-to-person.svg';
import fa__person_praying from './fa/person-praying.svg';
import fa__person_pregnant from './fa/person-pregnant.svg';
import fa__person_rays from './fa/person-rays.svg';
import fa__person_rifle from './fa/person-rifle.svg';
import fa__person_running from './fa/person-running.svg';
import fa__person_shelter from './fa/person-shelter.svg';
import fa__person_skating from './fa/person-skating.svg';
import fa__person_skiing_nordic from './fa/person-skiing-nordic.svg';
import fa__person_skiing from './fa/person-skiing.svg';
import fa__person_snowboarding from './fa/person-snowboarding.svg';
import fa__person_swimming from './fa/person-swimming.svg';
import fa__person_through_window from './fa/person-through-window.svg';
import fa__person_walking_arrow_loop_left from './fa/person-walking-arrow-loop-left.svg';
import fa__person_walking_arrow_right from './fa/person-walking-arrow-right.svg';
import fa__person_walking_dashed_line_arrow_right from './fa/person-walking-dashed-line-arrow-right.svg';
import fa__person_walking_luggage from './fa/person-walking-luggage.svg';
import fa__person_walking_with_cane from './fa/person-walking-with-cane.svg';
import fa__person_walking from './fa/person-walking.svg';
import fa__person from './fa/person.svg';
import fa__peseta_sign from './fa/peseta-sign.svg';
import fa__peso_sign from './fa/peso-sign.svg';
import fa__phone_flip from './fa/phone-flip.svg';
import fa__phone_slash from './fa/phone-slash.svg';
import fa__phone_volume from './fa/phone-volume.svg';
import fa__phone from './fa/phone.svg';
import fa__photo_film from './fa/photo-film.svg';
import fa__piggy_bank from './fa/piggy-bank.svg';
import fa__pills from './fa/pills.svg';
import fa__pizza_slice from './fa/pizza-slice.svg';
import fa__place_of_worship from './fa/place-of-worship.svg';
import fa__plane_arrival from './fa/plane-arrival.svg';
import fa__plane_circle_check from './fa/plane-circle-check.svg';
import fa__plane_circle_exclamation from './fa/plane-circle-exclamation.svg';
import fa__plane_circle_xmark from './fa/plane-circle-xmark.svg';
import fa__plane_departure from './fa/plane-departure.svg';
import fa__plane_lock from './fa/plane-lock.svg';
import fa__plane_slash from './fa/plane-slash.svg';
import fa__plane_up from './fa/plane-up.svg';
import fa__plane from './fa/plane.svg';
import fa__plant_wilt from './fa/plant-wilt.svg';
import fa__plate_wheat from './fa/plate-wheat.svg';
import fa__play from './fa/play.svg';
import fa__plug_circle_bolt from './fa/plug-circle-bolt.svg';
import fa__plug_circle_check from './fa/plug-circle-check.svg';
import fa__plug_circle_exclamation from './fa/plug-circle-exclamation.svg';
import fa__plug_circle_minus from './fa/plug-circle-minus.svg';
import fa__plug_circle_plus from './fa/plug-circle-plus.svg';
import fa__plug_circle_xmark from './fa/plug-circle-xmark.svg';
import fa__plug from './fa/plug.svg';
import fa__plus_minus from './fa/plus-minus.svg';
import fa__plus from './fa/plus.svg';
import fa__podcast from './fa/podcast.svg';
import fa__poo_storm from './fa/poo-storm.svg';
import fa__poo from './fa/poo.svg';
import fa__poop from './fa/poop.svg';
import fa__power_off from './fa/power-off.svg';
import fa__prescription_bottle_medical from './fa/prescription-bottle-medical.svg';
import fa__prescription_bottle from './fa/prescription-bottle.svg';
import fa__prescription from './fa/prescription.svg';
import fa__print from './fa/print.svg';
import fa__pump_medical from './fa/pump-medical.svg';
import fa__pump_soap from './fa/pump-soap.svg';
import fa__puzzle_piece from './fa/puzzle-piece.svg';
import fa__q from './fa/q.svg';
import fa__qrcode from './fa/qrcode.svg';
import fa__question from './fa/question.svg';
import fa__quote_left from './fa/quote-left.svg';
import fa__quote_right from './fa/quote-right.svg';
import fa__r from './fa/r.svg';
import fa__radiation from './fa/radiation.svg';
import fa__radio from './fa/radio.svg';
import fa__rainbow from './fa/rainbow.svg';
import fa__ranking_star from './fa/ranking-star.svg';
import fa__receipt from './fa/receipt.svg';
import fa__record_vinyl from './fa/record-vinyl.svg';
import fa__rectangle_ad from './fa/rectangle-ad.svg';
import fa__rectangle_list from './fa/rectangle-list.svg';
import fa__rectangle_xmark from './fa/rectangle-xmark.svg';
import fa__recycle from './fa/recycle.svg';
import fa__registered from './fa/registered.svg';
import fa__repeat from './fa/repeat.svg';
import fa__reply_all from './fa/reply-all.svg';
import fa__reply from './fa/reply.svg';
import fa__republican from './fa/republican.svg';
import fa__restroom from './fa/restroom.svg';
import fa__retweet from './fa/retweet.svg';
import fa__ribbon from './fa/ribbon.svg';
import fa__right_from_bracket from './fa/right-from-bracket.svg';
import fa__right_left from './fa/right-left.svg';
import fa__right_long from './fa/right-long.svg';
import fa__right_to_bracket from './fa/right-to-bracket.svg';
import fa__ring from './fa/ring.svg';
import fa__road_barrier from './fa/road-barrier.svg';
import fa__road_bridge from './fa/road-bridge.svg';
import fa__road_circle_check from './fa/road-circle-check.svg';
import fa__road_circle_exclamation from './fa/road-circle-exclamation.svg';
import fa__road_circle_xmark from './fa/road-circle-xmark.svg';
import fa__road_lock from './fa/road-lock.svg';
import fa__road_spikes from './fa/road-spikes.svg';
import fa__road from './fa/road.svg';
import fa__robot from './fa/robot.svg';
import fa__rocket from './fa/rocket.svg';
import fa__rotate_left from './fa/rotate-left.svg';
import fa__rotate_right from './fa/rotate-right.svg';
import fa__rotate from './fa/rotate.svg';
import fa__route from './fa/route.svg';
import fa__rss from './fa/rss.svg';
import fa__ruble_sign from './fa/ruble-sign.svg';
import fa__rug from './fa/rug.svg';
import fa__ruler_combined from './fa/ruler-combined.svg';
import fa__ruler_horizontal from './fa/ruler-horizontal.svg';
import fa__ruler_vertical from './fa/ruler-vertical.svg';
import fa__ruler from './fa/ruler.svg';
import fa__rupee_sign from './fa/rupee-sign.svg';
import fa__rupiah_sign from './fa/rupiah-sign.svg';
import fa__s from './fa/s.svg';
import fa__sack_dollar from './fa/sack-dollar.svg';
import fa__sack_xmark from './fa/sack-xmark.svg';
import fa__sailboat from './fa/sailboat.svg';
import fa__satellite_dish from './fa/satellite-dish.svg';
import fa__satellite from './fa/satellite.svg';
import fa__scale_balanced from './fa/scale-balanced.svg';
import fa__scale_unbalanced_flip from './fa/scale-unbalanced-flip.svg';
import fa__scale_unbalanced from './fa/scale-unbalanced.svg';
import fa__school_circle_check from './fa/school-circle-check.svg';
import fa__school_circle_exclamation from './fa/school-circle-exclamation.svg';
import fa__school_circle_xmark from './fa/school-circle-xmark.svg';
import fa__school_flag from './fa/school-flag.svg';
import fa__school_lock from './fa/school-lock.svg';
import fa__school from './fa/school.svg';
import fa__scissors from './fa/scissors.svg';
import fa__screwdriver_wrench from './fa/screwdriver-wrench.svg';
import fa__screwdriver from './fa/screwdriver.svg';
import fa__scroll_torah from './fa/scroll-torah.svg';
import fa__scroll from './fa/scroll.svg';
import fa__sd_card from './fa/sd-card.svg';
import fa__section from './fa/section.svg';
import fa__seedling from './fa/seedling.svg';
import fa__server from './fa/server.svg';
import fa__shapes from './fa/shapes.svg';
import fa__share_from_square from './fa/share-from-square.svg';
import fa__share_nodes from './fa/share-nodes.svg';
import fa__share from './fa/share.svg';
import fa__sheet_plastic from './fa/sheet-plastic.svg';
import fa__shekel_sign from './fa/shekel-sign.svg';
import fa__shield_cat from './fa/shield-cat.svg';
import fa__shield_dog from './fa/shield-dog.svg';
import fa__shield_halved from './fa/shield-halved.svg';
import fa__shield_heart from './fa/shield-heart.svg';
import fa__shield_virus from './fa/shield-virus.svg';
import fa__shield from './fa/shield.svg';
import fa__ship from './fa/ship.svg';
import fa__shirt from './fa/shirt.svg';
import fa__shoe_prints from './fa/shoe-prints.svg';
import fa__shop_lock from './fa/shop-lock.svg';
import fa__shop_slash from './fa/shop-slash.svg';
import fa__shop from './fa/shop.svg';
import fa__shower from './fa/shower.svg';
import fa__shrimp from './fa/shrimp.svg';
import fa__shuffle from './fa/shuffle.svg';
import fa__shuttle_space from './fa/shuttle-space.svg';
import fa__sign_hanging from './fa/sign-hanging.svg';
import fa__signal from './fa/signal.svg';
import fa__signature from './fa/signature.svg';
import fa__signs_post from './fa/signs-post.svg';
import fa__sim_card from './fa/sim-card.svg';
import fa__sink from './fa/sink.svg';
import fa__sitemap from './fa/sitemap.svg';
import fa__skull_crossbones from './fa/skull-crossbones.svg';
import fa__skull from './fa/skull.svg';
import fa__slash from './fa/slash.svg';
import fa__sleigh from './fa/sleigh.svg';
import fa__sliders from './fa/sliders.svg';
import fa__smog from './fa/smog.svg';
import fa__smoking from './fa/smoking.svg';
import fa__snowflake from './fa/snowflake.svg';
import fa__snowman from './fa/snowman.svg';
import fa__snowplow from './fa/snowplow.svg';
import fa__soap from './fa/soap.svg';
import fa__socks from './fa/socks.svg';
import fa__solar_panel from './fa/solar-panel.svg';
import fa__sort_down from './fa/sort-down.svg';
import fa__sort_up from './fa/sort-up.svg';
import fa__sort from './fa/sort.svg';
import fa__spa from './fa/spa.svg';
import fa__spaghetti_monster_flying from './fa/spaghetti-monster-flying.svg';
import fa__spell_check from './fa/spell-check.svg';
import fa__spider from './fa/spider.svg';
import fa__spinner from './fa/spinner.svg';
import fa__splotch from './fa/splotch.svg';
import fa__spoon from './fa/spoon.svg';
import fa__spray_can_sparkles from './fa/spray-can-sparkles.svg';
import fa__spray_can from './fa/spray-can.svg';
import fa__square_arrow_up_right from './fa/square-arrow-up-right.svg';
import fa__square_caret_down from './fa/square-caret-down.svg';
import fa__square_caret_left from './fa/square-caret-left.svg';
import fa__square_caret_right from './fa/square-caret-right.svg';
import fa__square_caret_up from './fa/square-caret-up.svg';
import fa__square_check from './fa/square-check.svg';
import fa__square_envelope from './fa/square-envelope.svg';
import fa__square_full from './fa/square-full.svg';
import fa__square_h from './fa/square-h.svg';
import fa__square_minus from './fa/square-minus.svg';
import fa__square_nfi from './fa/square-nfi.svg';
import fa__square_parking from './fa/square-parking.svg';
import fa__square_pen from './fa/square-pen.svg';
import fa__square_person_confined from './fa/square-person-confined.svg';
import fa__square_phone_flip from './fa/square-phone-flip.svg';
import fa__square_phone from './fa/square-phone.svg';
import fa__square_plus from './fa/square-plus.svg';
import fa__square_poll_horizontal from './fa/square-poll-horizontal.svg';
import fa__square_poll_vertical from './fa/square-poll-vertical.svg';
import fa__square_root_variable from './fa/square-root-variable.svg';
import fa__square_rss from './fa/square-rss.svg';
import fa__square_share_nodes from './fa/square-share-nodes.svg';
import fa__square_up_right from './fa/square-up-right.svg';
import fa__square_virus from './fa/square-virus.svg';
import fa__square_xmark from './fa/square-xmark.svg';
import fa__square from './fa/square.svg';
import fa__staff_snake from './fa/staff-snake.svg';
import fa__stairs from './fa/stairs.svg';
import fa__stamp from './fa/stamp.svg';
import fa__stapler from './fa/stapler.svg';
import fa__star_and_crescent from './fa/star-and-crescent.svg';
import fa__star_half_stroke from './fa/star-half-stroke.svg';
import fa__star_half from './fa/star-half.svg';
import fa__star_of_david from './fa/star-of-david.svg';
import fa__star_of_life from './fa/star-of-life.svg';
import fa__star from './fa/star.svg';
import fa__sterling_sign from './fa/sterling-sign.svg';
import fa__stethoscope from './fa/stethoscope.svg';
import fa__stop from './fa/stop.svg';
import fa__stopwatch_20 from './fa/stopwatch-20.svg';
import fa__stopwatch from './fa/stopwatch.svg';
import fa__store_slash from './fa/store-slash.svg';
import fa__store from './fa/store.svg';
import fa__street_view from './fa/street-view.svg';
import fa__strikethrough from './fa/strikethrough.svg';
import fa__stroopwafel from './fa/stroopwafel.svg';
import fa__subscript from './fa/subscript.svg';
import fa__suitcase_medical from './fa/suitcase-medical.svg';
import fa__suitcase_rolling from './fa/suitcase-rolling.svg';
import fa__suitcase from './fa/suitcase.svg';
import fa__sun_plant_wilt from './fa/sun-plant-wilt.svg';
import fa__sun from './fa/sun.svg';
import fa__superscript from './fa/superscript.svg';
import fa__swatchbook from './fa/swatchbook.svg';
import fa__synagogue from './fa/synagogue.svg';
import fa__syringe from './fa/syringe.svg';
import fa__t from './fa/t.svg';
import fa__table_cells_large from './fa/table-cells-large.svg';
import fa__table_cells from './fa/table-cells.svg';
import fa__table_columns from './fa/table-columns.svg';
import fa__table_list from './fa/table-list.svg';
import fa__table_tennis_paddle_ball from './fa/table-tennis-paddle-ball.svg';
import fa__table from './fa/table.svg';
import fa__tablet_button from './fa/tablet-button.svg';
import fa__tablet_screen_button from './fa/tablet-screen-button.svg';
import fa__tablet from './fa/tablet.svg';
import fa__tablets from './fa/tablets.svg';
import fa__tachograph_digital from './fa/tachograph-digital.svg';
import fa__tag from './fa/tag.svg';
import fa__tags from './fa/tags.svg';
import fa__tape from './fa/tape.svg';
import fa__tarp_droplet from './fa/tarp-droplet.svg';
import fa__tarp from './fa/tarp.svg';
import fa__taxi from './fa/taxi.svg';
import fa__teeth_open from './fa/teeth-open.svg';
import fa__teeth from './fa/teeth.svg';
import fa__temperature_arrow_down from './fa/temperature-arrow-down.svg';
import fa__temperature_arrow_up from './fa/temperature-arrow-up.svg';
import fa__temperature_empty from './fa/temperature-empty.svg';
import fa__temperature_full from './fa/temperature-full.svg';
import fa__temperature_half from './fa/temperature-half.svg';
import fa__temperature_high from './fa/temperature-high.svg';
import fa__temperature_low from './fa/temperature-low.svg';
import fa__temperature_quarter from './fa/temperature-quarter.svg';
import fa__temperature_three_quarters from './fa/temperature-three-quarters.svg';
import fa__tenge_sign from './fa/tenge-sign.svg';
import fa__tent_arrow_down_to_line from './fa/tent-arrow-down-to-line.svg';
import fa__tent_arrow_left_right from './fa/tent-arrow-left-right.svg';
import fa__tent_arrow_turn_left from './fa/tent-arrow-turn-left.svg';
import fa__tent_arrows_down from './fa/tent-arrows-down.svg';
import fa__tent from './fa/tent.svg';
import fa__tents from './fa/tents.svg';
import fa__terminal from './fa/terminal.svg';
import fa__text_height from './fa/text-height.svg';
import fa__text_slash from './fa/text-slash.svg';
import fa__text_width from './fa/text-width.svg';
import fa__thermometer from './fa/thermometer.svg';
import fa__thumbs_down from './fa/thumbs-down.svg';
import fa__thumbs_up from './fa/thumbs-up.svg';
import fa__thumbtack from './fa/thumbtack.svg';
import fa__ticket_simple from './fa/ticket-simple.svg';
import fa__ticket from './fa/ticket.svg';
import fa__timeline from './fa/timeline.svg';
import fa__toggle_off from './fa/toggle-off.svg';
import fa__toggle_on from './fa/toggle-on.svg';
import fa__toilet_paper_slash from './fa/toilet-paper-slash.svg';
import fa__toilet_paper from './fa/toilet-paper.svg';
import fa__toilet_portable from './fa/toilet-portable.svg';
import fa__toilet from './fa/toilet.svg';
import fa__toilets_portable from './fa/toilets-portable.svg';
import fa__toolbox from './fa/toolbox.svg';
import fa__tooth from './fa/tooth.svg';
import fa__torii_gate from './fa/torii-gate.svg';
import fa__tornado from './fa/tornado.svg';
import fa__tower_broadcast from './fa/tower-broadcast.svg';
import fa__tower_cell from './fa/tower-cell.svg';
import fa__tower_observation from './fa/tower-observation.svg';
import fa__tractor from './fa/tractor.svg';
import fa__trademark from './fa/trademark.svg';
import fa__traffic_light from './fa/traffic-light.svg';
import fa__trailer from './fa/trailer.svg';
import fa__train_subway from './fa/train-subway.svg';
import fa__train_tram from './fa/train-tram.svg';
import fa__train from './fa/train.svg';
import fa__transgender from './fa/transgender.svg';
import fa__trash_arrow_up from './fa/trash-arrow-up.svg';
import fa__trash_can_arrow_up from './fa/trash-can-arrow-up.svg';
import fa__trash_can from './fa/trash-can.svg';
import fa__trash from './fa/trash.svg';
import fa__tree_city from './fa/tree-city.svg';
import fa__tree from './fa/tree.svg';
import fa__triangle_exclamation from './fa/triangle-exclamation.svg';
import fa__trophy from './fa/trophy.svg';
import fa__trowel_bricks from './fa/trowel-bricks.svg';
import fa__trowel from './fa/trowel.svg';
import fa__truck_arrow_right from './fa/truck-arrow-right.svg';
import fa__truck_droplet from './fa/truck-droplet.svg';
import fa__truck_fast from './fa/truck-fast.svg';
import fa__truck_field_un from './fa/truck-field-un.svg';
import fa__truck_field from './fa/truck-field.svg';
import fa__truck_front from './fa/truck-front.svg';
import fa__truck_medical from './fa/truck-medical.svg';
import fa__truck_monster from './fa/truck-monster.svg';
import fa__truck_moving from './fa/truck-moving.svg';
import fa__truck_pickup from './fa/truck-pickup.svg';
import fa__truck_plane from './fa/truck-plane.svg';
import fa__truck_ramp_box from './fa/truck-ramp-box.svg';
import fa__truck from './fa/truck.svg';
import fa__tty from './fa/tty.svg';
import fa__turkish_lira_sign from './fa/turkish-lira-sign.svg';
import fa__turn_down from './fa/turn-down.svg';
import fa__turn_up from './fa/turn-up.svg';
import fa__tv from './fa/tv.svg';
import fa__u from './fa/u.svg';
import fa__umbrella_beach from './fa/umbrella-beach.svg';
import fa__umbrella from './fa/umbrella.svg';
import fa__underline from './fa/underline.svg';
import fa__universal_access from './fa/universal-access.svg';
import fa__unlock_keyhole from './fa/unlock-keyhole.svg';
import fa__unlock from './fa/unlock.svg';
import fa__up_down_left_right from './fa/up-down-left-right.svg';
import fa__up_down from './fa/up-down.svg';
import fa__up_long from './fa/up-long.svg';
import fa__up_right_and_down_left_from_center from './fa/up-right-and-down-left-from-center.svg';
import fa__up_right_from_square from './fa/up-right-from-square.svg';
import fa__upload from './fa/upload.svg';
import fa__user_astronaut from './fa/user-astronaut.svg';
import fa__user_check from './fa/user-check.svg';
import fa__user_clock from './fa/user-clock.svg';
import fa__user_doctor from './fa/user-doctor.svg';
import fa__user_gear from './fa/user-gear.svg';
import fa__user_graduate from './fa/user-graduate.svg';
import fa__user_group from './fa/user-group.svg';
import fa__user_injured from './fa/user-injured.svg';
import fa__user_large_slash from './fa/user-large-slash.svg';
import fa__user_large from './fa/user-large.svg';
import fa__user_lock from './fa/user-lock.svg';
import fa__user_minus from './fa/user-minus.svg';
import fa__user_ninja from './fa/user-ninja.svg';
import fa__user_nurse from './fa/user-nurse.svg';
import fa__user_pen from './fa/user-pen.svg';
import fa__user_plus from './fa/user-plus.svg';
import fa__user_secret from './fa/user-secret.svg';
import fa__user_shield from './fa/user-shield.svg';
import fa__user_slash from './fa/user-slash.svg';
import fa__user_tag from './fa/user-tag.svg';
import fa__user_tie from './fa/user-tie.svg';
import fa__user_xmark from './fa/user-xmark.svg';
import fa__user from './fa/user.svg';
import fa__users_between_lines from './fa/users-between-lines.svg';
import fa__users_gear from './fa/users-gear.svg';
import fa__users_line from './fa/users-line.svg';
import fa__users_rays from './fa/users-rays.svg';
import fa__users_rectangle from './fa/users-rectangle.svg';
import fa__users_slash from './fa/users-slash.svg';
import fa__users_viewfinder from './fa/users-viewfinder.svg';
import fa__users from './fa/users.svg';
import fa__utensils from './fa/utensils.svg';
import fa__v from './fa/v.svg';
import fa__van_shuttle from './fa/van-shuttle.svg';
import fa__vault from './fa/vault.svg';
import fa__vector_square from './fa/vector-square.svg';
import fa__venus_double from './fa/venus-double.svg';
import fa__venus_mars from './fa/venus-mars.svg';
import fa__venus from './fa/venus.svg';
import fa__vest_patches from './fa/vest-patches.svg';
import fa__vest from './fa/vest.svg';
import fa__vial_circle_check from './fa/vial-circle-check.svg';
import fa__vial_virus from './fa/vial-virus.svg';
import fa__vial from './fa/vial.svg';
import fa__vials from './fa/vials.svg';
import fa__video_slash from './fa/video-slash.svg';
import fa__video from './fa/video.svg';
import fa__vihara from './fa/vihara.svg';
import fa__virus_covid_slash from './fa/virus-covid-slash.svg';
import fa__virus_covid from './fa/virus-covid.svg';
import fa__virus_slash from './fa/virus-slash.svg';
import fa__virus from './fa/virus.svg';
import fa__viruses from './fa/viruses.svg';
import fa__voicemail from './fa/voicemail.svg';
import fa__volcano from './fa/volcano.svg';
import fa__volleyball from './fa/volleyball.svg';
import fa__volume_high from './fa/volume-high.svg';
import fa__volume_low from './fa/volume-low.svg';
import fa__volume_off from './fa/volume-off.svg';
import fa__volume_xmark from './fa/volume-xmark.svg';
import fa__vr_cardboard from './fa/vr-cardboard.svg';
import fa__w from './fa/w.svg';
import fa__walkie_talkie from './fa/walkie-talkie.svg';
import fa__wallet from './fa/wallet.svg';
import fa__wand_magic_sparkles from './fa/wand-magic-sparkles.svg';
import fa__wand_magic from './fa/wand-magic.svg';
import fa__wand_sparkles from './fa/wand-sparkles.svg';
import fa__warehouse from './fa/warehouse.svg';
import fa__water_ladder from './fa/water-ladder.svg';
import fa__water from './fa/water.svg';
import fa__wave_square from './fa/wave-square.svg';
import fa__weight_hanging from './fa/weight-hanging.svg';
import fa__weight_scale from './fa/weight-scale.svg';
import fa__wheat_awn_circle_exclamation from './fa/wheat-awn-circle-exclamation.svg';
import fa__wheat_awn from './fa/wheat-awn.svg';
import fa__wheelchair_move from './fa/wheelchair-move.svg';
import fa__wheelchair from './fa/wheelchair.svg';
import fa__whiskey_glass from './fa/whiskey-glass.svg';
import fa__wifi from './fa/wifi.svg';
import fa__wind from './fa/wind.svg';
import fa__window_maximize from './fa/window-maximize.svg';
import fa__window_minimize from './fa/window-minimize.svg';
import fa__window_restore from './fa/window-restore.svg';
import fa__wine_bottle from './fa/wine-bottle.svg';
import fa__wine_glass_empty from './fa/wine-glass-empty.svg';
import fa__wine_glass from './fa/wine-glass.svg';
import fa__won_sign from './fa/won-sign.svg';
import fa__worm from './fa/worm.svg';
import fa__wrench from './fa/wrench.svg';
import fa__x_ray from './fa/x-ray.svg';
import fa__x from './fa/x.svg';
import fa__xmark from './fa/xmark.svg';
import fa__xmarks_lines from './fa/xmarks-lines.svg';
import fa__y from './fa/y.svg';
import fa__yen_sign from './fa/yen-sign.svg';
import fa__yin_yang from './fa/yin-yang.svg';
import fa__z from './fa/z.svg';
import vaadin__abacus from './vaadin/abacus.svg';
import vaadin__absolute_position from './vaadin/absolute-position.svg';
import vaadin__academy_cap from './vaadin/academy-cap.svg';
import vaadin__accessibility from './vaadin/accessibility.svg';
import vaadin__accordion_menu from './vaadin/accordion-menu.svg';
import vaadin__add_dock from './vaadin/add-dock.svg';
import vaadin__adjust from './vaadin/adjust.svg';
import vaadin__adobe_flash from './vaadin/adobe-flash.svg';
import vaadin__airplane from './vaadin/airplane.svg';
import vaadin__alarm from './vaadin/alarm.svg';
import vaadin__align_center from './vaadin/align-center.svg';
import vaadin__align_justify from './vaadin/align-justify.svg';
import vaadin__align_left from './vaadin/align-left.svg';
import vaadin__align_right from './vaadin/align-right.svg';
import vaadin__alt_a from './vaadin/alt-a.svg';
import vaadin__alt from './vaadin/alt.svg';
import vaadin__ambulance from './vaadin/ambulance.svg';
import vaadin__anchor from './vaadin/anchor.svg';
import vaadin__angle_double_down from './vaadin/angle-double-down.svg';
import vaadin__angle_double_left from './vaadin/angle-double-left.svg';
import vaadin__angle_double_right from './vaadin/angle-double-right.svg';
import vaadin__angle_double_up from './vaadin/angle-double-up.svg';
import vaadin__angle_down from './vaadin/angle-down.svg';
import vaadin__angle_left from './vaadin/angle-left.svg';
import vaadin__angle_right from './vaadin/angle-right.svg';
import vaadin__angle_up from './vaadin/angle-up.svg';
import vaadin__archive from './vaadin/archive.svg';
import vaadin__archives from './vaadin/archives.svg';
import vaadin__area_select from './vaadin/area-select.svg';
import vaadin__arrow_backward from './vaadin/arrow-backward.svg';
import vaadin__arrow_circle_down_o from './vaadin/arrow-circle-down-o.svg';
import vaadin__arrow_circle_down from './vaadin/arrow-circle-down.svg';
import vaadin__arrow_circle_left_o from './vaadin/arrow-circle-left-o.svg';
import vaadin__arrow_circle_left from './vaadin/arrow-circle-left.svg';
import vaadin__arrow_circle_right_o from './vaadin/arrow-circle-right-o.svg';
import vaadin__arrow_circle_right from './vaadin/arrow-circle-right.svg';
import vaadin__arrow_circle_up_o from './vaadin/arrow-circle-up-o.svg';
import vaadin__arrow_circle_up from './vaadin/arrow-circle-up.svg';
import vaadin__arrow_down from './vaadin/arrow-down.svg';
import vaadin__arrow_forward from './vaadin/arrow-forward.svg';
import vaadin__arrow_left from './vaadin/arrow-left.svg';
import vaadin__arrow_long_down from './vaadin/arrow-long-down.svg';
import vaadin__arrow_long_left from './vaadin/arrow-long-left.svg';
import vaadin__arrow_right from './vaadin/arrow-right.svg';
import vaadin__arrow_up from './vaadin/arrow-up.svg';
import vaadin__arrows_cross from './vaadin/arrows-cross.svg';
import vaadin__arrows_long_h from './vaadin/arrows-long-h.svg';
import vaadin__arrows_long_right from './vaadin/arrows-long-right.svg';
import vaadin__arrows_long_up from './vaadin/arrows-long-up.svg';
import vaadin__arrows_long_v from './vaadin/arrows-long-v.svg';
import vaadin__arrows from './vaadin/arrows.svg';
import vaadin__asterisk from './vaadin/asterisk.svg';
import vaadin__at from './vaadin/at.svg';
import vaadin__automation from './vaadin/automation.svg';
import vaadin__backspace_a from './vaadin/backspace-a.svg';
import vaadin__backspace from './vaadin/backspace.svg';
import vaadin__backwards from './vaadin/backwards.svg';
import vaadin__ban from './vaadin/ban.svg';
import vaadin__bar_chart_h from './vaadin/bar-chart-h.svg';
import vaadin__bar_chart_v from './vaadin/bar-chart-v.svg';
import vaadin__bar_chart from './vaadin/bar-chart.svg';
import vaadin__barcode from './vaadin/barcode.svg';
import vaadin__bed from './vaadin/bed.svg';
import vaadin__bell_o from './vaadin/bell-o.svg';
import vaadin__bell_slash_o from './vaadin/bell-slash-o.svg';
import vaadin__bell_slash from './vaadin/bell-slash.svg';
import vaadin__bell from './vaadin/bell.svg';
import vaadin__boat from './vaadin/boat.svg';
import vaadin__bold from './vaadin/bold.svg';
import vaadin__bolt from './vaadin/bolt.svg';
import vaadin__bomb from './vaadin/bomb.svg';
import vaadin__book_dollar from './vaadin/book-dollar.svg';
import vaadin__book_percent from './vaadin/book-percent.svg';
import vaadin__book from './vaadin/book.svg';
import vaadin__bookmark_o from './vaadin/bookmark-o.svg';
import vaadin__bookmark from './vaadin/bookmark.svg';
import vaadin__briefcase from './vaadin/briefcase.svg';
import vaadin__browser from './vaadin/browser.svg';
import vaadin__bug_o from './vaadin/bug-o.svg';
import vaadin__bug from './vaadin/bug.svg';
import vaadin__building_o from './vaadin/building-o.svg';
import vaadin__building from './vaadin/building.svg';
import vaadin__bullets from './vaadin/bullets.svg';
import vaadin__bullseye from './vaadin/bullseye.svg';
import vaadin__bus from './vaadin/bus.svg';
import vaadin__button from './vaadin/button.svg';
import vaadin__calc_book from './vaadin/calc-book.svg';
import vaadin__calc from './vaadin/calc.svg';
import vaadin__calendar_briefcase from './vaadin/calendar-briefcase.svg';
import vaadin__calendar_clock from './vaadin/calendar-clock.svg';
import vaadin__calendar_envelope from './vaadin/calendar-envelope.svg';
import vaadin__calendar_o from './vaadin/calendar-o.svg';
import vaadin__calendar_user from './vaadin/calendar-user.svg';
import vaadin__calendar from './vaadin/calendar.svg';
import vaadin__camera from './vaadin/camera.svg';
import vaadin__car from './vaadin/car.svg';
import vaadin__caret_down from './vaadin/caret-down.svg';
import vaadin__caret_left from './vaadin/caret-left.svg';
import vaadin__caret_right from './vaadin/caret-right.svg';
import vaadin__caret_square_down_o from './vaadin/caret-square-down-o.svg';
import vaadin__caret_square_left_o from './vaadin/caret-square-left-o.svg';
import vaadin__caret_square_right_o from './vaadin/caret-square-right-o.svg';
import vaadin__caret_square_up_o from './vaadin/caret-square-up-o.svg';
import vaadin__caret_up from './vaadin/caret-up.svg';
import vaadin__cart_o from './vaadin/cart-o.svg';
import vaadin__cart from './vaadin/cart.svg';
import vaadin__cash from './vaadin/cash.svg';
import vaadin__chart_3d from './vaadin/chart-3d.svg';
import vaadin__chart_grid from './vaadin/chart-grid.svg';
import vaadin__chart_line from './vaadin/chart-line.svg';
import vaadin__chart_timeline from './vaadin/chart-timeline.svg';
import vaadin__chart from './vaadin/chart.svg';
import vaadin__chat from './vaadin/chat.svg';
import vaadin__check_circle_o from './vaadin/check-circle-o.svg';
import vaadin__check_circle from './vaadin/check-circle.svg';
import vaadin__check_square_o from './vaadin/check-square-o.svg';
import vaadin__check_square from './vaadin/check-square.svg';
import vaadin__check from './vaadin/check.svg';
import vaadin__chevron_circle_down_o from './vaadin/chevron-circle-down-o.svg';
import vaadin__chevron_circle_down from './vaadin/chevron-circle-down.svg';
import vaadin__chevron_circle_left_o from './vaadin/chevron-circle-left-o.svg';
import vaadin__chevron_circle_left from './vaadin/chevron-circle-left.svg';
import vaadin__chevron_circle_right_o from './vaadin/chevron-circle-right-o.svg';
import vaadin__chevron_circle_right from './vaadin/chevron-circle-right.svg';
import vaadin__chevron_circle_up_o from './vaadin/chevron-circle-up-o.svg';
import vaadin__chevron_circle_up from './vaadin/chevron-circle-up.svg';
import vaadin__chevron_down_small from './vaadin/chevron-down-small.svg';
import vaadin__chevron_down from './vaadin/chevron-down.svg';
import vaadin__chevron_left_small from './vaadin/chevron-left-small.svg';
import vaadin__chevron_left from './vaadin/chevron-left.svg';
import vaadin__chevron_right_small from './vaadin/chevron-right-small.svg';
import vaadin__chevron_right from './vaadin/chevron-right.svg';
import vaadin__chevron_up_small from './vaadin/chevron-up-small.svg';
import vaadin__chevron_up from './vaadin/chevron-up.svg';
import vaadin__child from './vaadin/child.svg';
import vaadin__circle_thin from './vaadin/circle-thin.svg';
import vaadin__circle from './vaadin/circle.svg';
import vaadin__clipboard_check from './vaadin/clipboard-check.svg';
import vaadin__clipboard_cross from './vaadin/clipboard-cross.svg';
import vaadin__clipboard_heart from './vaadin/clipboard-heart.svg';
import vaadin__clipboard_pulse from './vaadin/clipboard-pulse.svg';
import vaadin__clipboard_text from './vaadin/clipboard-text.svg';
import vaadin__clipboard_user from './vaadin/clipboard-user.svg';
import vaadin__clipboard from './vaadin/clipboard.svg';
import vaadin__clock from './vaadin/clock.svg';
import vaadin__close_big from './vaadin/close-big.svg';
import vaadin__close_circle_o from './vaadin/close-circle-o.svg';
import vaadin__close_circle from './vaadin/close-circle.svg';
import vaadin__close_small from './vaadin/close-small.svg';
import vaadin__close from './vaadin/close.svg';
import vaadin__cloud_download_o from './vaadin/cloud-download-o.svg';
import vaadin__cloud_download from './vaadin/cloud-download.svg';
import vaadin__cloud_o from './vaadin/cloud-o.svg';
import vaadin__cloud_upload_o from './vaadin/cloud-upload-o.svg';
import vaadin__cloud_upload from './vaadin/cloud-upload.svg';
import vaadin__cloud from './vaadin/cloud.svg';
import vaadin__cluster from './vaadin/cluster.svg';
import vaadin__code from './vaadin/code.svg';
import vaadin__coffee from './vaadin/coffee.svg';
import vaadin__cog_o from './vaadin/cog-o.svg';
import vaadin__cog from './vaadin/cog.svg';
import vaadin__cogs from './vaadin/cogs.svg';
import vaadin__coin_piles from './vaadin/coin-piles.svg';
import vaadin__coins from './vaadin/coins.svg';
import vaadin__combobox from './vaadin/combobox.svg';
import vaadin__comment_ellipsis_o from './vaadin/comment-ellipsis-o.svg';
import vaadin__comment_ellipsis from './vaadin/comment-ellipsis.svg';
import vaadin__comment_o from './vaadin/comment-o.svg';
import vaadin__comment from './vaadin/comment.svg';
import vaadin__comments_o from './vaadin/comments-o.svg';
import vaadin__comments from './vaadin/comments.svg';
import vaadin__compile from './vaadin/compile.svg';
import vaadin__compress_square from './vaadin/compress-square.svg';
import vaadin__compress from './vaadin/compress.svg';
import vaadin__connect_o from './vaadin/connect-o.svg';
import vaadin__connect from './vaadin/connect.svg';
import vaadin__controller from './vaadin/controller.svg';
import vaadin__copy_o from './vaadin/copy-o.svg';
import vaadin__copy from './vaadin/copy.svg';
import vaadin__copyright from './vaadin/copyright.svg';
import vaadin__corner_lower_left from './vaadin/corner-lower-left.svg';
import vaadin__corner_lower_right from './vaadin/corner-lower-right.svg';
import vaadin__corner_upper_left from './vaadin/corner-upper-left.svg';
import vaadin__corner_upper_right from './vaadin/corner-upper-right.svg';
import vaadin__credit_card from './vaadin/credit-card.svg';
import vaadin__crop from './vaadin/crop.svg';
import vaadin__cross_cutlery from './vaadin/cross-cutlery.svg';
import vaadin__crosshairs from './vaadin/crosshairs.svg';
import vaadin__css from './vaadin/css.svg';
import vaadin__ctrl_a from './vaadin/ctrl-a.svg';
import vaadin__ctrl from './vaadin/ctrl.svg';
import vaadin__cube from './vaadin/cube.svg';
import vaadin__cubes from './vaadin/cubes.svg';
import vaadin__curly_brackets from './vaadin/curly-brackets.svg';
import vaadin__cursor_o from './vaadin/cursor-o.svg';
import vaadin__cursor from './vaadin/cursor.svg';
import vaadin__cutlery from './vaadin/cutlery.svg';
import vaadin__dashboard from './vaadin/dashboard.svg';
import vaadin__database from './vaadin/database.svg';
import vaadin__date_input from './vaadin/date-input.svg';
import vaadin__deindent from './vaadin/deindent.svg';
import vaadin__del_a from './vaadin/del-a.svg';
import vaadin__del from './vaadin/del.svg';
import vaadin__dental_chair from './vaadin/dental-chair.svg';
import vaadin__desktop from './vaadin/desktop.svg';
import vaadin__diamond_o from './vaadin/diamond-o.svg';
import vaadin__diamond from './vaadin/diamond.svg';
import vaadin__diploma_scroll from './vaadin/diploma-scroll.svg';
import vaadin__diploma from './vaadin/diploma.svg';
import vaadin__disc from './vaadin/disc.svg';
import vaadin__doctor_briefcase from './vaadin/doctor-briefcase.svg';
import vaadin__doctor from './vaadin/doctor.svg';
import vaadin__dollar from './vaadin/dollar.svg';
import vaadin__dot_circle from './vaadin/dot-circle.svg';
import vaadin__download_alt from './vaadin/download-alt.svg';
import vaadin__download from './vaadin/download.svg';
import vaadin__drop from './vaadin/drop.svg';
import vaadin__edit from './vaadin/edit.svg';
import vaadin__eject from './vaadin/eject.svg';
import vaadin__elastic from './vaadin/elastic.svg';
import vaadin__ellipsis_circle_o from './vaadin/ellipsis-circle-o.svg';
import vaadin__ellipsis_circle from './vaadin/ellipsis-circle.svg';
import vaadin__ellipsis_dots_h from './vaadin/ellipsis-dots-h.svg';
import vaadin__ellipsis_dots_v from './vaadin/ellipsis-dots-v.svg';
import vaadin__ellipsis_h from './vaadin/ellipsis-h.svg';
import vaadin__ellipsis_v from './vaadin/ellipsis-v.svg';
import vaadin__enter_arrow from './vaadin/enter-arrow.svg';
import vaadin__enter from './vaadin/enter.svg';
import vaadin__envelope_o from './vaadin/envelope-o.svg';
import vaadin__envelope_open_o from './vaadin/envelope-open-o.svg';
import vaadin__envelope_open from './vaadin/envelope-open.svg';
import vaadin__envelope from './vaadin/envelope.svg';
import vaadin__envelopes_o from './vaadin/envelopes-o.svg';
import vaadin__envelopes from './vaadin/envelopes.svg';
import vaadin__eraser from './vaadin/eraser.svg';
import vaadin__esc_a from './vaadin/esc-a.svg';
import vaadin__esc from './vaadin/esc.svg';
import vaadin__euro from './vaadin/euro.svg';
import vaadin__exchange from './vaadin/exchange.svg';
import vaadin__exclamation_circle_o from './vaadin/exclamation-circle-o.svg';
import vaadin__exclamation_circle from './vaadin/exclamation-circle.svg';
import vaadin__exclamation from './vaadin/exclamation.svg';
import vaadin__exit_o from './vaadin/exit-o.svg';
import vaadin__exit from './vaadin/exit.svg';
import vaadin__expand_full from './vaadin/expand-full.svg';
import vaadin__expand_square from './vaadin/expand-square.svg';
import vaadin__expand from './vaadin/expand.svg';
import vaadin__external_browser from './vaadin/external-browser.svg';
import vaadin__external_link from './vaadin/external-link.svg';
import vaadin__eye_slash from './vaadin/eye-slash.svg';
import vaadin__eye from './vaadin/eye.svg';
import vaadin__eyedropper from './vaadin/eyedropper.svg';
import vaadin__facebook_square from './vaadin/facebook-square.svg';
import vaadin__facebook from './vaadin/facebook.svg';
import vaadin__factory from './vaadin/factory.svg';
import vaadin__family from './vaadin/family.svg';
import vaadin__fast_backward from './vaadin/fast-backward.svg';
import vaadin__fast_forward from './vaadin/fast-forward.svg';
import vaadin__female from './vaadin/female.svg';
import vaadin__file_add from './vaadin/file-add.svg';
import vaadin__file_code from './vaadin/file-code.svg';
import vaadin__file_font from './vaadin/file-font.svg';
import vaadin__file_movie from './vaadin/file-movie.svg';
import vaadin__file_o from './vaadin/file-o.svg';
import vaadin__file_picture from './vaadin/file-picture.svg';
import vaadin__file_presentation from './vaadin/file-presentation.svg';
import vaadin__file_process from './vaadin/file-process.svg';
import vaadin__file_refresh from './vaadin/file-refresh.svg';
import vaadin__file_remove from './vaadin/file-remove.svg';
import vaadin__file_search from './vaadin/file-search.svg';
import vaadin__file_sound from './vaadin/file-sound.svg';
import vaadin__file_start from './vaadin/file-start.svg';
import vaadin__file_table from './vaadin/file-table.svg';
import vaadin__file_text_o from './vaadin/file-text-o.svg';
import vaadin__file_text from './vaadin/file-text.svg';
import vaadin__file_tree_small from './vaadin/file-tree-small.svg';
import vaadin__file_tree_sub from './vaadin/file-tree-sub.svg';
import vaadin__file_tree from './vaadin/file-tree.svg';
import vaadin__file_zip from './vaadin/file-zip.svg';
import vaadin__file from './vaadin/file.svg';
import vaadin__fill from './vaadin/fill.svg';
import vaadin__film from './vaadin/film.svg';
import vaadin__filter from './vaadin/filter.svg';
import vaadin__fire from './vaadin/fire.svg';
import vaadin__flag_checkered from './vaadin/flag-checkered.svg';
import vaadin__flag_o from './vaadin/flag-o.svg';
import vaadin__flag from './vaadin/flag.svg';
import vaadin__flash from './vaadin/flash.svg';
import vaadin__flask from './vaadin/flask.svg';
import vaadin__flight_landing from './vaadin/flight-landing.svg';
import vaadin__flight_takeoff from './vaadin/flight-takeoff.svg';
import vaadin__flip_h from './vaadin/flip-h.svg';
import vaadin__flip_v from './vaadin/flip-v.svg';
import vaadin__folder_add from './vaadin/folder-add.svg';
import vaadin__folder_o from './vaadin/folder-o.svg';
import vaadin__folder_open_o from './vaadin/folder-open-o.svg';
import vaadin__folder_open from './vaadin/folder-open.svg';
import vaadin__folder_remove from './vaadin/folder-remove.svg';
import vaadin__folder_search from './vaadin/folder-search.svg';
import vaadin__folder from './vaadin/folder.svg';
import vaadin__font from './vaadin/font.svg';
import vaadin__form from './vaadin/form.svg';
import vaadin__forward from './vaadin/forward.svg';
import vaadin__frown_o from './vaadin/frown-o.svg';
import vaadin__function from './vaadin/function.svg';
import vaadin__funnel from './vaadin/funnel.svg';
import vaadin__gamepad from './vaadin/gamepad.svg';
import vaadin__gavel from './vaadin/gavel.svg';
import vaadin__gift from './vaadin/gift.svg';
import vaadin__glass from './vaadin/glass.svg';
import vaadin__glasses from './vaadin/glasses.svg';
import vaadin__globe_wire from './vaadin/globe-wire.svg';
import vaadin__globe from './vaadin/globe.svg';
import vaadin__golf from './vaadin/golf.svg';
import vaadin__google_plus_square from './vaadin/google-plus-square.svg';
import vaadin__google_plus from './vaadin/google-plus.svg';
import vaadin__grab from './vaadin/grab.svg';
import vaadin__grid_bevel from './vaadin/grid-bevel.svg';
import vaadin__grid_big_o from './vaadin/grid-big-o.svg';
import vaadin__grid_big from './vaadin/grid-big.svg';
import vaadin__grid_h from './vaadin/grid-h.svg';
import vaadin__grid_small_o from './vaadin/grid-small-o.svg';
import vaadin__grid_small from './vaadin/grid-small.svg';
import vaadin__grid_v from './vaadin/grid-v.svg';
import vaadin__grid from './vaadin/grid.svg';
import vaadin__group from './vaadin/group.svg';
import vaadin__hammer from './vaadin/hammer.svg';
import vaadin__hand from './vaadin/hand.svg';
import vaadin__handle_corner from './vaadin/handle-corner.svg';
import vaadin__hands_up from './vaadin/hands-up.svg';
import vaadin__handshake from './vaadin/handshake.svg';
import vaadin__harddrive_o from './vaadin/harddrive-o.svg';
import vaadin__harddrive from './vaadin/harddrive.svg';
import vaadin__hash from './vaadin/hash.svg';
import vaadin__header from './vaadin/header.svg';
import vaadin__headphones from './vaadin/headphones.svg';
import vaadin__headset from './vaadin/headset.svg';
import vaadin__health_card from './vaadin/health-card.svg';
import vaadin__heart_o from './vaadin/heart-o.svg';
import vaadin__heart from './vaadin/heart.svg';
import vaadin__home_o from './vaadin/home-o.svg';
import vaadin__home from './vaadin/home.svg';
import vaadin__hospital from './vaadin/hospital.svg';
import vaadin__hourglass_empty from './vaadin/hourglass-empty.svg';
import vaadin__hourglass_end from './vaadin/hourglass-end.svg';
import vaadin__hourglass_start from './vaadin/hourglass-start.svg';
import vaadin__hourglass from './vaadin/hourglass.svg';
import vaadin__inbox from './vaadin/inbox.svg';
import vaadin__indent from './vaadin/indent.svg';
import vaadin__info_circle_o from './vaadin/info-circle-o.svg';
import vaadin__info_circle from './vaadin/info-circle.svg';
import vaadin__info from './vaadin/info.svg';
import vaadin__input from './vaadin/input.svg';
import vaadin__insert from './vaadin/insert.svg';
import vaadin__institution from './vaadin/institution.svg';
import vaadin__invoice from './vaadin/invoice.svg';
import vaadin__italic from './vaadin/italic.svg';
import vaadin__key_o from './vaadin/key-o.svg';
import vaadin__key from './vaadin/key.svg';
import vaadin__keyboard_o from './vaadin/keyboard-o.svg';
import vaadin__keyboard from './vaadin/keyboard.svg';
import vaadin__laptop from './vaadin/laptop.svg';
import vaadin__layout from './vaadin/layout.svg';
import vaadin__level_down_bold from './vaadin/level-down-bold.svg';
import vaadin__level_down from './vaadin/level-down.svg';
import vaadin__level_left_bold from './vaadin/level-left-bold.svg';
import vaadin__level_left from './vaadin/level-left.svg';
import vaadin__level_right_bold from './vaadin/level-right-bold.svg';
import vaadin__level_right from './vaadin/level-right.svg';
import vaadin__level_up_bold from './vaadin/level-up-bold.svg';
import vaadin__level_up from './vaadin/level-up.svg';
import vaadin__lifebuoy from './vaadin/lifebuoy.svg';
import vaadin__lightbulb from './vaadin/lightbulb.svg';
import vaadin__line_bar_chart from './vaadin/line-bar-chart.svg';
import vaadin__line_chart from './vaadin/line-chart.svg';
import vaadin__line_h from './vaadin/line-h.svg';
import vaadin__line_v from './vaadin/line-v.svg';
import vaadin__lines_list from './vaadin/lines-list.svg';
import vaadin__lines from './vaadin/lines.svg';
import vaadin__link from './vaadin/link.svg';
import vaadin__list_ol from './vaadin/list-ol.svg';
import vaadin__list_select from './vaadin/list-select.svg';
import vaadin__list_ul from './vaadin/list-ul.svg';
import vaadin__list from './vaadin/list.svg';
import vaadin__location_arrow_circle_o from './vaadin/location-arrow-circle-o.svg';
import vaadin__location_arrow_circle from './vaadin/location-arrow-circle.svg';
import vaadin__location_arrow from './vaadin/location-arrow.svg';
import vaadin__lock from './vaadin/lock.svg';
import vaadin__m_account from './vaadin/m-account.svg';
import vaadin__m_bank from './vaadin/m-bank.svg';
import vaadin__m_calculator_variant from './vaadin/m-calculator-variant.svg';
import vaadin__m_card_account_details from './vaadin/m-card-account-details.svg';
import vaadin__m_folder_open from './vaadin/m-folder-open.svg';
import vaadin__m_folder from './vaadin/m-folder.svg';
import vaadin__m_logout from './vaadin/m-logout.svg';
import vaadin__m_safe_square_outline from './vaadin/m-safe-square-outline.svg';
import vaadin__m_safe_square from './vaadin/m-safe-square.svg';
import vaadin__magic from './vaadin/magic.svg';
import vaadin__magnet from './vaadin/magnet.svg';
import vaadin__mailbox from './vaadin/mailbox.svg';
import vaadin__male from './vaadin/male.svg';
import vaadin__map_marker from './vaadin/map-marker.svg';
import vaadin__margin_bottom from './vaadin/margin-bottom.svg';
import vaadin__margin_left from './vaadin/margin-left.svg';
import vaadin__margin_right from './vaadin/margin-right.svg';
import vaadin__margin_top from './vaadin/margin-top.svg';
import vaadin__margin from './vaadin/margin.svg';
import vaadin__medal from './vaadin/medal.svg';
import vaadin__megaphone from './vaadin/megaphone.svg';
import vaadin__meh_o from './vaadin/meh-o.svg';
import vaadin__menu from './vaadin/menu.svg';
import vaadin__microphone from './vaadin/microphone.svg';
import vaadin__minus_circle_o from './vaadin/minus-circle-o.svg';
import vaadin__minus_circle from './vaadin/minus-circle.svg';
import vaadin__minus_square_o from './vaadin/minus-square-o.svg';
import vaadin__minus from './vaadin/minus.svg';
import vaadin__mobile_browser from './vaadin/mobile-browser.svg';
import vaadin__mobile_retro from './vaadin/mobile-retro.svg';
import vaadin__mobile from './vaadin/mobile.svg';
import vaadin__modal_list from './vaadin/modal-list.svg';
import vaadin__modal from './vaadin/modal.svg';
import vaadin__money_deposit from './vaadin/money-deposit.svg';
import vaadin__money_exchange from './vaadin/money-exchange.svg';
import vaadin__money_withdraw from './vaadin/money-withdraw.svg';
import vaadin__money from './vaadin/money.svg';
import vaadin__moon_o from './vaadin/moon-o.svg';
import vaadin__moon from './vaadin/moon.svg';
import vaadin__morning from './vaadin/morning.svg';
import vaadin__movie from './vaadin/movie.svg';
import vaadin__music from './vaadin/music.svg';
import vaadin__mute from './vaadin/mute.svg';
import vaadin__native_button from './vaadin/native-button.svg';
import vaadin__newspaper from './vaadin/newspaper.svg';
import vaadin__notebook from './vaadin/notebook.svg';
import vaadin__nurse from './vaadin/nurse.svg';
import vaadin__office from './vaadin/office.svg';
import vaadin__open_book from './vaadin/open-book.svg';
import vaadin__option_a from './vaadin/option-a.svg';
import vaadin__option from './vaadin/option.svg';
import vaadin__options from './vaadin/options.svg';
import vaadin__orientation from './vaadin/orientation.svg';
import vaadin__out from './vaadin/out.svg';
import vaadin__outbox from './vaadin/outbox.svg';
import vaadin__package from './vaadin/package.svg';
import vaadin__padding_bottom from './vaadin/padding-bottom.svg';
import vaadin__padding_left from './vaadin/padding-left.svg';
import vaadin__padding_right from './vaadin/padding-right.svg';
import vaadin__padding_top from './vaadin/padding-top.svg';
import vaadin__padding from './vaadin/padding.svg';
import vaadin__paint_roll from './vaadin/paint-roll.svg';
import vaadin__paintbrush from './vaadin/paintbrush.svg';
import vaadin__palette from './vaadin/palette.svg';
import vaadin__panel from './vaadin/panel.svg';
import vaadin__paperclip from './vaadin/paperclip.svg';
import vaadin__paperplane_o from './vaadin/paperplane-o.svg';
import vaadin__paperplane from './vaadin/paperplane.svg';
import vaadin__paragraph from './vaadin/paragraph.svg';
import vaadin__password from './vaadin/password.svg';
import vaadin__paste from './vaadin/paste.svg';
import vaadin__pause from './vaadin/pause.svg';
import vaadin__pencil from './vaadin/pencil.svg';
import vaadin__phone_landline from './vaadin/phone-landline.svg';
import vaadin__phone from './vaadin/phone.svg';
import vaadin__picture from './vaadin/picture.svg';
import vaadin__pie_bar_chart from './vaadin/pie-bar-chart.svg';
import vaadin__pie_chart from './vaadin/pie-chart.svg';
import vaadin__piggy_bank_coin from './vaadin/piggy-bank-coin.svg';
import vaadin__piggy_bank from './vaadin/piggy-bank.svg';
import vaadin__pill from './vaadin/pill.svg';
import vaadin__pills from './vaadin/pills.svg';
import vaadin__pin_post from './vaadin/pin-post.svg';
import vaadin__pin from './vaadin/pin.svg';
import vaadin__play_circle_o from './vaadin/play-circle-o.svg';
import vaadin__play_circle from './vaadin/play-circle.svg';
import vaadin__play from './vaadin/play.svg';
import vaadin__plug from './vaadin/plug.svg';
import vaadin__plus_circle_o from './vaadin/plus-circle-o.svg';
import vaadin__plus_circle from './vaadin/plus-circle.svg';
import vaadin__plus_minus from './vaadin/plus-minus.svg';
import vaadin__plus_square_o from './vaadin/plus-square-o.svg';
import vaadin__plus from './vaadin/plus.svg';
import vaadin__pointer from './vaadin/pointer.svg';
import vaadin__power_off from './vaadin/power-off.svg';
import vaadin__presentation from './vaadin/presentation.svg';
import vaadin__print from './vaadin/print.svg';
import vaadin__progressbar from './vaadin/progressbar.svg';
import vaadin__puzzle_piece from './vaadin/puzzle-piece.svg';
import vaadin__pyramid_chart from './vaadin/pyramid-chart.svg';
import vaadin__qrcode from './vaadin/qrcode.svg';
import vaadin__question_circle_o from './vaadin/question-circle-o.svg';
import vaadin__question_circle from './vaadin/question-circle.svg';
import vaadin__question from './vaadin/question.svg';
import vaadin__quote_left from './vaadin/quote-left.svg';
import vaadin__quote_right from './vaadin/quote-right.svg';
import vaadin__random from './vaadin/random.svg';
import vaadin__raster_lower_left from './vaadin/raster-lower-left.svg';
import vaadin__raster from './vaadin/raster.svg';
import vaadin__records from './vaadin/records.svg';
import vaadin__recycle from './vaadin/recycle.svg';
import vaadin__refresh from './vaadin/refresh.svg';
import vaadin__reply_all from './vaadin/reply-all.svg';
import vaadin__reply from './vaadin/reply.svg';
import vaadin__resize_h from './vaadin/resize-h.svg';
import vaadin__resize_v from './vaadin/resize-v.svg';
import vaadin__retweet from './vaadin/retweet.svg';
import vaadin__rhombus from './vaadin/rhombus.svg';
import vaadin__road_branch from './vaadin/road-branch.svg';
import vaadin__road_branches from './vaadin/road-branches.svg';
import vaadin__road_split from './vaadin/road-split.svg';
import vaadin__road from './vaadin/road.svg';
import vaadin__rocket from './vaadin/rocket.svg';
import vaadin__rotate_left from './vaadin/rotate-left.svg';
import vaadin__rotate_right from './vaadin/rotate-right.svg';
import vaadin__rss_square from './vaadin/rss-square.svg';
import vaadin__rss from './vaadin/rss.svg';
import vaadin__safe_lock from './vaadin/safe-lock.svg';
import vaadin__safe from './vaadin/safe.svg';
import vaadin__scale_unbalance from './vaadin/scale-unbalance.svg';
import vaadin__scale from './vaadin/scale.svg';
import vaadin__scatter_chart from './vaadin/scatter-chart.svg';
import vaadin__scissors from './vaadin/scissors.svg';
import vaadin__screwdriver from './vaadin/screwdriver.svg';
import vaadin__search_minus from './vaadin/search-minus.svg';
import vaadin__search_plus from './vaadin/search-plus.svg';
import vaadin__search from './vaadin/search.svg';
import vaadin__select from './vaadin/select.svg';
import vaadin__server from './vaadin/server.svg';
import vaadin__share_square from './vaadin/share-square.svg';
import vaadin__share from './vaadin/share.svg';
import vaadin__shield from './vaadin/shield.svg';
import vaadin__shift_arrow from './vaadin/shift-arrow.svg';
import vaadin__shift from './vaadin/shift.svg';
import vaadin__shop from './vaadin/shop.svg';
import vaadin__sign_in_alt from './vaadin/sign-in-alt.svg';
import vaadin__sign_in from './vaadin/sign-in.svg';
import vaadin__sign_out_alt from './vaadin/sign-out-alt.svg';
import vaadin__sign_out from './vaadin/sign-out.svg';
import vaadin__signal from './vaadin/signal.svg';
import vaadin__sitemap from './vaadin/sitemap.svg';
import vaadin__slider from './vaadin/slider.svg';
import vaadin__sliders from './vaadin/sliders.svg';
import vaadin__smiley_o from './vaadin/smiley-o.svg';
import vaadin__sort from './vaadin/sort.svg';
import vaadin__sound_disable from './vaadin/sound-disable.svg';
import vaadin__spark_line from './vaadin/spark-line.svg';
import vaadin__specialist from './vaadin/specialist.svg';
import vaadin__spinner_arc from './vaadin/spinner-arc.svg';
import vaadin__spinner_third from './vaadin/spinner-third.svg';
import vaadin__spinner from './vaadin/spinner.svg';
import vaadin__spline_area_chart from './vaadin/spline-area-chart.svg';
import vaadin__spline_chart from './vaadin/spline-chart.svg';
import vaadin__split_h from './vaadin/split-h.svg';
import vaadin__split_v from './vaadin/split-v.svg';
import vaadin__split from './vaadin/split.svg';
import vaadin__spoon from './vaadin/spoon.svg';
import vaadin__square_shadow from './vaadin/square-shadow.svg';
import vaadin__star_half_left_o from './vaadin/star-half-left-o.svg';
import vaadin__star_half_left from './vaadin/star-half-left.svg';
import vaadin__star_half_right_o from './vaadin/star-half-right-o.svg';
import vaadin__star_half_right from './vaadin/star-half-right.svg';
import vaadin__star_o from './vaadin/star-o.svg';
import vaadin__star from './vaadin/star.svg';
import vaadin__start_cog from './vaadin/start-cog.svg';
import vaadin__step_backward from './vaadin/step-backward.svg';
import vaadin__step_forward from './vaadin/step-forward.svg';
import vaadin__stethoscope from './vaadin/stethoscope.svg';
import vaadin__stock from './vaadin/stock.svg';
import vaadin__stop_cog from './vaadin/stop-cog.svg';
import vaadin__stop from './vaadin/stop.svg';
import vaadin__stopwatch from './vaadin/stopwatch.svg';
import vaadin__storage from './vaadin/storage.svg';
import vaadin__strikethrough from './vaadin/strikethrough.svg';
import vaadin__subscript from './vaadin/subscript.svg';
import vaadin__suitcase from './vaadin/suitcase.svg';
import vaadin__sun_down from './vaadin/sun-down.svg';
import vaadin__sun_o from './vaadin/sun-o.svg';
import vaadin__sun_rise from './vaadin/sun-rise.svg';
import vaadin__superscript from './vaadin/superscript.svg';
import vaadin__sword from './vaadin/sword.svg';
import vaadin__tab_a from './vaadin/tab-a.svg';
import vaadin__tab from './vaadin/tab.svg';
import vaadin__table from './vaadin/table.svg';
import vaadin__tablet from './vaadin/tablet.svg';
import vaadin__tabs from './vaadin/tabs.svg';
import vaadin__tag from './vaadin/tag.svg';
import vaadin__tags from './vaadin/tags.svg';
import vaadin__tasks from './vaadin/tasks.svg';
import vaadin__taxi from './vaadin/taxi.svg';
import vaadin__teeth from './vaadin/teeth.svg';
import vaadin__terminal from './vaadin/terminal.svg';
import vaadin__text_height from './vaadin/text-height.svg';
import vaadin__text_input from './vaadin/text-input.svg';
import vaadin__text_label from './vaadin/text-label.svg';
import vaadin__text_width from './vaadin/text-width.svg';
import vaadin__thin_square from './vaadin/thin-square.svg';
import vaadin__thumbs_down_o from './vaadin/thumbs-down-o.svg';
import vaadin__thumbs_down from './vaadin/thumbs-down.svg';
import vaadin__thumbs_up_o from './vaadin/thumbs-up-o.svg';
import vaadin__thumbs_up from './vaadin/thumbs-up.svg';
import vaadin__ticket from './vaadin/ticket.svg';
import vaadin__time_backward from './vaadin/time-backward.svg';
import vaadin__time_forward from './vaadin/time-forward.svg';
import vaadin__timer from './vaadin/timer.svg';
import vaadin__toolbox from './vaadin/toolbox.svg';
import vaadin__tools from './vaadin/tools.svg';
import vaadin__tooth from './vaadin/tooth.svg';
import vaadin__touch from './vaadin/touch.svg';
import vaadin__train from './vaadin/train.svg';
import vaadin__trash from './vaadin/trash.svg';
import vaadin__tree_table from './vaadin/tree-table.svg';
import vaadin__trending_down from './vaadin/trending-down.svg';
import vaadin__trending_up from './vaadin/trending-up.svg';
import vaadin__trophy from './vaadin/trophy.svg';
import vaadin__truck from './vaadin/truck.svg';
import vaadin__twin_col_select from './vaadin/twin-col-select.svg';
import vaadin__twitter_square from './vaadin/twitter-square.svg';
import vaadin__twitter from './vaadin/twitter.svg';
import vaadin__umbrella from './vaadin/umbrella.svg';
import vaadin__underline from './vaadin/underline.svg';
import vaadin__unlink from './vaadin/unlink.svg';
import vaadin__unlock from './vaadin/unlock.svg';
import vaadin__upload_alt from './vaadin/upload-alt.svg';
import vaadin__upload from './vaadin/upload.svg';
import vaadin__user_card from './vaadin/user-card.svg';
import vaadin__user_check from './vaadin/user-check.svg';
import vaadin__user_clock from './vaadin/user-clock.svg';
import vaadin__user_heart from './vaadin/user-heart.svg';
import vaadin__user_star from './vaadin/user-star.svg';
import vaadin__user from './vaadin/user.svg';
import vaadin__users from './vaadin/users.svg';
import vaadin__vaadin_h from './vaadin/vaadin-h.svg';
import vaadin__vaadin_v from './vaadin/vaadin-v.svg';
import vaadin__viewport from './vaadin/viewport.svg';
import vaadin__vimeo_square from './vaadin/vimeo-square.svg';
import vaadin__vimeo from './vaadin/vimeo.svg';
import vaadin__volume_down from './vaadin/volume-down.svg';
import vaadin__volume_off from './vaadin/volume-off.svg';
import vaadin__volume_up from './vaadin/volume-up.svg';
import vaadin__volume from './vaadin/volume.svg';
import vaadin__wallet from './vaadin/wallet.svg';
import vaadin__warning from './vaadin/warning.svg';
import vaadin__workplace from './vaadin/workplace.svg';
import vaadin__wrench from './vaadin/wrench.svg';
import vaadin__youtube_square from './vaadin/youtube-square.svg';
import vaadin__youtube from './vaadin/youtube.svg';
  
export {
  fa__0
  ,fa__1
  ,fa__2
  ,fa__3
  ,fa__4
  ,fa__5
  ,fa__6
  ,fa__7
  ,fa__8
  ,fa__9
  ,fa__a
  ,fa__address_book
  ,fa__address_card
  ,fa__align_center
  ,fa__align_justify
  ,fa__align_left
  ,fa__align_right
  ,fa__anchor_circle_check
  ,fa__anchor_circle_exclamation
  ,fa__anchor_circle_xmark
  ,fa__anchor_lock
  ,fa__anchor
  ,fa__angle_down
  ,fa__angle_left
  ,fa__angle_right
  ,fa__angle_up
  ,fa__angles_down
  ,fa__angles_left
  ,fa__angles_right
  ,fa__angles_up
  ,fa__ankh
  ,fa__apple_whole
  ,fa__archway
  ,fa__arrow_down_1_9
  ,fa__arrow_down_9_1
  ,fa__arrow_down_a_z
  ,fa__arrow_down_long
  ,fa__arrow_down_short_wide
  ,fa__arrow_down_up_across_line
  ,fa__arrow_down_up_lock
  ,fa__arrow_down_wide_short
  ,fa__arrow_down_z_a
  ,fa__arrow_down
  ,fa__arrow_left_long
  ,fa__arrow_left
  ,fa__arrow_pointer
  ,fa__arrow_right_arrow_left
  ,fa__arrow_right_from_bracket
  ,fa__arrow_right_long
  ,fa__arrow_right_to_bracket
  ,fa__arrow_right_to_city
  ,fa__arrow_right
  ,fa__arrow_rotate_left
  ,fa__arrow_rotate_right
  ,fa__arrow_trend_down
  ,fa__arrow_trend_up
  ,fa__arrow_turn_down
  ,fa__arrow_turn_up
  ,fa__arrow_up_1_9
  ,fa__arrow_up_9_1
  ,fa__arrow_up_a_z
  ,fa__arrow_up_from_bracket
  ,fa__arrow_up_from_ground_water
  ,fa__arrow_up_from_water_pump
  ,fa__arrow_up_long
  ,fa__arrow_up_right_dots
  ,fa__arrow_up_right_from_square
  ,fa__arrow_up_short_wide
  ,fa__arrow_up_wide_short
  ,fa__arrow_up_z_a
  ,fa__arrow_up
  ,fa__arrows_down_to_line
  ,fa__arrows_down_to_people
  ,fa__arrows_left_right_to_line
  ,fa__arrows_left_right
  ,fa__arrows_rotate
  ,fa__arrows_spin
  ,fa__arrows_split_up_and_left
  ,fa__arrows_to_circle
  ,fa__arrows_to_dot
  ,fa__arrows_to_eye
  ,fa__arrows_turn_right
  ,fa__arrows_turn_to_dots
  ,fa__arrows_up_down_left_right
  ,fa__arrows_up_down
  ,fa__arrows_up_to_line
  ,fa__asterisk
  ,fa__at
  ,fa__atom
  ,fa__audio_description
  ,fa__austral_sign
  ,fa__award
  ,fa__b
  ,fa__baby_carriage
  ,fa__baby
  ,fa__backward_fast
  ,fa__backward_step
  ,fa__backward
  ,fa__bacon
  ,fa__bacteria
  ,fa__bacterium
  ,fa__bag_shopping
  ,fa__bahai
  ,fa__baht_sign
  ,fa__ban_smoking
  ,fa__ban
  ,fa__bandage
  ,fa__bangladeshi_taka_sign
  ,fa__barcode
  ,fa__bars_progress
  ,fa__bars_staggered
  ,fa__bars
  ,fa__baseball_bat_ball
  ,fa__baseball
  ,fa__basket_shopping
  ,fa__basketball
  ,fa__bath
  ,fa__battery_empty
  ,fa__battery_full
  ,fa__battery_half
  ,fa__battery_quarter
  ,fa__battery_three_quarters
  ,fa__bed_pulse
  ,fa__bed
  ,fa__beer_mug_empty
  ,fa__bell_concierge
  ,fa__bell_slash
  ,fa__bell
  ,fa__bezier_curve
  ,fa__bicycle
  ,fa__binoculars
  ,fa__biohazard
  ,fa__bitcoin_sign
  ,fa__blender_phone
  ,fa__blender
  ,fa__blog
  ,fa__bold
  ,fa__bolt_lightning
  ,fa__bolt
  ,fa__bomb
  ,fa__bone
  ,fa__bong
  ,fa__book_atlas
  ,fa__book_bible
  ,fa__book_bookmark
  ,fa__book_journal_whills
  ,fa__book_medical
  ,fa__book_open_reader
  ,fa__book_open
  ,fa__book_quran
  ,fa__book_skull
  ,fa__book_tanakh
  ,fa__book
  ,fa__bookmark
  ,fa__border_all
  ,fa__border_none
  ,fa__border_top_left
  ,fa__bore_hole
  ,fa__bottle_droplet
  ,fa__bottle_water
  ,fa__bowl_food
  ,fa__bowl_rice
  ,fa__bowling_ball
  ,fa__box_archive
  ,fa__box_open
  ,fa__box_tissue
  ,fa__box
  ,fa__boxes_packing
  ,fa__boxes_stacked
  ,fa__braille
  ,fa__brain
  ,fa__brazilian_real_sign
  ,fa__bread_slice
  ,fa__bridge_circle_check
  ,fa__bridge_circle_exclamation
  ,fa__bridge_circle_xmark
  ,fa__bridge_lock
  ,fa__bridge_water
  ,fa__bridge
  ,fa__briefcase_medical
  ,fa__briefcase
  ,fa__broom_ball
  ,fa__broom
  ,fa__brush
  ,fa__bucket
  ,fa__bug_slash
  ,fa__bug
  ,fa__bugs
  ,fa__building_circle_arrow_right
  ,fa__building_circle_check
  ,fa__building_circle_exclamation
  ,fa__building_circle_xmark
  ,fa__building_columns
  ,fa__building_flag
  ,fa__building_lock
  ,fa__building_ngo
  ,fa__building_shield
  ,fa__building_un
  ,fa__building_user
  ,fa__building_wheat
  ,fa__building
  ,fa__bullhorn
  ,fa__bullseye
  ,fa__burger
  ,fa__burst
  ,fa__bus_simple
  ,fa__bus
  ,fa__business_time
  ,fa__c
  ,fa__cable_car
  ,fa__cake_candles
  ,fa__calculator
  ,fa__calendar_check
  ,fa__calendar_day
  ,fa__calendar_days
  ,fa__calendar_minus
  ,fa__calendar_plus
  ,fa__calendar_week
  ,fa__calendar_xmark
  ,fa__calendar
  ,fa__camera_retro
  ,fa__camera_rotate
  ,fa__camera
  ,fa__campground
  ,fa__candy_cane
  ,fa__cannabis
  ,fa__capsules
  ,fa__car_battery
  ,fa__car_burst
  ,fa__car_on
  ,fa__car_rear
  ,fa__car_side
  ,fa__car_tunnel
  ,fa__car
  ,fa__caravan
  ,fa__caret_down
  ,fa__caret_left
  ,fa__caret_right
  ,fa__caret_up
  ,fa__carrot
  ,fa__cart_arrow_down
  ,fa__cart_flatbed_suitcase
  ,fa__cart_flatbed
  ,fa__cart_plus
  ,fa__cart_shopping
  ,fa__cash_register
  ,fa__cat
  ,fa__cedi_sign
  ,fa__cent_sign
  ,fa__certificate
  ,fa__chair
  ,fa__chalkboard_user
  ,fa__chalkboard
  ,fa__champagne_glasses
  ,fa__charging_station
  ,fa__chart_area
  ,fa__chart_bar
  ,fa__chart_column
  ,fa__chart_gantt
  ,fa__chart_line
  ,fa__chart_pie
  ,fa__chart_simple
  ,fa__check_double
  ,fa__check_to_slot
  ,fa__check
  ,fa__cheese
  ,fa__chess_bishop
  ,fa__chess_board
  ,fa__chess_king
  ,fa__chess_knight
  ,fa__chess_pawn
  ,fa__chess_queen
  ,fa__chess_rook
  ,fa__chess
  ,fa__chevron_down
  ,fa__chevron_left
  ,fa__chevron_right
  ,fa__chevron_up
  ,fa__child_combatant
  ,fa__child_dress
  ,fa__child_reaching
  ,fa__child
  ,fa__children
  ,fa__church
  ,fa__circle_arrow_down
  ,fa__circle_arrow_left
  ,fa__circle_arrow_right
  ,fa__circle_arrow_up
  ,fa__circle_check
  ,fa__circle_chevron_down
  ,fa__circle_chevron_left
  ,fa__circle_chevron_right
  ,fa__circle_chevron_up
  ,fa__circle_dollar_to_slot
  ,fa__circle_dot
  ,fa__circle_down
  ,fa__circle_exclamation
  ,fa__circle_h
  ,fa__circle_half_stroke
  ,fa__circle_info
  ,fa__circle_left
  ,fa__circle_minus
  ,fa__circle_nodes
  ,fa__circle_notch
  ,fa__circle_pause
  ,fa__circle_play
  ,fa__circle_plus
  ,fa__circle_question
  ,fa__circle_radiation
  ,fa__circle_right
  ,fa__circle_stop
  ,fa__circle_up
  ,fa__circle_user
  ,fa__circle_xmark
  ,fa__circle
  ,fa__city
  ,fa__clapperboard
  ,fa__clipboard_check
  ,fa__clipboard_list
  ,fa__clipboard_question
  ,fa__clipboard_user
  ,fa__clipboard
  ,fa__clock_rotate_left
  ,fa__clock
  ,fa__clone
  ,fa__closed_captioning
  ,fa__cloud_arrow_down
  ,fa__cloud_arrow_up
  ,fa__cloud_bolt
  ,fa__cloud_meatball
  ,fa__cloud_moon_rain
  ,fa__cloud_moon
  ,fa__cloud_rain
  ,fa__cloud_showers_heavy
  ,fa__cloud_showers_water
  ,fa__cloud_sun_rain
  ,fa__cloud_sun
  ,fa__cloud
  ,fa__clover
  ,fa__code_branch
  ,fa__code_commit
  ,fa__code_compare
  ,fa__code_fork
  ,fa__code_merge
  ,fa__code_pull_request
  ,fa__code
  ,fa__coins
  ,fa__colon_sign
  ,fa__comment_dollar
  ,fa__comment_dots
  ,fa__comment_medical
  ,fa__comment_slash
  ,fa__comment_sms
  ,fa__comment
  ,fa__comments_dollar
  ,fa__comments
  ,fa__compact_disc
  ,fa__compass_drafting
  ,fa__compass
  ,fa__compress
  ,fa__computer_mouse
  ,fa__computer
  ,fa__cookie_bite
  ,fa__cookie
  ,fa__copy
  ,fa__copyright
  ,fa__couch
  ,fa__cow
  ,fa__credit_card
  ,fa__crop_simple
  ,fa__crop
  ,fa__cross
  ,fa__crosshairs
  ,fa__crow
  ,fa__crown
  ,fa__crutch
  ,fa__cruzeiro_sign
  ,fa__cube
  ,fa__cubes_stacked
  ,fa__cubes
  ,fa__d
  ,fa__database
  ,fa__delete_left
  ,fa__democrat
  ,fa__desktop
  ,fa__dharmachakra
  ,fa__diagram_next
  ,fa__diagram_predecessor
  ,fa__diagram_project
  ,fa__diagram_successor
  ,fa__diamond_turn_right
  ,fa__diamond
  ,fa__dice_d20
  ,fa__dice_d6
  ,fa__dice_five
  ,fa__dice_four
  ,fa__dice_one
  ,fa__dice_six
  ,fa__dice_three
  ,fa__dice_two
  ,fa__dice
  ,fa__disease
  ,fa__display
  ,fa__divide
  ,fa__dna
  ,fa__dog
  ,fa__dollar_sign
  ,fa__dolly
  ,fa__dong_sign
  ,fa__door_closed
  ,fa__door_open
  ,fa__dove
  ,fa__down_left_and_up_right_to_center
  ,fa__down_long
  ,fa__download
  ,fa__dragon
  ,fa__draw_polygon
  ,fa__droplet_slash
  ,fa__droplet
  ,fa__drum_steelpan
  ,fa__drum
  ,fa__drumstick_bite
  ,fa__dumbbell
  ,fa__dumpster_fire
  ,fa__dumpster
  ,fa__dungeon
  ,fa__e
  ,fa__ear_deaf
  ,fa__ear_listen
  ,fa__earth_africa
  ,fa__earth_americas
  ,fa__earth_asia
  ,fa__earth_europe
  ,fa__earth_oceania
  ,fa__egg
  ,fa__eject
  ,fa__elevator
  ,fa__ellipsis_vertical
  ,fa__ellipsis
  ,fa__envelope_circle_check
  ,fa__envelope_open_text
  ,fa__envelope_open
  ,fa__envelope
  ,fa__envelopes_bulk
  ,fa__equals
  ,fa__eraser
  ,fa__ethernet
  ,fa__euro_sign
  ,fa__exclamation
  ,fa__expand
  ,fa__explosion
  ,fa__eye_dropper
  ,fa__eye_low_vision
  ,fa__eye_slash
  ,fa__eye
  ,fa__f
  ,fa__face_angry
  ,fa__face_dizzy
  ,fa__face_flushed
  ,fa__face_frown_open
  ,fa__face_frown
  ,fa__face_grimace
  ,fa__face_grin_beam_sweat
  ,fa__face_grin_beam
  ,fa__face_grin_hearts
  ,fa__face_grin_squint_tears
  ,fa__face_grin_squint
  ,fa__face_grin_stars
  ,fa__face_grin_tears
  ,fa__face_grin_tongue_squint
  ,fa__face_grin_tongue_wink
  ,fa__face_grin_tongue
  ,fa__face_grin_wide
  ,fa__face_grin_wink
  ,fa__face_grin
  ,fa__face_kiss_beam
  ,fa__face_kiss_wink_heart
  ,fa__face_kiss
  ,fa__face_laugh_beam
  ,fa__face_laugh_squint
  ,fa__face_laugh_wink
  ,fa__face_laugh
  ,fa__face_meh_blank
  ,fa__face_meh
  ,fa__face_rolling_eyes
  ,fa__face_sad_cry
  ,fa__face_sad_tear
  ,fa__face_smile_beam
  ,fa__face_smile_wink
  ,fa__face_smile
  ,fa__face_surprise
  ,fa__face_tired
  ,fa__fan
  ,fa__faucet_drip
  ,fa__faucet
  ,fa__fax
  ,fa__feather_pointed
  ,fa__feather
  ,fa__ferry
  ,fa__file_arrow_down
  ,fa__file_arrow_up
  ,fa__file_audio
  ,fa__file_circle_check
  ,fa__file_circle_exclamation
  ,fa__file_circle_minus
  ,fa__file_circle_plus
  ,fa__file_circle_question
  ,fa__file_circle_xmark
  ,fa__file_code
  ,fa__file_contract
  ,fa__file_csv
  ,fa__file_excel
  ,fa__file_export
  ,fa__file_image
  ,fa__file_import
  ,fa__file_invoice_dollar
  ,fa__file_invoice
  ,fa__file_lines
  ,fa__file_medical
  ,fa__file_pdf
  ,fa__file_pen
  ,fa__file_powerpoint
  ,fa__file_prescription
  ,fa__file_shield
  ,fa__file_signature
  ,fa__file_video
  ,fa__file_waveform
  ,fa__file_word
  ,fa__file_zipper
  ,fa__file
  ,fa__fill_drip
  ,fa__fill
  ,fa__film
  ,fa__filter_circle_dollar
  ,fa__filter_circle_xmark
  ,fa__filter
  ,fa__fingerprint
  ,fa__fire_burner
  ,fa__fire_extinguisher
  ,fa__fire_flame_curved
  ,fa__fire_flame_simple
  ,fa__fire
  ,fa__fish_fins
  ,fa__fish
  ,fa__flag_checkered
  ,fa__flag_usa
  ,fa__flag
  ,fa__flask_vial
  ,fa__flask
  ,fa__floppy_disk
  ,fa__florin_sign
  ,fa__folder_closed
  ,fa__folder_minus
  ,fa__folder_open
  ,fa__folder_plus
  ,fa__folder_tree
  ,fa__folder
  ,fa__font_awesome
  ,fa__font
  ,fa__football
  ,fa__forward_fast
  ,fa__forward_step
  ,fa__forward
  ,fa__franc_sign
  ,fa__frog
  ,fa__futbol
  ,fa__g
  ,fa__gamepad
  ,fa__gas_pump
  ,fa__gauge_high
  ,fa__gauge_simple_high
  ,fa__gauge_simple
  ,fa__gauge
  ,fa__gavel
  ,fa__gear
  ,fa__gears
  ,fa__gem
  ,fa__genderless
  ,fa__ghost
  ,fa__gift
  ,fa__gifts
  ,fa__glass_water_droplet
  ,fa__glass_water
  ,fa__glasses
  ,fa__globe
  ,fa__golf_ball_tee
  ,fa__gopuram
  ,fa__graduation_cap
  ,fa__greater_than_equal
  ,fa__greater_than
  ,fa__grip_lines_vertical
  ,fa__grip_lines
  ,fa__grip_vertical
  ,fa__grip
  ,fa__group_arrows_rotate
  ,fa__guarani_sign
  ,fa__guitar
  ,fa__gun
  ,fa__h
  ,fa__hammer
  ,fa__hamsa
  ,fa__hand_back_fist
  ,fa__hand_dots
  ,fa__hand_fist
  ,fa__hand_holding_dollar
  ,fa__hand_holding_droplet
  ,fa__hand_holding_hand
  ,fa__hand_holding_heart
  ,fa__hand_holding_medical
  ,fa__hand_holding
  ,fa__hand_lizard
  ,fa__hand_middle_finger
  ,fa__hand_peace
  ,fa__hand_point_down
  ,fa__hand_point_left
  ,fa__hand_point_right
  ,fa__hand_point_up
  ,fa__hand_pointer
  ,fa__hand_scissors
  ,fa__hand_sparkles
  ,fa__hand_spock
  ,fa__hand
  ,fa__handcuffs
  ,fa__hands_asl_interpreting
  ,fa__hands_bound
  ,fa__hands_bubbles
  ,fa__hands_clapping
  ,fa__hands_holding_child
  ,fa__hands_holding_circle
  ,fa__hands_holding
  ,fa__hands_praying
  ,fa__hands
  ,fa__handshake_angle
  ,fa__handshake_simple_slash
  ,fa__handshake_simple
  ,fa__handshake_slash
  ,fa__handshake
  ,fa__hanukiah
  ,fa__hard_drive
  ,fa__hashtag
  ,fa__hat_cowboy_side
  ,fa__hat_cowboy
  ,fa__hat_wizard
  ,fa__head_side_cough_slash
  ,fa__head_side_cough
  ,fa__head_side_mask
  ,fa__head_side_virus
  ,fa__heading
  ,fa__headphones_simple
  ,fa__headphones
  ,fa__headset
  ,fa__heart_circle_bolt
  ,fa__heart_circle_check
  ,fa__heart_circle_exclamation
  ,fa__heart_circle_minus
  ,fa__heart_circle_plus
  ,fa__heart_circle_xmark
  ,fa__heart_crack
  ,fa__heart_pulse
  ,fa__heart
  ,fa__helicopter_symbol
  ,fa__helicopter
  ,fa__helmet_safety
  ,fa__helmet_un
  ,fa__highlighter
  ,fa__hill_avalanche
  ,fa__hill_rockslide
  ,fa__hippo
  ,fa__hockey_puck
  ,fa__holly_berry
  ,fa__horse_head
  ,fa__horse
  ,fa__hospital_user
  ,fa__hospital
  ,fa__hot_tub_person
  ,fa__hotdog
  ,fa__hotel
  ,fa__hourglass_end
  ,fa__hourglass_half
  ,fa__hourglass_start
  ,fa__hourglass
  ,fa__house_chimney_crack
  ,fa__house_chimney_medical
  ,fa__house_chimney_user
  ,fa__house_chimney_window
  ,fa__house_chimney
  ,fa__house_circle_check
  ,fa__house_circle_exclamation
  ,fa__house_circle_xmark
  ,fa__house_crack
  ,fa__house_fire
  ,fa__house_flag
  ,fa__house_flood_water_circle_arrow_right
  ,fa__house_flood_water
  ,fa__house_laptop
  ,fa__house_lock
  ,fa__house_medical_circle_check
  ,fa__house_medical_circle_exclamation
  ,fa__house_medical_circle_xmark
  ,fa__house_medical_flag
  ,fa__house_medical
  ,fa__house_signal
  ,fa__house_tsunami
  ,fa__house_user
  ,fa__house
  ,fa__hryvnia_sign
  ,fa__hurricane
  ,fa__i_cursor
  ,fa__i
  ,fa__ice_cream
  ,fa__icicles
  ,fa__icons
  ,fa__id_badge
  ,fa__id_card_clip
  ,fa__id_card
  ,fa__igloo
  ,fa__image_portrait
  ,fa__image
  ,fa__images
  ,fa__inbox
  ,fa__indent
  ,fa__indian_rupee_sign
  ,fa__industry
  ,fa__infinity
  ,fa__info
  ,fa__italic
  ,fa__j
  ,fa__jar_wheat
  ,fa__jar
  ,fa__jedi
  ,fa__jet_fighter_up
  ,fa__jet_fighter
  ,fa__joint
  ,fa__jug_detergent
  ,fa__k
  ,fa__kaaba
  ,fa__key
  ,fa__keyboard
  ,fa__khanda
  ,fa__kip_sign
  ,fa__kit_medical
  ,fa__kitchen_set
  ,fa__kiwi_bird
  ,fa__l
  ,fa__land_mine_on
  ,fa__landmark_dome
  ,fa__landmark_flag
  ,fa__landmark
  ,fa__language
  ,fa__laptop_code
  ,fa__laptop_file
  ,fa__laptop_medical
  ,fa__laptop
  ,fa__lari_sign
  ,fa__layer_group
  ,fa__leaf
  ,fa__left_long
  ,fa__left_right
  ,fa__lemon
  ,fa__less_than_equal
  ,fa__less_than
  ,fa__life_ring
  ,fa__lightbulb
  ,fa__lines_leaning
  ,fa__link_slash
  ,fa__link
  ,fa__lira_sign
  ,fa__list_check
  ,fa__list_ol
  ,fa__list_ul
  ,fa__list
  ,fa__litecoin_sign
  ,fa__location_arrow
  ,fa__location_crosshairs
  ,fa__location_dot
  ,fa__location_pin_lock
  ,fa__location_pin
  ,fa__lock_open
  ,fa__lock
  ,fa__locust
  ,fa__lungs_virus
  ,fa__lungs
  ,fa__m
  ,fa__magnet
  ,fa__magnifying_glass_arrow_right
  ,fa__magnifying_glass_chart
  ,fa__magnifying_glass_dollar
  ,fa__magnifying_glass_location
  ,fa__magnifying_glass_minus
  ,fa__magnifying_glass_plus
  ,fa__magnifying_glass
  ,fa__manat_sign
  ,fa__map_location_dot
  ,fa__map_location
  ,fa__map_pin
  ,fa__map
  ,fa__marker
  ,fa__mars_and_venus_burst
  ,fa__mars_and_venus
  ,fa__mars_double
  ,fa__mars_stroke_right
  ,fa__mars_stroke_up
  ,fa__mars_stroke
  ,fa__mars
  ,fa__martini_glass_citrus
  ,fa__martini_glass_empty
  ,fa__martini_glass
  ,fa__mask_face
  ,fa__mask_ventilator
  ,fa__mask
  ,fa__masks_theater
  ,fa__mattress_pillow
  ,fa__maximize
  ,fa__medal
  ,fa__memory
  ,fa__menorah
  ,fa__mercury
  ,fa__message
  ,fa__meteor
  ,fa__microchip
  ,fa__microphone_lines_slash
  ,fa__microphone_lines
  ,fa__microphone_slash
  ,fa__microphone
  ,fa__microscope
  ,fa__mill_sign
  ,fa__minimize
  ,fa__minus
  ,fa__mitten
  ,fa__mobile_button
  ,fa__mobile_retro
  ,fa__mobile_screen_button
  ,fa__mobile_screen
  ,fa__mobile
  ,fa__money_bill_1_wave
  ,fa__money_bill_1
  ,fa__money_bill_transfer
  ,fa__money_bill_trend_up
  ,fa__money_bill_wave
  ,fa__money_bill_wheat
  ,fa__money_bill
  ,fa__money_bills
  ,fa__money_check_dollar
  ,fa__money_check
  ,fa__monument
  ,fa__moon
  ,fa__mortar_pestle
  ,fa__mosque
  ,fa__mosquito_net
  ,fa__mosquito
  ,fa__motorcycle
  ,fa__mound
  ,fa__mountain_city
  ,fa__mountain_sun
  ,fa__mountain
  ,fa__mug_hot
  ,fa__mug_saucer
  ,fa__music
  ,fa__n
  ,fa__naira_sign
  ,fa__network_wired
  ,fa__neuter
  ,fa__newspaper
  ,fa__not_equal
  ,fa__notdef
  ,fa__note_sticky
  ,fa__notes_medical
  ,fa__o
  ,fa__object_group
  ,fa__object_ungroup
  ,fa__oil_can
  ,fa__oil_well
  ,fa__om
  ,fa__otter
  ,fa__outdent
  ,fa__p
  ,fa__pager
  ,fa__paint_roller
  ,fa__paintbrush
  ,fa__palette
  ,fa__pallet
  ,fa__panorama
  ,fa__paper_plane
  ,fa__paperclip
  ,fa__parachute_box
  ,fa__paragraph
  ,fa__passport
  ,fa__paste
  ,fa__pause
  ,fa__paw
  ,fa__peace
  ,fa__pen_clip
  ,fa__pen_fancy
  ,fa__pen_nib
  ,fa__pen_ruler
  ,fa__pen_to_square
  ,fa__pen
  ,fa__pencil
  ,fa__people_arrows
  ,fa__people_carry_box
  ,fa__people_group
  ,fa__people_line
  ,fa__people_pulling
  ,fa__people_robbery
  ,fa__people_roof
  ,fa__pepper_hot
  ,fa__percent
  ,fa__person_arrow_down_to_line
  ,fa__person_arrow_up_from_line
  ,fa__person_biking
  ,fa__person_booth
  ,fa__person_breastfeeding
  ,fa__person_burst
  ,fa__person_cane
  ,fa__person_chalkboard
  ,fa__person_circle_check
  ,fa__person_circle_exclamation
  ,fa__person_circle_minus
  ,fa__person_circle_plus
  ,fa__person_circle_question
  ,fa__person_circle_xmark
  ,fa__person_digging
  ,fa__person_dots_from_line
  ,fa__person_dress_burst
  ,fa__person_dress
  ,fa__person_drowning
  ,fa__person_falling_burst
  ,fa__person_falling
  ,fa__person_half_dress
  ,fa__person_harassing
  ,fa__person_hiking
  ,fa__person_military_pointing
  ,fa__person_military_rifle
  ,fa__person_military_to_person
  ,fa__person_praying
  ,fa__person_pregnant
  ,fa__person_rays
  ,fa__person_rifle
  ,fa__person_running
  ,fa__person_shelter
  ,fa__person_skating
  ,fa__person_skiing_nordic
  ,fa__person_skiing
  ,fa__person_snowboarding
  ,fa__person_swimming
  ,fa__person_through_window
  ,fa__person_walking_arrow_loop_left
  ,fa__person_walking_arrow_right
  ,fa__person_walking_dashed_line_arrow_right
  ,fa__person_walking_luggage
  ,fa__person_walking_with_cane
  ,fa__person_walking
  ,fa__person
  ,fa__peseta_sign
  ,fa__peso_sign
  ,fa__phone_flip
  ,fa__phone_slash
  ,fa__phone_volume
  ,fa__phone
  ,fa__photo_film
  ,fa__piggy_bank
  ,fa__pills
  ,fa__pizza_slice
  ,fa__place_of_worship
  ,fa__plane_arrival
  ,fa__plane_circle_check
  ,fa__plane_circle_exclamation
  ,fa__plane_circle_xmark
  ,fa__plane_departure
  ,fa__plane_lock
  ,fa__plane_slash
  ,fa__plane_up
  ,fa__plane
  ,fa__plant_wilt
  ,fa__plate_wheat
  ,fa__play
  ,fa__plug_circle_bolt
  ,fa__plug_circle_check
  ,fa__plug_circle_exclamation
  ,fa__plug_circle_minus
  ,fa__plug_circle_plus
  ,fa__plug_circle_xmark
  ,fa__plug
  ,fa__plus_minus
  ,fa__plus
  ,fa__podcast
  ,fa__poo_storm
  ,fa__poo
  ,fa__poop
  ,fa__power_off
  ,fa__prescription_bottle_medical
  ,fa__prescription_bottle
  ,fa__prescription
  ,fa__print
  ,fa__pump_medical
  ,fa__pump_soap
  ,fa__puzzle_piece
  ,fa__q
  ,fa__qrcode
  ,fa__question
  ,fa__quote_left
  ,fa__quote_right
  ,fa__r
  ,fa__radiation
  ,fa__radio
  ,fa__rainbow
  ,fa__ranking_star
  ,fa__receipt
  ,fa__record_vinyl
  ,fa__rectangle_ad
  ,fa__rectangle_list
  ,fa__rectangle_xmark
  ,fa__recycle
  ,fa__registered
  ,fa__repeat
  ,fa__reply_all
  ,fa__reply
  ,fa__republican
  ,fa__restroom
  ,fa__retweet
  ,fa__ribbon
  ,fa__right_from_bracket
  ,fa__right_left
  ,fa__right_long
  ,fa__right_to_bracket
  ,fa__ring
  ,fa__road_barrier
  ,fa__road_bridge
  ,fa__road_circle_check
  ,fa__road_circle_exclamation
  ,fa__road_circle_xmark
  ,fa__road_lock
  ,fa__road_spikes
  ,fa__road
  ,fa__robot
  ,fa__rocket
  ,fa__rotate_left
  ,fa__rotate_right
  ,fa__rotate
  ,fa__route
  ,fa__rss
  ,fa__ruble_sign
  ,fa__rug
  ,fa__ruler_combined
  ,fa__ruler_horizontal
  ,fa__ruler_vertical
  ,fa__ruler
  ,fa__rupee_sign
  ,fa__rupiah_sign
  ,fa__s
  ,fa__sack_dollar
  ,fa__sack_xmark
  ,fa__sailboat
  ,fa__satellite_dish
  ,fa__satellite
  ,fa__scale_balanced
  ,fa__scale_unbalanced_flip
  ,fa__scale_unbalanced
  ,fa__school_circle_check
  ,fa__school_circle_exclamation
  ,fa__school_circle_xmark
  ,fa__school_flag
  ,fa__school_lock
  ,fa__school
  ,fa__scissors
  ,fa__screwdriver_wrench
  ,fa__screwdriver
  ,fa__scroll_torah
  ,fa__scroll
  ,fa__sd_card
  ,fa__section
  ,fa__seedling
  ,fa__server
  ,fa__shapes
  ,fa__share_from_square
  ,fa__share_nodes
  ,fa__share
  ,fa__sheet_plastic
  ,fa__shekel_sign
  ,fa__shield_cat
  ,fa__shield_dog
  ,fa__shield_halved
  ,fa__shield_heart
  ,fa__shield_virus
  ,fa__shield
  ,fa__ship
  ,fa__shirt
  ,fa__shoe_prints
  ,fa__shop_lock
  ,fa__shop_slash
  ,fa__shop
  ,fa__shower
  ,fa__shrimp
  ,fa__shuffle
  ,fa__shuttle_space
  ,fa__sign_hanging
  ,fa__signal
  ,fa__signature
  ,fa__signs_post
  ,fa__sim_card
  ,fa__sink
  ,fa__sitemap
  ,fa__skull_crossbones
  ,fa__skull
  ,fa__slash
  ,fa__sleigh
  ,fa__sliders
  ,fa__smog
  ,fa__smoking
  ,fa__snowflake
  ,fa__snowman
  ,fa__snowplow
  ,fa__soap
  ,fa__socks
  ,fa__solar_panel
  ,fa__sort_down
  ,fa__sort_up
  ,fa__sort
  ,fa__spa
  ,fa__spaghetti_monster_flying
  ,fa__spell_check
  ,fa__spider
  ,fa__spinner
  ,fa__splotch
  ,fa__spoon
  ,fa__spray_can_sparkles
  ,fa__spray_can
  ,fa__square_arrow_up_right
  ,fa__square_caret_down
  ,fa__square_caret_left
  ,fa__square_caret_right
  ,fa__square_caret_up
  ,fa__square_check
  ,fa__square_envelope
  ,fa__square_full
  ,fa__square_h
  ,fa__square_minus
  ,fa__square_nfi
  ,fa__square_parking
  ,fa__square_pen
  ,fa__square_person_confined
  ,fa__square_phone_flip
  ,fa__square_phone
  ,fa__square_plus
  ,fa__square_poll_horizontal
  ,fa__square_poll_vertical
  ,fa__square_root_variable
  ,fa__square_rss
  ,fa__square_share_nodes
  ,fa__square_up_right
  ,fa__square_virus
  ,fa__square_xmark
  ,fa__square
  ,fa__staff_snake
  ,fa__stairs
  ,fa__stamp
  ,fa__stapler
  ,fa__star_and_crescent
  ,fa__star_half_stroke
  ,fa__star_half
  ,fa__star_of_david
  ,fa__star_of_life
  ,fa__star
  ,fa__sterling_sign
  ,fa__stethoscope
  ,fa__stop
  ,fa__stopwatch_20
  ,fa__stopwatch
  ,fa__store_slash
  ,fa__store
  ,fa__street_view
  ,fa__strikethrough
  ,fa__stroopwafel
  ,fa__subscript
  ,fa__suitcase_medical
  ,fa__suitcase_rolling
  ,fa__suitcase
  ,fa__sun_plant_wilt
  ,fa__sun
  ,fa__superscript
  ,fa__swatchbook
  ,fa__synagogue
  ,fa__syringe
  ,fa__t
  ,fa__table_cells_large
  ,fa__table_cells
  ,fa__table_columns
  ,fa__table_list
  ,fa__table_tennis_paddle_ball
  ,fa__table
  ,fa__tablet_button
  ,fa__tablet_screen_button
  ,fa__tablet
  ,fa__tablets
  ,fa__tachograph_digital
  ,fa__tag
  ,fa__tags
  ,fa__tape
  ,fa__tarp_droplet
  ,fa__tarp
  ,fa__taxi
  ,fa__teeth_open
  ,fa__teeth
  ,fa__temperature_arrow_down
  ,fa__temperature_arrow_up
  ,fa__temperature_empty
  ,fa__temperature_full
  ,fa__temperature_half
  ,fa__temperature_high
  ,fa__temperature_low
  ,fa__temperature_quarter
  ,fa__temperature_three_quarters
  ,fa__tenge_sign
  ,fa__tent_arrow_down_to_line
  ,fa__tent_arrow_left_right
  ,fa__tent_arrow_turn_left
  ,fa__tent_arrows_down
  ,fa__tent
  ,fa__tents
  ,fa__terminal
  ,fa__text_height
  ,fa__text_slash
  ,fa__text_width
  ,fa__thermometer
  ,fa__thumbs_down
  ,fa__thumbs_up
  ,fa__thumbtack
  ,fa__ticket_simple
  ,fa__ticket
  ,fa__timeline
  ,fa__toggle_off
  ,fa__toggle_on
  ,fa__toilet_paper_slash
  ,fa__toilet_paper
  ,fa__toilet_portable
  ,fa__toilet
  ,fa__toilets_portable
  ,fa__toolbox
  ,fa__tooth
  ,fa__torii_gate
  ,fa__tornado
  ,fa__tower_broadcast
  ,fa__tower_cell
  ,fa__tower_observation
  ,fa__tractor
  ,fa__trademark
  ,fa__traffic_light
  ,fa__trailer
  ,fa__train_subway
  ,fa__train_tram
  ,fa__train
  ,fa__transgender
  ,fa__trash_arrow_up
  ,fa__trash_can_arrow_up
  ,fa__trash_can
  ,fa__trash
  ,fa__tree_city
  ,fa__tree
  ,fa__triangle_exclamation
  ,fa__trophy
  ,fa__trowel_bricks
  ,fa__trowel
  ,fa__truck_arrow_right
  ,fa__truck_droplet
  ,fa__truck_fast
  ,fa__truck_field_un
  ,fa__truck_field
  ,fa__truck_front
  ,fa__truck_medical
  ,fa__truck_monster
  ,fa__truck_moving
  ,fa__truck_pickup
  ,fa__truck_plane
  ,fa__truck_ramp_box
  ,fa__truck
  ,fa__tty
  ,fa__turkish_lira_sign
  ,fa__turn_down
  ,fa__turn_up
  ,fa__tv
  ,fa__u
  ,fa__umbrella_beach
  ,fa__umbrella
  ,fa__underline
  ,fa__universal_access
  ,fa__unlock_keyhole
  ,fa__unlock
  ,fa__up_down_left_right
  ,fa__up_down
  ,fa__up_long
  ,fa__up_right_and_down_left_from_center
  ,fa__up_right_from_square
  ,fa__upload
  ,fa__user_astronaut
  ,fa__user_check
  ,fa__user_clock
  ,fa__user_doctor
  ,fa__user_gear
  ,fa__user_graduate
  ,fa__user_group
  ,fa__user_injured
  ,fa__user_large_slash
  ,fa__user_large
  ,fa__user_lock
  ,fa__user_minus
  ,fa__user_ninja
  ,fa__user_nurse
  ,fa__user_pen
  ,fa__user_plus
  ,fa__user_secret
  ,fa__user_shield
  ,fa__user_slash
  ,fa__user_tag
  ,fa__user_tie
  ,fa__user_xmark
  ,fa__user
  ,fa__users_between_lines
  ,fa__users_gear
  ,fa__users_line
  ,fa__users_rays
  ,fa__users_rectangle
  ,fa__users_slash
  ,fa__users_viewfinder
  ,fa__users
  ,fa__utensils
  ,fa__v
  ,fa__van_shuttle
  ,fa__vault
  ,fa__vector_square
  ,fa__venus_double
  ,fa__venus_mars
  ,fa__venus
  ,fa__vest_patches
  ,fa__vest
  ,fa__vial_circle_check
  ,fa__vial_virus
  ,fa__vial
  ,fa__vials
  ,fa__video_slash
  ,fa__video
  ,fa__vihara
  ,fa__virus_covid_slash
  ,fa__virus_covid
  ,fa__virus_slash
  ,fa__virus
  ,fa__viruses
  ,fa__voicemail
  ,fa__volcano
  ,fa__volleyball
  ,fa__volume_high
  ,fa__volume_low
  ,fa__volume_off
  ,fa__volume_xmark
  ,fa__vr_cardboard
  ,fa__w
  ,fa__walkie_talkie
  ,fa__wallet
  ,fa__wand_magic_sparkles
  ,fa__wand_magic
  ,fa__wand_sparkles
  ,fa__warehouse
  ,fa__water_ladder
  ,fa__water
  ,fa__wave_square
  ,fa__weight_hanging
  ,fa__weight_scale
  ,fa__wheat_awn_circle_exclamation
  ,fa__wheat_awn
  ,fa__wheelchair_move
  ,fa__wheelchair
  ,fa__whiskey_glass
  ,fa__wifi
  ,fa__wind
  ,fa__window_maximize
  ,fa__window_minimize
  ,fa__window_restore
  ,fa__wine_bottle
  ,fa__wine_glass_empty
  ,fa__wine_glass
  ,fa__won_sign
  ,fa__worm
  ,fa__wrench
  ,fa__x_ray
  ,fa__x
  ,fa__xmark
  ,fa__xmarks_lines
  ,fa__y
  ,fa__yen_sign
  ,fa__yin_yang
  ,fa__z
  ,vaadin__abacus
  ,vaadin__absolute_position
  ,vaadin__academy_cap
  ,vaadin__accessibility
  ,vaadin__accordion_menu
  ,vaadin__add_dock
  ,vaadin__adjust
  ,vaadin__adobe_flash
  ,vaadin__airplane
  ,vaadin__alarm
  ,vaadin__align_center
  ,vaadin__align_justify
  ,vaadin__align_left
  ,vaadin__align_right
  ,vaadin__alt_a
  ,vaadin__alt
  ,vaadin__ambulance
  ,vaadin__anchor
  ,vaadin__angle_double_down
  ,vaadin__angle_double_left
  ,vaadin__angle_double_right
  ,vaadin__angle_double_up
  ,vaadin__angle_down
  ,vaadin__angle_left
  ,vaadin__angle_right
  ,vaadin__angle_up
  ,vaadin__archive
  ,vaadin__archives
  ,vaadin__area_select
  ,vaadin__arrow_backward
  ,vaadin__arrow_circle_down_o
  ,vaadin__arrow_circle_down
  ,vaadin__arrow_circle_left_o
  ,vaadin__arrow_circle_left
  ,vaadin__arrow_circle_right_o
  ,vaadin__arrow_circle_right
  ,vaadin__arrow_circle_up_o
  ,vaadin__arrow_circle_up
  ,vaadin__arrow_down
  ,vaadin__arrow_forward
  ,vaadin__arrow_left
  ,vaadin__arrow_long_down
  ,vaadin__arrow_long_left
  ,vaadin__arrow_right
  ,vaadin__arrow_up
  ,vaadin__arrows_cross
  ,vaadin__arrows_long_h
  ,vaadin__arrows_long_right
  ,vaadin__arrows_long_up
  ,vaadin__arrows_long_v
  ,vaadin__arrows
  ,vaadin__asterisk
  ,vaadin__at
  ,vaadin__automation
  ,vaadin__backspace_a
  ,vaadin__backspace
  ,vaadin__backwards
  ,vaadin__ban
  ,vaadin__bar_chart_h
  ,vaadin__bar_chart_v
  ,vaadin__bar_chart
  ,vaadin__barcode
  ,vaadin__bed
  ,vaadin__bell_o
  ,vaadin__bell_slash_o
  ,vaadin__bell_slash
  ,vaadin__bell
  ,vaadin__boat
  ,vaadin__bold
  ,vaadin__bolt
  ,vaadin__bomb
  ,vaadin__book_dollar
  ,vaadin__book_percent
  ,vaadin__book
  ,vaadin__bookmark_o
  ,vaadin__bookmark
  ,vaadin__briefcase
  ,vaadin__browser
  ,vaadin__bug_o
  ,vaadin__bug
  ,vaadin__building_o
  ,vaadin__building
  ,vaadin__bullets
  ,vaadin__bullseye
  ,vaadin__bus
  ,vaadin__button
  ,vaadin__calc_book
  ,vaadin__calc
  ,vaadin__calendar_briefcase
  ,vaadin__calendar_clock
  ,vaadin__calendar_envelope
  ,vaadin__calendar_o
  ,vaadin__calendar_user
  ,vaadin__calendar
  ,vaadin__camera
  ,vaadin__car
  ,vaadin__caret_down
  ,vaadin__caret_left
  ,vaadin__caret_right
  ,vaadin__caret_square_down_o
  ,vaadin__caret_square_left_o
  ,vaadin__caret_square_right_o
  ,vaadin__caret_square_up_o
  ,vaadin__caret_up
  ,vaadin__cart_o
  ,vaadin__cart
  ,vaadin__cash
  ,vaadin__chart_3d
  ,vaadin__chart_grid
  ,vaadin__chart_line
  ,vaadin__chart_timeline
  ,vaadin__chart
  ,vaadin__chat
  ,vaadin__check_circle_o
  ,vaadin__check_circle
  ,vaadin__check_square_o
  ,vaadin__check_square
  ,vaadin__check
  ,vaadin__chevron_circle_down_o
  ,vaadin__chevron_circle_down
  ,vaadin__chevron_circle_left_o
  ,vaadin__chevron_circle_left
  ,vaadin__chevron_circle_right_o
  ,vaadin__chevron_circle_right
  ,vaadin__chevron_circle_up_o
  ,vaadin__chevron_circle_up
  ,vaadin__chevron_down_small
  ,vaadin__chevron_down
  ,vaadin__chevron_left_small
  ,vaadin__chevron_left
  ,vaadin__chevron_right_small
  ,vaadin__chevron_right
  ,vaadin__chevron_up_small
  ,vaadin__chevron_up
  ,vaadin__child
  ,vaadin__circle_thin
  ,vaadin__circle
  ,vaadin__clipboard_check
  ,vaadin__clipboard_cross
  ,vaadin__clipboard_heart
  ,vaadin__clipboard_pulse
  ,vaadin__clipboard_text
  ,vaadin__clipboard_user
  ,vaadin__clipboard
  ,vaadin__clock
  ,vaadin__close_big
  ,vaadin__close_circle_o
  ,vaadin__close_circle
  ,vaadin__close_small
  ,vaadin__close
  ,vaadin__cloud_download_o
  ,vaadin__cloud_download
  ,vaadin__cloud_o
  ,vaadin__cloud_upload_o
  ,vaadin__cloud_upload
  ,vaadin__cloud
  ,vaadin__cluster
  ,vaadin__code
  ,vaadin__coffee
  ,vaadin__cog_o
  ,vaadin__cog
  ,vaadin__cogs
  ,vaadin__coin_piles
  ,vaadin__coins
  ,vaadin__combobox
  ,vaadin__comment_ellipsis_o
  ,vaadin__comment_ellipsis
  ,vaadin__comment_o
  ,vaadin__comment
  ,vaadin__comments_o
  ,vaadin__comments
  ,vaadin__compile
  ,vaadin__compress_square
  ,vaadin__compress
  ,vaadin__connect_o
  ,vaadin__connect
  ,vaadin__controller
  ,vaadin__copy_o
  ,vaadin__copy
  ,vaadin__copyright
  ,vaadin__corner_lower_left
  ,vaadin__corner_lower_right
  ,vaadin__corner_upper_left
  ,vaadin__corner_upper_right
  ,vaadin__credit_card
  ,vaadin__crop
  ,vaadin__cross_cutlery
  ,vaadin__crosshairs
  ,vaadin__css
  ,vaadin__ctrl_a
  ,vaadin__ctrl
  ,vaadin__cube
  ,vaadin__cubes
  ,vaadin__curly_brackets
  ,vaadin__cursor_o
  ,vaadin__cursor
  ,vaadin__cutlery
  ,vaadin__dashboard
  ,vaadin__database
  ,vaadin__date_input
  ,vaadin__deindent
  ,vaadin__del_a
  ,vaadin__del
  ,vaadin__dental_chair
  ,vaadin__desktop
  ,vaadin__diamond_o
  ,vaadin__diamond
  ,vaadin__diploma_scroll
  ,vaadin__diploma
  ,vaadin__disc
  ,vaadin__doctor_briefcase
  ,vaadin__doctor
  ,vaadin__dollar
  ,vaadin__dot_circle
  ,vaadin__download_alt
  ,vaadin__download
  ,vaadin__drop
  ,vaadin__edit
  ,vaadin__eject
  ,vaadin__elastic
  ,vaadin__ellipsis_circle_o
  ,vaadin__ellipsis_circle
  ,vaadin__ellipsis_dots_h
  ,vaadin__ellipsis_dots_v
  ,vaadin__ellipsis_h
  ,vaadin__ellipsis_v
  ,vaadin__enter_arrow
  ,vaadin__enter
  ,vaadin__envelope_o
  ,vaadin__envelope_open_o
  ,vaadin__envelope_open
  ,vaadin__envelope
  ,vaadin__envelopes_o
  ,vaadin__envelopes
  ,vaadin__eraser
  ,vaadin__esc_a
  ,vaadin__esc
  ,vaadin__euro
  ,vaadin__exchange
  ,vaadin__exclamation_circle_o
  ,vaadin__exclamation_circle
  ,vaadin__exclamation
  ,vaadin__exit_o
  ,vaadin__exit
  ,vaadin__expand_full
  ,vaadin__expand_square
  ,vaadin__expand
  ,vaadin__external_browser
  ,vaadin__external_link
  ,vaadin__eye_slash
  ,vaadin__eye
  ,vaadin__eyedropper
  ,vaadin__facebook_square
  ,vaadin__facebook
  ,vaadin__factory
  ,vaadin__family
  ,vaadin__fast_backward
  ,vaadin__fast_forward
  ,vaadin__female
  ,vaadin__file_add
  ,vaadin__file_code
  ,vaadin__file_font
  ,vaadin__file_movie
  ,vaadin__file_o
  ,vaadin__file_picture
  ,vaadin__file_presentation
  ,vaadin__file_process
  ,vaadin__file_refresh
  ,vaadin__file_remove
  ,vaadin__file_search
  ,vaadin__file_sound
  ,vaadin__file_start
  ,vaadin__file_table
  ,vaadin__file_text_o
  ,vaadin__file_text
  ,vaadin__file_tree_small
  ,vaadin__file_tree_sub
  ,vaadin__file_tree
  ,vaadin__file_zip
  ,vaadin__file
  ,vaadin__fill
  ,vaadin__film
  ,vaadin__filter
  ,vaadin__fire
  ,vaadin__flag_checkered
  ,vaadin__flag_o
  ,vaadin__flag
  ,vaadin__flash
  ,vaadin__flask
  ,vaadin__flight_landing
  ,vaadin__flight_takeoff
  ,vaadin__flip_h
  ,vaadin__flip_v
  ,vaadin__folder_add
  ,vaadin__folder_o
  ,vaadin__folder_open_o
  ,vaadin__folder_open
  ,vaadin__folder_remove
  ,vaadin__folder_search
  ,vaadin__folder
  ,vaadin__font
  ,vaadin__form
  ,vaadin__forward
  ,vaadin__frown_o
  ,vaadin__function
  ,vaadin__funnel
  ,vaadin__gamepad
  ,vaadin__gavel
  ,vaadin__gift
  ,vaadin__glass
  ,vaadin__glasses
  ,vaadin__globe_wire
  ,vaadin__globe
  ,vaadin__golf
  ,vaadin__google_plus_square
  ,vaadin__google_plus
  ,vaadin__grab
  ,vaadin__grid_bevel
  ,vaadin__grid_big_o
  ,vaadin__grid_big
  ,vaadin__grid_h
  ,vaadin__grid_small_o
  ,vaadin__grid_small
  ,vaadin__grid_v
  ,vaadin__grid
  ,vaadin__group
  ,vaadin__hammer
  ,vaadin__hand
  ,vaadin__handle_corner
  ,vaadin__hands_up
  ,vaadin__handshake
  ,vaadin__harddrive_o
  ,vaadin__harddrive
  ,vaadin__hash
  ,vaadin__header
  ,vaadin__headphones
  ,vaadin__headset
  ,vaadin__health_card
  ,vaadin__heart_o
  ,vaadin__heart
  ,vaadin__home_o
  ,vaadin__home
  ,vaadin__hospital
  ,vaadin__hourglass_empty
  ,vaadin__hourglass_end
  ,vaadin__hourglass_start
  ,vaadin__hourglass
  ,vaadin__inbox
  ,vaadin__indent
  ,vaadin__info_circle_o
  ,vaadin__info_circle
  ,vaadin__info
  ,vaadin__input
  ,vaadin__insert
  ,vaadin__institution
  ,vaadin__invoice
  ,vaadin__italic
  ,vaadin__key_o
  ,vaadin__key
  ,vaadin__keyboard_o
  ,vaadin__keyboard
  ,vaadin__laptop
  ,vaadin__layout
  ,vaadin__level_down_bold
  ,vaadin__level_down
  ,vaadin__level_left_bold
  ,vaadin__level_left
  ,vaadin__level_right_bold
  ,vaadin__level_right
  ,vaadin__level_up_bold
  ,vaadin__level_up
  ,vaadin__lifebuoy
  ,vaadin__lightbulb
  ,vaadin__line_bar_chart
  ,vaadin__line_chart
  ,vaadin__line_h
  ,vaadin__line_v
  ,vaadin__lines_list
  ,vaadin__lines
  ,vaadin__link
  ,vaadin__list_ol
  ,vaadin__list_select
  ,vaadin__list_ul
  ,vaadin__list
  ,vaadin__location_arrow_circle_o
  ,vaadin__location_arrow_circle
  ,vaadin__location_arrow
  ,vaadin__lock
  ,vaadin__m_account
  ,vaadin__m_bank
  ,vaadin__m_calculator_variant
  ,vaadin__m_card_account_details
  ,vaadin__m_folder_open
  ,vaadin__m_folder
  ,vaadin__m_logout
  ,vaadin__m_safe_square_outline
  ,vaadin__m_safe_square
  ,vaadin__magic
  ,vaadin__magnet
  ,vaadin__mailbox
  ,vaadin__male
  ,vaadin__map_marker
  ,vaadin__margin_bottom
  ,vaadin__margin_left
  ,vaadin__margin_right
  ,vaadin__margin_top
  ,vaadin__margin
  ,vaadin__medal
  ,vaadin__megaphone
  ,vaadin__meh_o
  ,vaadin__menu
  ,vaadin__microphone
  ,vaadin__minus_circle_o
  ,vaadin__minus_circle
  ,vaadin__minus_square_o
  ,vaadin__minus
  ,vaadin__mobile_browser
  ,vaadin__mobile_retro
  ,vaadin__mobile
  ,vaadin__modal_list
  ,vaadin__modal
  ,vaadin__money_deposit
  ,vaadin__money_exchange
  ,vaadin__money_withdraw
  ,vaadin__money
  ,vaadin__moon_o
  ,vaadin__moon
  ,vaadin__morning
  ,vaadin__movie
  ,vaadin__music
  ,vaadin__mute
  ,vaadin__native_button
  ,vaadin__newspaper
  ,vaadin__notebook
  ,vaadin__nurse
  ,vaadin__office
  ,vaadin__open_book
  ,vaadin__option_a
  ,vaadin__option
  ,vaadin__options
  ,vaadin__orientation
  ,vaadin__out
  ,vaadin__outbox
  ,vaadin__package
  ,vaadin__padding_bottom
  ,vaadin__padding_left
  ,vaadin__padding_right
  ,vaadin__padding_top
  ,vaadin__padding
  ,vaadin__paint_roll
  ,vaadin__paintbrush
  ,vaadin__palette
  ,vaadin__panel
  ,vaadin__paperclip
  ,vaadin__paperplane_o
  ,vaadin__paperplane
  ,vaadin__paragraph
  ,vaadin__password
  ,vaadin__paste
  ,vaadin__pause
  ,vaadin__pencil
  ,vaadin__phone_landline
  ,vaadin__phone
  ,vaadin__picture
  ,vaadin__pie_bar_chart
  ,vaadin__pie_chart
  ,vaadin__piggy_bank_coin
  ,vaadin__piggy_bank
  ,vaadin__pill
  ,vaadin__pills
  ,vaadin__pin_post
  ,vaadin__pin
  ,vaadin__play_circle_o
  ,vaadin__play_circle
  ,vaadin__play
  ,vaadin__plug
  ,vaadin__plus_circle_o
  ,vaadin__plus_circle
  ,vaadin__plus_minus
  ,vaadin__plus_square_o
  ,vaadin__plus
  ,vaadin__pointer
  ,vaadin__power_off
  ,vaadin__presentation
  ,vaadin__print
  ,vaadin__progressbar
  ,vaadin__puzzle_piece
  ,vaadin__pyramid_chart
  ,vaadin__qrcode
  ,vaadin__question_circle_o
  ,vaadin__question_circle
  ,vaadin__question
  ,vaadin__quote_left
  ,vaadin__quote_right
  ,vaadin__random
  ,vaadin__raster_lower_left
  ,vaadin__raster
  ,vaadin__records
  ,vaadin__recycle
  ,vaadin__refresh
  ,vaadin__reply_all
  ,vaadin__reply
  ,vaadin__resize_h
  ,vaadin__resize_v
  ,vaadin__retweet
  ,vaadin__rhombus
  ,vaadin__road_branch
  ,vaadin__road_branches
  ,vaadin__road_split
  ,vaadin__road
  ,vaadin__rocket
  ,vaadin__rotate_left
  ,vaadin__rotate_right
  ,vaadin__rss_square
  ,vaadin__rss
  ,vaadin__safe_lock
  ,vaadin__safe
  ,vaadin__scale_unbalance
  ,vaadin__scale
  ,vaadin__scatter_chart
  ,vaadin__scissors
  ,vaadin__screwdriver
  ,vaadin__search_minus
  ,vaadin__search_plus
  ,vaadin__search
  ,vaadin__select
  ,vaadin__server
  ,vaadin__share_square
  ,vaadin__share
  ,vaadin__shield
  ,vaadin__shift_arrow
  ,vaadin__shift
  ,vaadin__shop
  ,vaadin__sign_in_alt
  ,vaadin__sign_in
  ,vaadin__sign_out_alt
  ,vaadin__sign_out
  ,vaadin__signal
  ,vaadin__sitemap
  ,vaadin__slider
  ,vaadin__sliders
  ,vaadin__smiley_o
  ,vaadin__sort
  ,vaadin__sound_disable
  ,vaadin__spark_line
  ,vaadin__specialist
  ,vaadin__spinner_arc
  ,vaadin__spinner_third
  ,vaadin__spinner
  ,vaadin__spline_area_chart
  ,vaadin__spline_chart
  ,vaadin__split_h
  ,vaadin__split_v
  ,vaadin__split
  ,vaadin__spoon
  ,vaadin__square_shadow
  ,vaadin__star_half_left_o
  ,vaadin__star_half_left
  ,vaadin__star_half_right_o
  ,vaadin__star_half_right
  ,vaadin__star_o
  ,vaadin__star
  ,vaadin__start_cog
  ,vaadin__step_backward
  ,vaadin__step_forward
  ,vaadin__stethoscope
  ,vaadin__stock
  ,vaadin__stop_cog
  ,vaadin__stop
  ,vaadin__stopwatch
  ,vaadin__storage
  ,vaadin__strikethrough
  ,vaadin__subscript
  ,vaadin__suitcase
  ,vaadin__sun_down
  ,vaadin__sun_o
  ,vaadin__sun_rise
  ,vaadin__superscript
  ,vaadin__sword
  ,vaadin__tab_a
  ,vaadin__tab
  ,vaadin__table
  ,vaadin__tablet
  ,vaadin__tabs
  ,vaadin__tag
  ,vaadin__tags
  ,vaadin__tasks
  ,vaadin__taxi
  ,vaadin__teeth
  ,vaadin__terminal
  ,vaadin__text_height
  ,vaadin__text_input
  ,vaadin__text_label
  ,vaadin__text_width
  ,vaadin__thin_square
  ,vaadin__thumbs_down_o
  ,vaadin__thumbs_down
  ,vaadin__thumbs_up_o
  ,vaadin__thumbs_up
  ,vaadin__ticket
  ,vaadin__time_backward
  ,vaadin__time_forward
  ,vaadin__timer
  ,vaadin__toolbox
  ,vaadin__tools
  ,vaadin__tooth
  ,vaadin__touch
  ,vaadin__train
  ,vaadin__trash
  ,vaadin__tree_table
  ,vaadin__trending_down
  ,vaadin__trending_up
  ,vaadin__trophy
  ,vaadin__truck
  ,vaadin__twin_col_select
  ,vaadin__twitter_square
  ,vaadin__twitter
  ,vaadin__umbrella
  ,vaadin__underline
  ,vaadin__unlink
  ,vaadin__unlock
  ,vaadin__upload_alt
  ,vaadin__upload
  ,vaadin__user_card
  ,vaadin__user_check
  ,vaadin__user_clock
  ,vaadin__user_heart
  ,vaadin__user_star
  ,vaadin__user
  ,vaadin__users
  ,vaadin__vaadin_h
  ,vaadin__vaadin_v
  ,vaadin__viewport
  ,vaadin__vimeo_square
  ,vaadin__vimeo
  ,vaadin__volume_down
  ,vaadin__volume_off
  ,vaadin__volume_up
  ,vaadin__volume
  ,vaadin__wallet
  ,vaadin__warning
  ,vaadin__workplace
  ,vaadin__wrench
  ,vaadin__youtube_square
  ,vaadin__youtube};
  