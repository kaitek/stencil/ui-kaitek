import { Build, Component, Element, Event, EventEmitter, Method, Prop, h } from '@stencil/core';

import { DataTableColumn } from '../px-interfaces';
import { UIFunctions } from '../ui-functions/ui-functions';
import { UIGrid } from '../ui-grid/ui-grid';
import * as UIIcons from '../ui-icons';
import { UITree } from '../ui-tree/ui-tree';
import { UIUpload } from '../ui-upload/ui-upload';

import { Button } from '@vaadin/button';
//import { Dialog } from '@vaadin/dialog';

import '@vaadin/button';
import '@vaadin/checkbox';
import '@vaadin/dialog';
import '@vaadin/email-field';
import '@vaadin/form-layout';
import '@vaadin/horizontal-layout';
import '@vaadin/icon';
import '@vaadin/password-field';
import '@vaadin/radio-group';
import '@vaadin/text-field';
import '@vaadin/vertical-layout';

import type { FormLayout, FormLayoutResponsiveStep } from '@vaadin/form-layout';
@Component({
  tag: 'app-user',
  styleUrl: 'app-user.scss',
  shadow: true
})
export class AppUser {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Prop() args: any = { idx: 1, clientHeight: 600, perm: { add: 1, edit: 1 } };
  //isDev: boolean = false;

  uiFns: UIFunctions = new UIFunctions();

  contentContainer: HTMLDivElement;
  dialogCustomRight: any;
  tabFilter: HTMLDivElement;
  tabForm: HTMLDivElement;
  tabList: HTMLDivElement;
  filterLayout: FormLayout;
  formLayout: FormLayout;
  dataGrid: UIGrid;
  userPermissionTree: UITree;
  btnClose: Button;
  btnList: Button;
  btnSave: Button;
  btnFilter: Button;
  btnClear: Button;
  btnBack: Button;
  avatar: UIUpload;

  currentPage: number = 1;
  recordsPerPage: number = 25;
  totalRecords: number = 0;

  menuTreeData: any[] = [];
  permissionTreeData: any[] = [];
  selectedCustomRightData: any = {};
  selectedUser: any = {};
  filterData: any[] = [];
  sortData: any[] = [];

  isInvalid: boolean = false;
  baseRequestParams: any;

  async componentWillLoad() {
    //let t = this;
    //if (Build.isDev) {
    //  console.log('CWL|', t.myElement.tagName.toLowerCase());
    //}
  }

  async componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    t.baseRequestParams = {
      emitter: t.appEvent, controller: '', tagName: t.myElement.tagName.toLowerCase(), idx: t.args.idx, recordsPerPage: t.recordsPerPage
    };
    //const els = document.getElementsByTagName('html');
    //t.isDev = typeof els[0].attributes["isdev"] !== 'undefined';
    await t.uiFns.hideAllContainers(t.contentContainer);
    if (t.tabList.classList.contains('hidden')) {
      t.tabList.classList.remove('hidden');
    }
    t.filterLayout = await t.uiFns.generateFilterForm(t, t.tabFilter, t.dataGrid.columns, t.appEvents);
    //await t.uiFns.generateFilterForm(t.tabFilter, t.dataGrid.columns);
    //t.filterLayout = t.tabFilter.querySelector('vaadin-form-layout');
    //const itemsFireClick = t.tabFilter.querySelectorAll('vaadin-button');
    //if (itemsFireClick.length > 0) {
    //  await t.uiFns.asyncForEach(itemsFireClick, async (item: Button) => {
    //    item.addEventListener('click', t.appEvents.bind(t, { type: 'button', element: item.part[0] }), { passive: true });
    //  });
    //}
    //@@todo: custom_right
    //console.log(t.args.perm);
    //t.appEvent.emit({ event: 'componentDidLoad', component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
    await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
    if (Build.isDev) {
      await t.uiFns.delay(500);
      await t.dataGrid.afterLoad();
    }
    if (Build.isDev) {
      //console.log(t.formLayout)
      t.menuTreeData = [{
        "id": "23", "text": "Stok", p_menu: 'exists', p_add: 'exists', p_edit: 'exists', p_del: 'exists', custom_right: []
        , "data": [{
          "id": "10", "text": "Stok Kartlar\u0131", p_menu: true, p_add: true, p_edit: true, p_del: true
          , custom_right: [
            { fieldName: "d1", fieldLabel: "deneme-1", fieldType: "boolean", fieldValue: "false" }
            , { fieldName: "d2", fieldLabel: "deneme-2", fieldType: "text", fieldValue: "11" }
          ]
        }, {
          "id": "24", "text": "Depo \u00dcr\u00fcn Talebi", custom_right: [
            { fieldName: "d1", fieldLabel: "deneme-1", fieldType: "boolean", fieldValue: "true" }
            , { fieldName: "d2", fieldLabel: "deneme-2", fieldType: "text", fieldValue: "00" }
          ]
        }, { "id": "25", "text": "Stok Hareket" }, { "id": "33", "text": "|" }, { "id": "4", "text": "Stok Listesi" }, { "id": "32", "text": "Stok Detay Listesi" }, { "id": "34", "text": "Stok Hareket" }]
      }
        , {
        "id": "20", "text": "Cari", custom_right: []
        , "data": [{ "id": "12", "text": "Cari Hesap Tan\u0131mlar\u0131" }, { "id": "16", "text": "Cari \u0130\u015flem Fi\u015fi" }, { "id": "36", "text": "Virman \u0130\u015flemleri" }, { "id": "39", "text": "|" }, { "id": "40", "text": "Cari Hareket" }, { "id": "61", "text": "Cari Hesap Ekstresi" }, { "id": "62", "text": "Cari Hesap Ekstresi Dovizli" }, { "id": "64", "text": "Cari Hesap Ekstresi-2" }, { "id": "78", "text": "Cari Listesi" }]
      }
        , {
        "id": "19", "text": "Sat\u0131nalma", custom_right: []
        , "data": [{ "id": "18", "text": "Tedarik\u00e7i Sipari\u015f Giri\u015fi" }, { "id": "22", "text": "Sat\u0131nalma Faturas\u0131 (T. Mal)" }, { "id": "71", "text": "Masraf Giri\u015fi" }]
      }
        , {
        "id": "21", "text": "Sat\u0131\u015f", custom_right: []
        , "data": [{ "id": "26", "text": "Sat\u0131\u015f Sipari\u015fi" }, { "id": "41", "text": "\u0130rsaliye" }, { "id": "14", "text": "Sat\u0131\u015f Faturas\u0131" }, { "id": "49", "text": "|" }, { "id": "50", "text": "Sat\u0131\u015f Hareketleri" }, { "id": "68", "text": "Sat\u0131\u015f Hareketleri-2" }]
      }
        , {
        "id": "42", "text": "Banka", custom_right: []
        , "data": [{ "id": "15", "text": "Banka Tan\u0131mlar\u0131" }, { "id": "44", "text": "Banka Hesap Tan\u0131mlar\u0131" }, { "id": "45", "text": "|" }, { "id": "46", "text": "Banka \u0130\u015flem Fi\u015fi" }, { "id": "79", "text": "Banka Hesap Ekstresi" }]
      }, { "id": "43", "text": "Kasa", custom_right: [], "data": [{ "id": "59", "text": "Kasa Tan\u0131mlar\u0131" }, { "id": "60", "text": "Kasa \u0130\u015flem Fi\u015fi" }] }
        , {
        "id": "27", "text": "Muhasebe", custom_right: []
        , "data": []
      }
        , {
        "id": "1", "text": "Parametreler", custom_right: []
        , "data": [{ "id": "17", "text": "Depo Tan\u0131mlar\u0131" }, { "id": "9", "text": "\u00dcr\u00fcn Gruplar\u0131" }, { "id": "38", "text": "\u00dcr\u00fcn T\u00fcrleri" }, { "id": "8", "text": "|" }, { "id": "37", "text": "Kur Giri\u015fi" }, { "id": "63", "text": "\u00dclke Tan\u0131mlar\u0131" }, { "id": "65", "text": "Sistem Parametreleri" }, { "id": "67", "text": "Personel Tan\u0131mlar\u0131" }, { "id": "69", "text": "|" }, { "id": "70", "text": "Masraf T\u00fcrleri" }]
      }, {
        "id": "3", "text": "Y\u00f6netici \u0130\u015flemleri", custom_right: []
        , "data": [{ "id": "5", "text": "Kullan\u0131c\u0131 Tan\u0131mlama" }, { "id": "66", "text": "Devir \u0130\u015flemleri" }]
      }, {
        "id": "51", "text": "Site \u0130\u00e7erik ve Parametreleri", custom_right: []
        , "data": [{ "id": "55", "text": "Sistem Parametreleri" }, { "id": "54", "text": "Giri\u015f Reklam\u0131" }, { "id": "52", "text": "Genel \u0130\u00e7erikler" }, { "id": "53", "text": "Kullan\u0131c\u0131 Men\u00fcleri" }, { "id": "56", "text": "Kurumsal Bilgiler" }, { "id": "57", "text": "\u00d6ne \u00c7\u0131kanlar" }, { "id": "58", "text": "\u00dcr\u00fcn Kategorileri" }]
      }];
    }
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
    //}
    switch (obj.element) {
      case 'dataGrid': {
        t.currentPage = obj.currentPage;
        t.recordsPerPage = obj.recordsPerPage;
        t.totalRecords = obj.totalRecords;
        t.dataGrid.setData(obj);
        t.menuTreeData = obj.menuTreeData;
        t.permissionTreeData = obj.permissionTreeData;
        break;
      }
      case 'userPermissionTree': {
        t.permissionTreeData = obj.records;
        await t.userPermissionTree.load(t.permissionTreeData);
        break;
      }
      case 'ui-upload': {
        //console.log('ui-upload', obj);
        switch (obj.method) {
          case 'file-delete': {
            //dosya silinmiş
            t.avatar.value = '';
            break;
          }
          case 'file-view': {
            t.avatar.thumbnail = obj.records[0];
            break;
          }

          default:
            break;
        }
        //if (obj.records.length === 0) {
        //  //dosya silinmiş
        //  t.avatar.value = '';
        //} else {
        //  //dosya açılmak istenmiş
        //}
        break;
      }
      default: {
        if (Build.isDev) {
          console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
        }
        break;
      }
    }
  }

  @Method()
  async callbackRequestData(obj: any, resp: any) {
    let t = this;
    if (Build.isDev) {
      console.log('CBRD|', t.myElement.tagName.toLowerCase(), obj, resp);
    }
    await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
    return await t.appEvents({ type: 'button', element: 'list' });
  }

  @Method()
  async resize(height: number) {
    let t = this;
    //console.log('resize|', t.myElement.tagName.toLowerCase(), height, t.cacheData);
    t.tabFilter.style.height = (height - 2) + 'px';
    t.tabForm.style.height = (height - 2) + 'px';
    t.tabList.style.height = (height - 2) + 'px';
    t.dataGrid.height = (height - 2) + 'px';
    setTimeout(async () => {
      await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
    }, 50);
  }

  async appEvents(obj: any) {
    let t = this;
    if (Build.isDev) {
      console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
    }
    switch (obj.type) {
      case 'button': {
        switch (obj.element) {
          case 'first': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            break;
          }
          case 'prev': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: (t.currentPage - 1), where: t.filterData, order: t.sortData });
            break;
          }
          case 'next': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: (t.currentPage + 1), where: t.filterData, order: t.sortData });
            break;
          }
          case 'last': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: Math.ceil(t.totalRecords / t.recordsPerPage), where: t.filterData, order: t.sortData });
            break;
          }
          case 'add': {
            //t.appEvent.emit({ event: 'btn-' + obj.element, component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
            t.btnClose.disabled = false;
            t.btnList.disabled = false;
            if (t.args.perm.add !== 0) {
              t.btnSave.disabled = false;
            }
            t.dataGrid.clearSelectedRows();
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabForm.classList.contains('hidden')) {
              t.tabForm.classList.remove('hidden');
            }
            await t.uiFns.setFormValues(t.formLayout, {});
            await t.userPermissionTree.load(t.menuTreeData);
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'edit': {
            t.btnClose.disabled = false;
            t.btnList.disabled = false;
            if (t.args.perm.add !== 0) {
              t.btnSave.disabled = false;
            }
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabForm.classList.contains('hidden')) {
              t.tabForm.classList.remove('hidden');
            }
            await t.uiFns.setFormValues(t.formLayout, t.selectedUser);
            await t.uiFns.requestData(
              {
                emitter: t.appEvent
                , controller: ''
                , tagName: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , recordsPerPage: t.recordsPerPage
              }
              , {
                element: t.userPermissionTree, elementName: 'userPermissionTree', route: 'getPermissionData', currentPage: 1, where: [{
                  condition: "=",
                  fields: ["u.email"],
                  keyword: t.selectedUser.email
                }], order: []
              }
            );
            if (Build.isDev) {
              await t.uiFns.delay(500);
              if (Build.isDev) {
                t.permissionTreeData = t.menuTreeData;
              }
              await t.userPermissionTree.load(t.permissionTreeData);
            }
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'delete': {
            //const values: any = await t.getFormValues();
            if (t.args.perm.del === 0 && t.selectedUser.id !== '') {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Bu işlem için yetkili değilsiniz.'
              });
            } else {
              t.selectedUser.user_permission = [];
              t.appEvent.emit({
                event: 'sendData'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , data: t.selectedUser
                , id: t.selectedUser.id
                , method: 'delete'
              });
            }
            break;
          }
          case 'refresh': {
            await t.uiFns.setFormValues(t.filterLayout, {});
            t.filterData = [];
            t.sortData = [];
            t.dataGrid.clearSortData();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
            break;
          }
          case 'filter': {
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabFilter.classList.contains('hidden')) {
              t.tabFilter.classList.remove('hidden');
            }
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'save': {
            const values: any = await t.getFormValues();
            if (!t.isInvalid) {
              if (t.args.perm.add === 0 || (t.args.perm.add === 1 && t.args.perm.edit === 0 && values.id !== '')) {
                t.rendererEvent.emit({
                  event: 'showNotification'
                  , component: t.myElement.tagName.toLowerCase()
                  , idx: t.args.idx
                  , message: 'Bu işlem için yetkili değilsiniz.'
                });
                t.btnClose.disabled = false;
                t.btnList.disabled = false;
                if (t.args.perm.add !== 0) {
                  t.btnSave.disabled = false;
                }
              } else {
                t.appEvent.emit({
                  event: 'sendData'
                  , component: t.myElement.tagName.toLowerCase()
                  , idx: t.args.idx
                  , data: values
                  , method: (typeof values.id !== 'undefined' && values.id !== '' ? 'patch' : 'post')
                });
              }
            } else {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Gerekli alanları eksiksiz olarak doldurunuz.'
              });
              t.btnClose.disabled = false;
              t.btnList.disabled = false;
              if (t.args.perm.add !== 0) {
                t.btnSave.disabled = false;
              }
            }
            break;
          }
          case 'list': {
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.userPermissionTree.load([]);
            await t.uiFns.setFormValues(t.formLayout, {});
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
            break;
          }
          case 'close': {
            t.rendererEvent.emit({
              event: 'close'
              , component: t.myElement.tagName.toLowerCase()
              , idx: t.args.idx
            });
            break;
          }
          case 'btnSaveCustomRight': {
            t.dialogCustomRight.dialog.opened = false;
            //const recordsCustomRight: any = await t.dialogCustomRightGrid.getData();
            t.selectedCustomRightData.custom_right = JSON.parse(JSON.stringify(obj.data));
            //console.log('recordsCustomRight', t.selectedCustomRightData);
            break;
          }
          case 'btnCancelCustomRight': {
            t.dialogCustomRight.dialog.opened = false;
            break;
          }
          case 'back': {
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            break;
          }
          case 'filter_records': {
            //console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            t.filterData = await t.uiFns.getFilterValues(t.filterLayout);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          case 'filter_clear': {
            await t.uiFns.setFormValues(t.filterLayout, {});
            t.filterData = [];
            t.sortData = [];
            t.dataGrid.clearSortData();
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          default: {
            console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            break;
          }
        }
        break;
      }
      case 'change-currentPage': {
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: parseInt(obj.data, 10), where: t.filterData, order: t.sortData });
        break;
      }
      case 'change-recordsPerPage': {
        t.recordsPerPage = parseInt(obj.data, 10);
        t.baseRequestParams.recordsPerPage = t.recordsPerPage;
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
        break;
      }
      case 'col-sort': {
        t.sortData = obj.data;
        //console.log('col-sort',t.sortData);
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
        break;
      }
      case 'custom_rights': {
        t.selectedCustomRightData = obj.item;
        //console.log('custom_rights', t.selectedCustomRightData);
        t.dialogCustomRight = await t.uiFns.generateDialog(t, t.contentContainer
          , [
            { label: 'Adı', dataIndex: 'fieldName', style: { width: "150px" }, type: 'text', options: { readonly: true } }
            , { label: 'Etiketi', dataIndex: 'fieldLabel', style: { width: "300px" }, type: 'text', options: { readonly: true } }
            , { label: 'Tipi', dataIndex: 'fieldType', style: { width: "100px" }, type: 'text', options: { readonly: true } }
            , { label: 'Değer', dataIndex: 'fieldValue', style: { width: "250px" }, type: 'text', options: { clearButtonVisible: true } }
          ]
          , t.appEvents
          , {
            id: "dialogCustomRight"
            , headerTitle: "Özel Hak Tanımları"
            , draggable: true
            , modeless: false
            , resizable: false
            , noCloseOnEsc: true
            , noCloseOnOutsideClick: true
            , data: t.selectedCustomRightData.custom_right
            , buttons: [
              { title: 'Kaydet', theme: 'primary', event: 'save', element: 'btnSaveCustomRight' }
              , { title: 'Vazgeç', theme: 'primary contrast', event: 'cancel', element: 'btnCancelCustomRight' }
            ]
            , gridType: 'UIGridEditable'
          });
        //t.dialogCustomRight.dialog.opened = true;
        break;
      }
      case 'file': {
        switch (obj.element) {
          case 'fileDelete': {
            const values: any = await t.getFormValues();
            if (t.args.perm.add === 0 || (t.args.perm.add === 1 && t.args.perm.edit === 0 && values.id !== '')) {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Bu işlem için yetkili değilsiniz.'
              });
            } else {
              t.appEvent.emit({
                event: 'sendData'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , element: 'ui-upload'
                , data: obj.detail
                , id: t.selectedUser.id
                , method: 'file-delete'
              });
            }
            break;
          }
          case 'fileOpen': {
            console.log(obj)
            t.appEvent.emit({
              event: 'sendData'
              , component: t.myElement.tagName.toLowerCase()
              , idx: t.args.idx
              , element: 'ui-upload'
              , data: obj.detail
              , id: t.selectedUser.id
              , method: 'file-open'
            });
            break;
          }
          case 'fileView': {
            //console.log(obj)
            t.appEvent.emit({
              event: 'sendData'
              , component: t.myElement.tagName.toLowerCase()
              , idx: t.args.idx
              , element: 'ui-upload'
              , data: obj.detail
              , id: t.selectedUser.id
              , method: 'file-view'
            });
            break;
          }
          default: {
            console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            break;
          }
        }
        break;
      }
      case 'row-dblclick': {
        t.selectedUser = obj.data;
        t.appEvents({ type: 'button', element: 'edit' });
        break;
      }
      case 'row-select': {
        t.selectedUser = obj.data;
        break;
      }
      default: {
        console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
        break;
      }
    }
  }

  private async getFormValues() {
    let t = this;
    const formElements: NodeListOf<ChildNode> = await t.uiFns.getFormElements(t.formLayout);
    t.isInvalid = false;
    //const preData: any = Object.assign({}, t.selectedTreeNode);
    let formData: any = {};
    //console.log(await t.userPermissionTree.getData(), await t.userPermissionTree.getData(true), t.permissionTreeData);
    await t.uiFns.asyncForEach(formElements, async (item: any) => {
      if (typeof item.getAttribute === 'function') {
        const dataIndex: string = item.getAttribute('dataIndex');
        if (dataIndex !== null && dataIndex !== '') {
          if (typeof item.focus === 'function') {
            item.focus();
          }
          if (typeof item.blur === 'function') {
            item.blur();
          }
          if (dataIndex === 'password_confirm') {
            if (formData.password !== item.value) {
              item.invalid = true;
            }
          }
          if (item.invalid) {
            //console.log(item, formData);
            if (formData.id !== '' && (dataIndex === 'password' || dataIndex === 'password_confirm') && item.value === '') {
              item.removeAttribute('invalid');
            } else {
              t.isInvalid = true;
            }
          }
          const tagName: string = item.tagName.toLowerCase();
          if (tagName === "vaadin-integer-field") {
            formData[dataIndex] = parseInt(item.value, 10);
          } else if (tagName === "vaadin-checkbox") {
            formData[dataIndex] = item.checked;
          } else {
            formData[dataIndex] = item.value;
          }
        }
      }
    });
    if (!t.isInvalid) {
      formData.user_permission = await t.userPermissionTree.getData(true);//t.permissionTreeData;
      for (const row of formData.user_permission) {
        row.email = formData.email;
      }
    } else {
      formData = {};
    }
    return formData;
  }

  private responsiveSteps: FormLayoutResponsiveStep[] = [
    // Use one column by default
    { minWidth: 0, columns: 1 },
    // Use two columns, if layout's width exceeds 500px
    { minWidth: '300px', columns: 2 },
  ];

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('render|', t.myElement.tagName.toLowerCase());
    //}
    const clientHeight: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 2) + 'px' : '600px';
    let formHeight: string = 'calc(100% - 0px)';
    let styleHeader: any = { height: "50px", width: 'calc(100% - 2px)', border: "1px solid var(--lumo-contrast-20pct)" };
    formHeight = 'calc(100% - 53px)';
    return (
      <div ref={(el) => t.contentContainer = el as HTMLDivElement}>
        <div ref={(el) => t.tabFilter = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>

        </div >
        <div ref={(el) => t.tabForm = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <div //ref={(el) => t.elContainerHeader = el as HTMLDivElement}
            style={styleHeader}>
            <vaadin-horizontal-layout
              class="flex--middle flex--wrap flex--right"
              theme="spacing"
              style={{ height: "100%", paddingLeft: "10px", paddingRight: "10px" }}
            >
              <vaadin-button theme="primary" ref={(el) => t.btnSave = el as Button}
                disabled={t.args.perm.add === 0}
                hidden={t.args.perm.add === 0}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'save' });
                }}>
                <vaadin-icon src={UIIcons['fa__check']} slot="prefix"></vaadin-icon>
                Kaydet
              </vaadin-button>
              <vaadin-button theme="primary success" ref={(el) => t.btnList = el as Button}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'list' });
                }}>
                <vaadin-icon src={UIIcons['fa__list']} slot="prefix"></vaadin-icon>
                Listele
              </vaadin-button>
              <vaadin-button theme="primary contrast" ref={(el) => t.btnClose = el as Button}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'close' });
                }}>
                <vaadin-icon src={UIIcons['fa__square_xmark']} slot="prefix"></vaadin-icon>
                Kapat
              </vaadin-button>
            </vaadin-horizontal-layout>
          </div>
          <div style={{ height: formHeight, width: "calc(100% - 2px)", border: "1px solid var(--lumo-contrast-20pct)", borderTop: "none" }} >
            <div class="flex flex--row" style={{ height: "100%", width: "100%" }}>
              <div style={{ height: "100%", width: "50%", overflowY: "auto" }}>
                <vaadin-form-layout ref={(el) => t.formLayout = el as FormLayout} responsiveSteps={t.responsiveSteps} style={{ padding: "10px" }}>
                  <vaadin-text-field
                    label="ID"
                    dataIndex="id"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>
                  <vaadin-text-field
                    label="version"
                    dataIndex="version"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>
                  <vaadin-text-field
                    label="Adı"
                    dataIndex="first_name"
                    max-length="50"
                    clear-button-visible
                    required
                  ></vaadin-text-field>
                  <vaadin-text-field
                    label="Soyadı"
                    dataIndex="last_name"
                    max-length="50"
                    clear-button-visible
                    required
                  ></vaadin-text-field>
                  <vaadin-email-field
                    colspan="2"
                    label="E-Posta"
                    dataIndex="email"
                    max-length="250"
                    clear-button-visible
                    required
                  ></vaadin-email-field>
                  <vaadin-password-field
                    label="Şifre"
                    dataIndex="password"
                    max-length="50"
                    clear-button-visible
                    required
                  ></vaadin-password-field>
                  <vaadin-password-field
                    label="Şifre (Tekrar)"
                    dataIndex="password_confirm"
                    max-length="50"
                    clear-button-visible
                    required
                  ></vaadin-password-field>
                  <vaadin-checkbox class="label" label="Aktif" dataIndex="is_active"></vaadin-checkbox>
                  <ui-upload
                    ref={(el) => t.avatar = el as unknown as UIUpload}
                    dataIndex="avatar"
                    target="/user/upload-handler"
                    accept="image/*"
                    viewTarget="uploads/"

                    on-fileReject={(e: CustomEvent) => {
                      //console.log('fileReject', e.detail);
                      t.appEvents({ type: 'file', element: 'fileReject', detail: e.detail });
                    }}
                    on-filesChanged={(e: CustomEvent) => {
                      if (e.detail.value.length > 0) {
                        console.log('filesChanged', e.detail);
                        t.appEvents({ type: 'file', element: 'filesChanged', detail: e.detail });
                      }
                    }}
                    on-maxFilesReachedChanged={(e: CustomEvent) => {
                      if (e.detail.value !== false) {
                        console.log('maxFilesReachedChanged', e.detail);
                        t.appEvents({ type: 'file', element: 'maxFilesReachedChanged', detail: e.detail });
                      }
                    }}
                    on-uploadBefore={(e: CustomEvent) => {
                      //console.log('uploadBefore', e.detail);
                      t.appEvents({ type: 'file', element: 'uploadBefore', detail: e.detail });
                    }}
                    on-uploadStart={(e: CustomEvent) => {
                      //console.log('uploadStart', e.detail);
                      t.appEvents({ type: 'file', element: 'uploadStart', detail: e.detail });
                    }}
                    on-uploadProgress={(e: CustomEvent) => {
                      //console.log('uploadProgress', e.detail);
                      t.appEvents({ type: 'file', element: 'uploadProgress', detail: e.detail });
                    }}
                    on-uploadResponse={(e: CustomEvent) => {
                      //console.log('uploadResponse', e.detail);
                      t.appEvents({ type: 'file', element: 'uploadResponse', detail: e.detail });
                    }}
                    on-uploadSuccess={(e: CustomEvent) => {
                      //console.log('uploadSuccess', e.detail);
                      //console.log('uploadSuccess', e.detail.xhr.responseText);
                      t.appEvents({ type: 'file', element: 'uploadSuccess', detail: e.detail });
                      try {
                        const resp: any = JSON.parse(e.detail.xhr.responseText);
                        //console.log(resp);
                        (e.target as unknown as UIUpload).value = resp.filename;
                      } catch (error) {

                      }
                    }}
                    on-uploadError={(e: CustomEvent) => {
                      //console.log('uploadError', e.detail);
                      t.appEvents({ type: 'file', element: 'uploadError', detail: e.detail });
                    }}
                    on-uploadRequest={(e: CustomEvent) => {
                      //console.log('uploadRequest', e.detail);
                      t.appEvents({ type: 'file', element: 'uploadRequest', detail: e.detail });
                    }}
                    on-uploadRetry={(e: CustomEvent) => {
                      //console.log('uploadRetry', e.detail);
                      t.appEvents({ type: 'file', element: 'uploadRetry', detail: e.detail });
                    }}
                    on-uploadAbort={(e: CustomEvent) => {
                      //console.log('uploadAbort', e.detail);
                      t.appEvents({ type: 'file', element: 'uploadAbort', detail: e.detail });
                    }}
                    on-fileDelete={(e: CustomEvent) => {
                      //console.log('fileDelete', e.detail);
                      t.appEvents({ type: 'file', element: 'fileDelete', detail: e.detail });
                    }}
                    on-fileOpen={(e: CustomEvent) => {
                      //console.log('fileOpen', e.detail);
                      t.appEvents({ type: 'file', element: 'fileOpen', detail: e.detail });
                    }}
                    on-fileView={(e: CustomEvent) => {
                      //console.log('fileView', e.detail);
                      t.appEvents({ type: 'file', element: 'fileView', detail: e.detail });
                    }}
                  ></ui-upload>
                </vaadin-form-layout>
              </div>
              <div style={{ height: "100%", width: "50%", paddingLeft: "10px", borderLeft: "1px solid var(--lumo-contrast-20pct)" }}>
                <ui-tree
                  ref={(el) => t.userPermissionTree = el as unknown as UITree}
                  height="100%" width="100%"
                  columns={[
                    { header: "Sayfa", dataIndex: 'title', width: 300 },
                    { header: "Gör", dataIndex: 'p_menu', width: 100, align: 'center' },
                    { header: "Ekle", dataIndex: 'p_add', width: 100, align: 'center' },
                    { header: "Düzenle", dataIndex: 'p_edit', width: 100, align: 'center' },
                    { header: "Sil", dataIndex: 'p_del', width: 100, align: 'center' },
                    { header: "Özel", dataIndex: 'custom_right', width: 100, align: 'center', triggerClick: true }
                  ]}
                  on-uiTreeColClickEvent={(e: CustomEvent) => t.appEvents({ type: 'custom_rights', item: e.detail })}
                ></ui-tree>
              </div>
            </div>
          </div>
        </div>
        <div ref={(el) => t.tabList = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <ui-grid
            ref={(el) => t.dataGrid = el as unknown as UIGrid}
            columns={[
              //{ label: 'Seç', dataIndex: 'select', style: { width: "60px", textAlign: "center" }, type: "toggle", columnSelect: true }
              new DataTableColumn({ label: 'Adı', dataIndex: 'first_name', style: { width: "200px" }, alias: "_users", filter: { field: 'text', type: 'between' } })
              , new DataTableColumn({ label: 'Soyadı', dataIndex: 'last_name', style: { width: "200px" }, filter: { field: 'text', type: 'between' } })
              , new DataTableColumn({ label: 'Aktif', dataIndex: 'is_active', style: { width: "100px", textAlign: "center" }, type: "toggle", filter: { field: 'radio', details: [{ value: "true", label: "Evet" }, { value: "false", label: "Hayır" }] } })
              , new DataTableColumn({ label: 'e-mail', dataIndex: 'email', filter: { field: 'email' } })
            ]}
            height={clientHeight}
            isShowFooter={false}
            isSortMultiple={true}
            hiddenPageBarButtons={(t.args.perm.del === 0 ? ['del'] : [])}
            on-uiDataSourceEvent={(e: CustomEvent) => t.appEvents(e.detail)}
          ></ui-grid>
        </div>
      </div >
    );
  }
}
