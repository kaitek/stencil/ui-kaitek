import { Build, Component, Element, Event, EventEmitter, Method, Prop, h } from '@stencil/core';

import { DataTableColumn } from '../px-interfaces';
import { UIFunctions } from '../ui-functions/ui-functions';
import { UIGrid } from '../ui-grid/ui-grid';
import * as UIIcons from '../ui-icons';

import { Button } from '@vaadin/button';

import '@vaadin/button';
import '@vaadin/checkbox';
import '@vaadin/form-layout';
import '@vaadin/horizontal-layout';
import '@vaadin/icon';
import '@vaadin/text-field';

import type { FormLayout, FormLayoutResponsiveStep } from '@vaadin/form-layout';

@Component({
  tag: 'app-empty',
  styleUrl: 'app-empty.scss',
  shadow: true
})
export class AppEmpty {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Prop() args: any = { idx: 1, clientHeight: 600, perm: { add: 1, edit: 1 } };

  uiFns: UIFunctions = new UIFunctions();

  contentContainer: HTMLDivElement;
  tabFilter: HTMLDivElement;
  tabForm: HTMLDivElement;
  tabList: HTMLDivElement;
  filterLayout: FormLayout;
  formLayout: FormLayout;
  dataGrid: UIGrid;
  btnClose: Button;
  btnList: Button;
  btnSave: Button;
  btnFilter: Button;
  btnClear: Button;
  btnBack: Button;

  currentPage: number = 1;
  recordsPerPage: number = 25;
  totalRecords: number = 0;

  selectedRecord: any = {};
  filterData: any[] = [];
  sortData: any[] = [];

  isInvalid: boolean = false;
  baseRequestParams: any;

  async componentWillLoad() {
    //let t = this;
    //if (Build.isDev) {
    //  console.log('CWL|', t.myElement.tagName.toLowerCase());
    //}
  }

  async componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    t.baseRequestParams = {
      emitter: t.appEvent, controller: '', tagName: t.myElement.tagName.toLowerCase(), idx: t.args.idx, recordsPerPage: t.recordsPerPage
    };
    //const els = document.getElementsByTagName('html');
    //t.isDev = typeof els[0].attributes["isdev"] !== 'undefined';
    await t.uiFns.hideAllContainers(t.contentContainer);
    if (t.tabList.classList.contains('hidden')) {
      t.tabList.classList.remove('hidden');
    }
    t.filterLayout = await t.uiFns.generateFilterForm(t, t.tabFilter, t.dataGrid.columns, t.appEvents);
    //await t.uiFns.generateFilterForm(t.tabFilter, t.dataGrid.columns);
    //t.filterLayout = t.tabFilter.querySelector('vaadin-form-layout');
    //const itemsFireClick = t.tabFilter.querySelectorAll('vaadin-button');
    //if (itemsFireClick.length > 0) {
    //  await t.uiFns.asyncForEach(itemsFireClick, async (item: Button) => {
    //    item.addEventListener('click', t.appEvents.bind(t, { type: 'button', element: item.part[0] }), { passive: true });
    //  });
    //}
    //@@todo: custom_right
    //console.log(t.args.perm);
    //t.appEvent.emit({ event: 'componentDidLoad', component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
    await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
    if (Build.isDev) {
      await t.uiFns.delay(500);
      await t.dataGrid.afterLoad();
    }
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
    //}
    switch (obj.element) {
      case 'dataGrid': {
        t.currentPage = obj.currentPage;
        t.recordsPerPage = obj.recordsPerPage;
        t.totalRecords = obj.totalRecords;
        t.dataGrid.setData(obj);
        break;
      }
      default: {
        if (Build.isDev) {
          console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
        }
        break;
      }
    }
  }

  @Method()
  async callbackRequestData(obj: any, resp: any) {
    let t = this;
    if (Build.isDev) {
      console.log('CBRD|', t.myElement.tagName.toLowerCase(), obj, resp);
    }
    await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
    return await t.appEvents({ type: 'button', element: 'list' });
  }

  @Method()
  async resize(height: number) {
    let t = this;
    //console.log('resize|', t.myElement.tagName.toLowerCase(), height, t.cacheData);
    t.tabFilter.style.height = (height - 2) + 'px';
    t.tabForm.style.height = (height - 2) + 'px';
    t.tabList.style.height = (height - 2) + 'px';
    t.dataGrid.height = (height - 2) + 'px';
    setTimeout(async () => {
      await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
    }, 50);
  }

  async appEvents(obj: any) {
    let t = this;
    if (Build.isDev) {
      console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
    }
    switch (obj.type) {
      case 'button': {
        switch (obj.element) {
          case 'first': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            break;
          }
          case 'prev': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: (t.currentPage - 1), where: t.filterData, order: t.sortData });
            break;
          }
          case 'next': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: (t.currentPage + 1), where: t.filterData, order: t.sortData });
            break;
          }
          case 'last': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: Math.ceil(t.totalRecords / t.recordsPerPage), where: t.filterData, order: t.sortData });
            break;
          }
          case 'add': {
            //t.appEvent.emit({ event: 'btn-' + obj.element, component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
            t.btnClose.disabled = false;
            t.btnList.disabled = false;
            if (t.args.perm.add !== 0) {
              t.btnSave.disabled = false;
            }
            t.dataGrid.clearSelectedRows();
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabForm.classList.contains('hidden')) {
              t.tabForm.classList.remove('hidden');
            }
            await t.uiFns.setFormValues(t.formLayout, {});
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'edit': {
            t.btnClose.disabled = false;
            t.btnList.disabled = false;
            if (t.args.perm.add !== 0) {
              t.btnSave.disabled = false;
            }
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabForm.classList.contains('hidden')) {
              t.tabForm.classList.remove('hidden');
            }
            await t.uiFns.setFormValues(t.formLayout, t.selectedRecord);
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'delete': {
            //const values: any = await t.getFormValues();
            if (t.args.perm.del === 0 && t.selectedRecord.id !== '') {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Bu işlem için yetkili değilsiniz.'
              });
            } else {
              t.appEvent.emit({
                event: 'sendData'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , data: t.selectedRecord
                , id: t.selectedRecord.id
                , method: 'delete'
              });
            }
            break;
          }
          case 'refresh': {
            await t.uiFns.setFormValues(t.filterLayout, {});
            t.filterData = [];
            t.sortData = [];
            t.dataGrid.clearSortData();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
            break;
          }
          case 'filter': {
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabFilter.classList.contains('hidden')) {
              t.tabFilter.classList.remove('hidden');
            }
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'save': {
            const values: any = await t.uiFns.getFormValues(t.formLayout);
            if (!values.isInvalid) {
              if (t.args.perm.add === 0 || (t.args.perm.add === 1 && t.args.perm.edit === 0 && values.data.id !== '')) {
                t.rendererEvent.emit({
                  event: 'showNotification'
                  , component: t.myElement.tagName.toLowerCase()
                  , idx: t.args.idx
                  , message: 'Bu işlem için yetkili değilsiniz.'
                });
                t.btnClose.disabled = false;
                t.btnList.disabled = false;
                if (t.args.perm.add !== 0) {
                  t.btnSave.disabled = false;
                }
              } else {
                t.appEvent.emit({
                  event: 'sendData'
                  , component: t.myElement.tagName.toLowerCase()
                  , idx: t.args.idx
                  , data: values.data
                  , method: (typeof values.data.id !== 'undefined' && values.data.id !== '' ? 'patch' : 'post')
                });
              }
            } else {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Gerekli alanları eksiksiz olarak doldurunuz.'
              });
              t.btnClose.disabled = false;
              t.btnList.disabled = false;
              if (t.args.perm.add !== 0) {
                t.btnSave.disabled = false;
              }
            }
            break;
          }
          case 'list': {
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.setFormValues(t.formLayout, {});
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
            break;
          }
          case 'close': {
            t.rendererEvent.emit({
              event: 'close'
              , component: t.myElement.tagName.toLowerCase()
              , idx: t.args.idx
            });
            break;
          }
          case 'back': {
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            break;
          }
          case 'filter_records': {
            //console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            t.filterData = await t.uiFns.getFilterValues(t.filterLayout);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          case 'filter_clear': {
            await t.uiFns.setFormValues(t.filterLayout, {});
            t.filterData = [];
            t.sortData = [];
            t.dataGrid.clearSortData();
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          default: {
            console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            break;
          }
        }
        break;
      }
      case 'change-currentPage': {
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: parseInt(obj.data, 10), where: t.filterData, order: t.sortData });
        break;
      }
      case 'change-recordsPerPage': {
        t.recordsPerPage = parseInt(obj.data, 10);
        t.baseRequestParams.recordsPerPage = t.recordsPerPage;
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
        break;
      }
      case 'col-sort': {
        t.sortData = obj.data;
        //console.log('col-sort',t.sortData);
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
        break;
      }
      case 'row-dblclick': {
        t.selectedRecord = obj.data;
        t.appEvents({ type: 'button', element: 'edit' });
        break;
      }
      case 'row-select': {
        t.selectedRecord = obj.data;
        break;
      }
      default: {
        console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
        break;
      }
    }
  }

  private responsiveSteps: FormLayoutResponsiveStep[] = [
    // Use one column by default
    { minWidth: 0, columns: 1 },
    // Use two columns, if layout's width exceeds 500px
    { minWidth: '500px', columns: 2 },
  ];

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('render|', t.myElement.tagName.toLowerCase());
    //}
    const clientHeight: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 2) + 'px' : '600px';
    let formHeight: string = 'calc(100% - 0px)';
    let styleHeader: any = { height: "50px", width: 'calc(100% - 2px)', border: "1px solid var(--lumo-contrast-20pct)" };
    formHeight = 'calc(100% - 53px)';
    return (
      <div ref={(el) => t.contentContainer = el as HTMLDivElement}>
        <div ref={(el) => t.tabFilter = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>

        </div >
        <div ref={(el) => t.tabForm = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <div //ref={(el) => t.elContainerHeader = el as HTMLDivElement}
            style={styleHeader}>
            <vaadin-horizontal-layout
              class="flex--middle flex--wrap flex--right"
              theme="spacing"
              style={{ height: "100%", paddingLeft: "10px", paddingRight: "10px" }}
            >
              <vaadin-button theme="primary" ref={(el) => t.btnSave = el as Button}
                disabled={t.args.perm.add === 0}
                hidden={t.args.perm.add === 0}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'save' });
                }}>
                <vaadin-icon src={UIIcons['fa__check']} slot="prefix"></vaadin-icon>
                Kaydet
              </vaadin-button>
              <vaadin-button theme="primary success" ref={(el) => t.btnList = el as Button}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'list' });
                }}>
                <vaadin-icon src={UIIcons['fa__list']} slot="prefix"></vaadin-icon>
                Listele
              </vaadin-button>
              <vaadin-button theme="primary contrast" ref={(el) => t.btnClose = el as Button}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'close' });
                }}>
                <vaadin-icon src={UIIcons['fa__square_xmark']} slot="prefix"></vaadin-icon>
                Kapat
              </vaadin-button>
            </vaadin-horizontal-layout>
          </div>
          <div style={{ height: formHeight, width: "calc(100% - 2px)", border: "1px solid var(--lumo-contrast-20pct)", borderTop: "none" }} >
            <div class="flex flex--row flex--center" style={{ height: "100%", width: "100%" }}>
              <div style={{ height: "100%", width: "50%", overflowY: "auto" }}>
                <vaadin-form-layout ref={(el) => t.formLayout = el as FormLayout} responsiveSteps={t.responsiveSteps} style={{ padding: "10px" }}>
                  <vaadin-text-field
                    label="ID"
                    dataIndex="id"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>
                  <vaadin-text-field
                    label="version"
                    dataIndex="version"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>

                </vaadin-form-layout>
              </div>
            </div>
          </div>
        </div >
        <div ref={(el) => t.tabList = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <ui-grid
            ref={(el) => t.dataGrid = el as unknown as UIGrid}
            columns={[
              //{ label: 'Seç', dataIndex: 'select', style: { width: "60px", textAlign: "center" }, type: "toggle", columnSelect: true }
              new DataTableColumn({ label: 'Stok Kodu', dataIndex: 'code', style: { width: "150px", maxWidth: "200px" }, alias: "empty", filter: { field: 'text', type: 'between' } })
              , new DataTableColumn({ label: 'Barkod', dataIndex: 'barcode', style: { width: "150px" }, filter: { field: 'text', type: 'between' } })
              , new DataTableColumn({ label: 'Stok Adı', dataIndex: 'name', style: { width: "600px" }, filter: { field: 'text', type: 'between' } })
              //, new DataTableColumn({ label: 'Fiyatı', dataIndex: 'price', style: { width: "120px", textAlign: "right" }, filter: { field: 'number', type: 'between' } })
              //, new DataTableColumn({ label: 'Stok', dataIndex: 'quantity', style: { width: "120px", textAlign: "right" }, filter: { field: 'number', type: 'between' } })
              //, new DataTableColumn({ label: 'Stok Takibi', dataIndex: 'is_tracking', style: { width: "120px", textAlign: "center" }, type: "toggle", filter: { field: 'radio', details: [{ value: "true", label: "Yapılsın" }, { value: "false", label: "Yapılmasın" }] } })
              //, new DataTableColumn({ label: 'Aktif', dataIndex: 'is_active', style: { width: "120px", textAlign: "center" }, type: "toggle", filter: { field: 'radio', details: [{ value: "true", label: "Evet" }, { value: "false", label: "Hayır" }] } })
            ]}
            height={clientHeight}
            isShowFooter={false}
            isSortMultiple={true}
            hiddenPageBarButtons={(t.args.perm.del === 0 ? ['del'] : [])}
            on-uiDataSourceEvent={(e: CustomEvent) => t.appEvents(e.detail)}
          ></ui-grid>
        </div >
      </div >
    );
  }
}
