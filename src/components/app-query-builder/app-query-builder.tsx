import { Component, Element, Event, EventEmitter, h, Prop } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';
import { IQueryBuilderOptions, QueryBuilderFilterItem, QueryBuilderInputOptions, QueryBuilderInputValidation, QueryBuilderOptions } from '../px-interfaces';
import '@vaadin/button';
import '@vaadin/checkbox';
import '@vaadin/checkbox-group';
import '@vaadin/combo-box';
import '@vaadin/date-picker';
import '@vaadin/icon';
import '@vaadin/integer-field';
import '@vaadin/item';
import '@vaadin/list-box';
import '@vaadin/multi-select-combo-box';
import '@vaadin/number-field';
import '@vaadin/radio-group';
import '@vaadin/select';
import '@vaadin/text-field';
import { UIQueryBuilder } from '../ui-query-builder/ui-query-builder';
@Component({
  tag: 'app-query-builder',
  styleUrl: 'app-query-builder.scss',
  shadow: true
})
export class AppQueryBuilder {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Prop() args: any = {};

  uiFns: UIFunctions = new UIFunctions();

  qb: UIQueryBuilder;
  options: IQueryBuilderOptions;

  async componentDidLoad() {
    let t = this;

    t.options = t.args?.options || new QueryBuilderOptions({
      filters: [
        new QueryBuilderFilterItem({
          id: "client"
          , field: "cpd.client"
          , label: "İstasyon"
          , type: "string"
          //, unique: true
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "clientint"
          , field: "cpd.client"
          , label: "İstasyon-Int"
          , type: "integer"
          //, unique: true
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "clientnumber"
          , field: "cpd.client"
          , label: "İstasyon-Number"
          , type: "integer"
          , input: "number"
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "day"
          , field: "cpd.day"
          , label: "Gün"
          , type: "date"
          //, unique: true
          , validation: new QueryBuilderInputValidation({ format: "YYYY-MM-DD" })
          //, input_options: {
          //, plugin: "datepicker"
          //, plugin_config: { format: "yyyy-mm-dd", todayBtn: "linked", todayHighlight: true, autoclose: true, language: "tr" }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "jobrotation"
          , field: "cpd.jobrotation"
          , label: "Vardiya"
          , type: "string"
          , input: "select"
          , input_options: new QueryBuilderInputOptions({
            multiple: true
            , values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
            //, plugin: "selectize"
            //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
          })
          , operators: ["in", "not_in", "is_empty", "is_not_empty", "is_null", "is_not_null"]
        })
        , new QueryBuilderFilterItem({
          id: "jobrotationcheck"
          , field: "cpd.jobrotation"
          , label: "Vardiya-Check"
          , type: "string"
          , input: "checkbox"
          , input_options: new QueryBuilderInputOptions({
            values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
            //, plugin: "selectize"
            //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
          })
          , operators: ["in", "not_in", "is_empty", "is_not_empty", "is_null", "is_not_null"]
        })
        , new QueryBuilderFilterItem({
          id: "jobrotationcombo"
          , field: "cpd.jobrotation"
          , label: "Vardiya-Combo"
          , type: "string"
          , input: "combobox"
          , input_options: new QueryBuilderInputOptions({
            values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
            , path: "id"
            //, plugin: "selectize"
            //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
          })
          , operators: ["in", "not_in", "is_empty", "is_not_empty", "is_null", "is_not_null"]
        })
        , new QueryBuilderFilterItem({
          id: "jobrotationradio"
          , field: "cpd.jobrotation"
          , label: "Vardiya-Radio"
          , type: "string"
          , input: "radio"
          , input_options: new QueryBuilderInputOptions({
            values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
            //, plugin: "selectize"
            //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
          })
          , operators: ["equal"]
        })
        , new QueryBuilderFilterItem({
          id: "opname"
          , field: "cpd.opname"
          , label: "Operasyon"
          , type: "string"
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "erprefnumber"
          , field: "cpd.erprefnumber"
          , label: "Erp Ref Number"
          , type: "string"
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
      ]
      , rules: [
        /*new QueryBuilderRules({
          "condition": "AND",
          "rules": [
            new QueryBuilderRuleItem({
              "id": "day",
              "field": "cpd.day",
              "type": "date",
              "input": "date",
              "operator": "between",
              value: "Gün",
              flags: {
                filter_readonly: true,
                no_delete: true
              }
            }),
            new QueryBuilderRuleItem({
              "id": "jobrotation",
              "field": "cpd.jobrotation",
              "type": "string",
              "input": "select",
              "operator": "in",
              "value": "Vardiya"
            }),
            new QueryBuilderRuleItem({
              "id": "jobrotationcheck",
              "field": "cpd.jobrotation",
              "type": "string",
              "input": "checkbox",
              "operator": "in",
              "value": "Vardiya-Check"
            }),
            new QueryBuilderRuleItem({
              "id": "jobrotationradio",
              "field": "cpd.jobrotation",
              "type": "string",
              "input": "radio",
              "operator": "equal",
              "value": "Vardiya-Radio"
            }),
            new QueryBuilderRuleItem({
              "id": "client",
              "field": "cpd.client",
              "type": "string",
              "input": "text",
              "operator": "in",
              "value": "İstasyon"
            }),
            new QueryBuilderRuleItem({
              "id": "clientint",
              "field": "category",
              "type": "number",
              "input": "integer",
              "operator": "equal",
              "value": "İstasyon-Int"
            }),
            new QueryBuilderRuleItem({
              "id": "clientnumber",
              "field": "category",
              "type": "number",
              "input": "integer",
              "operator": "equal",
              "value": "İstasyon-Number"
            }),
            //new QueryBuilderRules({
            //  "condition": "OR",
            //  "rules": [
            //    new QueryBuilderRuleItem({
            //      "id": "client",
            //      "field": "category",
            //      "type": "integer",
            //      "input": "checkbox",
            //      "operator": "in",
            //      "value": [
            //        1,
            //        2
            //      ]
            //    }),
            //    new QueryBuilderRuleItem({
            //      "id": "jobrotation",
            //      "field": "in_stock",
            //      "type": "integer",
            //      "input": "radio",
            //      "operator": "equal",
            //      "value": 0
            //    }),
            //    new QueryBuilderRules({
            //      "condition": "AND",
            //      "rules": [
            //        new QueryBuilderRuleItem({
            //          "id": "opname",
            //          "field": "price",
            //          "type": "double",
            //          "input": "number",
            //          "operator": "less_or_equal",
            //          "value": 200
            //        })
            //      ],
            //      "not": false
            //    })
            //  ],
            //  "not": false
            //}),
            //new QueryBuilderRules({
            //  "condition": "AND",
            //  "rules": [
            //    new QueryBuilderRuleItem({
            //      "id": "erprefnumber",
            //      "field": "in_stock",
            //      "type": "integer",
            //      "input": "radio",
            //      "operator": "equal",
            //      "value": 1
            //    })
            //  ],
            //  "not": true
            //})
          ],
          "not": true,
          "valid": true
        })*/
      ]
    });
  }

  async appEvents(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //  console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
    //}
    switch (obj.type) {
      case 'condition_change': {
        console.log(t.myElement.tagName.toLowerCase(), obj);
        break;
      }
      default: {
        console.log(t.myElement.tagName.toLowerCase(), 'appEvents', obj);
        break;
      }
    }
  }

  render() {
    let t = this;
    const argument = {
      options: new QueryBuilderOptions({
        filters: [
          new QueryBuilderFilterItem({
            id: "client"
            , field: "cpd.client"
            , label: "İstasyon"
            , type: "string"
            //, unique: true
            , input_options: new QueryBuilderInputOptions({
              value_separator: ","
              , size: 30
            })
            , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
            //, description: (rule) => {
            //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
            //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
            //  }
            //}
          })
          , new QueryBuilderFilterItem({
            id: "clientint"
            , field: "cpd.client"
            , label: "İstasyon-Int"
            , type: "integer"
            //, unique: true
            , input_options: new QueryBuilderInputOptions({
              value_separator: ","
              , size: 30
            })
            , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
            //, description: (rule) => {
            //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
            //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
            //  }
            //}
          })
          , new QueryBuilderFilterItem({
            id: "clientnumber"
            , field: "cpd.client"
            , label: "İstasyon-Number"
            , type: "integer"
            , input: "number"
            , input_options: new QueryBuilderInputOptions({
              value_separator: ","
              , size: 30
            })
            , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
            //, description: (rule) => {
            //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
            //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
            //  }
            //}
          })
          , new QueryBuilderFilterItem({
            id: "day"
            , field: "cpd.day"
            , label: "Gün"
            , type: "date"
            //, unique: true
            , validation: new QueryBuilderInputValidation({ format: "YYYY-MM-DD" })
            //, input_options: {
            //, plugin: "datepicker"
            //, plugin_config: { format: "yyyy-mm-dd", todayBtn: "linked", todayHighlight: true, autoclose: true, language: "tr" }
            //}
          })
          , new QueryBuilderFilterItem({
            id: "jobrotation"
            , field: "cpd.jobrotation"
            , label: "Vardiya"
            , type: "string"
            , input: "select"
            , input_options: new QueryBuilderInputOptions({
              multiple: true
              , values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
              //, plugin: "selectize"
              //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
            })
            , operators: ["in", "not_in", "is_empty", "is_not_empty", "is_null", "is_not_null"]
          })
          , new QueryBuilderFilterItem({
            id: "jobrotationcheck"
            , field: "cpd.jobrotation"
            , label: "Vardiya-Check"
            , type: "string"
            , input: "checkbox"
            , input_options: new QueryBuilderInputOptions({
              values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
              //, plugin: "selectize"
              //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
            })
            , operators: ["in", "not_in", "is_empty", "is_not_empty", "is_null", "is_not_null"]
          })
          , new QueryBuilderFilterItem({
            id: "jobrotationcombo"
            , field: "cpd.jobrotation"
            , label: "Vardiya-Combo"
            , type: "string"
            , input: "combobox"
            , input_options: new QueryBuilderInputOptions({
              values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
              , path: "id"
              //, plugin: "selectize"
              //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
            })
            , operators: ["in", "not_in", "is_empty", "is_not_empty", "is_null", "is_not_null"]
          })
          , new QueryBuilderFilterItem({
            id: "jobrotationradio"
            , field: "cpd.jobrotation"
            , label: "Vardiya-Radio"
            , type: "string"
            , input: "radio"
            , input_options: new QueryBuilderInputOptions({
              values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
              //, plugin: "selectize"
              //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
            })
            , operators: ["equal"]
          })
          , new QueryBuilderFilterItem({
            id: "opname"
            , field: "cpd.opname"
            , label: "Operasyon"
            , type: "string"
            , input_options: new QueryBuilderInputOptions({
              value_separator: ","
              , size: 30
            })
            , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
            //, description: (rule) => {
            //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
            //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
            //  }
            //}
          })
          , new QueryBuilderFilterItem({
            id: "erprefnumber"
            , field: "cpd.erprefnumber"
            , label: "Erp Ref Number"
            , type: "string"
            , input_options: new QueryBuilderInputOptions({
              value_separator: ","
              , size: 30
            })
            , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
            //, description: (rule) => {
            //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
            //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
            //  }
            //}
          })
        ]
        , rules: [
          /*new QueryBuilderRules({
            "condition": "AND",
            "rules": [
              new QueryBuilderRuleItem({
                "id": "day",
                "field": "cpd.day",
                "type": "date",
                "input": "date",
                "operator": "between",
                value: "Gün",
                flags: {
                  filter_readonly: true,
                  no_delete: true
                }
              }),
              new QueryBuilderRuleItem({
                "id": "jobrotation",
                "field": "cpd.jobrotation",
                "type": "string",
                "input": "select",
                "operator": "in",
                "value": "Vardiya"
              }),
              new QueryBuilderRuleItem({
                "id": "jobrotationcheck",
                "field": "cpd.jobrotation",
                "type": "string",
                "input": "checkbox",
                "operator": "in",
                "value": "Vardiya-Check"
              }),
              new QueryBuilderRuleItem({
                "id": "jobrotationradio",
                "field": "cpd.jobrotation",
                "type": "string",
                "input": "radio",
                "operator": "equal",
                "value": "Vardiya-Radio"
              }),
              new QueryBuilderRuleItem({
                "id": "client",
                "field": "cpd.client",
                "type": "string",
                "input": "text",
                "operator": "in",
                "value": "İstasyon"
              }),
              new QueryBuilderRuleItem({
                "id": "clientint",
                "field": "category",
                "type": "number",
                "input": "integer",
                "operator": "equal",
                "value": "İstasyon-Int"
              }),
              new QueryBuilderRuleItem({
                "id": "clientnumber",
                "field": "category",
                "type": "number",
                "input": "integer",
                "operator": "equal",
                "value": "İstasyon-Number"
              }),
              //new QueryBuilderRules({
              //  "condition": "OR",
              //  "rules": [
              //    new QueryBuilderRuleItem({
              //      "id": "client",
              //      "field": "category",
              //      "type": "integer",
              //      "input": "checkbox",
              //      "operator": "in",
              //      "value": [
              //        1,
              //        2
              //      ]
              //    }),
              //    new QueryBuilderRuleItem({
              //      "id": "jobrotation",
              //      "field": "in_stock",
              //      "type": "integer",
              //      "input": "radio",
              //      "operator": "equal",
              //      "value": 0
              //    }),
              //    new QueryBuilderRules({
              //      "condition": "AND",
              //      "rules": [
              //        new QueryBuilderRuleItem({
              //          "id": "opname",
              //          "field": "price",
              //          "type": "double",
              //          "input": "number",
              //          "operator": "less_or_equal",
              //          "value": 200
              //        })
              //      ],
              //      "not": false
              //    })
              //  ],
              //  "not": false
              //}),
              //new QueryBuilderRules({
              //  "condition": "AND",
              //  "rules": [
              //    new QueryBuilderRuleItem({
              //      "id": "erprefnumber",
              //      "field": "in_stock",
              //      "type": "integer",
              //      "input": "radio",
              //      "operator": "equal",
              //      "value": 1
              //    })
              //  ],
              //  "not": true
              //})
            ],
            "not": true,
            "valid": true
          })*/
        ]
      })
      , statement: "named"
    };
    return (
      <div class="container">
        <ui-query-builder
          ref={(el) => t.qb = el as unknown as UIQueryBuilder}
          args={argument}
          on-pxQueryBuilderConditionChange={(e: CustomEvent) => t.appEvents({ type: 'condition_change', data: e.detail })}
        ></ui-query-builder>
      </div>
    );
  }
}
