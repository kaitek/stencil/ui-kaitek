import { Build, Component, Element, Event, EventEmitter, Method, Prop, h } from '@stencil/core';

import { DataTableColumn } from '../px-interfaces';
import { UIFunctions } from '../ui-functions/ui-functions';
import { UIGrid } from '../ui-grid/ui-grid';
import { UIGridEditable } from '../ui-grid/ui-grid-editable';
import * as UIIcons from '../ui-icons';
import { UILoadingIndicator } from '../ui-loading-indicator/ui-loading-indicator';

import { Button } from '@vaadin/button';
//import { Dialog } from '@vaadin/dialog';

import '@vaadin/button';
import '@vaadin/checkbox';
import '@vaadin/date-picker';
import '@vaadin/dialog';
import '@vaadin/form-layout';
import '@vaadin/horizontal-layout';
import '@vaadin/icon';
import '@vaadin/select';
import '@vaadin/text-area';
import '@vaadin/text-field';

import { DatePicker } from '@vaadin/date-picker';
import type { FormLayout, FormLayoutResponsiveStep } from '@vaadin/form-layout';
import { NumberField } from '@vaadin/number-field';
import { TextArea, TextAreaValueChangedEvent } from '@vaadin/text-area';
import { TextField } from '@vaadin/text-field';

@Component({
  tag: 'app-invoice-sell',
  styleUrl: 'app-invoice-sell.scss',
  shadow: true
})
export class AppInvoiceSell {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Prop() args: any = { idx: 1, clientHeight: 600, perm: { add: 1, edit: 1 } };

  uiFns: UIFunctions = new UIFunctions();

  contentContainer: HTMLDivElement;
  receiptGridContainer: HTMLDivElement;
  tabFilter: HTMLDivElement;
  tabForm: HTMLDivElement;
  tabList: HTMLDivElement;
  filterLayout: FormLayout;
  formLayout: FormLayout;
  dataGrid: UIGrid;
  receiptGrid: UIGridEditable;
  dialogLookup: any;
  btnClose: Button;
  btnList: Button;
  btnSave: Button;
  btnFilter: Button;
  btnClear: Button;
  btnBack: Button;

  currentPage: number = 1;
  recordsPerPage: number = 25;
  totalRecords: number = 0;

  selectedRecord: any = {};
  filterData: any[] = [];
  sortData: any[] = [];

  isInvalid: boolean = false;
  baseRequestParams: any;

  elLoadingIndicator: UILoadingIndicator;

  elReceiptSerial: TextField;
  elReceiptNumber: NumberField;
  elReceiptDate: DatePicker;
  elDescription: TextArea;
  elCustomerId: TextField;
  elCustomerLookup: TextField;

  elAmount: Text;
  elTaxAmount: Text;
  elTotal: Text;

  async componentWillLoad() {
    //let t = this;
    //if (Build.isDev) {
    //  console.log('CWL|', t.myElement.tagName.toLowerCase());
    //}
  }

  async componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    t.baseRequestParams = {
      emitter: t.appEvent
      , controller: ''
      , tagName: t.myElement.tagName.toLowerCase()
      , idx: t.args.idx
      , recordsPerPage: t.recordsPerPage
    };
    //const els = document.getElementsByTagName('html');
    //t.isDev = typeof els[0].attributes["isdev"] !== 'undefined';
    await t.uiFns.hideAllContainers(t.contentContainer);
    if (t.tabList.classList.contains('hidden')) {
      t.tabList.classList.remove('hidden');
    }
    t.filterLayout = await t.uiFns.generateFilterForm(t, t.tabFilter, t.dataGrid.columns, t.appEvents);
    //await t.uiFns.generateFilterForm(t.tabFilter, t.dataGrid.columns);
    //t.filterLayout = t.tabFilter.querySelector('vaadin-form-layout');
    //const itemsFireClick = t.tabFilter.querySelectorAll('vaadin-button');
    //if (itemsFireClick.length > 0) {
    //  await t.uiFns.asyncForEach(itemsFireClick, async (item: Button) => {
    //    item.addEventListener('click', t.appEvents.bind(t, { type: 'button', element: item.part[0] }), { passive: true });
    //  });
    //}

    const dp: NodeListOf<DatePicker> = t.tabForm.querySelectorAll("vaadin-date-picker");
    const dpf: NodeListOf<DatePicker> = t.tabFilter.querySelectorAll("vaadin-date-picker");
    //console.log(t.myElement.tagName.toLowerCase(), Array.from(dp).concat(Array.from(dpf)));
    await t.uiFns.asyncForEach(Array.from(dp).concat(Array.from(dpf)), async (item: DatePicker) => {
      item.i18n = {
        ...item.i18n,
        formatDate: t.uiFns.dateFormatIso8601,
        parseDate: t.uiFns.dateParseIso8601,
        firstDayOfWeek: 1,
        monthNames: [
          'Ocak',
          'Şubat',
          'Mart',
          'Nisan',
          'Mayıs',
          'Haziran',
          'Temmuz',
          'Ağustos',
          'Eylül',
          'Ekim',
          'Kasım',
          'Aralık',
        ],
        weekdays: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
        weekdaysShort: ['Paz', 'Pzt', 'Sal', 'Çrş', 'Prş', 'Cum', 'Cmt'],
        today: 'Bugün',
        cancel: 'Vazgeç',
      };
    });
    //@@todo: custom_right
    //console.log(t.args.perm);
    //t.appEvent.emit({ event: 'componentDidLoad', component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
    await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
    if (Build.isDev) {
      await t.uiFns.delay(500);
      await t.dataGrid.afterLoad();
    }
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
    //}
    switch (obj.element) {
      case 'dataGrid': {
        t.currentPage = obj.currentPage;
        t.recordsPerPage = obj.recordsPerPage;
        t.totalRecords = obj.totalRecords;
        t.dataGrid.setData(obj);
        break;
      }
      case 'dialogLookup': {
        t.dialogLookup.dGrid.setData(obj);
        break;
      }
      case 'formLayout': {
        if (obj.totalRecords > 0) {
          //await t.uiFns.setFormValues(t.formLayout, t.selectedRecord);
          await t.uiFns.setFormValues(t.formLayout, obj.records[0]);
          await t.createReceiptGrid(obj.receipt_detail);
          //await t.receiptGrid.setData({ records: obj.receipt_detail });
        }
        t.elLoadingIndicator.setVisible(false);
        //console.log('setItemData', obj);
        break;
      }
      default: {
        if (Build.isDev) {
          console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
        }
        break;
      }
    }
  }

  @Method()
  async callbackRequestData(obj: any, resp: any) {
    let t = this;
    if (Build.isDev) {
      console.log('CBRD|', t.myElement.tagName.toLowerCase(), obj, resp);
    }
    await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
    return await t.appEvents({ type: 'button', element: 'list', eventEmit: false });
  }

  @Method()
  async resize(height: number) {
    let t = this;
    //console.log('resize|', t.myElement.tagName.toLowerCase(), height);
    t.tabFilter.style.height = (height - 2) + 'px';
    t.tabForm.style.height = (height - 2) + 'px';
    t.tabList.style.height = (height - 2) + 'px';
    t.dataGrid.height = (height - 2) + 'px';
    setTimeout(async () => {
      await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
    }, 50);
  }

  async appEvents(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
    //}
    switch (obj.type) {
      case 'button': {
        switch (obj.element) {
          case 'first': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            break;
          }
          case 'prev': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: (t.currentPage - 1), where: t.filterData, order: t.sortData });
            break;
          }
          case 'next': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: (t.currentPage + 1), where: t.filterData, order: t.sortData });
            break;
          }
          case 'last': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: Math.ceil(t.totalRecords / t.recordsPerPage), where: t.filterData, order: t.sortData });
            break;
          }
          case 'add': {
            //t.appEvent.emit({ event: 'btn-' + obj.element, component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
            t.btnClose.disabled = false;
            t.btnList.disabled = false;
            if (t.args.perm.add !== 0) {
              t.btnSave.disabled = false;
            }
            t.dataGrid.clearSelectedRows();
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabForm.classList.contains('hidden')) {
              t.tabForm.classList.remove('hidden');
            }
            await t.createReceiptGrid([]);
            await t.uiFns.setFormValues(t.formLayout, {});
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'edit': {
            t.btnClose.disabled = false;
            t.btnList.disabled = false;
            if (t.args.perm.add !== 0) {
              t.btnSave.disabled = false;
            }
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabForm.classList.contains('hidden')) {
              t.tabForm.classList.remove('hidden');
            }
            t.elLoadingIndicator.setVisible(true);
            await t.createReceiptGrid([]);
            //@@info:seçili kaydın bilgisi sunucudan istenecek
            //await t.uiFns.setFormValues(t.formLayout, t.selectedRecord);
            //console.log('t.selectedRecord:', t.selectedRecord);
            //await t.createReceiptGrid(t.selectedRecord.receipt_type);
            //await t.dataGrid.afterLoad();
            await t.uiFns.requestData(t.baseRequestParams
              , {
                element: t.formLayout
                , elementName: 'formLayout'
                , route: 'get-record-details'
                , currentPage: 1
                , where: [{
                  condition: "=",
                  fields: ['rm.id'],
                  type: 'int',
                  keyword: t.selectedRecord.id
                }]
                , order: []
                //, order: [{ field: 'alignment', dir: 'ASC' }]
              });
            break;
          }
          case 'delete': {
            //const values: any = await t.getFormValues();
            if (t.args.perm.del === 0 && t.selectedRecord.id !== '') {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Bu işlem için yetkili değilsiniz.'
              });
            } else {
              t.selectedRecord.receipt_detail = [];
              t.appEvent.emit({
                event: 'sendData'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , data: t.selectedRecord
                , id: t.selectedRecord.id
                , method: 'delete'
              });
            }
            break;
          }
          case 'refresh': {
            await t.uiFns.setFormValues(t.filterLayout, {});
            t.filterData = [];
            t.sortData = [];
            t.dataGrid.clearSortData();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
            break;
          }
          case 'filter': {
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabFilter.classList.contains('hidden')) {
              t.tabFilter.classList.remove('hidden');
            }
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'save': {
            if ((t.elAmount as any).value === '') {
              (t.elAmount as any).value = '0,00';
            }
            if ((t.elTaxAmount as any).value === '') {
              (t.elTaxAmount as any).value = "0,00";
            }
            if ((t.elTotal as any).value === '') {
              (t.elTotal as any).value = "0,00";
            }
            let values: any = await t.uiFns.getFormValues(t.formLayout);
            if (!values.isInvalid) {
              if (t.args.perm.add === 0 || (t.args.perm.add === 1 && t.args.perm.edit === 0 && values.data.id !== '')) {
                t.rendererEvent.emit({
                  event: 'showNotification'
                  , component: t.myElement.tagName.toLowerCase()
                  , idx: t.args.idx
                  , message: 'Bu işlem için yetkili değilsiniz.'
                });
                t.btnClose.disabled = false;
                t.btnList.disabled = false;
                if (t.args.perm.add !== 0) {
                  t.btnSave.disabled = false;
                }
              } else {
                let isOK: boolean = true;
                if (isOK) {
                  const receiptGridData: any[] = await t.receiptGrid.getData();
                  let receiptDetail: any[] = [];
                  for (const row of receiptGridData) {
                    if ((row?.stock_id && row?.stock_id !== '') && (row.amount !== 0 && row.total !== 0)) {
                      receiptDetail.push(row);
                    }
                    if (receiptDetail.length > 0) {
                      receiptDetail[receiptDetail.length - 1]['alignment'] = receiptDetail.length;
                      receiptDetail[receiptDetail.length - 1]['invoice_master_id'] = values.data.id;
                    }
                  }
                  values.data.receipt_type = 'sell';
                  values.data.receipt_detail = receiptDetail;
                  if (values.data.receipt_number !== '' && values.data.receipt_number !== null) {
                    values.data.receipt_number = parseInt(values.data.receipt_number);
                  }
                  values.data.amount = parseFloat(values.data.amount);
                  values.data.tax_amount = parseFloat(values.data.tax_amount);
                  values.data.total = parseFloat(values.data.total);
                  t.appEvent.emit({
                    event: 'sendData'
                    , component: t.myElement.tagName.toLowerCase()
                    , idx: t.args.idx
                    , data: values.data
                    , method: (typeof values.data.id !== 'undefined' && values.data.id !== '' ? 'patch' : 'post')
                  });
                }
              }
            } else {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Gerekli alanları eksiksiz olarak doldurunuz.'
              });
              t.btnClose.disabled = false;
              t.btnList.disabled = false;
              if (t.args.perm.add !== 0) {
                t.btnSave.disabled = false;
              }
            }
            break;
          }
          case 'list': {
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.setFormValues(t.formLayout, {});
            if (obj.eventEmit) {
              await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
            }
            break;
          }
          case 'close': {
            t.rendererEvent.emit({
              event: 'close'
              , component: t.myElement.tagName.toLowerCase()
              , idx: t.args.idx
            });
            break;
          }
          case 'back': {
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            break;
          }
          case 'filter_records': {
            t.filterData = await t.uiFns.getFilterValues(t.filterLayout);
            //console.log('appEvents', t.myElement.tagName.toLowerCase(), obj, t.filterData);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          case 'filter_clear': {
            await t.uiFns.setFormValues(t.filterLayout, {});
            t.filterData = [];
            t.sortData = [];
            t.dataGrid.clearSortData();
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          case 'btnCancelLookupCustomer': {
            t.dialogLookup.dialog.opened = false;
            t.dialogLookup.dialog.remove();
            break;
          }
          case 'btnSelectLookupCustomer': {
            t.dialogLookup.dialog.opened = false;
            //console.log('btnSelectLookupCustomer', obj.data)
            t.elCustomerId.value = obj.data.selectedLookupField.id;
            t.elCustomerLookup.value = obj.data.selectedLookupField.value;
            t.elCustomerLookup.invalid = false;
            t.dialogLookup.dialog.remove();
            break;
          }
          case 'btnLookupSearchCustomer': {
            //t.dialogLookup.dGrid.setData({ records: [] });
            await t.uiFns.requestData(
              {
                emitter: t.appEvent
                , controller: 'customer'
                , tagName: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , recordsPerPage: t.recordsPerPage
              }
              , {
                element: t.dialogLookup.dGrid
                , elementName: 'dialogLookup'
                , route: 'filtered-records'
                , currentPage: 1
                , where: [
                  {
                    fields: ['title'],
                    condition: 'like',
                    keyword: obj.data,
                    type: 'string'
                  }
                  , {
                    fields: ['is_active'],
                    condition: '=',
                    keyword: true,
                    type: 'boolean'
                  }
                ]
                , order: [{ field: 'title', dir: 'asc' }]
              });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          case 'btnCancelLookupStock': {
            t.dialogLookup.dialog.opened = false;
            t.dialogLookup.dialog.remove();
            break;
          }
          case 'btnSelectLookupStock': {
            t.dialogLookup.dialog.opened = false;
            let currentData: any = await t.receiptGrid.getData();
            const idx: number = await t.uiFns.getIndex(currentData, 'alignment', obj.data.row.alignment);
            //console.log('btnSelectLookupStock', obj.data)
            if (idx !== -1) {
              currentData[idx][obj.data.column.dataIndex.replace('_lookup', '_id')] = obj.data.selectedLookupField.id;
              currentData[idx][obj.data.column.dataIndex] = obj.data.selectedLookupField.value;
            }
            //await t.receiptGrid.setData({ records: currentData });
            await t.receiptGrid.setSelectedCellValue(obj.data.selectedLookupField.value);
            t.dialogLookup.dialog.remove();
            break;
          }
          case 'btnLookupSearchStock': {
            //t.dialogLookup.dGrid.setData({ records: [] });
            await t.uiFns.requestData(
              {
                emitter: t.appEvent
                , controller: 'stock'
                , tagName: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , recordsPerPage: t.recordsPerPage
              }
              , {
                element: t.dialogLookup.dGrid
                , elementName: 'dialogLookup'
                , route: 'filtered-records'
                , currentPage: 1
                , where: [
                  {
                    fields: ['code', 'name', 'barcode'],
                    condition: 'like',
                    keyword: obj.data,
                    type: 'string'
                  }
                  , {
                    fields: ['is_active'],
                    condition: '=',
                    keyword: true,
                    type: 'boolean'
                  }
                ]
                , order: [{ field: 'name', dir: 'asc' }]
              });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          default: {
            console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            break;
          }
        }
        break;
      }
      case 'change-currentPage': {
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: parseInt(obj.data, 10), where: t.filterData, order: t.sortData });
        break;
      }
      case 'change-recordsPerPage': {
        t.recordsPerPage = parseInt(obj.data, 10);
        t.baseRequestParams.recordsPerPage = t.recordsPerPage;
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
        break;
      }
      case 'col-sort': {
        t.sortData = obj.data;
        //console.log('col-sort',t.sortData);
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
        break;
      }
      case 'row-dblclick': {
        t.selectedRecord = obj.data;
        t.appEvents({ type: 'button', element: 'edit' });
        break;
      }
      case 'row-select': {
        t.selectedRecord = obj.data;
        break;
      }
      case 'lookup-search': {
        //console.log('lookup-search', obj, obj.element.value);
        t.dialogLookup = await t.uiFns.generateDialog(t, t.contentContainer,
          [
            { label: 'ID', dataIndex: 'id', style: { width: "100px" } },
            { label: 'Adı', dataIndex: 'title', style: { width: "300px" } }
          ],
          t.appEvents,
          {
            id: "dialogLookup_customer",
            headerTitle: "Cari Hesap Tanımları",
            draggable: true,
            modeless: false,
            resizable: false,
            noCloseOnEsc: true,
            noCloseOnOutsideClick: true,
            data: [],
            buttons: [
              { title: 'Seç', theme: 'primary', event: 'save', element: 'btnSelectLookupCustomer' },
              { title: 'Vazgeç', theme: 'primary contrast', event: 'cancel', element: 'btnCancelLookupCustomer' }
            ],
            gridType: 'UIGrid',
            isSearchField: true,
            searchValue: obj.element.value,
            searchEvent: 'btnLookupSearchCustomer',
            row: null,
            column: null
          });
        //console.log('t.dialogLookup', t.dialogLookup)
        break;
      }
      default: {
        console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
        break;
      }
    }
  }

  private createReceiptGrid(records: any) {
    let t = this;
    t.receiptGridContainer.innerHTML = '';
    //console.log('createReceiptGrid', records)
    const editGrid = document.createElement('ui-grid-editable');
    editGrid.id = "ui-grid-editable_sell";
    let cols: any[] = [];
    let calculations: any[] = [];
    let totals: any[] = [];

    cols = [
      { label: "", dataIndex: 'id', style: { width: "20px", textAlign: "right", display: "none" } }
      , { label: "", dataIndex: 'invoice_master_id', style: { width: "20px", textAlign: "right", display: "none" } }
      , { label: "", dataIndex: 'version', style: { width: "20px", textAlign: "right", display: "none" } }
      , { label: "#", dataIndex: 'alignment', style: { width: "25px", maxWidth: "44px", textAlign: "right", fontWeight: "bold" }, type: 'icon', options: { autoIncrement: true, icon: 'vaadin__hash' } }
      , { label: "+", dataIndex: 'add', style: { width: "24px", maxWidth: "44px", textAlign: "center", color: "green", paddingLeft: "10px", paddingRight: "10px" }, type: 'icon', options: { icon: 'vaadin__plus_circle' } }
      , { label: "-", dataIndex: 'del', style: { width: "24px", maxWidth: "44px", textAlign: "center", color: "red", paddingLeft: "10px", paddingRight: "10px" }, type: 'icon', options: { icon: 'vaadin__minus_circle' } }
      , { label: 'Stok Kodu', dataIndex: 'stock_lookup', style: { width: "300px" }, type: 'search', options: {} }
      , { label: "Miktar", dataIndex: 'quantity', style: { maxWidth: "80px", textAlign: "right" }, type: 'number', options: { clearButtonVisible: true, min: 0 } }
      , { label: 'Fiyat', dataIndex: 'price', style: { maxWidth: "150px", textAlign: "right" }, type: 'money', options: { clearButtonVisible: true } }
      , { label: 'Tutar', dataIndex: 'amount', style: { width: "150px", textAlign: "right" }, type: 'money', options: { clearButtonVisible: true, readonly: false } }
      , { label: "KDV%", dataIndex: 'tax_rate', style: { maxWidth: "70px", textAlign: "right" }, type: 'number', options: { clearButtonVisible: true, min: 0 } }
      , { label: 'KDV', dataIndex: 'tax_amount', style: { maxWidth: "150px", textAlign: "right" }, type: 'money', options: { clearButtonVisible: true, readonly: false } }
      , { label: 'Toplam', dataIndex: 'total', style: { width: "150px", textAlign: "right" }, type: 'money', options: { readonly: false } }
    ];
    calculations = [
      { dataIndex: 'amount', proc: '*', fields: ['quantity', 'price'] }
      , { dataIndex: 'tax_amount', proc: '%', fields: ['amount', 'tax_rate'] }
      , { dataIndex: 'total', proc: '+', fields: ['amount', 'tax_amount'] }
    ];
    totals = [
      { dataIndex: 'amount' }
      , { dataIndex: 'tax_amount' }
      , { dataIndex: 'total' }
    ];
    t.receiptGrid = editGrid as unknown as UIGridEditable;
    t.receiptGrid.border = "none";
    t.receiptGrid.columns = cols;
    t.receiptGrid.calculations = calculations;
    t.receiptGrid.totals = totals;
    //t.receiptGrid.records = records;
    //t.receiptGrid.isFreeze = false;
    //t.receiptGrid.isFillCell = true;
    //console.log('height', t.receiptGridContainer.clientHeight)
    t.receiptGrid.height = t.receiptGridContainer.clientHeight + 'px';
    t.receiptGridContainer.appendChild(editGrid);
    let myPromise = new Promise<any>((resolve) => {
      (async () => {
        await customElements.whenDefined('ui-grid-editable');
        if (records.length > 0) {
          await t.receiptGrid.setData({ records });
        }
        const el = t.receiptGridContainer.querySelector("ui-grid-editable[id='ui-grid-editable_sell']");
        el.addEventListener('uiDataSourceEvent', async (event: CustomEvent) => {
          //console.log('uiDataSourceEvent', event.detail);
          switch (event?.detail?.type) {
            case 'row-search': {
              //console.log('row-search', event.detail);
              if (event?.detail?.data?.column?.dataIndex === 'stock_lookup') {
                t.dialogLookup = await t.uiFns.generateDialog(t, t.contentContainer,
                  [
                    { label: 'ID', dataIndex: 'id', style: { width: "100px" } },
                    { label: 'Adı', dataIndex: 'name', style: { width: "300px" } },
                    { label: 'Kod', dataIndex: 'code', style: { width: "100px" } },
                    { label: 'Barkod', dataIndex: 'barcode', style: { width: "100px" } }
                  ],
                  t.appEvents,
                  {
                    id: "dialogLookup_" + (event.detail.data.row.alignment),
                    headerTitle: "Stok Kartı Tanımları",
                    draggable: true,
                    modeless: false,
                    resizable: false,
                    noCloseOnEsc: true,
                    noCloseOnOutsideClick: true,
                    data: [],
                    buttons: [
                      { title: 'Seç', theme: 'primary', event: 'save', element: 'btnSelectLookupStock' },
                      { title: 'Vazgeç', theme: 'primary contrast', event: 'cancel', element: 'btnCancelLookupStock' }
                    ],
                    gridType: 'UIGrid',
                    isSearchField: true,
                    searchValue: event.detail.data.row[event.detail.data.column.dataIndex],
                    searchEvent: 'btnLookupSearchStock',
                    row: event.detail.data.row,
                    column: event.detail.data.column
                  });
              }
              //console.log('t.dialogLookup', t.dialogLookup)
              break;
            }
            case 'calc-change': {
              //t._menuItemClosed(event.detail.idx);
              break;
            }
            case 'total-change': {
              (t.elAmount as any).value = "0,00";
              (t.elTaxAmount as any).value = "0,00";
              (t.elTotal as any).value = "0,00";
              for (const el of event.detail.data) {
                if (el.dataIndex === 'amount') {
                  (t.elAmount as any).value = el.totalFormatted;
                }
                if (el.dataIndex === 'tax_amount') {
                  (t.elTaxAmount as any).value = el.totalFormatted;
                }
                if (el.dataIndex === 'total') {
                  (t.elTotal as any).value = el.totalFormatted;
                }
              }
              break;
            }
            default: {
              console.log('uiDataSourceEvent', event);
              break;
            }
          }
        });
        resolve(true);
      })();
    });
    return myPromise;
  }

  private responsiveSteps: FormLayoutResponsiveStep[] = [
    // Use one column by default
    { minWidth: 0, columns: 1 },
    // Use two columns, if layout's width exceeds 500px
    { minWidth: '300px', columns: 2 },
  ];

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('render|', t.myElement.tagName.toLowerCase());
    //}
    const clientHeight: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 2) + 'px' : '600px';
    let formHeight: string = 'calc(100% - 0px)';
    let styleHeader: any = { height: "50px", width: 'calc(100% - 2px)', border: "1px solid var(--lumo-contrast-20pct)" };
    formHeight = 'calc(100% - 53px)';
    return (
      <div ref={(el) => t.contentContainer = el as HTMLDivElement}>
        <div ref={(el) => t.tabFilter = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>

        </div >
        <div ref={(el) => t.tabForm = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <ui-loading-indicator
            ref={(el) => t.elLoadingIndicator = el as unknown as UILoadingIndicator}
            text='Lüften bekleyiniz...'
          >
          </ui-loading-indicator>
          <div //ref={(el) => t.elContainerHeader = el as HTMLDivElement}
            style={styleHeader}>
            <vaadin-horizontal-layout
              class="flex--middle flex--wrap flex--right"
              theme="spacing"
              style={{ height: "100%", paddingLeft: "10px", paddingRight: "10px" }}
            >
              <vaadin-button theme="primary" ref={(el) => t.btnSave = el as Button}
                disabled={t.args.perm.add === 0}
                hidden={t.args.perm.add === 0}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'save' });
                }}>
                <vaadin-icon src={UIIcons['fa__check']} slot="prefix"></vaadin-icon>
                Kaydet
              </vaadin-button>
              <vaadin-button theme="primary success" ref={(el) => t.btnList = el as Button}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'list', eventEmit: true });
                }}>
                <vaadin-icon src={UIIcons['fa__list']} slot="prefix"></vaadin-icon>
                Listele
              </vaadin-button>
              <vaadin-button theme="primary contrast" ref={(el) => t.btnClose = el as Button}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'close' });
                }}>
                <vaadin-icon src={UIIcons['fa__square_xmark']} slot="prefix"></vaadin-icon>
                Kapat
              </vaadin-button>
            </vaadin-horizontal-layout>
          </div>
          <div style={{ height: formHeight, width: "calc(100% - 2px)", border: "1px solid var(--lumo-contrast-20pct)", borderTop: "none" }} >
            <div class="flex flex--row" style={{ height: "100%", width: "100%" }}>
              <div style={{ height: "100%", width: "20%", overflowY: "auto" }}>
                <vaadin-form-layout ref={(el) => t.formLayout = el as FormLayout} responsiveSteps={t.responsiveSteps} style={{ padding: "10px" }}>
                  <vaadin-text-field
                    label="ID"
                    dataIndex="id"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>
                  <vaadin-text-field
                    label="version"
                    dataIndex="version"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>
                  <vaadin-text-field
                    ref={(el) => t.elCustomerId = el as TextField}
                    label="customer_id"
                    dataIndex="customer_id"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>

                  <vaadin-text-field
                    ref={(el) => t.elReceiptSerial = el as TextField}
                    label="Seri No"
                    dataIndex="receipt_serial"
                    theme="align-right"
                    value=""
                    clear-button-visible
                    required
                  ></vaadin-text-field>
                  <vaadin-number-field
                    ref={(el) => t.elReceiptNumber = el as NumberField}
                    label="Sıra No"
                    dataIndex="receipt_number"
                    theme="align-right"
                    value=""
                    clear-button-visible
                    required
                  //format='money'
                  //fractionDigits="2"
                  ></vaadin-number-field>
                  <vaadin-date-picker
                    ref={(el) => t.elReceiptDate = el as DatePicker}
                    label="Tarih"
                    dataIndex="receipt_date"
                    helper-text=""
                    placeholder={''}
                    clear-button-visible={true}

                    show-week-numbers={true}
                    auto-open-disabled={true}

                    disabled={false}
                    readonly={false}
                    required
                    class={"focusable "}
                    style={{}}
                    value={''}
                    on-value-changed={(/*e: CustomEvent*/) => {
                      //row[column.dataIndex] = e.detail.value;
                      //if (t.isEventListenerReady) {
                      //  t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
                      //}
                      //console.log('elReceiptDate:date-value-changed', e.detail);
                    }}
                  ></vaadin-date-picker>
                  <vaadin-checkbox
                    class="label"
                    label="KDV Dahil"
                    dataIndex="is_tax_included"
                  ></vaadin-checkbox>

                  <vaadin-text-field
                    ref={(el) => t.elCustomerLookup = el as TextField}
                    label="Cari Hesap"
                    dataIndex="customer_lookup"
                    helper-text=""
                    placeholder=""
                    clear-button-visible={true}

                    required={true}
                    max-length={250}
                    min-length={0}
                    pattern=''
                    allowed-char-pattern=''

                    disabled={false}
                    readonly={false}

                    //class={"focusable " + colIdentifier}
                    //style={{  paddingBottom: "0px", paddingTop: "0px" }}
                    //value={}
                    colspan="2"
                    on-keydown={(e: KeyboardEvent) => {
                      e.stopPropagation();
                      if (e.key === 'Enter') {
                        t.appEvents({ type: 'lookup-search', element: t.elCustomerLookup });
                      }
                    }}
                    on-value-changed={(e: CustomEvent) => {
                      e.stopPropagation();
                      if (e.detail.value !== '') {
                        (e.target as TextField).invalid = true;
                      } else {
                        (e.target as TextField).invalid = false;
                      }
                      //console.log('search-on-value-changed', e.detail, e);
                    }}
                  >
                    <vaadin-icon
                      slot="prefix"
                      src={UIIcons['fa__magnifying_glass']}
                      onClick={() => {
                        t.appEvents({ type: 'lookup-search', element: t.elCustomerLookup });
                      }}
                    ></vaadin-icon>
                  </vaadin-text-field>

                  <vaadin-text-area
                    ref={(el) => t.elDescription = el as TextArea}
                    label="Açıklama"
                    dataIndex="description"
                    maxlength="250"
                    colspan="2"
                    helperText="0/250"
                    clear-button-visible
                    on-value-changed={(event: TextAreaValueChangedEvent) => { t.elDescription.helperText = event.detail.value.length + "/250"; }}
                  ></vaadin-text-area>

                  <vaadin-text-field
                    ref={(el) => t.elAmount = el as Text}
                    theme="align-right"
                    label="Tutar"
                    dataIndex="amount"
                    value="0,00"
                    readonly
                    format='money'
                    fractionDigits="2"
                    colspan="2"
                  ></vaadin-text-field>
                  <vaadin-text-field
                    ref={(el) => t.elTaxAmount = el as Text}
                    theme="align-right"
                    label="KDV"
                    dataIndex="tax_amount"
                    value="0,00"
                    readonly
                    format='money'
                    fractionDigits="2"
                    colspan="2"
                  ></vaadin-text-field>
                  <vaadin-text-field
                    ref={(el) => t.elTotal = el as Text}
                    theme="align-right"
                    label="Toplam"
                    dataIndex="total"
                    value="0,00"
                    readonly
                    format='money'
                    fractionDigits="2"
                    colspan="2"
                  ></vaadin-text-field>

                </vaadin-form-layout>
              </div>
              <div ref={(el) => t.receiptGridContainer = el as HTMLDivElement} style={{ height: "100%", width: "80%", paddingLeft: "10px", borderLeft: "1px solid var(--lumo-contrast-20pct)" }}>

              </div>
            </div>
          </div>
        </div >
        <div ref={(el) => t.tabList = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <ui-grid
            ref={(el) => t.dataGrid = el as unknown as UIGrid}
            columns={[
              new DataTableColumn({ label: 'Tarih', dataIndex: 'receipt_date', style: { width: "150px" }, filter: { field: 'date', type: 'between' } })
              , new DataTableColumn({ label: 'Seri No', dataIndex: 'receipt_serial', style: { width: "100px" }, filter: { field: 'text', type: 'between' } })
              , new DataTableColumn({ label: 'Sıra No', dataIndex: 'receipt_number', style: { width: "100px", textAlign: "right" }, filter: { field: 'number', type: 'between' } })
              , new DataTableColumn({ label: 'Ünvan', dataIndex: 'customer_lookup', style: {}, filter: { field: 'text' } })
              , new DataTableColumn({ label: 'Tutar', dataIndex: 'amount', style: { width: "150px", textAlign: "right" } })
              , new DataTableColumn({ label: 'KDV', dataIndex: 'tax', style: { width: "150px", textAlign: "right" } })
              , new DataTableColumn({ label: 'Toplam', dataIndex: 'total', style: { width: "150px", textAlign: "right" } })
            ]}
            height={clientHeight}
            isShowFooter={false}
            isSortMultiple={true}
            hiddenPageBarButtons={(t.args.perm.del === 0 ? ['del'] : [])}
            on-uiDataSourceEvent={(e: CustomEvent) => t.appEvents(e.detail)}
          ></ui-grid>
        </div >
      </div >
    );
  }
}

