import { Component, Element, Event, EventEmitter, h, Method, Prop } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';
import '@vaadin/checkbox';
import '@vaadin/icon';
import * as UIIcons from '../ui-icons';

import { UILoadingIndicator } from '../ui-loading-indicator/ui-loading-indicator';
import { Icon } from '@vaadin/icon';
import { Checkbox } from '@vaadin/checkbox';

@Component({
  tag: 'ui-tree',
  styleUrl: 'ui-tree.scss',
  shadow: true
})
export class UITree {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'uiTreeColClickEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uiTreeColClickEvent: EventEmitter;

  @Event({
    eventName: 'uiTreeNodeSelectEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uiTreeNodeSelectEvent: EventEmitter;

  @Event({
    eventName: 'uiTreeNodeDeSelectEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uiTreeNodeDeSelectEvent: EventEmitter;

  @Prop() args: any;
  @Prop() columnRenderer: string = 'default';
  @Prop() expanded: boolean = true;
  @Prop() isHideBorder: boolean = true;
  @Prop() isHideSeperator: boolean = true;
  @Prop() isCheckIndeterminate: boolean = true;
  @Prop() isCheckableNode: boolean = false; //columns===[]
  @Prop() isEnableLoadingIndicator: boolean = true;
  @Prop() height: string = '600px';
  @Prop() width: string = '850px';
  @Prop() columns: any = [];
  //isDev: boolean = false;

  uiFns: UIFunctions = new UIFunctions();

  data: any[] = [];
  dataFlat: any[] = [];
  elLoadingIndicator: UILoadingIndicator;
  elContainer: HTMLDivElement;
  renderedRow: number = 0;
  clickTimeout: any;
  async componentWillLoad() {
    //let t = this;
    //const els = document.getElementsByTagName('html');
    //const isDev = typeof els[0].attributes["isdev"] !== 'undefined';
    //if (Build.isDev) {
    //  t.columns = [
    //    { header: "Sayfa", dataIndex: 'title', width: 300 },
    //    { header: "Görüntüleme", dataIndex: 'p_menu', width: 100, align: 'center' },
    //    { header: "Ekleme", dataIndex: 'p_add', width: 100, align: 'center' },
    //    { header: "Düzenleme", dataIndex: 'p_edit', width: 100, align: 'center' },
    //    { header: "Silme", dataIndex: 'p_del', width: 100, align: 'center' },
    //    { header: "Özel Hak", dataIndex: 'custom_rights_check', width: 100, align: 'center', triggerClick: true }
    //  ];
    //}
  }

  async componentDidLoad() {
    //let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    //const els = document.getElementsByTagName('html');
    //t.isDev = typeof els[0].attributes["isdev"] !== 'undefined';
    /*if (Build.isDev) {
      await t.load([{
        "id": "23", "text": "Stok", "icon": "vaadin__file_tree", p_menu: 'exists', p_add: 'exists', p_edit: 'exists', p_del: 'exists', custom_rights_check: 'none'
        , "data": [{ "id": "10", "text": "Stok Kartlar\u0131", "icon": "vaadin__file_tree", p_menu: true, p_add: true, p_edit: true, p_del: true, custom_rights_check: 'exists' }, { "id": "24", "text": "Depo \u00dcr\u00fcn Talebi", "leaf": true }, { "id": "25", "text": "Stok Hareket", "leaf": true }, { "id": "33", "text": "|", "leaf": true }, { "id": "4", "text": "Stok Listesi", "leaf": true }, { "id": "32", "text": "Stok Detay Listesi", "leaf": true }, { "id": "34", "text": "Stok Hareket", "leaf": true }]
      }
        , {
        "id": "20", "text": "Cari", "leaf": false, custom_rights_check: 'none'
        , "data": [{ "id": "12", "text": "Cari Hesap Tan\u0131mlar\u0131", "leaf": true }, { "id": "16", "text": "Cari \u0130\u015flem Fi\u015fi", "leaf": true }, { "id": "36", "text": "Virman \u0130\u015flemleri", "leaf": true }, { "id": "39", "text": "|", "leaf": true }, { "id": "40", "text": "Cari Hareket", "leaf": true }, { "id": "61", "text": "Cari Hesap Ekstresi", "leaf": true }, { "id": "62", "text": "Cari Hesap Ekstresi Dovizli", "leaf": true }, { "id": "64", "text": "Cari Hesap Ekstresi-2", "leaf": true }, { "id": "78", "text": "Cari Listesi", "leaf": true }]
      }
        , {
        "id": "19", "text": "Sat\u0131nalma", "leaf": false, custom_rights_check: 'none'
        , "data": [{ "id": "18", "text": "Tedarik\u00e7i Sipari\u015f Giri\u015fi", "leaf": true }, { "id": "22", "text": "Sat\u0131nalma Faturas\u0131 (T. Mal)", "leaf": true }, { "id": "71", "text": "Masraf Giri\u015fi", "leaf": true }]
      }
        , {
        "id": "21", "text": "Sat\u0131\u015f", "leaf": false, custom_rights_check: 'none'
        , "data": [{ "id": "26", "text": "Sat\u0131\u015f Sipari\u015fi", "leaf": true }, { "id": "41", "text": "\u0130rsaliye", "leaf": true }, { "id": "14", "text": "Sat\u0131\u015f Faturas\u0131", "leaf": true }, { "id": "49", "text": "|", "leaf": true }, { "id": "50", "text": "Sat\u0131\u015f Hareketleri", "leaf": true }, { "id": "68", "text": "Sat\u0131\u015f Hareketleri-2", "leaf": true }]
      }
        , {
        "id": "42", "text": "Banka", "leaf": false, custom_rights_check: 'none'
        , "data": [{ "id": "15", "text": "Banka Tan\u0131mlar\u0131", "leaf": true }, { "id": "44", "text": "Banka Hesap Tan\u0131mlar\u0131", "leaf": true }, { "id": "45", "text": "|", "leaf": true }, { "id": "46", "text": "Banka \u0130\u015flem Fi\u015fi", "leaf": true }, { "id": "79", "text": "Banka Hesap Ekstresi", "leaf": true }]
      }, { "id": "43", "text": "Kasa", "leaf": false, custom_rights_check: 'none', "data": [{ "id": "59", "text": "Kasa Tan\u0131mlar\u0131", "leaf": true }, { "id": "60", "text": "Kasa \u0130\u015flem Fi\u015fi", "leaf": true }] }
        , {
        "id": "27", "text": "Muhasebe", "leaf": false, custom_rights_check: 'none'
        , "data": [{ "id": "28", "text": "Hesap Plan\u0131", "leaf": true }, { "id": "29", "text": "Fi\u015f Giri\u015fi", "leaf": true }, { "id": "35", "text": "|", "leaf": true }, { "id": "31", "text": "Mizan", "leaf": true }]
      }
        , {
        "id": "1", "text": "Parametreler", "leaf": false, custom_rights_check: 'none'
        , "data": [{ "id": "17", "text": "Depo Tan\u0131mlar\u0131", "leaf": true }, { "id": "9", "text": "\u00dcr\u00fcn Gruplar\u0131", "leaf": true }, { "id": "38", "text": "\u00dcr\u00fcn T\u00fcrleri", "leaf": true }, { "id": "8", "text": "|", "leaf": true }, { "id": "37", "text": "Kur Giri\u015fi", "leaf": true }, { "id": "63", "text": "\u00dclke Tan\u0131mlar\u0131", "leaf": true }, { "id": "65", "text": "Sistem Parametreleri", "leaf": true }, { "id": "67", "text": "Personel Tan\u0131mlar\u0131", "leaf": true }, { "id": "69", "text": "|", "leaf": true }, { "id": "70", "text": "Masraf T\u00fcrleri", "leaf": true }]
      }, {
        "id": "3", "text": "Y\u00f6netici \u0130\u015flemleri", "leaf": false, custom_rights_check: 'none'
        , "data": [{ "id": "5", "text": "Kullan\u0131c\u0131 Tan\u0131mlama", "leaf": true }, { "id": "66", "text": "Devir \u0130\u015flemleri", "leaf": true }]
      }, {
        "id": "51", "text": "Site \u0130\u00e7erik ve Parametreleri", "leaf": false, custom_rights_check: 'none'
        , "data": [{ "id": "55", "text": "Sistem Parametreleri", "leaf": true }, { "id": "54", "text": "Giri\u015f Reklam\u0131", "leaf": true }, { "id": "52", "text": "Genel \u0130\u00e7erikler", "leaf": true }, { "id": "53", "text": "Kullan\u0131c\u0131 Men\u00fcleri", "leaf": true }, { "id": "56", "text": "Kurumsal Bilgiler", "leaf": true }, { "id": "57", "text": "\u00d6ne \u00c7\u0131kanlar", "leaf": true }, { "id": "58", "text": "\u00dcr\u00fcn Kategorileri", "leaf": true }]
      }]);
    }*/
  }

  async addNode(container: HTMLElement, item: any, level: number, childIndex: number = 0) {
    let t = this;
    const hasChild: boolean = (typeof item.data !== 'undefined' && item.data.length > 0);
    if (t.isHideSeperator && item.text === '|') {
      return;
    }
    const li = document.createElement('li');
    let clsRow: string = 'row-' + (t.renderedRow - level) + ' c-row-' + childIndex;
    item.group = (t.renderedRow - level);
    item.rowIndex = childIndex;
    li.setAttribute('class', 'ui-tree-node ' + clsRow);
    li.setAttribute('unselectable', 'on');
    const divNode = document.createElement('div');
    divNode.setAttribute('class', 'ui-tree-node-el ' + clsRow);
    divNode.setAttribute('unselectable', 'on');
    divNode.addEventListener('click', (e) => {
      if (t.clickTimeout !== null) {
        clearTimeout(t.clickTimeout);
        t.clickTimeout = null;
      }
      t.clickTimeout = setTimeout(function () {
        if (e.detail === 1) {
          t.selectNode(divNode);
        }
      }, 200);
    });
    divNode.addEventListener('dblclick', () => {
      if (t.clickTimeout !== null) {
        clearTimeout(t.clickTimeout);
        t.clickTimeout = null;
      }
      t.dblclickNode(divNode);
    });
    if (t.columns.length === 0) {
      t.dataFlat = [...t.dataFlat, item];
      if (hasChild) {
        const icon = document.createElement('vaadin-icon');
        icon.addEventListener('click', t.toggleNode.bind(t, icon), { passive: true });
        icon.setAttribute('class', 'ui-tree-node-icon');
        if (t.expanded) {
          icon.setAttribute('src', UIIcons['fa__angle_down']);
        } else {
          icon.setAttribute('src', UIIcons['fa__angle_right']);
        }
        icon.setAttribute('unselectable', 'on');
        divNode.appendChild(icon);
      }
      if (t.isCheckableNode) {
        const chk = document.createElement('vaadin-checkbox');
        chk.setAttribute('class', 'chk-' + item.text.split(' ').join('_').toLowerCase());
        chk.addEventListener('change', t.checkNodeWithoutColumns.bind(t, chk), { passive: true });
        chk.setAttribute('unselectable', 'on');
        divNode.appendChild(chk);
      }
      if (typeof item.icon !== 'undefined') {
        const iconEl = document.createElement('vaadin-icon');
        iconEl.setAttribute('src', UIIcons[item.icon]);
        iconEl.setAttribute('style', 'padding:2px;');
        iconEl.setAttribute('unselectable', 'on');
        if (t.isCheckableNode) {
          iconEl.addEventListener('click', t.clickIconWithoutColumns.bind(t, iconEl, hasChild), { passive: true });
        }
        divNode.appendChild(iconEl);
      }
      const span = document.createElement('span');
      span.setAttribute('class', 'ui-tree-text');
      span.appendChild(document.createTextNode(item.text));
      span.setAttribute('unselectable', 'on');
      if (t.isCheckableNode) {
        span.addEventListener('click', t.clickSpanWithoutColumns.bind(t, span, hasChild), { passive: true });
      }
      divNode.appendChild(span);
    } else {
      //console.log(item)
      let tmpItem: any = { id: item.id, menu_id: item.menu_id, text: item.text, group: item.group, rowIndex: item.rowIndex, version: item.version };
      let isFirst: boolean = true;
      let counter: number = 0;
      for (const colItem of t.columns) {
        const divNodeCol = document.createElement('div');
        let clsCol: string = clsRow + ' col-' + colItem.dataIndex + ' col-' + (counter++);
        divNodeCol.setAttribute('class', 'ui-tree-node-c-col ' + clsCol);
        divNodeCol.setAttribute('unselectable', 'on');
        divNodeCol.setAttribute('style', 'width:' + (colItem.width - (isFirst ? (level * 40) : 0)) + 'px;');
        divNode.appendChild(divNodeCol);
        if (isFirst) {
          if (hasChild) {
            const icon = document.createElement('vaadin-icon');
            icon.addEventListener('click', t.toggleNode.bind(t, icon), { passive: true });
            icon.setAttribute('class', 'ui-tree-node-icon');
            if (t.expanded) {
              icon.setAttribute('src', UIIcons['fa__angle_down']);
            } else {
              icon.setAttribute('src', UIIcons['fa__angle_right']);
            }
            icon.setAttribute('unselectable', 'on');
            divNodeCol.appendChild(icon);
          }
          if (typeof item.icon !== 'undefined') {
            const iconEl = document.createElement('vaadin-icon');
            iconEl.setAttribute('src', UIIcons[item.icon]);
            iconEl.setAttribute('style', 'padding:2px;');
            iconEl.setAttribute('unselectable', 'on');
            divNodeCol.appendChild(iconEl);
          }
          const span = document.createElement('span');
          span.setAttribute('class', 'ui-tree-text');
          span.appendChild(document.createTextNode(item.text));
          span.setAttribute('unselectable', 'on');
          divNodeCol.appendChild(span);
          isFirst = false;
        } else {
          const center = document.createElement('center');
          center.setAttribute('unselectable', 'on');
          const chk = document.createElement('vaadin-checkbox');
          chk.setAttribute('class', clsCol);
          if (typeof item[colItem.dataIndex] !== 'undefined') {
            tmpItem[colItem.dataIndex] = item[colItem.dataIndex];
            if (item[colItem.dataIndex] === true) {
              chk.setAttribute('checked', 'true');
            }
            if (item[colItem.dataIndex] === 'exists'
              || (colItem?.triggerClick === true && item[colItem.dataIndex] instanceof Array && item[colItem.dataIndex].length > 0)) {
              chk.setAttribute('indeterminate', 'true');
            }
          } else {
            tmpItem[colItem.dataIndex] = null;
          }
          if (typeof colItem.triggerClick === 'undefined') {
            chk.addEventListener('change', t.checkNode.bind(t, chk, tmpItem, colItem.dataIndex), { passive: true });
          } else {
            chk.addEventListener('change', t.triggerNode.bind(t, chk, tmpItem), { capture: true, passive: true });
          }
          if ((typeof colItem.triggerClick === 'undefined')
            || (colItem?.triggerClick === true && item[colItem.dataIndex] instanceof Array && item[colItem.dataIndex].length > 0)
          ) {
            chk.setAttribute('unselectable', 'on');
            center.appendChild(chk);
          }
          divNodeCol.appendChild(center);
        }
      }
      t.dataFlat = [...t.dataFlat, tmpItem];
    }
    li.appendChild(divNode);
    if (level === 0) {
      t.renderedRow = t.renderedRow + 1;
    }
    //console.log(item, level, childIndex);
    if (hasChild) {
      const ul = document.createElement('ul');
      if (t.expanded) {
        ul.setAttribute('class', 'ui-tree-node-ct row-' + (t.renderedRow - 1)) + ' c-row-' + childIndex;
      } else {
        ul.setAttribute('class', 'ui-tree-node-ct row-' + (t.renderedRow - 1) + ' c-row-' + childIndex + ' hidden');
      }
      ul.setAttribute('draggable', 'false');
      ul.setAttribute('unselectable', 'on');
      li.appendChild(ul);
      level++;
      let index: number = 0
      for (const cItem of item.data) {
        for (const col of t.columns) {
          if (col.dataIndex !== 'title' && typeof col.triggerClick === 'undefined') {
            if (cItem[col.dataIndex] === 0) {
              cItem[col.dataIndex] = false;
            }
            if (cItem[col.dataIndex] === 1) {
              cItem[col.dataIndex] = true;
            }
            if (cItem[col.dataIndex] === 2) {
              cItem[col.dataIndex] = "exists";
            }
          }
        }
        await t.addNode(ul, cItem, level, ++index);
      }
    }
    container.appendChild(li);
  }

  async checkNode(el, item, dataIndex) {//, event
    let t = this;
    item[dataIndex] = el.checked;
    if (t.isCheckIndeterminate) {
      //console.log('checkNode', item, el, dataIndex);//t.dataFlat
      if (item.rowIndex === 0) {
        // change group header
        for (const row of t.dataFlat) {
          if (row.group === item.group) {
            //if (row.rowIndex > 0) {
            row[dataIndex] = el.checked;
            const parentClass: string = 'ui-tree-node.row-' + item.group + '.c-row-0';
            const checkClass: string = 'row-' + item.group + '.c-row-' + row.rowIndex + '.col-' + dataIndex;
            const checkEl = t.elContainer.querySelector('li.' + parentClass).querySelector('vaadin-checkbox.' + checkClass);
            if (el.checked) {
              checkEl.setAttribute('checked', 'true');
              //if (dataIndex !== 'p_menu') {
              //  const menuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_menu';
              //  const menuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + menuItemClass);
              //  console.log('menuItemClass', menuItemClass, 'menuItemEl', menuItemEl);
              //  if (menuItemEl.checked !== true) {
              //    menuItemEl.checked = true;
              //    await t.checkNode(menuItemEl, item, 'p_menu');
              //  }
              //}
              if (dataIndex === 'p_add' || dataIndex === 'p_edit' || dataIndex === 'p_del') {
                const menuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_menu';
                const menuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + menuItemClass);
                //console.log('menuItemClass', menuItemClass, 'menuItemEl', menuItemEl);
                if (menuItemEl.checked !== true) {
                  menuItemEl.checked = true;
                  await t.checkNode(menuItemEl, item, 'p_menu');
                }
              }
              if (dataIndex === 'p_edit' || dataIndex === 'p_del') {
                const menuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_add';
                const menuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + menuItemClass);
                //console.log('menuItemClass', menuItemClass, 'menuItemEl', menuItemEl);
                if (menuItemEl.checked !== true) {
                  menuItemEl.checked = true;
                  await t.checkNode(menuItemEl, item, 'p_add');
                }
              }
              if (dataIndex === 'p_del') {
                const menuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_edit';
                const menuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + menuItemClass);
                //console.log('menuItemClass', menuItemClass, 'menuItemEl', menuItemEl);
                if (menuItemEl.checked !== true) {
                  menuItemEl.checked = true;
                  await t.checkNode(menuItemEl, item, 'p_edit');
                }
              }
            } else {
              checkEl.removeAttribute('checked');
              if (dataIndex === 'p_menu') {
                const addmenuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_add';
                const addmenuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + addmenuItemClass);
                if (addmenuItemEl.checked === true || addmenuItemEl.indeterminate) {
                  addmenuItemEl.checked = false;
                  addmenuItemEl.removeAttribute('indeterminate');
                  await t.checkNode(addmenuItemEl, item, 'p_add');
                }

                const updmenuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_edit';
                const updmenuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + updmenuItemClass);
                if (updmenuItemEl.checked === true || updmenuItemEl.indeterminate) {
                  updmenuItemEl.checked = false;
                  updmenuItemEl.removeAttribute('indeterminate');
                  await t.checkNode(updmenuItemEl, item, 'p_edit');
                }

                const delmenuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_del';
                const delmenuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + delmenuItemClass);
                if (delmenuItemEl.checked === true || delmenuItemEl.indeterminate) {
                  delmenuItemEl.checked = false;
                  delmenuItemEl.removeAttribute('indeterminate');
                  await t.checkNode(delmenuItemEl, item, 'p_del');
                }
              }
            }
            //}
          }
        }
      } else {
        // change group child
        //let records: any = t.dataFlat.filter(row => (row.group === item.group && row.rowIndex > 0 && ((el.checked && row[dataIndex] === null) || row[dataIndex] === !el.checked)));
        let records: any = t.dataFlat.filter(row => (row.group === item.group && ((el.checked && row[dataIndex] === null) || row[dataIndex] === !el.checked)));
        const parentIdx: number = await t.uiFns.getIndex(t.dataFlat, 'group|rowIndex', item.group + '|0');
        const parentClass: string = 'ui-tree-node-el.row-' + item.group + '.c-row-0';
        const checkClass: string = 'row-' + item.group + '.c-row-0.col-' + dataIndex;
        const parentEl = t.elContainer.querySelector('div.' + parentClass).querySelector('vaadin-checkbox.' + checkClass);
        if (parentEl) {
          //console.log('parentEl', parentEl, records);
          if (records.length > 0) {
            if (el.checked) {
              if (parentIdx !== -1) {
                t.dataFlat[parentIdx][dataIndex] = 'exists';
              }
              parentEl.setAttribute('indeterminate', 'true');
            } else {
              parentEl.removeAttribute('checked');
              parentEl.setAttribute('indeterminate', 'true');
              if (parentIdx !== -1) {
                t.dataFlat[parentIdx][dataIndex] = 'exists';
              }
            }
          } else {
            if (el.checked) {
              parentEl.removeAttribute('indeterminate');
              parentEl.setAttribute('checked', 'true');
              if (parentIdx !== -1) {
                t.dataFlat[parentIdx][dataIndex] = true;
              }
            } else {
              parentEl.removeAttribute('indeterminate');
              if (parentIdx !== -1) {
                t.dataFlat[parentIdx][dataIndex] = false;
              }
            }
          }
        }
        if (dataIndex === 'p_menu') {
          if (!el.checked) {
            const addmenuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_add';
            const addmenuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + addmenuItemClass);
            if (addmenuItemEl.checked === true) {
              addmenuItemEl.checked = false;
              await t.checkNode(addmenuItemEl, item, 'p_add');
            }

            const updmenuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_edit';
            const updmenuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + updmenuItemClass);
            if (updmenuItemEl.checked === true) {
              updmenuItemEl.checked = false;
              await t.checkNode(updmenuItemEl, item, 'p_edit');
            }

            const delmenuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_del';
            const delmenuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + delmenuItemClass);
            if (delmenuItemEl.checked === true) {
              delmenuItemEl.checked = false;
              await t.checkNode(delmenuItemEl, item, 'p_del');
            }
          }
        } else {
          if (el.checked) {
            if (dataIndex === 'p_add' || dataIndex === 'p_edit' || dataIndex === 'p_del') {
              const menuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_menu';
              const menuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + menuItemClass);
              //console.log('menuItemClass', menuItemClass, 'menuItemEl', menuItemEl);
              if (menuItemEl.checked !== true) {
                menuItemEl.checked = true;
                await t.checkNode(menuItemEl, item, 'p_menu');
              }
            }
            if (dataIndex === 'p_edit' || dataIndex === 'p_del') {
              const menuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_add';
              const menuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + menuItemClass);
              //console.log('menuItemClass', menuItemClass, 'menuItemEl', menuItemEl);
              if (menuItemEl.checked !== true) {
                menuItemEl.checked = true;
                await t.checkNode(menuItemEl, item, 'p_add');
              }
            }
            if (dataIndex === 'p_del') {
              const menuItemClass: string = 'row-' + item.group + '.c-row-' + item.rowIndex + '.col-p_edit';
              const menuItemEl: Checkbox = t.elContainer.querySelector('vaadin-checkbox.' + menuItemClass);
              //console.log('menuItemClass', menuItemClass, 'menuItemEl', menuItemEl);
              if (menuItemEl.checked !== true) {
                menuItemEl.checked = true;
                await t.checkNode(menuItemEl, item, 'p_edit');
              }
            }
          }
        }
        //console.log('child', t.dataFlat[parentIdx], records);
      }
    }
    // if (Build.isDev) {
    //console.log(t.data, t.dataFlat);
    //  console.log('checkNode', el, el.checked, item, item.rowIndex, dataIndex);
    //console.log(t.dataFlat);
    // }
  }

  async checkNodeWithoutColumns(el) {
    let t = this;
    //console.log('checkNodeWithoutColumns', el, el.parentNode.childNodes[el.parentNode.childNodes.length - 1].innerHTML);
    const title: string = el.parentNode.childNodes[el.parentNode.childNodes.length - 1].innerHTML;
    await t.clearAllCheckNodes();
    el.checked = true;
    let item: any = null;
    for (const row of t.dataFlat) {
      //console.log(row);
      if (row.text === title) {
        item = row;
        break;
      }
    }
    //console.log('item', item, title)
    t.uiTreeNodeSelectEvent.emit(item);
  }

  async clickIconWithoutColumns(el, hasChild) {
    let t = this;
    const childIndex: number = (hasChild ? 1 : 0);
    //console.log('clickSpanWithoutColumns', el, el.parentNode.childNodes, hasChild, el.parentNode.childNodes[childIndex]);
    await t.clearAllCheckNodes();
    return await t.checkNodeWithoutColumns(el.parentNode.childNodes[childIndex]);
  }

  async clickSpanWithoutColumns(el, hasChild) {
    let t = this;
    const childIndex: number = (hasChild ? 1 : 0);
    //console.log('clickSpanWithoutColumns', el, el.parentNode.childNodes, hasChild, el.parentNode.childNodes[childIndex]);
    await t.clearAllCheckNodes();
    return await t.checkNodeWithoutColumns(el.parentNode.childNodes[childIndex]);
  }

  async clearAllCheckNodes() {
    let t = this;
    const checkEls: NodeListOf<Checkbox> = t.elContainer.querySelectorAll('vaadin-checkbox[checked]');
    await checkEls.forEach(async (checkEl: Checkbox) => {
      checkEl.checked = false;
    });
  }

  @Method()
  async clearAllSelectedNodes() {
    let t = this;
    //Array.from(t.elContainer.querySelectorAll('li.selected')).forEach(
    //  (el) => el.classList.remove('selected')
    //);
    let isSelectedFound: boolean = false;
    await t.uiFns.asyncForEach(t.elContainer.querySelectorAll('div.selected'), async (el: any) => {
      isSelectedFound = true;
      el.classList.remove('selected');
    }
    );
    if (isSelectedFound) {
      t.uiTreeNodeDeSelectEvent.emit();
    }
  }

  async selectNode(el: HTMLDivElement) {
    let t = this;
    await t.clearAllSelectedNodes();
    el.classList.add('selected');
    const title: string = (el.lastChild as HTMLSpanElement).innerHTML;
    let item: any = null;
    for (const row of t.dataFlat) {
      if (row.text === title) {
        item = row;
        break;
      }
    }
    t.uiTreeNodeSelectEvent.emit(item);
  }

  async dblclickNode(el: HTMLDivElement) {
    let t = this;
    await t.clearAllSelectedNodes();
    if (!el.classList.contains('selected')) {
      if (el.parentNode.childNodes.length > 1) {
        t.toggleNode(el.firstChild as Icon);
      }
      if ((el.firstChild as Icon).src === UIIcons['fa__angle_down']) {
        await t.selectNode(el);
      }
    } else {
      if (el.parentNode.childNodes.length > 1) {
        t.toggleNode(el.firstChild as Icon);
        if ((el.firstChild as Icon).src === UIIcons['fa__angle_down']) {
          await t.selectNode(el);
        }
      }
    }
  }

  async triggerNode(el, item, event) {
    let t = this;
    event.stopPropagation();
    event.preventDefault();
    item.element = el;
    //if (Build.isDev) {
    //  console.log('triggerNode', item, event);
    //}
    t.uiTreeColClickEvent.emit(item);
  }

  async toggleNode(icon: Icon) {
    let t = this;
    if (icon.src === UIIcons['fa__angle_right']) {
      //expand
      icon.src = UIIcons['fa__angle_down'];
      if (t.columns.length > 0) {
        (icon.parentElement.parentElement.nextSibling as HTMLUListElement).classList.remove('hidden');
      } else {
        (icon.parentElement.nextSibling as HTMLUListElement).classList.remove('hidden');
      }
    } else {
      //collapse
      icon.src = UIIcons['fa__angle_right'];
      if (t.columns.length > 0) {
        (icon.parentElement.parentElement.nextSibling as HTMLUListElement).classList.add('hidden');
      } else {
        (icon.parentElement.nextSibling as HTMLUListElement).classList.add('hidden');
      }
    }
  }

  @Method()
  async afterLoad() {
    let t = this;
    if (t.isEnableLoadingIndicator) {
      t.elLoadingIndicator.setVisible(false);
    }
  }

  @Method()
  async beforeLoad() {
    let t = this;
    if (t.isEnableLoadingIndicator) {
      await t.elLoadingIndicator.setVisible(true);
    }
    return;
  }

  @Method()
  async getData(isFlat?: boolean) {
    let t = this;
    if (isFlat) {
      // @@info: deep copy without referance
      let tmpData: any[] = t.dataFlat.map(item => {
        return { ...item };
      });
      for (const item of tmpData) {
        if (typeof item.element !== 'undefined') {
          delete item.element;
        }
        delete item.group;
        //delete item.id;
        delete item.rowIndex;
        for (const col of t.columns) {
          if (col.dataIndex !== 'title' && typeof col.triggerClick === 'undefined') {
            if (item[col.dataIndex] === null || item[col.dataIndex] === false) {
              item[col.dataIndex] = 0;
            }
            if (item[col.dataIndex] === true) {
              item[col.dataIndex] = 1;
            }
            if (item[col.dataIndex] === "exists") {
              item[col.dataIndex] = 2;
            }
          }
        }
      }
      return tmpData;
    } else {
      return t.data;
    }
  }

  @Method()
  async load(data: any) {
    let t = this;
    await t.beforeLoad();
    t.elContainer.innerHTML = '';
    if (t.columns.length > 0) {
      const divHeaderRow = document.createElement('div');
      divHeaderRow.setAttribute('class', 'ui-tree-header-row');
      divHeaderRow.setAttribute('draggable', 'false');
      divHeaderRow.setAttribute('unselectable', 'on');
      for (const hItem of t.columns) {
        const divHeaderCol = document.createElement('div');
        divHeaderCol.setAttribute('class', 'ui-tree-header-col');
        divHeaderCol.setAttribute('draggable', 'false');
        divHeaderCol.setAttribute('unselectable', 'on');
        divHeaderCol.setAttribute('style', 'width:' + hItem.width + 'px;text-align:' + (typeof hItem.align !== 'undefined' ? hItem.align : 'left'));
        divHeaderCol.appendChild(document.createTextNode(hItem.header));
        divHeaderRow.appendChild(divHeaderCol);
      }
      t.elContainer.appendChild(divHeaderRow);
    }
    t.dataFlat = [];
    if (data.length > 0) {
      t.data = data;
      const ul = document.createElement('ul');
      ul.setAttribute('class', 'ui-tree ui-tree-root');
      ul.setAttribute('draggable', 'false');
      ul.setAttribute('unselectable', 'on');
      const divRoot = document.createElement('div');
      divRoot.setAttribute('class', 'ui-tree ui-tree-root');
      divRoot.setAttribute('draggable', 'false');
      divRoot.setAttribute('unselectable', 'on');
      ul.appendChild(divRoot);
      t.elContainer.appendChild(ul);
      for (const item of t.data) {
        for (const col of t.columns) {
          if (col.dataIndex !== 'title' && typeof col.triggerClick === 'undefined') {
            if (item[col.dataIndex] === 0) {
              item[col.dataIndex] = false;
            }
            if (item[col.dataIndex] === 1) {
              item[col.dataIndex] = true;
            }
            if (item[col.dataIndex] === 2) {
              item[col.dataIndex] = "exists";
            }
          }
        }
        await t.addNode(divRoot, item, 0, 0);
      }
      //if (Build.isDev) {
      //  console.log(t.data, t.dataFlat);
      //}
      // const icons = t.elContainer.querySelectorAll("vaadin-icon.ui-tree-node-icon");
      // console.log(icons);
    }
    await t.uiFns.delay(200);
    await t.afterLoad();
  }

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('Render|', t.myElement.tagName.toLowerCase());
    //}
    const strBorder: string = (t.isHideBorder ? '' : '1px solid var(--lumo-contrast-10pct)');
    return (<div style={{ height: t.height, width: t.width, overflow: "auto", border: strBorder }} >
      <ui-loading-indicator
        ref={(el) => t.elLoadingIndicator = el as unknown as UILoadingIndicator}
        text='Lüften bekleyiniz...'
      >
      </ui-loading-indicator>
      <div class="flex flex--col" ref={(el) => t.elContainer = el as HTMLDivElement} style={{}} >
      </div>
    </div>
    );
  }
}
