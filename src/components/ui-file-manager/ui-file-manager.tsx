import { Component, Element, Event, EventEmitter, Method, Prop, h } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';
import * as UIIcons from '../ui-icons';
import { UITree } from '../ui-tree/ui-tree';
import { UIGrid } from '../ui-grid/ui-grid';
import { DataTableColumn } from '../px-interfaces';
import { UILoadingIndicator } from '../ui-loading-indicator/ui-loading-indicator';
import { Button } from '@vaadin/button';
import { Dialog, DialogOverlay } from '@vaadin/dialog';

import '@vaadin/button';
import '@vaadin/dialog';
import '@vaadin/icon';
import '@vaadin/split-layout';
import '@vaadin/text-field';
import '@vaadin/tooltip';
import '@vaadin/vertical-layout';

@Component({
  tag: 'ui-file-manager',
  styleUrl: 'ui-file-manager.scss',
  shadow: true
})
export class UiFileManager {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'uiDataSourceEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uiDataSourceEvent: EventEmitter;

  @Prop() args: any = { idx: 1, clientHeight: 600, perm: { add: 1, edit: 1 } };

  uiFns: UIFunctions = new UIFunctions();
  elLoadingIndicator: UILoadingIndicator;
  container: HTMLDivElement;
  contentContainer: HTMLDivElement;
  folderTree: UITree;
  folderGrid: UIGrid;
  fmDialog: Dialog;

  btnAddFolder: Button;
  btnAddFile: Button;
  btnCut: Button;
  btnCopy: Button;
  btnPaste: Button;
  btnDelete: Button;
  btnRename: Button;
  btnRefresh: Button;

  selectedFolder: any = null;

  filterData: any[] = [];
  sortData: any[] = [];
  baseRequestParams: any;

  async componentDidLoad() {
    let t = this;
    await t.elLoadingIndicator.refit();
    t.uiDataSourceEvent.emit({ type: 'loadData', element: 'folderTree' });
    t.elLoadingIndicator.setVisible(true);
  }

  @Method()
  async getSelectedItem() {
    let t = this;
    return await t.folderGrid.getSelectedRows();
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
    //}
    switch (obj.element) {
      case 'folderTree': {
        t.folderGrid.setData({ records: [] });
        t.folderTree.load(obj.folders);
        t.elLoadingIndicator.setVisible(false);
        break;
      }
      default: {
        //if (Build.isDev) {
        console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
        //}
        break;
      }
    }
  }

  async appEvents(obj: any) {
    let t = this;
    //console.log(t.myElement.tagName.toLowerCase(), 'appEvents', obj);
    switch (obj.type) {
      case 'button': {
        switch (obj.element) {
          case 'addFolder': {
            t.fmDialog = await t.dialogRenderer({
              id: "addFolderDialog",
              headerTitle: "Yeni Klasör",
              inputVal: '',
              draggable: true,
              modeless: false,
              resizable: false,
              noCloseOnEsc: true,
              noCloseOnOutsideClick: true,
              buttons: [
                { title: 'Kaydet', theme: 'primary', icon: 'fa__check', event: 'save', element: 'btnSaveAddFolder' },
                { title: 'Vazgeç', theme: 'primary contrast', icon: 'vaadin__arrow_backward', event: 'cancel', element: 'btnCancelAddFolder' }
              ]
            });
            break;
          }
          case 'addFile': {
            t.fmDialog = await t.dialogRenderer({
              id: "addFileDialog",
              headerTitle: "Yeni Rapor",
              inputVal: '',
              draggable: true,
              modeless: false,
              resizable: false,
              noCloseOnEsc: true,
              noCloseOnOutsideClick: true,
              buttons: [
                { title: 'Kaydet', theme: 'primary', icon: 'fa__check', event: 'save', element: 'btnSaveAddFile' },
                { title: 'Vazgeç', theme: 'primary contrast', icon: 'vaadin__arrow_backward', event: 'cancel', element: 'btnCancelAddFile' }
              ]
            });
            break;
          }
          case 'cut': {

            break;
          }
          case 'copy': {

            break;
          }
          case 'paste': {

            break;
          }
          case 'delete': {
            const gridSR = await t.folderGrid.getSelectedRows();
            t.uiDataSourceEvent.emit({ type: 'deleteItem', element: 'folderTree', folder: t.selectedFolder, file: (gridSR.length > 0 ? gridSR[0].text : '') });
            break;
          }
          case 'rename': {
            const gridSR = await t.folderGrid.getSelectedRows();
            t.fmDialog = await t.dialogRenderer({
              id: "renameDialog",
              headerTitle: "Yeniden Adlandır",
              inputVal: (gridSR.length > 0 ? gridSR[0].title : t.selectedFolder.text),
              draggable: true,
              modeless: false,
              resizable: false,
              noCloseOnEsc: true,
              noCloseOnOutsideClick: true,
              buttons: [
                { title: 'Kaydet', theme: 'primary', icon: 'fa__check', event: 'save', element: 'btnSaveRename' },
                { title: 'Vazgeç', theme: 'primary contrast', icon: 'vaadin__arrow_backward', event: 'cancel', element: 'btnCancelRename' }
              ]
            });
            break;
          }
          case 'refresh': {
            await t.folderTree.clearAllSelectedNodes();
            t.uiDataSourceEvent.emit({ type: 'loadData', element: 'folderTree' });
            t.elLoadingIndicator.setVisible(true);
            break;
          }
          case 'btnSaveAddFolder': {
            t.fmDialog.opened = false;
            t.fmDialog.remove();
            if (obj.inputVal !== '') {
              t.uiDataSourceEvent.emit({ type: 'addFolder', element: 'folderTree', selectedFolder: t.selectedFolder, name: obj.inputVal });
            }
            break;
          }
          case 'btnCancelAddFolder': {
            t.fmDialog.opened = false;
            t.fmDialog.remove();
            break;
          }
          case 'btnSaveAddFile': {
            t.fmDialog.opened = false;
            t.fmDialog.remove();
            if (obj.inputVal !== '') {
              t.uiDataSourceEvent.emit({ type: 'addFile', element: 'folderTree', selectedFolder: t.selectedFolder, name: obj.inputVal });
            }
            break;
          }
          case 'btnCancelAddFile': {
            t.fmDialog.opened = false;
            t.fmDialog.remove();
            break;
          }
          case 'btnSaveRename': {
            t.fmDialog.opened = false;
            t.fmDialog.remove();
            if (obj.inputVal !== '') {
              const gridSR = await t.folderGrid.getSelectedRows();
              if (gridSR.length > 0) {
                t.uiDataSourceEvent.emit({ type: 'renameFile', element: 'folderTree', selectedFile: gridSR[0], name: obj.inputVal });
              } else {
                t.uiDataSourceEvent.emit({ type: 'renameFolder', element: 'folderTree', selectedFolder: t.selectedFolder, name: obj.inputVal });
              }
            }
            break;
          }
          case 'btnCancelRename': {
            t.fmDialog.opened = false;
            t.fmDialog.remove();
            break;
          }
        }
        break;
      }
      case 'row-select': {
        t.btnAddFolder.hidden = true;
        t.btnAddFile.hidden = true;
        t.btnRefresh.hidden = true;
        t.btnDelete.hidden = false;
        t.btnRename.hidden = false;
        t.btnDelete.disabled = false;
        t.btnRename.disabled = false;
        break;
      }
      case 'clear-row-select': {
        t.btnCut.hidden = true;
        t.btnCopy.hidden = true;
        t.btnPaste.hidden = true;
        t.btnDelete.hidden = false;
        t.btnAddFolder.hidden = false;
        t.btnAddFile.hidden = false;
        t.btnRename.hidden = false;
        if (t.selectedFolder?.isRoot || false) {
          t.btnAddFolder.disabled = false;
        }
        t.btnAddFile.disabled = false;
        if (typeof t.selectedFolder.isRoot === 'undefined') {
          t.btnDelete.disabled = false;
          t.btnRename.disabled = false;
        } else {
          t.btnDelete.disabled = true;
          t.btnRename.disabled = true;
        }
        t.btnRefresh.hidden = false;
        break;
      }
    }
  }

  private async dialogRenderer(options: any) {
    let t = this;
    const dialog: Dialog = document.createElement('vaadin-dialog');
    dialog.id = options?.id || 'dialog';
    dialog.ariaLabel = options?.id || 'dialog';
    dialog.headerTitle = options?.headerTitle || '';
    dialog.draggable = options?.draggable || true;
    dialog.modeless = options?.modeless || false;
    dialog.resizable = options?.resizable || false;
    dialog.noCloseOnEsc = options?.noCloseOnEsc || true;
    dialog.noCloseOnOutsideClick = options?.noCloseOnOutsideClick || true;
    //dialog.opened = options?.opened || false;
    dialog.opened = true;
    t.container.append(dialog);

    const currentVal = options?.inputVal || '';
    const vLayout = document.createElement('vaadin-vertical-layout');
    const vLayout2 = document.createElement('vaadin-vertical-layout');
    const fi = document.createElement('vaadin-text-field');
    fi.clearButtonVisible = true;
    fi.label = '';
    fi.maxlength = 50;
    fi.value = currentVal;
    fi.setAttribute("dataIndex", 'name');
    fi.setAttribute("style", 'flex-grow: 1;');
    vLayout2.appendChild(fi);
    vLayout2.style.alignItems = "stretch";
    vLayout.appendChild(vLayout2);
    vLayout.setAttribute("theme", "spacing");
    vLayout.style.alignItems = "stretch";
    const overlay = document.body.querySelector("vaadin-dialog-overlay[aria-label='" + dialog.ariaLabel + "']");
    (overlay as DialogOverlay).style.userSelect = "none";
    overlay.appendChild(vLayout);
    //const divOverlay = overlay.shadowRoot.querySelector("div[part='overlay']");
    //divOverlay.setAttribute("style", "user-select:none;");
    const div = document.createElement('div');
    div.setAttribute("slot", "footer");
    for (const item of options.buttons) {
      const btn = document.createElement('vaadin-button');
      btn.setAttribute("theme", item.theme);
      btn.appendChild(document.createTextNode(item.title));
      const iconButton = document.createElement('vaadin-icon');
      iconButton.setAttribute("slot", "prefix");
      iconButton.setAttribute("src", UIIcons[item.icon]);
      btn.appendChild(iconButton);
      div.appendChild(btn);
      btn.addEventListener('click'
        , async () => {
          t.appEvents.apply(t, [{
            type: 'button', element: item.element
            , data: []
            , input: fi
            , inputVal: fi.value
          }])
        }
        , { passive: true }
      );
      if (item.event === 'save') {
        (item as any).ref = btn;
      }
    }
    overlay.appendChild(div);
    const footer = overlay.shadowRoot.querySelector('footer');
    footer.setAttribute("style", "display:flex !important");
    fi.focus();
    return dialog;
  }

  render() {
    let t = this;
    const clientHeight: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 0) + 'px' : '600px';
    let formHeight: string = 'calc(100% - 48px)';
    let styleHeader: any = { height: "45px", width: 'calc(100% - 2px)', border: "1px solid var(--lumo-contrast-10pct)" };
    return (<div ref={(el) => t.container = el as HTMLDivElement} style={{ overflow: "auto" }} >
      <ui-loading-indicator
        ref={(el) => t.elLoadingIndicator = el as unknown as UILoadingIndicator}
        text='Lüften bekleyiniz...'
      >
      </ui-loading-indicator>
      <div class="flex flex--col" ref={(el) => t.contentContainer = el as HTMLDivElement} style={{}} >
        <div class="content" style={{ height: clientHeight }}>
          <div //ref={(el) => t.elContainerHeader = el as HTMLDivElement}
            style={styleHeader}>
            <vaadin-horizontal-layout
              class="flex--middle flex--wrap flex--left"
              theme="spacing"
              style={{ height: "100%", paddingLeft: "10px", paddingRight: "10px" }}
            >
              <vaadin-button theme="icon tertiary large" ref={(el) => t.btnAddFolder = el as Button}
                disabled={true}
                hidden={false}
                on-click={() => {
                  t.appEvents({ type: 'button', element: 'addFolder' });
                }}>
                <vaadin-icon src={UIIcons['fa__folder_plus']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Yeni Klasör"></vaadin-tooltip>
              </vaadin-button>
              <vaadin-button theme="icon tertiary large" ref={(el) => t.btnAddFile = el as Button}
                disabled={true}
                hidden={false}
                on-click={() => {
                  t.appEvents({ type: 'button', element: 'addFile' });
                }}>
                <vaadin-icon src={UIIcons['fa__file_circle_plus']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Yeni Dosya"></vaadin-tooltip>
              </vaadin-button>
              <vaadin-button theme="icon tertiary large" ref={(el) => t.btnCut = el as Button}
                disabled={false}
                hidden={true}
                on-click={() => {
                  t.appEvents({ type: 'button', element: 'cut' });
                }}>
                <vaadin-icon src={UIIcons['fa__scissors']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Kes"></vaadin-tooltip>
              </vaadin-button>
              <vaadin-button theme="icon tertiary large" ref={(el) => t.btnCopy = el as Button}
                disabled={false}
                hidden={true}
                on-click={() => {
                  t.appEvents({ type: 'button', element: 'copy' });
                }}>
                <vaadin-icon src={UIIcons['fa__copy']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Kopyala"></vaadin-tooltip>
              </vaadin-button>
              <vaadin-button theme="icon tertiary large" ref={(el) => t.btnPaste = el as Button}
                disabled={false}
                hidden={true}
                on-click={() => {
                  t.appEvents({ type: 'button', element: 'paste' });
                }}>
                <vaadin-icon src={UIIcons['fa__paste']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Yapıştır"></vaadin-tooltip>
              </vaadin-button>
              <vaadin-button theme="icon tertiary large" ref={(el) => t.btnDelete = el as Button}
                disabled={true}
                hidden={false}
                on-click={() => {
                  t.appEvents({ type: 'button', element: 'delete' });
                }}>
                <vaadin-icon src={UIIcons['fa__trash']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Sil"></vaadin-tooltip>
              </vaadin-button>
              <vaadin-button theme="icon tertiary large" ref={(el) => t.btnRename = el as Button}
                disabled={true}
                hidden={false}
                on-click={() => {
                  t.appEvents({ type: 'button', element: 'rename' });
                }}>
                <vaadin-icon src={UIIcons['fa__pen_to_square']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Yeniden adlandır"></vaadin-tooltip>
              </vaadin-button>
              <vaadin-button theme="icon tertiary large" ref={(el) => t.btnRefresh = el as Button}
                disabled={false}
                hidden={false}
                on-click={() => {
                  t.appEvents({ type: 'button', element: 'refresh' });
                }}>
                <vaadin-icon src={UIIcons['fa__rotate']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Yenile"></vaadin-tooltip>
              </vaadin-button>
            </vaadin-horizontal-layout>
          </div>
          <div style={{ height: formHeight, width: "calc(100% - 2px)", border: "1px solid var(--lumo-contrast-20pct)", borderTop: "none" }} >
            <vaadin-split-layout style={{ width: "100%", height: "100%" }}>
              <div style={{ width: "15%", height: "100%", maxWidth: "400px", minWidth: "280px" }}>
                <ui-tree
                  ref={(el) => t.folderTree = el as unknown as UITree}
                  height="100%" width="100%"
                  expanded={false}
                  isCheckableNode={false}
                  isEnableLoadingIndicator={false}
                  on-uiTreeNodeSelectEvent={async (e: CustomEvent) => {
                    await t.folderGrid.clearSelectedRows();
                    //console.log(t.myElement.tagName.toLowerCase(), 'uiTreeNodeSelectEvent:', e);
                    for (const fileItem of e.detail.files) {
                      //fileItem.select = false;
                      if (typeof fileItem.select !== 'undefined') {
                        delete fileItem.select;
                      }
                    }
                    t.selectedFolder = e.detail;
                    t.btnCut.hidden = true;
                    t.btnCopy.hidden = true;
                    t.btnPaste.hidden = true;
                    if (t.selectedFolder?.isRoot || false) {
                      t.btnAddFolder.disabled = false;
                    }
                    t.btnAddFile.disabled = false;
                    if (typeof t.selectedFolder.isRoot === 'undefined') {
                      t.btnDelete.disabled = false;
                      t.btnRename.disabled = false;
                    } else {
                      t.btnDelete.disabled = true;
                      t.btnRename.disabled = true;
                    }
                    //t.uiDataSourceEvent.emit({ type: 'loadData', element: 'folderTree', where: e.detail.path });
                    //t.elLoadingIndicator.setVisible(true);
                    t.folderGrid.setData({ records: e.detail.files });
                  }}
                  on-uiTreeNodeDeSelectEvent={async () => {
                    t.selectedFolder = null;
                    t.btnAddFolder.disabled = true;
                    t.btnAddFile.disabled = true;
                    t.btnDelete.disabled = true;
                    t.btnRename.disabled = true;
                    t.folderGrid.setData({ records: [] });
                  }}
                ></ui-tree>
              </div>
              <div style={{ width: "85%", height: "100%" }}>
                <ui-grid
                  ref={(el) => t.folderGrid = el as unknown as UIGrid}
                  columns={[
                    new DataTableColumn({ label: '', dataIndex: 'select', style: { width: "50px", textAlign: "center" }, type: "toggle", columnSelect: true })
                    , new DataTableColumn({ label: 'Adı', dataIndex: 'text', style: { maxWidth: "700px" } })
                    , new DataTableColumn({ label: 'Değişim Zamanı', dataIndex: 'modifiedtime', style: { width: "170px" } })
                    , new DataTableColumn({ label: 'Boyutu', dataIndex: 'size', style: { width: "100px", textAlign: "right" } })
                  ]}
                  selectionMode="single"
                  isSelectAll={false}
                  isShowFooter={false}
                  isShowHeader={false}
                  on-uiDataSourceEvent={(e: CustomEvent) => t.appEvents(e.detail)}
                  tableBorderTop='none'
                  tableBorderBottom='none'
                  tableBorderLeft='none'
                  tableBorderRight='none'
                ></ui-grid>
              </div>
            </vaadin-split-layout>
          </div>
        </div >
      </div>
    </div>
    );
  }
}
