import { EventEmitter } from '@stencil/core';
import { DataTableColumn } from '../px-interfaces';
import * as UIIcons from '../ui-icons';

import type { FormLayout, FormLayoutResponsiveStep } from '@vaadin/form-layout';
import { Dialog, DialogOverlay } from '@vaadin/dialog';
import { UIGridEditable } from '../ui-grid/ui-grid-editable';
import { UIGrid } from '../ui-grid/ui-grid';
import { format, parse } from "date-fns";
import { DatePickerDate } from '@vaadin/date-picker';
export class UIFunctions {
  async asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  };

  dateFormatIso8601(dateParts: DatePickerDate) {
    const { year, month, day } = dateParts;
    const date = new Date(year, month, day);
    //return dateFnsFormat(date, 'yyyy-MM-dd');
    return format(date, 'yyyy-MM-dd');
  }

  dateParseIso8601(inputValue: string) {
    //const date = dateFnsParse(inputValue, 'yyyy-MM-dd', new Date());
    const date = parse(inputValue, 'yyyy-MM-dd', new Date())
    return { year: date.getFullYear(), month: date.getMonth(), day: date.getDate() };
  }

  async dateCurrent() {
    const date = new Date();
    //return dateFnsFormat(date, 'yyyy-MM-dd');
    return format(date, 'yyyyMMdd_HHiiss');
  }

  async delay(ms: number) {
    return new Promise(res => setTimeout(res, ms));
  }

  // async clearFilterValues(container: FormLayout) {
  //   let t = this;
  //   const filterElements: NodeListOf<ChildNode> = await t.getFormElements(container);
  //   await t.asyncForEach(filterElements, async (item: any) => {
  //     if (typeof item.getAttribute === 'function') {
  //       const dataIndex: string = item.getAttribute('dataIndex');
  //       if (dataIndex !== null && dataIndex !== '') {
  //         const tagName: string = item.tagName.toLowerCase();
  //         if (tagName === "vaadin-integer-field") {
  //           item.value = '';
  //         } else if (tagName === "vaadin-checkbox") {
  //           item.checked = false;
  //         } else {
  //           item.value = '';
  //         }
  //         item.removeAttribute('invalid');
  //       }
  //     }
  //   });
  // }

  async generateDialog(scope: any, container: HTMLElement, columns: DataTableColumn[], cb: Function, options: any) {
    let selectedRow: any = null;
    const dialog: Dialog = document.createElement('vaadin-dialog');
    dialog.id = options?.id || 'dialog';
    dialog.ariaLabel = options?.id || 'dialog';
    dialog.headerTitle = options?.headerTitle || '';
    dialog.draggable = options?.draggable || true;
    dialog.modeless = options?.modeless || false;
    dialog.resizable = options?.resizable || false;
    dialog.noCloseOnEsc = options?.noCloseOnEsc || true;
    dialog.noCloseOnOutsideClick = options?.noCloseOnOutsideClick || true;
    //dialog.opened = options?.opened || false;
    dialog.opened = true;
    container.prepend(dialog);

    const vLayout = document.createElement('vaadin-vertical-layout');
    const vLayout2 = document.createElement('vaadin-vertical-layout');
    const grid = document.createElement(((options?.gridType || 'UIGrid') === 'UIGridEditable') ? 'ui-grid-editable' : 'ui-grid');
    const cols = columns || [];
    //grid.setAttribute("columns", JSON.stringify(cols));
    //grid.setAttribute("isfreeze", "true");
    //grid.setAttribute("height", "400px");
    //grid.setData({ records: t.selectedCustomRightData.custom_right });
    let dGrid;
    if ((options?.gridType || 'UIGrid') === 'UIGridEditable') {
      dGrid = grid as unknown as UIGridEditable;
      dGrid.isFreeze = true;
    } else {
      dGrid = grid as unknown as UIGrid;
      grid.id = 'ui-grid_' + dialog.id;
      dGrid.hiddenPageBarButtons = ['add', 'edit', 'del', 'filter', 'pager'];
      dGrid.isShowFooter = false;
      dGrid.isShowHeader = false;
      dGrid.isSortable = false;
    }
    if ((options?.isSearchField || false)) {
      const hLayout = document.createElement('vaadin-horizontal-layout');
      hLayout.setAttribute("style", 'width: 100%;align-items: baseline;');
      hLayout.setAttribute("theme", 'spacing-s');
      const fi = document.createElement('vaadin-text-field');
      fi.clearButtonVisible = true;
      fi.label = '';
      fi.maxlength = 50;
      fi.placeholder = 'Arama için buraya yazabilirsiniz...';
      fi.setAttribute("dataIndex", 'di');
      fi.setAttribute("style", 'flex-grow: 1;');
      hLayout.appendChild(fi);
      fi.addEventListener('keydown'
        , async (e: KeyboardEvent) => {
          if (e.key === 'Enter') {
            btnSearch.click();
          }
        }
        , { passive: true }
      );

      const btnSearch = document.createElement('vaadin-button');
      btnSearch.setAttribute("theme", "primary");
      btnSearch.appendChild(document.createTextNode("Ara"));
      const iconFilter = document.createElement('vaadin-icon');
      iconFilter.setAttribute("slot", "prefix");
      iconFilter.setAttribute("src", UIIcons['fa__magnifying_glass']);
      btnSearch.appendChild(iconFilter);
      hLayout.appendChild(btnSearch);
      btnSearch.addEventListener('click'
        //, cb.bind(scope
        //  , {
        //    type: 'button', element: (options?.searchEvent || 'btnLookupSearch')
        //    , data: fi.innerHTML
        //  }
        //)
        , async () => {
          cb.apply(scope, [{
            type: 'button', element: (options?.searchEvent || 'btnLookupSearch')
            , data: fi.value
          }]);
        }
        , { passive: true }
      );
      //if ((options?.searchValue || null) !== null) {
      fi.value = (((options?.searchValue || null) !== null) ? options?.searchValue : '');
      setTimeout(() => {
        btnSearch.click();
      }, 250);
      //}
      vLayout.appendChild(hLayout);
    }
    dGrid.height = '400px';
    dGrid.columns = cols;
    vLayout2.appendChild(grid);
    vLayout2.style.alignItems = "stretch";
    vLayout.appendChild(vLayout2);
    vLayout.setAttribute("theme", "spacing");
    vLayout.style.width = "980px";
    vLayout.style.maxWidth = "100%";
    vLayout.style.alignItems = "stretch";
    const overlay = await document.body.querySelector("vaadin-dialog-overlay[aria-label='" + dialog.ariaLabel + "']");
    (overlay as DialogOverlay).style.userSelect = "none";
    overlay.appendChild(vLayout);
    if ((options?.gridType || 'UIGrid') === 'UIGrid') {
      (async () => {
        await customElements.whenDefined('ui-grid');
        const elGrid = document.body.querySelector("ui-grid[id='ui-grid_" + dialog.id + "']");
        elGrid.addEventListener('uiDataSourceEvent', async (event: CustomEvent) => {
          //console.log('event.detail.type:', event.detail);
          if (event.detail.type === 'clear-row-select') {
            selectedRow = null;
          }
          if (event.detail.type === 'row-select') {
            selectedRow = event.detail.data;
          }
          if (event.detail.type === 'row-dblclick') {
            selectedRow = event.detail.data;
            for (const item of options?.buttons) {
              if (typeof item.ref !== 'undefined') {
                item.ref.click();
              }
            }
          }
          //console.log('selectedRow:', selectedRow);
        });
      })();
    }
    let tmpData: any[] = JSON.parse(JSON.stringify(options?.data || []));
    await dGrid.setData({ records: tmpData });
    if ((options?.buttons || []).length > 0) {
      const div = document.createElement('div');
      div.setAttribute("slot", "footer");
      for (const item of options?.buttons) {
        const btn = document.createElement('vaadin-button');
        btn.setAttribute("theme", item.theme);
        btn.appendChild(document.createTextNode(item.title));
        div.appendChild(btn);
        btn.addEventListener('click'
          , async () => {
            cb.apply(scope, [{
              type: 'button', element: item.element
              , data: (item.event === 'save' ? ((options?.gridType || 'UIGrid') === 'UIGridEditable' ? await dGrid.getData() :
                {
                  row: (options?.row || null)
                  , column: (options?.column || null)
                  , selectedLookupRow: selectedRow
                  , selectedLookupField: { id: selectedRow[columns[0].dataIndex], value: selectedRow[columns[1].dataIndex] }
                }) : [])
            }])
          }
          , { passive: true }
        );
        if (item.event === 'save') {
          item.ref = btn;
        }
      }
      overlay.appendChild(div);
    }
    const footer = overlay.shadowRoot.querySelector('footer');
    footer.setAttribute("style", "display:flex !important");
    return { dialog, dGrid };
  }

  async generateFilterForm(scope: any, container: HTMLElement, columns: DataTableColumn[], cb: Function) {
    //@@todo: diğer filtre elemet tipleri için kodlama yapılacak
    //let t = this;
    //console.log('generateFilterForm', container, columns);
    const buttons: any = [
      { part: 'filter_records', icon: 'fa__filter', text: 'Filtrele', theme: 'primary' }
      , { part: 'filter_clear', icon: 'fa__filter_circle_xmark', text: 'Temizle', theme: 'primary' }
      , { part: 'back', icon: 'vaadin__arrow_backward', text: 'Geri', theme: 'primary contrast' }
    ];
    const responsiveSteps: FormLayoutResponsiveStep[] = [
      // Use one column by default
      { minWidth: 0, columns: 1 },
      // Use two columns, if layout's width exceeds 500px
      { minWidth: '300px', columns: 2 },
    ];
    const dHeader = document.createElement('div');
    dHeader.setAttribute("style", 'height: 50px; width: calc(100% - 2px); border: 1px solid var(--lumo-contrast-20pct);');
    const headerButtonContainer = document.createElement('vaadin-horizontal-layout');
    headerButtonContainer.className = "flex--middle flex--wrap flex--right";
    headerButtonContainer.setAttribute("theme", "spacing");
    headerButtonContainer.setAttribute("style", 'height: 100%; padding-left: 10px; padding-right: 10px;');
    for (const item of buttons) {
      const btn = document.createElement('vaadin-button');
      btn.addEventListener('click', cb.bind(scope, { type: 'button', element: item.part }), { passive: true });
      btn.setAttribute("theme", item.theme);
      btn.setAttribute('part', item.part);
      const iconFilter = document.createElement('vaadin-icon');
      iconFilter.setAttribute("slot", "prefix");
      iconFilter.setAttribute("src", UIIcons[item.icon]);
      btn.appendChild(iconFilter);
      btn.appendChild(document.createTextNode(item.text));
      headerButtonContainer.appendChild(btn);
    }
    dHeader.appendChild(headerButtonContainer);
    container.appendChild(dHeader);
    const dContainer = document.createElement('div');
    dContainer.setAttribute("style", 'height: calc(100% - 53px); width: calc(100% - 2px); border: 1px solid var(--lumo-contrast-20pct); border-top:none;');
    const dContainerF = document.createElement('div');
    dContainerF.className = "flex flex--row flex--center"
    dContainerF.setAttribute("style", 'height: 100%; width: 100%;');
    const dContainerC = document.createElement('div');
    dContainerC.setAttribute("style", 'height: 100%; width: 40%;overflow-y:auto;');
    const containerFormLayout = document.createElement('vaadin-form-layout');
    containerFormLayout.setAttribute("style", 'padding: 10px;');
    containerFormLayout.responsiveSteps = responsiveSteps;
    for (const item of columns) {
      //console.log(item);
      if (typeof item.filter !== 'undefined' && item.filter !== null) {
        const fieldName: string = ((typeof item.alias !== 'undefined' && item.alias !== '') ? item.alias + '.' : '') + item.fieldName;
        if (item.filter.type === 'between') {
          if (item.filter.field === 'date') {
            const fi1 = document.createElement('vaadin-date-picker');
            fi1.autoOpenDisabled = true;
            fi1.clearButtonVisible = true;
            fi1.label = item.label;
            fi1.showWeekNumbers = true;
            fi1.setAttribute("dataIndex", fieldName + "|s");
            //fi1.setAttribute("show-week-numbers", "true");
            containerFormLayout.appendChild(fi1);
            const fi2 = document.createElement('vaadin-date-picker');
            fi2.autoOpenDisabled = true;
            fi2.clearButtonVisible = true;
            fi2.label = "";
            fi2.showWeekNumbers = true;
            fi2.setAttribute("dataIndex", fieldName + "|f");
            containerFormLayout.appendChild(fi2);
          }
          if (item.filter.field === 'number') {
            const fi1 = document.createElement('vaadin-number-field');
            fi1.clearButtonVisible = true;
            fi1.label = item.label;
            fi1.min = 0
            fi1.max = 999999999;
            fi1.value = "0";
            fi1.setAttribute("dataIndex", fieldName + "|s");
            fi1.setAttribute("theme", "align-right");
            containerFormLayout.appendChild(fi1);
            const fi2 = document.createElement('vaadin-number-field');
            fi2.clearButtonVisible = true;
            fi2.label = "";
            fi2.min = 0
            fi2.max = 999999999;
            fi2.value = "999999999";
            fi2.setAttribute("dataIndex", fieldName + "|f");
            fi2.setAttribute("theme", "align-right");
            containerFormLayout.appendChild(fi2);
          }
          if (item.filter.field === 'text') {
            const fi1 = document.createElement('vaadin-text-field');
            fi1.clearButtonVisible = true;
            fi1.label = item.label;
            fi1.maxlength = 50;
            fi1.setAttribute("dataIndex", fieldName + "|s");
            containerFormLayout.appendChild(fi1);
            const fi2 = document.createElement('vaadin-text-field');
            fi2.clearButtonVisible = true;
            fi2.label = "";
            fi2.maxlength = 50;
            fi2.setAttribute("dataIndex", fieldName + "|f");
            containerFormLayout.appendChild(fi2);
          }
        } else {
          if (item.filter.field === 'combo') {
            const fi = document.createElement('vaadin-combo-box');
            fi.label = item.label;
            fi.setAttribute("dataIndex", fieldName);
            fi.clearButtonVisible = true;
            fi.items = item.filter.details;
            containerFormLayout.appendChild(fi);
          }
          if (item.filter.field === 'email') {
            const fi = document.createElement('vaadin-email-field');
            fi.clearButtonVisible = true;
            fi.label = item.label;
            fi.maxlength = 250;
            fi.setAttribute("colSpan", "2");
            fi.setAttribute("dataIndex", fieldName);
            containerFormLayout.appendChild(fi);
          }
          if (item.filter.field === 'radio') {
            const fi = document.createElement('vaadin-radio-group');
            fi.label = item.label;
            fi.setAttribute("dataIndex", fieldName);
            for (const detail of item.filter.details) {
              const rb = document.createElement('vaadin-radio-button');
              rb.label = detail.label;
              rb.value = detail.value;
              fi.appendChild(rb);
            }
            containerFormLayout.appendChild(fi);
          }
          if (item.filter.field === 'text') {
            const fi = document.createElement('vaadin-text-field');
            fi.clearButtonVisible = true;
            fi.label = item.label;
            fi.maxlength = 50;
            fi.setAttribute("dataIndex", fieldName);
            containerFormLayout.appendChild(fi);
          }
          const p = document.createElement('p');
          containerFormLayout.appendChild(p);
        }
        //console.log('filter', item.filter);
      }
    }
    dContainerC.appendChild(containerFormLayout);
    dContainerF.appendChild(dContainerC);
    dContainer.appendChild(dContainerF);
    container.appendChild(dContainer);
    return containerFormLayout;
  }

  async getFilterValues(container: FormLayout) {
    let t = this;
    const filterElements: NodeListOf<ChildNode> = await t.getFormElements(container);
    let filterData: any = [];
    await t.asyncForEach(filterElements, async (item: any) => {
      if (typeof item.getAttribute === 'function') {
        const dataIndex: string = item.getAttribute('dataIndex');
        if (dataIndex !== null && dataIndex !== '') {
          let tmpItem: any = {
            condition: "",
            fields: [],
            type: '',
            keyword: ''
          };
          const tagName: string = item.tagName.toLowerCase();
          //const part: object = typeof item?.part?.value !== 'undefined' && item?.part?.value !== '' ? JSON.parse(item?.part?.value) : {};
          const arrDataIndex: string[] = dataIndex.split('|');
          tmpItem.fields = [...tmpItem.fields, arrDataIndex[0]];
          //console.log(tagName)
          if (tagName === "vaadin-integer-field" || tagName === 'vaadin-number-field') {
            //console.log('item.value:', item.value);
            //console.log('parseInt(item.value):', parseInt(item.value, 10));
            //console.log('isNaN(item.value):', isNaN(item.value));
            if (item.value !== '') {
              tmpItem.keyword = parseInt(item.value, 10);
              tmpItem.type = "int";
              if (arrDataIndex.length > 1) {
                if (arrDataIndex[1] === 's') {
                  tmpItem.condition = '>=';
                } else {
                  tmpItem.condition = '<=';
                }
              } else {
                tmpItem.condition = '=';
              }
            }
          } else if (tagName === "vaadin-checkbox") {
            tmpItem.keyword = item.checked;
            tmpItem.condition = '=';
            tmpItem.type = "boolean";
          } else if (tagName === "vaadin-radio-group") {
            tmpItem.keyword = item.value;
            tmpItem.condition = '=';
            tmpItem.type = "boolean";
          } else {
            tmpItem.type = "string";
            tmpItem.keyword = item.value;
            if (arrDataIndex.length > 1) {
              if (arrDataIndex[1] === 's') {
                tmpItem.condition = '>=';
              } else {
                tmpItem.condition = '<=';
              }
            } else {
              tmpItem.condition = 'like';
            }
          }
          if (tmpItem.keyword !== '') {
            filterData = [...filterData, tmpItem];
          }
        }
      }
    });
    return filterData;
  }

  async getFormElements(container: FormLayout) {
    //let t = this;
    return container.querySelectorAll('[dataIndex]');
  }

  async getFormValues(container: FormLayout) {
    let t = this;
    const formElements: NodeListOf<ChildNode> = await t.getFormElements(container);
    let isInvalid = false;
    let formData: any = {};
    await t.asyncForEach(formElements, async (item: any) => {
      if (typeof item.getAttribute === 'function') {
        const dataIndex: string = item.getAttribute('dataIndex');
        if (dataIndex !== null && dataIndex !== '') {
          if (typeof item.focus === 'function') {
            item.focus();
          }
          if (typeof item.blur === 'function') {
            item.blur();
          }
          if (item.invalid) {
            isInvalid = true;
          }
          const tagName: string = item.tagName.toLowerCase();
          if (tagName === "vaadin-integer-field") {
            formData[dataIndex] = parseInt(item.value, 10);
          } else if (tagName === "vaadin-checkbox") {
            formData[dataIndex] = item.checked;
          } else {
            const format: string = item.getAttribute('format');
            if (format === 'money') {
              formData[dataIndex] = item.value.split('.').join('').split(',').join('.');
            } else {
              formData[dataIndex] = item.value;
            }
          }
        }
      }
    });
    return { isInvalid, data: formData };
  }

  async getIndex(arr, key, keyValue) {
    if (typeof arr.length !== 'undefined') {
      let i: number = -1;
      if (arr.length > 0) {
        for (let row of arr) {
          i++;
          if (key.indexOf('|') === -1) {
            if (typeof row[key] !== 'undefined') {
              if (row[key].toString() === keyValue.toString()) {
                return i;
              }
              //} else {
              //  return -1;
            }
          } else {
            let f = true;
            let arrKey = key.split('|');
            let arrVal = keyValue.split('|');
            for (let j = 0; j < arrKey.length; j++) {
              if (typeof row[arrKey[j]] !== 'undefined' && typeof arrVal[j] !== 'undefined') {
                let vRow = row[arrKey[j]];
                let vVal = ((arrVal[j].toString() === 'true' || arrVal[j].toString() === 'false')
                  ? Boolean(arrVal[j])
                  : arrVal[j]
                );
                vRow = (vRow === null || vRow === 'null') ? '' : vRow;
                vVal = (vVal === null || vVal === 'null') ? '' : vVal;
                if (vRow.toString() !== vVal.toString()) {
                  f = false;
                }
              }
            }
            if (f) {
              return i;
            }
          }
        }
      }
    } else {
      for (let i in arr) {
        if (typeof arr[i] !== 'function') {
          if (key.indexOf('|') === -1) {
            if (typeof arr[i][key] !== 'undefined') {
              if (arr[i][key].toString() === keyValue.toString()) {
                return i as unknown as number;
              }
            }
          } else {
            let f = true;
            let arrKey = key.split('|');
            let arrVal = keyValue.split('|');
            for (let j = 0; j < arrKey.length; j++) {
              if (typeof arr[i][arrKey[j]] !== 'undefined' && typeof arrVal[j] !== 'undefined') {
                if (arr[i][arrKey[j]].toString() !== arrVal[j].toString()) {
                  f = false;
                }
              }
            }
            if (f) {
              return i as unknown as number;
            }
          }
        }
      }
    }
    return -1;
    //if (typeof arr.length !== 'undefined') {
    //  let i = -1;
    //  if (arr.length > 0) {
    //    for (let row of arr) {
    //      i++;
    //      if (typeof row[key] !== 'undefined') {
    //        if (row[key].toString() === keyValue.toString()) {
    //          return i;
    //        }
    //      } else {
    //        return -1;
    //      }
    //    }
    //  }
    //}
    //return -1;
  }

  async hideAllContainers(container: HTMLDivElement) {
    let t = this;
    //açık olan içerikler gizleniyor
    const contentContainers = container.querySelectorAll('.content');
    if (contentContainers.length > 0) {
      await t.asyncForEach(contentContainers, async (item: HTMLDivElement) => {
        if (!item.classList.contains('hidden')) {
          item.classList.add('hidden');
        }
      });
    }
  }

  async requestData(
    baseObj: {
      emitter: EventEmitter, controller: string, tagName: string, idx: string, recordsPerPage: number
    }, obj: {
      element: any, elementName: string, route: string, currentPage: number
      , where?: any, order?: any, report?: string
    }
  ) {
    const { emitter, controller, tagName, idx, recordsPerPage } = baseObj;
    const { element, elementName, route, currentPage, where, order, report } = obj;
    if (typeof element.beforeLoad === 'function') {
      await element.beforeLoad();
      //await delay(200);
    }
    //await delay(250);
    emitter.emit({
      event: 'sendData'
      , method: 'get'
      , controller
      , route
      , component: tagName
      , idx: idx
      , element: elementName
      , recordsPerPage: recordsPerPage
      , currentPage
      , where
      , order
      , report
    });
  }

  async setFormValues(container: FormLayout, data: any) {
    let t = this;
    const formElements: NodeListOf<ChildNode> = await t.getFormElements(container);
    //console.log('formElements', formElements, data)
    await t.asyncForEach(formElements, async (item: any) => {
      if (typeof item.getAttribute === 'function') {
        const dataIndex: string = item.getAttribute('dataIndex');
        if (dataIndex !== null && dataIndex !== '') {
          const tagName: string = item.tagName.toLowerCase();
          if (typeof data[dataIndex] !== 'undefined') {
            if (data[dataIndex] !== null) {
              const format: string = item.getAttribute('format');
              if (format !== null && format !== '') {
                const fractionDigits: number = item.getAttribute('fractionDigits');
                let cellValue = new Intl.NumberFormat('tr-TR', { minimumFractionDigits: (fractionDigits || 2), maximumFractionDigits: (fractionDigits || 2) }).format(data[dataIndex]);
                //console.log('dataIndex:', dataIndex, 'cellValue:', cellValue, 'tagName:', tagName);
                if (tagName === "vaadin-integer-field" || tagName === "vaadin-number-field") {
                  cellValue = cellValue.split('.').join('').split(',').join('.');
                }
                await t.setItemValue(item, tagName, cellValue);
              } else {
                await t.setItemValue(item, tagName, data[dataIndex]);
              }
              //if (tagName === 'ui-upload' && typeof data[dataIndex + '_thumbnail'] !== 'undefined') {
              //  item.thumbnail = data[dataIndex + '_thumbnail'];
              //}
            } else {
              await t.setItemValue(item, tagName, '');
            }
          } else {
            await t.setItemValue(item, tagName, '');
            if (typeof item.min !== 'undefined' && dataIndex.indexOf('|s') !== -1) {
              await t.setItemValue(item, tagName, item.min);
            }
            if (typeof item.max !== 'undefined' && dataIndex.indexOf('|f') !== -1) {
              await t.setItemValue(item, tagName, item.max);
            }
          }
          item.removeAttribute('invalid');
        }
      }
    });
  }

  async setItemValue(item: any, tagName: string, value: any) {
    //const tagName: string = item.tagName.toLowerCase();
    if (tagName === "vaadin-integer-field" || tagName === "vaadin-number-field") {
      item.value = value;
    } else if (tagName === "vaadin-checkbox") {
      item.checked = ((value === true || value === 1) ? true : false);
    } else if (tagName === "vaadin-upload") {
      //if (value === '') {
      //  if (item.files.length > 0) {
      //    item.files = [];
      //  }
      //}
      item.value = value;
    } else {
      item.value = value;
    }
    return;
  }

}
