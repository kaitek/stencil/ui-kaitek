import { Build, Component, Element, Event, EventEmitter, Method, Prop, h } from '@stencil/core';

import { DataTableColumn } from '../px-interfaces';
import { UIFunctions } from '../ui-functions/ui-functions';
import { UIGrid } from '../ui-grid/ui-grid';
import * as UIIcons from '../ui-icons';

import { Button } from '@vaadin/button';

import '@vaadin/button';
import '@vaadin/checkbox';
import '@vaadin/date-picker';
import '@vaadin/form-layout';
import '@vaadin/horizontal-layout';
import '@vaadin/icon';
import '@vaadin/number-field';
import '@vaadin/text-field';

import type { FormLayout, FormLayoutResponsiveStep } from '@vaadin/form-layout';
import { DatePicker } from '@vaadin/date-picker';

@Component({
  tag: 'app-stock',
  styleUrl: 'app-stock.scss',
  shadow: true
})
export class AppStock {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Prop() args: any = { idx: 1, clientHeight: 600, perm: { add: 1, edit: 1 } };

  uiFns: UIFunctions = new UIFunctions();

  contentContainer: HTMLDivElement;
  tabFilter: HTMLDivElement;
  tabForm: HTMLDivElement;
  tabList: HTMLDivElement;
  tabExtract: HTMLDivElement;
  filterLayout: FormLayout;
  formLayout: FormLayout;
  dataGrid: UIGrid;
  dataGridExtract: UIGrid;
  btnClose: Button;
  btnList: Button;
  btnSave: Button;
  btnFilter: Button;
  btnClear: Button;
  btnBack: Button;

  elExtractFinish: DatePicker;
  elExtractStart: DatePicker;
  currentPage: number = 1;
  recordsPerPage: number = 25;
  totalRecords: number = 0;

  selectedRecord: any = {};
  filterData: any[] = [];
  sortData: any[] = [];

  //isInvalid: boolean = false;
  baseRequestParams: any;

  async componentWillLoad() {
    //let t = this;
    //if (Build.isDev) {
    //  console.log('CWL|', t.myElement.tagName.toLowerCase());
    //}
  }

  async componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    t.baseRequestParams = {
      emitter: t.appEvent, controller: '', tagName: t.myElement.tagName.toLowerCase(), idx: t.args.idx, recordsPerPage: t.recordsPerPage
    };
    //const els = document.getElementsByTagName('html');
    //t.isDev = typeof els[0].attributes["isdev"] !== 'undefined';
    await t.uiFns.hideAllContainers(t.contentContainer);
    if (t.tabList.classList.contains('hidden')) {
      t.tabList.classList.remove('hidden');
    }
    t.filterLayout = await t.uiFns.generateFilterForm(t, t.tabFilter, t.dataGrid.columns, t.appEvents);
    //await t.uiFns.generateFilterForm(t.tabFilter, t.dataGrid.columns);
    //t.filterLayout = t.tabFilter.querySelector('vaadin-form-layout');
    //const itemsFireClick = t.tabFilter.querySelectorAll('vaadin-button');
    //if (itemsFireClick.length > 0) {
    //  await t.uiFns.asyncForEach(itemsFireClick, async (item: Button) => {
    //    item.addEventListener('click', t.appEvents.bind(t, { type: 'button', element: item.part[0] }), { passive: true });
    //  });
    //}

    const dp: NodeListOf<DatePicker> = t.tabExtract.querySelectorAll("vaadin-date-picker");
    //const dpf: NodeListOf<DatePicker> = t.tabFilter.querySelectorAll("vaadin-date-picker");
    // console.log(dp, dpf, Array.from(dp).concat(Array.from(dpf)));
    await t.uiFns.asyncForEach(Array.from(dp), async (item: DatePicker) => {
      item.i18n = {
        ...item.i18n,
        formatDate: t.uiFns.dateFormatIso8601,
        parseDate: t.uiFns.dateParseIso8601,
        firstDayOfWeek: 1,
        monthNames: [
          'Ocak',
          'Şubat',
          'Mart',
          'Nisan',
          'Mayıs',
          'Haziran',
          'Temmuz',
          'Ağustos',
          'Eylül',
          'Ekim',
          'Kasım',
          'Aralık',
        ],
        weekdays: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
        weekdaysShort: ['Paz', 'Pzt', 'Sal', 'Çrş', 'Prş', 'Cum', 'Cmt'],
        today: 'Bugün',
        cancel: 'Vazgeç',
      };
    });

    //@@todo: custom_right
    //console.log(t.args.perm);
    //t.appEvent.emit({ event: 'componentDidLoad', component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
    await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
    if (Build.isDev) {
      await t.uiFns.delay(500);
      await t.dataGrid.afterLoad();
    }
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
    //}
    switch (obj.element) {
      case 'dataGrid': {
        t.currentPage = obj.currentPage;
        t.recordsPerPage = obj.recordsPerPage;
        t.totalRecords = obj.totalRecords;
        t.dataGrid.setData(obj);
        break;
      }
      case 'dataGridExtract': {
        t.dataGridExtract.setData(obj);
        break;
      }
      default: {
        if (Build.isDev) {
          console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
        }
        break;
      }
    }
  }

  @Method()
  async callbackRequestData(obj: any, resp: any) {
    let t = this;
    if (Build.isDev) {
      console.log('CBRD|', t.myElement.tagName.toLowerCase(), obj, resp);
    }
    if (!t.tabExtract.classList.contains('hidden')) {
      await t.dataGridExtract.afterLoad();
    }
    await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
    return await t.appEvents({ type: 'button', element: 'list' });
  }

  @Method()
  async resize(height: number) {
    let t = this;
    //console.log('resize|', t.myElement.tagName.toLowerCase(), height, t.cacheData);
    t.tabFilter.style.height = (height - 2) + 'px';
    t.tabForm.style.height = (height - 2) + 'px';
    t.tabList.style.height = (height - 2) + 'px';
    t.tabExtract.style.height = (height - 2) + 'px';
    t.dataGrid.height = (height - 2) + 'px';
    t.dataGridExtract.height = (height - 50 - 2 - 2) + 'px';
    if (!t.tabList.classList.contains('hidden')) {
      setTimeout(async () => {
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
      }, 50);
    }
    if (!t.tabExtract.classList.contains('hidden')) {
      setTimeout(async () => {
        t.appEvents({ type: 'button', element: 'listExtract' });
      }, 50);
    }
  }

  async appEvents(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
    //}
    switch (obj.type) {
      case 'button': {
        switch (obj.element) {
          case 'first': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            break;
          }
          case 'prev': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: (t.currentPage - 1), where: t.filterData, order: t.sortData });
            break;
          }
          case 'next': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: (t.currentPage + 1), where: t.filterData, order: t.sortData });
            break;
          }
          case 'last': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: Math.ceil(t.totalRecords / t.recordsPerPage), where: t.filterData, order: t.sortData });
            break;
          }
          case 'add': {
            //t.appEvent.emit({ event: 'btn-' + obj.element, component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
            t.btnClose.disabled = false;
            t.btnList.disabled = false;
            if (t.args.perm.add !== 0) {
              t.btnSave.disabled = false;
            }
            t.dataGrid.clearSelectedRows();
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabForm.classList.contains('hidden')) {
              t.tabForm.classList.remove('hidden');
            }
            await t.uiFns.setFormValues(t.formLayout, {});
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'edit': {
            t.btnClose.disabled = false;
            t.btnList.disabled = false;
            if (t.args.perm.add !== 0) {
              t.btnSave.disabled = false;
            }
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabForm.classList.contains('hidden')) {
              t.tabForm.classList.remove('hidden');
            }
            await t.uiFns.setFormValues(t.formLayout, t.selectedRecord);
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'delete': {
            //const values: any = await t.getFormValues();
            if (t.args.perm.del === 0 && t.selectedRecord.id !== '') {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Bu işlem için yetkili değilsiniz.'
              });
            } else {
              t.appEvent.emit({
                event: 'sendData'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , data: t.selectedRecord
                , id: t.selectedRecord.id
                , method: 'delete'
              });
            }
            break;
          }
          case 'refresh': {
            await t.uiFns.setFormValues(t.filterLayout, {});
            t.filterData = [];
            t.sortData = [];
            t.dataGrid.clearSortData();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
            break;
          }
          case 'filter': {
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabFilter.classList.contains('hidden')) {
              t.tabFilter.classList.remove('hidden');
            }
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'save': {
            const values: any = await t.uiFns.getFormValues(t.formLayout);
            if (!values.isInvalid) {
              if (t.args.perm.add === 0 || (t.args.perm.add === 1 && t.args.perm.edit === 0 && values.data.id !== '')) {
                t.rendererEvent.emit({
                  event: 'showNotification'
                  , component: t.myElement.tagName.toLowerCase()
                  , idx: t.args.idx
                  , message: 'Bu işlem için yetkili değilsiniz.'
                });
                t.btnClose.disabled = false;
                t.btnList.disabled = false;
                if (t.args.perm.add !== 0) {
                  t.btnSave.disabled = false;
                }
              } else {
                t.appEvent.emit({
                  event: 'sendData'
                  , component: t.myElement.tagName.toLowerCase()
                  , idx: t.args.idx
                  , data: values.data
                  , method: (typeof values.data.id !== 'undefined' && values.data.id !== '' ? 'patch' : 'post')
                });
                if (Build.isDev) {
                  console.log('save', values);
                }
              }
            } else {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Gerekli alanları eksiksiz olarak doldurunuz.'
              });
              t.btnClose.disabled = false;
              t.btnList.disabled = false;
              if (t.args.perm.add !== 0) {
                t.btnSave.disabled = false;
              }
            }
            break;
          }
          case 'list': {
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.setFormValues(t.formLayout, {});
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
            break;
          }
          case 'close': {
            t.rendererEvent.emit({
              event: 'close'
              , component: t.myElement.tagName.toLowerCase()
              , idx: t.args.idx
            });
            break;
          }
          case 'back': {
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            break;
          }
          case 'filter_records': {
            //console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            t.filterData = await t.uiFns.getFilterValues(t.filterLayout);
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          case 'filter_clear': {
            await t.uiFns.setFormValues(t.filterLayout, {});
            t.filterData = [];
            t.sortData = [];
            t.dataGrid.clearSortData();
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabList.classList.contains('hidden')) {
              t.tabList.classList.remove('hidden');
            }
            await t.dataGrid.clearSelectedRows();
            await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
            if (Build.isDev) {
              await t.uiFns.delay(500);
              await t.dataGrid.afterLoad();
            }
            break;
          }
          case 'customerExtract': {
            await t.uiFns.hideAllContainers(t.contentContainer);
            if (t.tabExtract.classList.contains('hidden')) {
              t.tabExtract.classList.remove('hidden');
            }
            const d = new Date();
            t.elExtractStart.value = d.getFullYear().toString() + '-01-01';
            t.elExtractFinish.value = d.getFullYear().toString() + '-12-31';
            setTimeout(async () => {
              t.appEvents({ type: 'button', element: 'listExtract' });
            }, 50);
            break;
          }
          case 'listExtract': {
            await t.dataGridExtract.beforeLoad();
            await t.uiFns.requestData(t.baseRequestParams
              , {
                element: t.dataGridExtract, elementName: 'dataGridExtract', route: 'extract-records', currentPage: t.currentPage
                , where: [
                  { condition: "=", fields: ["id"], type: "int", keyword: t.selectedRecord.id }
                  , { condition: "=", fields: ["date_start"], type: "string", keyword: t.elExtractStart.value }
                  , { condition: "=", fields: ["date_finish"], type: "string", keyword: t.elExtractFinish.value }
                ]
                , order: []
              });
            if (Build.isDev) {
              await t.uiFns.delay(800);
              await t.dataGridExtract.afterLoad();
            }
            break;
          }
          default: {
            console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            break;
          }
        }
        break;
      }
      case 'change-currentPage': {
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: parseInt(obj.data, 10), where: t.filterData, order: t.sortData });
        break;
      }
      case 'change-recordsPerPage': {
        t.recordsPerPage = parseInt(obj.data, 10);
        t.baseRequestParams.recordsPerPage = t.recordsPerPage;
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: 1, where: t.filterData, order: t.sortData });
        break;
      }
      case 'clear-row-select': {
        t.selectedRecord = {};
        t.dataGrid.setButtonState('customerExtract', 'disabled');
        break;
      }
      case 'col-sort': {
        t.sortData = obj.data;
        //console.log('col-sort',t.sortData);
        await t.uiFns.requestData(t.baseRequestParams, { element: t.dataGrid, elementName: 'dataGrid', route: 'filtered-records', currentPage: t.currentPage, where: t.filterData, order: t.sortData });
        break;
      }
      case 'row-dblclick': {
        t.selectedRecord = obj.data;
        t.appEvents({ type: 'button', element: 'edit' });
        break;
      }
      case 'row-select': {
        t.selectedRecord = obj.data;
        t.dataGrid.setButtonState('customerExtract', 'enabled');
        break;
      }
      default: {
        console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
        break;
      }
    }
  }

  private responsiveSteps: FormLayoutResponsiveStep[] = [
    // Use one column by default
    { minWidth: 0, columns: 1 },
    // Use two columns, if layout's width exceeds 500px
    { minWidth: '500px', columns: 2 },
  ];

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('render|', t.myElement.tagName.toLowerCase());
    //}
    const clientHeight: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 2) + 'px' : '600px';
    const clientExtract: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 2 - 52) + 'px' : '550px';
    let formHeight: string = 'calc(100% - 0px)';
    let styleHeader: any = { height: "50px", width: 'calc(100% - 2px)', border: "1px solid var(--lumo-contrast-20pct)" };
    formHeight = 'calc(100% - 53px)';
    return (
      <div ref={(el) => t.contentContainer = el as HTMLDivElement}>
        <div ref={(el) => t.tabFilter = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>

        </div >
        <div ref={(el) => t.tabForm = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <div //ref={(el) => t.elContainerHeader = el as HTMLDivElement}
            style={styleHeader}>
            <vaadin-horizontal-layout
              class="flex--middle flex--wrap flex--right"
              theme="spacing"
              style={{ height: "100%", paddingLeft: "10px", paddingRight: "10px" }}
            >
              <vaadin-button theme="primary" ref={(el) => t.btnSave = el as Button}
                disabled={t.args.perm.add === 0}
                hidden={t.args.perm.add === 0}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'save' });
                }}>
                <vaadin-icon src={UIIcons['fa__check']} slot="prefix"></vaadin-icon>
                Kaydet
              </vaadin-button>
              <vaadin-button theme="primary success" ref={(el) => t.btnList = el as Button}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'list' });
                }}>
                <vaadin-icon src={UIIcons['fa__list']} slot="prefix"></vaadin-icon>
                Listele
              </vaadin-button>
              <vaadin-button theme="primary contrast" ref={(el) => t.btnClose = el as Button}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.btnList.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'close' });
                }}>
                <vaadin-icon src={UIIcons['fa__square_xmark']} slot="prefix"></vaadin-icon>
                Kapat
              </vaadin-button>
            </vaadin-horizontal-layout>
          </div>
          <div style={{ height: formHeight, width: "calc(100% - 2px)", border: "1px solid var(--lumo-contrast-20pct)", borderTop: "none" }} >
            <div class="flex flex--row flex--center" style={{ height: "100%", width: "100%" }}>
              <div style={{ height: "100%", width: "50%", overflowY: "auto" }}>
                <vaadin-form-layout ref={(el) => t.formLayout = el as FormLayout} responsiveSteps={t.responsiveSteps} style={{ padding: "10px" }}>
                  <vaadin-text-field
                    label="ID"
                    dataIndex="id"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>
                  <vaadin-text-field
                    label="version"
                    dataIndex="version"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>
                  <vaadin-checkbox
                    class="label"
                    label="Aktif"
                    dataIndex="is_active"
                  ></vaadin-checkbox>
                  <vaadin-checkbox
                    class="label"
                    label="Stok Takibi"
                    dataIndex="is_tracking"
                  ></vaadin-checkbox>
                  <vaadin-text-field
                    label="Stok Kodu"
                    dataIndex="code"
                    max-length="50"
                    clear-button-visible
                    required
                  ></vaadin-text-field>
                  <vaadin-text-field
                    label="Barkod"
                    dataIndex="barcode"
                    max-length="50"
                    clear-button-visible
                  ></vaadin-text-field>
                  <vaadin-text-field
                    label="Stok Adı"
                    dataIndex="name"
                    max-length="250"
                    colspan="2"
                    clear-button-visible
                    required
                  ></vaadin-text-field>
                  <vaadin-number-field
                    label="Başlangıç Stok Miktarı"
                    dataIndex="quantity_start"
                    theme="align-right"
                    value="0"
                    clear-button-visible
                    required
                    format='money'
                    fractionDigits="2"
                  ></vaadin-number-field>
                  <vaadin-number-field
                    label="Mevcut Stok"
                    dataIndex="quantity"
                    theme="align-right"
                    value="0"
                    readonly
                    format='money'
                    fractionDigits="2"
                  ></vaadin-number-field>
                  <vaadin-number-field
                    label="Satış Fiyatı"
                    dataIndex="price"
                    theme="align-right"
                    helper-text="Vergiler hariç"
                    clear-button-visible
                    required
                    format='money'
                    fractionDigits="2"
                  ></vaadin-number-field>
                  <vaadin-number-field
                    label="KDV (%)"
                    dataIndex="tax"
                    theme="align-right"
                    clear-button-visible
                    required
                    value="20"
                  ></vaadin-number-field>

                </vaadin-form-layout>
              </div>
            </div>
          </div>
        </div >
        <div ref={(el) => t.tabList = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <ui-grid
            ref={(el) => t.dataGrid = el as unknown as UIGrid}
            columns={[
              //{ label: 'Seç', dataIndex: 'select', style: { width: "60px", textAlign: "center" }, type: "toggle", columnSelect: true }
              new DataTableColumn({ label: 'Aktif', dataIndex: 'is_active', style: { width: "100px", textAlign: "center" }, type: "toggle", filter: { field: 'radio', details: [{ value: "true", label: "Evet" }, { value: "false", label: "Hayır" }] } })
              , new DataTableColumn({ label: 'Stok Kodu', dataIndex: 'code', style: { width: "150px", maxWidth: "200px" }, alias: "stocks", filter: { field: 'text', type: 'between' } })
              , new DataTableColumn({ label: 'Barkod', dataIndex: 'barcode', style: { width: "150px" }, filter: { field: 'text', type: 'between' } })
              , new DataTableColumn({ label: 'Stok Adı', dataIndex: 'name', style: { width: "900px" }, filter: { field: 'text', type: 'between' } })
              , new DataTableColumn({ label: 'Fiyatı', dataIndex: 'price', style: { width: "120px", textAlign: "right" }, filter: { field: 'number', type: 'between' }, options: { format: 'money', fractionDigits: 2 } })
              , new DataTableColumn({ label: 'Stok', dataIndex: 'quantity', style: { width: "120px", textAlign: "right" }, filter: { field: 'number', type: 'between' }, options: { format: 'money', fractionDigits: 2 } })
              , new DataTableColumn({ label: 'Stok Takibi', dataIndex: 'is_tracking', style: { width: "120px", textAlign: "center" }, type: "toggle", filter: { field: 'radio', details: [{ value: "true", label: "Yapılsın" }, { value: "false", label: "Yapılmasın" }] } })
            ]}
            height={clientHeight}
            footerButtons={[
              //{
              //  theme: "icon tertiary large"
              //  , part: "customerSummary"
              //  , icon: "vaadin__m_calculator_variant"
              //  , tooltip: 'Hesap Özeti'
              //  , text: ''
              //},
              {
                theme: "icon tertiary large"
                , part: "customerExtract"
                , icon: "fa__file_excel"
                , tooltip: 'Hesap Ekstresi'
                , text: ''
                , disabled: true
              }
            ]}
            isShowFooter={true}
            isSortMultiple={true}
            hiddenPageBarButtons={(t.args.perm.del === 0 ? ['del'] : [])}
            on-uiDataSourceEvent={(e: CustomEvent) => t.appEvents(e.detail)}
          ></ui-grid>
        </div >
        <div ref={(el) => t.tabExtract = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <div class="flex flex--row"
            style={styleHeader}>
            <div style={{ height: "100%", width: "50%" }}>
              <vaadin-horizontal-layout
                class="flex--middle flex--wrap flex--left"
                theme="spacing"
                style={{ height: "100%", paddingLeft: "10px", paddingRight: "10px" }}
              >
                <vaadin-form-item>
                  <label slot="label" style={{ paddingRight: "10px" }}>Zaman Aralığı</label>
                  <vaadin-date-picker
                    ref={(el) => t.elExtractStart = el as DatePicker}
                    dataIndex="date_start"
                    helper-text=""
                    placeholder={''}
                    clear-button-visible={true}

                    show-week-numbers={true}
                    auto-open-disabled={true}

                    disabled={false}
                    readonly={false}
                    required
                    class={"focusable "}
                    style={{ width: "160px" }}
                    value={''}
                  ></vaadin-date-picker>
                </vaadin-form-item>
                <vaadin-form-item>
                  <label slot="label" style={{ paddingRight: "15px" }}>-</label>
                  <vaadin-date-picker
                    ref={(el) => t.elExtractFinish = el as DatePicker}
                    dataIndex="date_finish"
                    helper-text=""
                    placeholder={''}
                    clear-button-visible={true}

                    show-week-numbers={true}
                    auto-open-disabled={true}

                    disabled={false}
                    readonly={false}
                    required
                    class={"focusable "}
                    style={{ width: "160px" }}
                    value={''}
                  ></vaadin-date-picker>
                </vaadin-form-item>
                <vaadin-button theme="primary"
                  on-click={() => {
                    if (!t.elExtractStart.invalid && !t.elExtractFinish.invalid) {
                      t.appEvents({ type: 'button', element: 'listExtract' });
                    }
                  }}>
                  <vaadin-icon src={UIIcons['fa__magnifying_glass']} slot="prefix"></vaadin-icon>
                  Getir
                </vaadin-button>
              </vaadin-horizontal-layout>
            </div>
            <div style={{ height: "100%", width: "50%" }}>
              <vaadin-horizontal-layout
                class="flex--middle flex--wrap flex--right"
                theme="spacing"
                style={{ height: "100%", paddingLeft: "10px", paddingRight: "10px" }}
              >
                <vaadin-button theme="primary contrast"
                  on-click={() => {
                    t.appEvents({ type: 'button', element: 'list' });
                  }}>
                  <vaadin-icon src={UIIcons['vaadin__arrow_backward']} slot="prefix"></vaadin-icon>
                  Geri
                </vaadin-button>
              </vaadin-horizontal-layout>
            </div>
          </div>
          <div style={{ height: formHeight, width: "calc(100% - 2px)", border: "1px solid var(--lumo-contrast-20pct)", borderTop: "none" }} >
            <div class="flex flex--row flex--center" style={{ height: "100%", width: "100%" }}>
              <ui-grid
                ref={(el) => t.dataGridExtract = el as unknown as UIGrid}
                columns={[
                  new DataTableColumn({ label: 'Tarih', dataIndex: 'transaction_date', style: { width: "160px" } })
                  , new DataTableColumn({ label: 'İşlem', dataIndex: 'info', style: { width: "300px" } })
                  , new DataTableColumn({ label: 'Açıklama', dataIndex: 'description', style: { width: "400px" } })
                  , new DataTableColumn({ label: 'Giriş', dataIndex: 'purchased', style: { width: "120px", textAlign: "right" }, options: { format: 'money', fractionDigits: 2 } })
                  , new DataTableColumn({ label: 'Çıkış', dataIndex: 'sold', style: { width: "120px", textAlign: "right" }, options: { format: 'money', fractionDigits: 2 } })
                  , new DataTableColumn({ label: 'Kalan', dataIndex: 'balance', style: { width: "120px", textAlign: "right" }, options: { format: 'money', fractionDigits: 2 } })
                ]}
                height={clientExtract}
                isShowFooter={false}
                isShowHeader={false}
                isSortable={false}
                tableBorderBottom="none"
                tableBorderTop="none"
              //tableBorderLeft="none"
              //tableBorderRight="none"
              ></ui-grid>
            </div>
          </div>
        </div>
      </div >
    );
  }
}
