import { Build, Component, Element, Event, EventEmitter, h, Method, Prop } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';
//px-components
import '@vaadin/button';
import { PxDataTable } from '../px-data-table/px-data-table';
//ui-components
import { Button } from '@vaadin/button';
import * as UIIcons from '../ui-icons';
import { UILoadingIndicator } from '../ui-loading-indicator/ui-loading-indicator';
import { UIPagebar } from '../ui-pagebar/ui-pagebar';
import { DataTableColumn } from '../px-interfaces';
// interfaces
//import { GridDataSource } from '../ui-interfaces/ui-gridDataSource';

@Component({
  tag: 'ui-grid',
  styleUrl: 'ui-grid.scss',
  shadow: true
})
export class UIGrid {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'uiDataSourceEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uiDataSourceEvent: EventEmitter;

  @Prop() args: any;
  @Prop() columnRenderer: string = 'default';
  @Prop() height: string = 'calc(100%)';
  @Prop() width: string = '100%';
  @Prop() isSelectAll: boolean = false;
  @Prop() isShowHeader: boolean = true;
  @Prop() isShowFooter: boolean = true;
  @Prop() isSortable: boolean = true;
  @Prop() isSortMultiple: boolean = false;
  @Prop() selectionMode: 'none' | 'single' | 'multi' = 'single';
  @Prop() columns: DataTableColumn[] = Build.isDev ? [{ label: 'Seç', dataIndex: 'select', style: { width: "60px", textAlign: "center", }, type: "toggle", columnSelect: true }
    , { label: 'Adı', dataIndex: 'first_name', style: { width: "200px" } }
    , { label: 'Soyadı', dataIndex: 'last_name', style: { width: "100px" } }
    , { label: 'e-mail', dataIndex: 'email' }] : [];
  @Prop() hiddenPageBarButtons: any = Build.isDev ? [] : [];
  @Prop() footerButtons: any = Build.isDev ? [] : [];
  @Prop() tableBorderTop: string = "1px solid var(--lumo-contrast-20pct)";
  @Prop() tableBorderBottom: string = "1px solid var(--lumo-contrast-20pct)";
  @Prop() tableBorderLeft: string = "1px solid var(--lumo-contrast-20pct)";
  @Prop() tableBorderRight: string = "1px solid var(--lumo-contrast-20pct)";
  //isDev: boolean = false;

  uiFns: UIFunctions = new UIFunctions();

  pxDataTable: PxDataTable;
  elContainerFooter: HTMLDivElement;
  elContainerHeader: HTMLDivElement;
  elLoadingIndicator: UILoadingIndicator;
  elPagebar: UIPagebar;
  //dataSource: GridDataSource;

  btnFilter: Button;
  records: any = [];
  sorts: any[] = [];

  //async componentWillLoad() {
  //  let t = this;
  //}

  async componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    if (t.hiddenPageBarButtons.length > 0) {
      for (const item of t.hiddenPageBarButtons) {
        await t.elPagebar.setButtonDisplay(item, 'hidden');
      }
    }
    //const els = document.getElementsByTagName('html');
    //t.isDev = typeof els[0].attributes["isdev"] !== 'undefined';

    /*if (Build.isDev) {
      t.records = [
        {
          "id": "1",
          "title": "Leo Williams",
          "first_name": "Leo",
          "last_name": "Williams",
          "modifiedtime": "2024-08-08 15:00:00",
          "filesize": "100KB",
          "address": {
            "street": "4439 Clear Fox Crossing",
            "city": "Hemingway Town",
            "state": "Tennessee",
            "zip": "37983-8021",
            "country": "USA",
            "phone": "(731) 555-5556"
          },
          "email": "leo.williams@company.com",
          "avatar": "user-avatars/e54c5de4-9f17-433e-9ece-d01fd59adb41.PNG",
          "avatar_thumbnail": "user-avatars/e54c5de4-9f17-433e-9ece-d01fd59adb41-thumbnail.PNG",
          "debt": "1123000.1000",
          "credit": "-1000.0000",
          "balance": "1500.2500",
          "balance_start": "1500.2500",
          "quantity": "1.0000",
          "quantity_start": "5.0000",
          "price": "3526.2000",
          "currency": "TL",
          p_menu: true,
          p_add: false,
          p_edit: true,
          p_del: false
        },
        {
          "id": "2",
          "first_name": "Easton",
          "last_name": "Morgan",
          "address": {
            "street": "8892 Wishing Leaf Parade",
            "city": "Jenny Lind",
            "state": "Pennsylvania",
            "zip": "18926-4904",
            "country": "USA",
            "phone": "(215) 555-7870"
          },
          "email": "easton.morgan@company.com"
        },
        {
          "id": "3",
          "first_name": "Samantha",
          "last_name": "Miller",
          "address": {
            "street": "4439 Clear Fox Crossing",
            "city": "Hemingway Town",
            "state": "Tennessee",
            "zip": "37983-8021",
            "country": "USA",
            "phone": "(731) 555-5556"
          },
          "email": "samantha.miller@company.com"
        },
        {
          "first_name": "Madeline",
          "last_name": "Robinson",
          "address": {
            "street": "1330 Lazy Fawn Loop",
            "city": "South Pole",
            "state": "Wisconsin",
            "zip": "54248-2841",
            "country": "USA",
            "phone": "(608) 555-5438"
          },
          "email": "madeline.robinson@company.com"
        },
        {
          "first_name": "Katherine",
          "last_name": "Foster",
          "address": {
            "street": "3946 Cozy Turnabout",
            "city": "Hog Heaven",
            "state": "Texas",
            "zip": "79747-9284",
            "country": "USA",
            "phone": "(409) 555-4708"
          },
          "email": "katherine.foster@company.com"
        },
        {
          "first_name": "Stella",
          "last_name": "Long",
          "address": {
            "street": "2560 Iron Brook Downs",
            "city": "Friendship",
            "state": "Tennessee",
            "zip": "38489-1588",
            "country": "USA",
            "phone": "(865) 555-9665"
          },
          "email": "stella.long@company.com"
        },
        {
          "first_name": "Jayden",
          "last_name": "Robinson",
          "address": {
            "street": "6609 Old Anchor Stead",
            "city": "Suspension",
            "state": "North Dakota",
            "zip": "58306-5667",
            "country": "USA",
            "phone": "(701) 555-6229"
          },
          "email": "jayden.robinson@company.com"
        },
        {
          "first_name": "Emma",
          "last_name": "Kelly",
          "address": {
            "street": "9874 Quiet Isle",
            "city": "Paydown",
            "state": "Connecticut",
            "zip": "06323-9300",
            "country": "USA",
            "phone": "(475) 555-6713"
          },
          "email": "emma.kelly@company.com"
        },
        {
          "first_name": "Hannah",
          "last_name": "Robinson",
          "address": {
            "street": "4439 Clear Fox Crossing",
            "city": "Hemingway Town",
            "state": "Tennessee",
            "zip": "37983-8021",
            "country": "USA",
            "phone": "(731) 555-5556"
          },
          "email": "hannah.robinson@company.com"
        },
        {
          "first_name": "Alexandra",
          "last_name": "Stewart",
          "address": {
            "street": "4623 Silent Place",
            "city": "Kickapoo",
            "state": "Indiana",
            "zip": "47603-4383",
            "country": "USA",
            "phone": "(317) 555-8105"
          },
          "email": "alexandra.stewart@company.com"
        },
        {
          "first_name": "Claire",
          "last_name": "Anderson",
          "address": {
            "street": "3851 Gentle Hill",
            "city": "Zodiac",
            "state": "Delaware",
            "zip": "19970-0027",
            "country": "USA",
            "phone": "(302) 555-2277"
          },
          "email": "claire.anderson@company.com"
        },
        {
          "first_name": "Jace",
          "last_name": "Howard",
          "address": {
            "street": "5921 Silver Diversion",
            "city": "Parchment",
            "state": "Texas",
            "zip": "76301-0113",
            "country": "USA",
            "phone": "(915) 555-9170"
          },
          "email": "jace.howard@company.com"
        },
        {
          "first_name": "Oliver",
          "last_name": "White",
          "address": {
            "street": "2139 Dewy Autumn Orchard",
            "city": "Wax",
            "state": "Florida",
            "zip": "34109-9657",
            "country": "USA",
            "phone": "(863) 555-4371"
          },
          "email": "oliver.white@company.com"
        },
        {
          "first_name": "Joseph",
          "last_name": "Lewis",
          "address": {
            "street": "2375 Thunder Beacon Landing",
            "city": "Beaverkill",
            "state": "Virginia",
            "zip": "24056-9087",
            "country": "USA",
            "phone": "(571) 555-1697"
          },
          "email": "joseph.lewis@company.com"
        },
        {
          "first_name": "Ryan",
          "last_name": "Evans",
          "address": {
            "street": "2375 Thunder Beacon Landing",
            "city": "Beaverkill",
            "state": "Virginia",
            "zip": "24056-9087",
            "country": "USA",
            "phone": "(571) 555-1697"
          },
          "email": "ryan.evans@company.com"
        },
        {
          "first_name": "Michael",
          "last_name": "Morgan",
          "address": {
            "street": "92 Harvest Court",
            "city": "Chickasawhatchee",
            "state": "Vermont",
            "zip": "05892-3796",
            "country": "USA",
            "phone": "(802) 555-3041"
          },
          "email": "michael.morgan@company.com"
        },
        {
          "first_name": "Alyssa",
          "last_name": "Scott",
          "address": {
            "street": "7664 Cotton Leaf Pathway",
            "city": "Skamokawa",
            "state": "Michigan",
            "zip": "49087-0251",
            "country": "USA",
            "phone": "(313) 555-1120"
          },
          "email": "alyssa.scott@company.com"
        },
        {
          "first_name": "Tristan",
          "last_name": "Sanchez",
          "address": {
            "street": "1846 Broad Log By-pass",
            "city": "Bucksnort",
            "state": "Indiana",
            "zip": "47082-9236",
            "country": "USA",
            "phone": "(317) 555-2631"
          },
          "email": "tristan.sanchez@company.com"
        },
        {
          "first_name": "Sadie",
          "last_name": "Collins",
          "address": {
            "street": "44 Tawny Knoll",
            "city": "Ocean Roar",
            "state": "Kansas",
            "zip": "67746-1597",
            "country": "USA",
            "phone": "(316) 555-1193"
          },
          "email": "sadie.collins@company.com"
        },
        {
          "first_name": "Brianna",
          "last_name": "Reyes",
          "address": {
            "street": "9386 Crystal Oak Beach",
            "city": "Bacon",
            "state": "Maryland",
            "zip": "21997-5878",
            "country": "USA",
            "phone": "(240) 555-5122"
          },
          "email": "brianna.reyes@company.com"
        },
        {
          "first_name": "Grace",
          "last_name": "Rodriguez",
          "address": {
            "street": "1060 Gentle Village",
            "city": "Beehunter",
            "state": "Idaho",
            "zip": "83534-8575",
            "country": "USA",
            "phone": "(208) 555-7520"
          },
          "email": "grace.rodriguez@company.com"
        },
        {
          "first_name": "Chase",
          "last_name": "Young",
          "address": {
            "street": "4783 Merry Sky Glade",
            "city": "Blowville",
            "state": "Wyoming",
            "zip": "83099-4319",
            "country": "USA",
            "phone": "(307) 555-7524"
          },
          "email": "chase.young@company.com"
        },
        {
          "first_name": "Leo",
          "last_name": "Young",
          "address": {
            "street": "8787 Velvet Deer Via",
            "city": "First Cliff",
            "state": "North Carolina",
            "zip": "27141-0975",
            "country": "USA",
            "phone": "(252) 555-2998"
          },
          "email": "leo.young@company.com"
        },
        {
          "first_name": "Austin",
          "last_name": "Allen",
          "address": {
            "street": "2718 Cozy Range",
            "city": "Mousie",
            "state": "Montana",
            "zip": "59984-7576",
            "country": "USA",
            "phone": "(406) 555-0326"
          },
          "email": "austin.allen@company.com"
        },
        {
          "first_name": "Wyatt",
          "last_name": "Morales",
          "address": {
            "street": "9478 Hazy Green",
            "city": "Ragged Top",
            "state": "New Hampshire",
            "zip": "03658-0772",
            "country": "USA",
            "phone": "(603) 555-5774"
          },
          "email": "wyatt.morales@company.com"
        },
        {
          "first_name": "Nora",
          "last_name": "Brooks",
          "address": {
            "street": "4783 Merry Sky Glade",
            "city": "Blowville",
            "state": "Wyoming",
            "zip": "83099-4319",
            "country": "USA",
            "phone": "(307) 555-7524"
          },
          "email": "nora.brooks@company.com"
        },
        {
          "first_name": "Alexis",
          "last_name": "Campbell",
          "address": {
            "street": "8635 Gentle Deer Heath",
            "city": "Dutch John",
            "state": "Colorado",
            "zip": "80166-1633",
            "country": "USA",
            "phone": "(303) 555-6720"
          },
          "email": "alexis.campbell@company.com"
        },
        {
          "first_name": "Robert",
          "last_name": "Lopez",
          "address": {
            "street": "5232 Merry Street",
            "city": "Poker Flat",
            "state": "California",
            "zip": "92157-2896",
            "country": "USA",
            "phone": "(714) 555-2238"
          },
          "email": "robert.lopez@company.com"
        },
        {
          "first_name": "Asher",
          "last_name": "King",
          "address": {
            "street": "2157 Easy Leaf Wharf",
            "city": "Plenty Bears",
            "state": "Illinois",
            "zip": "62792-5590",
            "country": "USA",
            "phone": "(630) 555-9818"
          },
          "email": "asher.king@company.com"
        },
        {
          "first_name": "Brianna",
          "last_name": "Russell",
          "address": {
            "street": "1875 Dewy Corners",
            "city": "Blackhawk",
            "state": "Wyoming",
            "zip": "82373-2034",
            "country": "USA",
            "phone": "(307) 555-6261"
          },
          "email": "brianna.russell@company.com"
        },
        {
          "first_name": "Anna",
          "last_name": "Anderson",
          "address": {
            "street": "8731 Emerald Woods",
            "city": "Old Roach",
            "state": "Alabama",
            "zip": "36429-6229",
            "country": "USA",
            "phone": "(256) 555-0619"
          },
          "email": "anna.anderson@company.com"
        },
        {
          "first_name": "Katherine",
          "last_name": "Campbell",
          "address": {
            "street": "2096 Misty Valley",
            "city": "Basalt",
            "state": "Maryland",
            "zip": "20757-2037",
            "country": "USA",
            "phone": "(443) 555-5124"
          },
          "email": "katherine.campbell@company.com"
        },
        {
          "first_name": "Abigail",
          "last_name": "Powell",
          "address": {
            "street": "2514 Thunder Square",
            "city": "Confidence",
            "state": "New Jersey",
            "zip": "07454-7884",
            "country": "USA",
            "phone": "(551) 555-6031"
          },
          "email": "abigail.powell@company.com"
        },
        {
          "first_name": "Skylar",
          "last_name": "Cox",
          "address": {
            "street": "4623 Silent Place",
            "city": "Kickapoo",
            "state": "Indiana",
            "zip": "47603-4383",
            "country": "USA",
            "phone": "(317) 555-8105"
          },
          "email": "skylar.cox@company.com"
        },
        {
          "first_name": "Zoe",
          "last_name": "Hill",
          "address": {
            "street": "8787 Velvet Deer Via",
            "city": "First Cliff",
            "state": "North Carolina",
            "zip": "27141-0975",
            "country": "USA",
            "phone": "(252) 555-2998"
          },
          "email": "zoe.hill@company.com"
        },
        {
          "first_name": "Katherine",
          "last_name": "Ross",
          "address": {
            "street": "4623 Silent Place",
            "city": "Kickapoo",
            "state": "Indiana",
            "zip": "47603-4383",
            "country": "USA",
            "phone": "(317) 555-8105"
          },
          "email": "katherine.ross@company.com"
        },
        {
          "first_name": "Penelope",
          "last_name": "Myers",
          "address": {
            "street": "3946 Cozy Turnabout",
            "city": "Hog Heaven",
            "state": "Texas",
            "zip": "79747-9284",
            "country": "USA",
            "phone": "(409) 555-4708"
          },
          "email": "penelope.myers@company.com"
        },
        {
          "first_name": "Jacob",
          "last_name": "Martin",
          "address": {
            "street": "4444 Iron Trace",
            "city": "Will-O-The-Wisp",
            "state": "Ohio",
            "zip": "45668-1931",
            "country": "USA",
            "phone": "(740) 555-6537"
          },
          "email": "jacob.martin@company.com"
        },
        {
          "first_name": "Cameron",
          "last_name": "Price",
          "address": {
            "street": "8635 Gentle Deer Heath",
            "city": "Dutch John",
            "state": "Colorado",
            "zip": "80166-1633",
            "country": "USA",
            "phone": "(303) 555-6720"
          },
          "email": "cameron.price@company.com"
        },
        {
          "first_name": "Camila",
          "last_name": "Jackson",
          "address": {
            "street": "2233 Little Drive",
            "city": "Muddy Ford",
            "state": "District of Columbia",
            "zip": "20079-0675",
            "country": "USA",
            "phone": "(202) 555-1024"
          },
          "email": "camila.jackson@company.com"
        },
        {
          "first_name": "Eva",
          "last_name": "Perry",
          "address": {
            "street": "2239 Quaking Row",
            "city": "Zipp",
            "state": "South Carolina",
            "zip": "29814-0324",
            "country": "USA",
            "phone": "(843) 555-4899"
          },
          "email": "eva.perry@company.com"
        },
        {
          "first_name": "Lucy",
          "last_name": "Brooks",
          "address": {
            "street": "7123 Tawny Branch Knoll",
            "city": "Ko Ko",
            "state": "Minnesota",
            "zip": "55334-2779",
            "country": "USA",
            "phone": "(612) 555-2064"
          },
          "email": "lucy.brooks@company.com"
        },
        {
          "first_name": "Noah",
          "last_name": "Walker",
          "address": {
            "street": "7123 Tawny Branch Knoll",
            "city": "Ko Ko",
            "state": "Minnesota",
            "zip": "55334-2779",
            "country": "USA",
            "phone": "(612) 555-2064"
          },
          "email": "noah.walker@company.com"
        },
        {
          "first_name": "Camden",
          "last_name": "Peterson",
          "address": {
            "street": "3188 Shady Grove Promenade",
            "city": "Pluto",
            "state": "Florida",
            "zip": "32069-2719",
            "country": "USA",
            "phone": "(305) 555-5351"
          },
          "email": "camden.peterson@company.com"
        },
        {
          "first_name": "Ethan",
          "last_name": "Wright",
          "address": {
            "street": "3946 Cozy Turnabout",
            "city": "Hog Heaven",
            "state": "Texas",
            "zip": "79747-9284",
            "country": "USA",
            "phone": "(409) 555-4708"
          },
          "email": "ethan.wright@company.com"
        },
        {
          "first_name": "Aaliyah",
          "last_name": "Bailey",
          "address": {
            "street": "5206 Golden Bluff Road",
            "city": "Hurricane Shoals",
            "state": "Mississippi",
            "zip": "38921-5783",
            "country": "USA",
            "phone": "(769) 555-8966"
          },
          "email": "aaliyah.bailey@company.com"
        },
        {
          "first_name": "Gianna",
          "last_name": "Howard",
          "address": {
            "street": "8332 Sunny Farms",
            "city": "Oka Kapassa",
            "state": "Virginia",
            "zip": "23023-5843",
            "country": "USA",
            "phone": "(540) 555-4489"
          },
          "email": "gianna.howard@company.com"
        },
        {
          "first_name": "Jaxon",
          "last_name": "Murphy",
          "address": {
            "street": "6893 Dusty Hickory Nook",
            "city": "Gaza",
            "state": "New Jersey",
            "zip": "07856-8731",
            "country": "USA",
            "phone": "(848) 555-0981"
          },
          "email": "jaxon.murphy@company.com"
        },
        {
          "first_name": "Camila",
          "last_name": "Collins",
          "address": {
            "street": "8474 Red Circuit",
            "city": "Hustler",
            "state": "Delaware",
            "zip": "19765-0566",
            "country": "USA",
            "phone": "(302) 555-0234"
          },
          "email": "camila.collins@company.com"
        },
        {
          "first_name": "Gabriella",
          "last_name": "Miller",
          "address": {
            "street": "4027 Amber Lake Canyon",
            "city": "Cozy Nook",
            "state": "Arkansas",
            "zip": "72333-5884",
            "country": "USA",
            "phone": "(501) 555-9128"
          },
          "email": "gabriella.miller@company.com"
        },
        {
          "first_name": "Brayden",
          "last_name": "Young",
          "address": {
            "street": "985 Cinder View Diversion",
            "city": "Fannie",
            "state": "Massachusetts",
            "zip": "02480-6179",
            "country": "USA",
            "phone": "(857) 555-1613"
          },
          "email": "brayden.young@company.com"
        },
        {
          "first_name": "Harper",
          "last_name": "Jenkins",
          "address": {
            "street": "1502 Round Limits",
            "city": "Ben Lomond",
            "state": "California",
            "zip": "92880-4018",
            "country": "USA",
            "phone": "(707) 555-5739"
          },
          "email": "harper.jenkins@company.com"
        },
        {
          "first_name": "Robert",
          "last_name": "Cox",
          "address": {
            "street": "2157 Easy Leaf Wharf",
            "city": "Plenty Bears",
            "state": "Illinois",
            "zip": "62792-5590",
            "country": "USA",
            "phone": "(630) 555-9818"
          },
          "email": "robert.cox@company.com"
        },
        {
          "first_name": "Connor",
          "last_name": "Jenkins",
          "address": {
            "street": "9874 Quiet Isle",
            "city": "Paydown",
            "state": "Connecticut",
            "zip": "06323-9300",
            "country": "USA",
            "phone": "(475) 555-6713"
          },
          "email": "connor.jenkins@company.com"
        },
        {
          "first_name": "Mason",
          "last_name": "Perez",
          "address": {
            "street": "2718 Cozy Range",
            "city": "Mousie",
            "state": "Montana",
            "zip": "59984-7576",
            "country": "USA",
            "phone": "(406) 555-0326"
          },
          "email": "mason.perez@company.com"
        },
        {
          "first_name": "Nevaeh",
          "last_name": "Jackson",
          "address": {
            "street": "1846 Broad Log By-pass",
            "city": "Bucksnort",
            "state": "Indiana",
            "zip": "47082-9236",
            "country": "USA",
            "phone": "(317) 555-2631"
          },
          "email": "nevaeh.jackson@company.com"
        },
        {
          "first_name": "Logan",
          "last_name": "Watson",
          "address": {
            "street": "7214 Hazy View Manor",
            "city": "Twodot",
            "state": "Alabama",
            "zip": "35845-0767",
            "country": "USA",
            "phone": "(205) 555-8967"
          },
          "email": "logan.watson@company.com"
        },
        {
          "first_name": "Cameron",
          "last_name": "Rivera",
          "address": {
            "street": "7408 Hazy Lake Private",
            "city": "Dry Run",
            "state": "District of Columbia",
            "zip": "20021-5276",
            "country": "USA",
            "phone": "(202) 555-6280"
          },
          "email": "cameron.rivera@company.com"
        },
        {
          "first_name": "Aaliyah",
          "last_name": "Phillips",
          "address": {
            "street": "3467 Green Rise Park",
            "city": "Ko Ko",
            "state": "Missouri",
            "zip": "64085-3098",
            "country": "USA",
            "phone": "(314) 555-7357"
          },
          "email": "aaliyah.phillips@company.com"
        },
        {
          "first_name": "Sarah",
          "last_name": "Stewart",
          "address": {
            "street": "9431 Lost Highlands",
            "city": "Old Ford",
            "state": "Arkansas",
            "zip": "71608-2346",
            "country": "USA",
            "phone": "(501) 555-6629"
          },
          "email": "sarah.stewart@company.com"
        },
        {
          "first_name": "Peyton",
          "last_name": "Howard",
          "address": {
            "street": "9386 Crystal Oak Beach",
            "city": "Bacon",
            "state": "Maryland",
            "zip": "21997-5878",
            "country": "USA",
            "phone": "(240) 555-5122"
          },
          "email": "peyton.howard@company.com"
        },
        {
          "first_name": "Emily",
          "last_name": "Barnes",
          "address": {
            "street": "4721 Noble Maze",
            "city": "Kalifornsky",
            "state": "Nevada",
            "zip": "89270-4436",
            "country": "USA",
            "phone": "(702) 555-8425"
          },
          "email": "emily.barnes@company.com"
        },
        {
          "first_name": "Hannah",
          "last_name": "Nelson",
          "address": {
            "street": "7641 Grand Pioneer Highlands",
            "city": "Four Towns",
            "state": "California",
            "zip": "96184-8070",
            "country": "USA",
            "phone": "(408) 555-7595"
          },
          "email": "hannah.nelson@company.com"
        },
        {
          "first_name": "Aria",
          "last_name": "Butler",
          "address": {
            "street": "2139 Dewy Autumn Orchard",
            "city": "Wax",
            "state": "Florida",
            "zip": "34109-9657",
            "country": "USA",
            "phone": "(863) 555-4371"
          },
          "email": "aria.butler@company.com"
        },
        {
          "first_name": "Peyton",
          "last_name": "Smith",
          "address": {
            "street": "9683 Silent Private",
            "city": "Fate",
            "state": "Alabama",
            "zip": "36269-7910",
            "country": "USA",
            "phone": "(251) 555-5530"
          },
          "email": "peyton.smith@company.com"
        },
        {
          "first_name": "Mackenzie",
          "last_name": "Collins",
          "address": {
            "street": "2096 Misty Valley",
            "city": "Basalt",
            "state": "Maryland",
            "zip": "20757-2037",
            "country": "USA",
            "phone": "(443) 555-5124"
          },
          "email": "mackenzie.collins@company.com"
        },
        {
          "first_name": "Anna",
          "last_name": "Watson",
          "address": {
            "street": "6318 Dusty Field",
            "city": "Fort Fizzle",
            "state": "California",
            "zip": "93071-6997",
            "country": "USA",
            "phone": "(619) 555-7933"
          },
          "email": "anna.watson@company.com"
        },
        {
          "first_name": "Thomas",
          "last_name": "Williams",
          "address": {
            "street": "1392 Silver Lagoon Pointe",
            "city": "Cheektowasa",
            "state": "Maryland",
            "zip": "21643-8254",
            "country": "USA",
            "phone": "(410) 555-8464"
          },
          "email": "thomas.williams@company.com"
        },
        {
          "first_name": "Jack",
          "last_name": "Lopez",
          "address": {
            "street": "3188 Shady Grove Promenade",
            "city": "Pluto",
            "state": "Florida",
            "zip": "32069-2719",
            "country": "USA",
            "phone": "(305) 555-5351"
          },
          "email": "jack.lopez@company.com"
        },
        {
          "first_name": "Ava",
          "last_name": "Rivera",
          "address": {
            "street": "2157 Easy Leaf Wharf",
            "city": "Plenty Bears",
            "state": "Illinois",
            "zip": "62792-5590",
            "country": "USA",
            "phone": "(630) 555-9818"
          },
          "email": "ava.rivera@company.com"
        },
        {
          "first_name": "Sadie",
          "last_name": "Watson",
          "address": {
            "street": "4439 Clear Fox Crossing",
            "city": "Hemingway Town",
            "state": "Tennessee",
            "zip": "37983-8021",
            "country": "USA",
            "phone": "(731) 555-5556"
          },
          "email": "sadie.watson@company.com"
        },
        {
          "first_name": "Isabelle",
          "last_name": "Long",
          "address": {
            "street": "4143 Burning Log Mountain",
            "city": "Skin Corner",
            "state": "Massachusetts",
            "zip": "01884-4559",
            "country": "USA",
            "phone": "(857) 555-1675"
          },
          "email": "isabelle.long@company.com"
        },
        {
          "first_name": "Ava",
          "last_name": "Baker",
          "address": {
            "street": "9685 Amber Dale",
            "city": "Horsetooth Heights",
            "state": "North Dakota",
            "zip": "58038-0313",
            "country": "USA",
            "phone": "(701) 555-6004"
          },
          "email": "ava.baker@company.com"
        },
        {
          "first_name": "Harper",
          "last_name": "Lee",
          "address": {
            "street": "4700 Merry Elk Heath",
            "city": "Morning Star",
            "state": "Rhode Island",
            "zip": "02858-4490",
            "country": "USA",
            "phone": "(401) 555-0971"
          },
          "email": "harper.lee@company.com"
        },
        {
          "first_name": "Anna",
          "last_name": "Watson",
          "address": {
            "street": "4817 Fallen Swale",
            "city": "Louisiana",
            "state": "District of Columbia",
            "zip": "20084-2834",
            "country": "USA",
            "phone": "(202) 555-7072"
          },
          "email": "anna.watson@company.com"
        },
        {
          "first_name": "Camila",
          "last_name": "Russell",
          "address": {
            "street": "3759 Grand Timber Concession",
            "city": "Zulu",
            "state": "Kentucky",
            "zip": "40570-2685",
            "country": "USA",
            "phone": "(859) 555-0145"
          },
          "email": "camila.russell@company.com"
        },
        {
          "first_name": "Leah",
          "last_name": "Murphy",
          "address": {
            "street": "92 Harvest Court",
            "city": "Chickasawhatchee",
            "state": "Vermont",
            "zip": "05892-3796",
            "country": "USA",
            "phone": "(802) 555-3041"
          },
          "email": "leah.murphy@company.com"
        },
        {
          "first_name": "Aubrey",
          "last_name": "Cruz",
          "address": {
            "street": "5868 Cinder Centre",
            "city": "Apponagansett",
            "state": "North Dakota",
            "zip": "58777-1298",
            "country": "USA",
            "phone": "(701) 555-0519"
          },
          "email": "aubrey.cruz@company.com"
        },
        {
          "first_name": "Ethan",
          "last_name": "Brown",
          "address": {
            "street": "2718 Cozy Range",
            "city": "Mousie",
            "state": "Montana",
            "zip": "59984-7576",
            "country": "USA",
            "phone": "(406) 555-0326"
          },
          "email": "ethan.brown@company.com"
        },
        {
          "first_name": "Tyler",
          "last_name": "King",
          "address": {
            "street": "8157 Silver Heath",
            "city": "Steam Corners",
            "state": "Connecticut",
            "zip": "06278-3936",
            "country": "USA",
            "phone": "(860) 555-7560"
          },
          "email": "tyler.king@company.com"
        },
        {
          "first_name": "Aria",
          "last_name": "White",
          "address": {
            "street": "8170 Fallen Spring Path",
            "city": "Five Brooks",
            "state": "Mississippi",
            "zip": "39548-5507",
            "country": "USA",
            "phone": "(228) 555-6575"
          },
          "email": "aria.white@company.com"
        },
        {
          "first_name": "Chase",
          "last_name": "Hall",
          "address": {
            "street": "3977 Heather Way",
            "city": "Nankipooh",
            "state": "Minnesota",
            "zip": "56063-2417",
            "country": "USA",
            "phone": "(320) 555-1707"
          },
          "email": "chase.hall@company.com"
        },
        {
          "first_name": "Annabelle",
          "last_name": "Nguyen",
          "address": {
            "street": "9546 Middle Passage",
            "city": "Beans Corner",
            "state": "Connecticut",
            "zip": "06257-5597",
            "country": "USA",
            "phone": "(860) 555-6030"
          },
          "email": "annabelle.nguyen@company.com"
        },
        {
          "first_name": "Carson",
          "last_name": "Miller",
          "address": {
            "street": "3353 Thunder Passage",
            "city": "Experiment",
            "state": "Georgia",
            "zip": "39953-7027",
            "country": "USA",
            "phone": "(470) 555-7432"
          },
          "email": "carson.miller@company.com"
        },
        {
          "first_name": "Adam",
          "last_name": "Turner",
          "address": {
            "street": "541 Sunny Timber View",
            "city": "Belgium",
            "state": "Alaska",
            "zip": "99922-5965",
            "country": "USA",
            "phone": "(907) 555-4767"
          },
          "email": "adam.turner@company.com"
        },
        {
          "first_name": "Madeline",
          "last_name": "Robinson",
          "address": {
            "street": "9236 Quiet Round",
            "city": "Vopolo Havoka",
            "state": "Missouri",
            "zip": "64519-3548",
            "country": "USA",
            "phone": "(417) 555-8373"
          },
          "email": "madeline.robinson@company.com"
        },
        {
          "first_name": "Alexa",
          "last_name": "Phillips",
          "address": {
            "street": "1223 Red Embers Expressway",
            "city": "Advance",
            "state": "Montana",
            "zip": "59254-1467",
            "country": "USA",
            "phone": "(406) 555-1801"
          },
          "email": "alexa.phillips@company.com"
        },
        {
          "first_name": "Emma",
          "last_name": "Taylor",
          "address": {
            "street": "7768 Cinder Creek Turnabout",
            "city": "Stotonic",
            "state": "Oregon",
            "zip": "97076-6131",
            "country": "USA",
            "phone": "(458) 555-5993"
          },
          "email": "emma.taylor@company.com"
        },
        {
          "first_name": "Leo",
          "last_name": "Adams",
          "address": {
            "street": "9570 Lazy Oak Farm",
            "city": "Hoosick",
            "state": "South Carolina",
            "zip": "29606-1498",
            "country": "USA",
            "phone": "(843) 555-1545"
          },
          "email": "leo.adams@company.com"
        },
        {
          "first_name": "Kevin",
          "last_name": "Bell",
          "address": {
            "street": "489 Colonial Mount",
            "city": "Veteran",
            "state": "Kentucky",
            "zip": "41046-0449",
            "country": "USA",
            "phone": "(270) 555-6688"
          },
          "email": "kevin.bell@company.com"
        },
        {
          "first_name": "Serenity",
          "last_name": "Williams",
          "address": {
            "street": "3851 Gentle Hill",
            "city": "Zodiac",
            "state": "Delaware",
            "zip": "19970-0027",
            "country": "USA",
            "phone": "(302) 555-2277"
          },
          "email": "serenity.williams@company.com"
        },
        {
          "first_name": "Henry",
          "last_name": "Cooper",
          "address": {
            "street": "9386 Crystal Oak Beach",
            "city": "Bacon",
            "state": "Maryland",
            "zip": "21997-5878",
            "country": "USA",
            "phone": "(240) 555-5122"
          },
          "email": "henry.cooper@company.com"
        },
        {
          "first_name": "Sophia",
          "last_name": "Baker",
          "address": {
            "street": "4864 Tawny Bend",
            "city": "Plentywood",
            "state": "Iowa",
            "zip": "51377-1040",
            "country": "USA",
            "phone": "(319) 555-1931"
          },
          "email": "sophia.baker@company.com"
        },
        {
          "first_name": "Jose",
          "last_name": "Young",
          "address": {
            "street": "2233 Little Drive",
            "city": "Muddy Ford",
            "state": "District of Columbia",
            "zip": "20079-0675",
            "country": "USA",
            "phone": "(202) 555-1024"
          },
          "email": "jose.young@company.com"
        },
        {
          "first_name": "Luis",
          "last_name": "Gray",
          "address": {
            "street": "3467 Green Rise Park",
            "city": "Ko Ko",
            "state": "Missouri",
            "zip": "64085-3098",
            "country": "USA",
            "phone": "(314) 555-7357"
          },
          "email": "luis.gray@company.com"
        },
        {
          "first_name": "Lincoln",
          "last_name": "Sanchez",
          "address": {
            "street": "4941 Wishing Hills Court",
            "city": "Black Lick",
            "state": "Georgia",
            "zip": "39848-1414",
            "country": "USA",
            "phone": "(912) 555-5819"
          },
          "email": "lincoln.sanchez@company.com"
        },
        {
          "first_name": "Joseph",
          "last_name": "Ross",
          "address": {
            "street": "7214 Hazy View Manor",
            "city": "Twodot",
            "state": "Alabama",
            "zip": "35845-0767",
            "country": "USA",
            "phone": "(205) 555-8967"
          },
          "email": "joseph.ross@company.com"
        },
        {
          "first_name": "Chase",
          "last_name": "Fisher",
          "address": {
            "street": "4783 Merry Sky Glade",
            "city": "Blowville",
            "state": "Wyoming",
            "zip": "83099-4319",
            "country": "USA",
            "phone": "(307) 555-7524"
          },
          "email": "chase.fisher@company.com"
        },
        {
          "first_name": "Gianna",
          "last_name": "Cruz",
          "address": {
            "street": "3977 Heather Way",
            "city": "Nankipooh",
            "state": "Minnesota",
            "zip": "56063-2417",
            "country": "USA",
            "phone": "(320) 555-1707"
          },
          "email": "gianna.cruz@company.com"
        },
        {
          "first_name": "William",
          "last_name": "Baker",
          "address": {
            "street": "605 Pleasant Ramp",
            "city": "Stangelville",
            "state": "Wisconsin",
            "zip": "53364-6936",
            "country": "USA",
            "phone": "(534) 555-4436"
          },
          "email": "william.baker@company.com"
        },
        {
          "first_name": "Vivian",
          "last_name": "Davis",
          "address": {
            "street": "4027 Amber Lake Canyon",
            "city": "Cozy Nook",
            "state": "Arkansas",
            "zip": "72333-5884",
            "country": "USA",
            "phone": "(501) 555-9128"
          },
          "email": "vivian.davis@company.com"
        },
        {
          "first_name": "Hailey",
          "last_name": "Foster",
          "address": {
            "street": "7851 Rustic Canyon",
            "city": "Convent",
            "state": "Minnesota",
            "zip": "55320-7161",
            "country": "USA",
            "phone": "(612) 555-1693"
          },
          "email": "hailey.foster@company.com"
        },
        {
          "first_name": "Matthew",
          "last_name": "Ward",
          "address": {
            "street": "489 Colonial Mount",
            "city": "Veteran",
            "state": "Kentucky",
            "zip": "41046-0449",
            "country": "USA",
            "phone": "(270) 555-6688"
          },
          "email": "matthew.ward@company.com"
        },
        {
          "first_name": "Genesis",
          "last_name": "Allen",
          "address": {
            "street": "9431 Lost Highlands",
            "city": "Old Ford",
            "state": "Arkansas",
            "zip": "71608-2346",
            "country": "USA",
            "phone": "(501) 555-6629"
          },
          "email": "genesis.allen@company.com"
        },
        {
          "first_name": "Harper",
          "last_name": "James",
          "address": {
            "street": "3759 Grand Timber Concession",
            "city": "Zulu",
            "state": "Kentucky",
            "zip": "40570-2685",
            "country": "USA",
            "phone": "(859) 555-0145"
          },
          "email": "harper.james@company.com"
        },
        {
          "first_name": "Peyton",
          "last_name": "Long",
          "address": {
            "street": "3848 Sleepy Mews",
            "city": "Big River",
            "state": "Hawaii",
            "zip": "96761-3780",
            "country": "USA",
            "phone": "(808) 555-7346"
          },
          "email": "peyton.long@company.com"
        },
        {
          "first_name": "Madeline",
          "last_name": "Gray",
          "address": {
            "street": "5553 Bright Wagon Dell",
            "city": "Duckroost",
            "state": "Alabama",
            "zip": "36802-7139",
            "country": "USA",
            "phone": "(205) 555-1102"
          },
          "email": "madeline.gray@company.com"
        },
        {
          "first_name": "Faith",
          "last_name": "Flores",
          "address": {
            "street": "6053 Lost Shadow Vista",
            "city": "Nebraska",
            "state": "Colorado",
            "zip": "81162-2553",
            "country": "USA",
            "phone": "(720) 555-3890"
          },
          "email": "faith.flores@company.com"
        },
        {
          "first_name": "Owen",
          "last_name": "Rivera",
          "address": {
            "street": "9431 Lost Highlands",
            "city": "Old Ford",
            "state": "Arkansas",
            "zip": "71608-2346",
            "country": "USA",
            "phone": "(501) 555-6629"
          },
          "email": "owen.rivera@company.com"
        },
        {
          "first_name": "Bentley",
          "last_name": "White",
          "address": {
            "street": "3946 Cozy Turnabout",
            "city": "Hog Heaven",
            "state": "Texas",
            "zip": "79747-9284",
            "country": "USA",
            "phone": "(409) 555-4708"
          },
          "email": "bentley.white@company.com"
        },
        {
          "first_name": "Ryder",
          "last_name": "Howard",
          "address": {
            "street": "8332 Sunny Farms",
            "city": "Oka Kapassa",
            "state": "Virginia",
            "zip": "23023-5843",
            "country": "USA",
            "phone": "(540) 555-4489"
          },
          "email": "ryder.howard@company.com"
        },
        {
          "first_name": "Nolan",
          "last_name": "Sanders",
          "address": {
            "street": "8916 Emerald Vale",
            "city": "Skidaway Island",
            "state": "Pennsylvania",
            "zip": "17306-4188",
            "country": "USA",
            "phone": "(610) 555-4521"
          },
          "email": "nolan.sanders@company.com"
        },
        {
          "first_name": "Autumn",
          "last_name": "Adams",
          "address": {
            "street": "3171 Old Pond Dale",
            "city": "Poverty Ridge",
            "state": "Oregon",
            "zip": "97961-0175",
            "country": "USA",
            "phone": "(971) 555-2050"
          },
          "email": "autumn.adams@company.com"
        },
        {
          "first_name": "Angel",
          "last_name": "Smith",
          "address": {
            "street": "6609 Old Anchor Stead",
            "city": "Suspension",
            "state": "North Dakota",
            "zip": "58306-5667",
            "country": "USA",
            "phone": "(701) 555-6229"
          },
          "email": "angel.smith@company.com"
        },
        {
          "first_name": "Emily",
          "last_name": "Powell",
          "address": {
            "street": "5868 Cinder Centre",
            "city": "Apponagansett",
            "state": "North Dakota",
            "zip": "58777-1298",
            "country": "USA",
            "phone": "(701) 555-0519"
          },
          "email": "emily.powell@company.com"
        },
        {
          "first_name": "Alexander",
          "last_name": "Green",
          "address": {
            "street": "3353 Thunder Passage",
            "city": "Experiment",
            "state": "Georgia",
            "zip": "39953-7027",
            "country": "USA",
            "phone": "(470) 555-7432"
          },
          "email": "alexander.green@company.com"
        },
        {
          "first_name": "Aaron",
          "last_name": "Gray",
          "address": {
            "street": "2560 Iron Brook Downs",
            "city": "Friendship",
            "state": "Tennessee",
            "zip": "38489-1588",
            "country": "USA",
            "phone": "(865) 555-9665"
          },
          "email": "aaron.gray@company.com"
        },
        {
          "first_name": "Alexa",
          "last_name": "Brown",
          "address": {
            "street": "9236 Quiet Round",
            "city": "Vopolo Havoka",
            "state": "Missouri",
            "zip": "64519-3548",
            "country": "USA",
            "phone": "(417) 555-8373"
          },
          "email": "alexa.brown@company.com"
        },
        {
          "first_name": "John",
          "last_name": "Gutierrez",
          "address": {
            "street": "5868 Cinder Centre",
            "city": "Apponagansett",
            "state": "North Dakota",
            "zip": "58777-1298",
            "country": "USA",
            "phone": "(701) 555-0519"
          },
          "email": "john.gutierrez@company.com"
        },
        {
          "first_name": "Caleb",
          "last_name": "Nguyen",
          "address": {
            "street": "2239 Quaking Row",
            "city": "Zipp",
            "state": "South Carolina",
            "zip": "29814-0324",
            "country": "USA",
            "phone": "(843) 555-4899"
          },
          "email": "caleb.nguyen@company.com"
        },
        {
          "first_name": "Katherine",
          "last_name": "Gomez",
          "address": {
            "street": "6609 Old Anchor Stead",
            "city": "Suspension",
            "state": "North Dakota",
            "zip": "58306-5667",
            "country": "USA",
            "phone": "(701) 555-6229"
          },
          "email": "katherine.gomez@company.com"
        },
        {
          "first_name": "Avery",
          "last_name": "Wood",
          "address": {
            "street": "9797 Noble Hills Trail",
            "city": "Iron Horse",
            "state": "Pennsylvania",
            "zip": "16906-7969",
            "country": "USA",
            "phone": "(267) 555-3375"
          },
          "email": "avery.wood@company.com"
        },
        {
          "first_name": "Avery",
          "last_name": "Ortiz",
          "address": {
            "street": "5712 Pleasant Branch Quay",
            "city": "Converse",
            "state": "Idaho",
            "zip": "83932-4717",
            "country": "USA",
            "phone": "(208) 555-8465"
          },
          "email": "avery.ortiz@company.com"
        },
        {
          "first_name": "Madison",
          "last_name": "Jackson",
          "address": {
            "street": "8983 Misty Grove",
            "city": "Chuathbaluk",
            "state": "Utah",
            "zip": "84914-5656",
            "country": "USA",
            "phone": "(801) 555-5857"
          },
          "email": "madison.jackson@company.com"
        },
        {
          "first_name": "Lillian",
          "last_name": "Sanders",
          "address": {
            "street": "6261 Hidden Lake Manor",
            "city": "Foot of Ten",
            "state": "Alabama",
            "zip": "36262-6582",
            "country": "USA",
            "phone": "(251) 555-1099"
          },
          "email": "lillian.sanders@company.com"
        },
        {
          "first_name": "Serenity",
          "last_name": "Jenkins",
          "address": {
            "street": "9797 Noble Hills Trail",
            "city": "Iron Horse",
            "state": "Pennsylvania",
            "zip": "16906-7969",
            "country": "USA",
            "phone": "(267) 555-3375"
          },
          "email": "serenity.jenkins@company.com"
        },
        {
          "first_name": "Nora",
          "last_name": "Rivera",
          "address": {
            "street": "4623 Silent Place",
            "city": "Kickapoo",
            "state": "Indiana",
            "zip": "47603-4383",
            "country": "USA",
            "phone": "(317) 555-8105"
          },
          "email": "nora.rivera@company.com"
        },
        {
          "first_name": "Evan",
          "last_name": "Clark",
          "address": {
            "street": "8443 Middle Pony Estates",
            "city": "Ty Ty",
            "state": "Arizona",
            "zip": "86773-7692",
            "country": "USA",
            "phone": "(623) 555-7979"
          },
          "email": "evan.clark@company.com"
        },
        {
          "first_name": "Joseph",
          "last_name": "Morgan",
          "address": {
            "street": "9878 Emerald Beacon Wynd",
            "city": "Tomboy",
            "state": "Wisconsin",
            "zip": "54995-7449",
            "country": "USA",
            "phone": "(920) 555-6872"
          },
          "email": "joseph.morgan@company.com"
        },
        {
          "first_name": "Nathaniel",
          "last_name": "Butler",
          "address": {
            "street": "489 Colonial Mount",
            "city": "Veteran",
            "state": "Kentucky",
            "zip": "41046-0449",
            "country": "USA",
            "phone": "(270) 555-6688"
          },
          "email": "nathaniel.butler@company.com"
        },
        {
          "first_name": "Lily",
          "last_name": "Moore",
          "address": {
            "street": "2718 Cozy Range",
            "city": "Mousie",
            "state": "Montana",
            "zip": "59984-7576",
            "country": "USA",
            "phone": "(406) 555-0326"
          },
          "email": "lily.moore@company.com"
        },
        {
          "first_name": "Natalie",
          "last_name": "Long",
          "address": {
            "street": "2157 Easy Leaf Wharf",
            "city": "Plenty Bears",
            "state": "Illinois",
            "zip": "62792-5590",
            "country": "USA",
            "phone": "(630) 555-9818"
          },
          "email": "natalie.long@company.com"
        },
        {
          "first_name": "Anna",
          "last_name": "Murphy",
          "address": {
            "street": "1972 Red Fawn Isle",
            "city": "Sanitarium",
            "state": "Colorado",
            "zip": "81052-1708",
            "country": "USA",
            "phone": "(970) 555-4558"
          },
          "email": "anna.murphy@company.com"
        },
        {
          "first_name": "Arianna",
          "last_name": "Long",
          "address": {
            "street": "7766 Misty Bay",
            "city": "Mattawoman",
            "state": "New Mexico",
            "zip": "87068-7317",
            "country": "USA",
            "phone": "(575) 555-6257"
          },
          "email": "arianna.long@company.com"
        },
        {
          "first_name": "Connor",
          "last_name": "Long",
          "address": {
            "street": "9207 Green Lagoon Forest",
            "city": "Soaptown",
            "state": "Vermont",
            "zip": "05992-4967",
            "country": "USA",
            "phone": "(802) 555-6894"
          },
          "email": "connor.long@company.com"
        },
        {
          "first_name": "Jordan",
          "last_name": "Ortiz",
          "address": {
            "street": "7009 Sunny Elk By-pass",
            "city": "Herculaneum",
            "state": "Louisiana",
            "zip": "70799-1187",
            "country": "USA",
            "phone": "(225) 555-5532"
          },
          "email": "jordan.ortiz@company.com"
        },
        {
          "first_name": "Jace",
          "last_name": "Sanders",
          "address": {
            "street": "9980 Heather Subdivision",
            "city": "Coosawhatchie",
            "state": "Rhode Island",
            "zip": "02845-0062",
            "country": "USA",
            "phone": "(401) 555-0842"
          },
          "email": "jace.sanders@company.com"
        },
        {
          "first_name": "Mia",
          "last_name": "Turner",
          "address": {
            "street": "2096 Misty Valley",
            "city": "Basalt",
            "state": "Maryland",
            "zip": "20757-2037",
            "country": "USA",
            "phone": "(443) 555-5124"
          },
          "email": "mia.turner@company.com"
        },
        {
          "first_name": "Daniel",
          "last_name": "Ortiz",
          "address": {
            "street": "3851 Gentle Hill",
            "city": "Zodiac",
            "state": "Delaware",
            "zip": "19970-0027",
            "country": "USA",
            "phone": "(302) 555-2277"
          },
          "email": "daniel.ortiz@company.com"
        },
        {
          "first_name": "Katherine",
          "last_name": "Morgan",
          "address": {
            "street": "9391 Amber Pioneer Manor",
            "city": "Manassas",
            "state": "Kansas",
            "zip": "66888-1745",
            "country": "USA",
            "phone": "(316) 555-2652"
          },
          "email": "katherine.morgan@company.com"
        },
        {
          "first_name": "Eva",
          "last_name": "Lopez",
          "address": {
            "street": "9431 Lost Highlands",
            "city": "Old Ford",
            "state": "Arkansas",
            "zip": "71608-2346",
            "country": "USA",
            "phone": "(501) 555-6629"
          },
          "email": "eva.lopez@company.com"
        },
        {
          "first_name": "Adam",
          "last_name": "Taylor",
          "address": {
            "street": "6013 Grand Hills Key",
            "city": "King Salmon",
            "state": "Idaho",
            "zip": "83765-8184",
            "country": "USA",
            "phone": "(208) 555-4624"
          },
          "email": "adam.taylor@company.com"
        },
        {
          "first_name": "Vivian",
          "last_name": "Phillips",
          "address": {
            "street": "9797 Noble Hills Trail",
            "city": "Iron Horse",
            "state": "Pennsylvania",
            "zip": "16906-7969",
            "country": "USA",
            "phone": "(267) 555-3375"
          },
          "email": "vivian.phillips@company.com"
        },
        {
          "first_name": "Brianna",
          "last_name": "Jackson",
          "address": {
            "street": "8332 Sunny Farms",
            "city": "Oka Kapassa",
            "state": "Virginia",
            "zip": "23023-5843",
            "country": "USA",
            "phone": "(540) 555-4489"
          },
          "email": "brianna.jackson@company.com"
        },
        {
          "first_name": "Genesis",
          "last_name": "Hughes",
          "address": {
            "street": "9690 Easy Spring Thicket",
            "city": "Dowdy",
            "state": "West Virginia",
            "zip": "24724-6172",
            "country": "USA",
            "phone": "(304) 555-2786"
          },
          "email": "genesis.hughes@company.com"
        },
        {
          "first_name": "Evelyn",
          "last_name": "Lewis",
          "address": {
            "street": "9878 Emerald Beacon Wynd",
            "city": "Tomboy",
            "state": "Wisconsin",
            "zip": "54995-7449",
            "country": "USA",
            "phone": "(920) 555-6872"
          },
          "email": "evelyn.lewis@company.com"
        },
        {
          "first_name": "Mason",
          "last_name": "Rodriguez",
          "address": {
            "street": "4042 High Prairie Port",
            "city": "Shiprock",
            "state": "Hawaii",
            "zip": "96851-7918",
            "country": "USA",
            "phone": "(808) 555-6259"
          },
          "email": "mason.rodriguez@company.com"
        },
        {
          "first_name": "Kayden",
          "last_name": "Diaz",
          "address": {
            "street": "1875 Dewy Corners",
            "city": "Blackhawk",
            "state": "Wyoming",
            "zip": "82373-2034",
            "country": "USA",
            "phone": "(307) 555-6261"
          },
          "email": "kayden.diaz@company.com"
        },
        {
          "first_name": "Mackenzie",
          "last_name": "Bell",
          "address": {
            "street": "8332 Sunny Farms",
            "city": "Oka Kapassa",
            "state": "Virginia",
            "zip": "23023-5843",
            "country": "USA",
            "phone": "(540) 555-4489"
          },
          "email": "mackenzie.bell@company.com"
        },
        {
          "first_name": "Dylan",
          "last_name": "Reed",
          "address": {
            "street": "7282 Rustic Pioneer Jetty",
            "city": "Philadelphia",
            "state": "Connecticut",
            "zip": "06634-5832",
            "country": "USA",
            "phone": "(475) 555-0867"
          },
          "email": "dylan.reed@company.com"
        },
        {
          "first_name": "Noah",
          "last_name": "Kelly",
          "address": {
            "street": "4721 Noble Maze",
            "city": "Kalifornsky",
            "state": "Nevada",
            "zip": "89270-4436",
            "country": "USA",
            "phone": "(702) 555-8425"
          },
          "email": "noah.kelly@company.com"
        },
        {
          "first_name": "Gavin",
          "last_name": "Lopez",
          "address": {
            "street": "8029 Honey Goose Via",
            "city": "Rowdyville",
            "state": "Delaware",
            "zip": "19969-9253",
            "country": "USA",
            "phone": "(302) 555-6748"
          },
          "email": "gavin.lopez@company.com"
        },
        {
          "first_name": "Hadley",
          "last_name": "Parker",
          "address": {
            "street": "7851 Rustic Canyon",
            "city": "Convent",
            "state": "Minnesota",
            "zip": "55320-7161",
            "country": "USA",
            "phone": "(612) 555-1693"
          },
          "email": "hadley.parker@company.com"
        },
        {
          "first_name": "Asher",
          "last_name": "Turner",
          "address": {
            "street": "2822 Wishing Pond Run",
            "city": "Prettyboy Garth",
            "state": "Utah",
            "zip": "84010-1908",
            "country": "USA",
            "phone": "(435) 555-4684"
          },
          "email": "asher.turner@company.com"
        },
        {
          "first_name": "Layla",
          "last_name": "Watson",
          "address": {
            "street": "3171 Old Pond Dale",
            "city": "Poverty Ridge",
            "state": "Oregon",
            "zip": "97961-0175",
            "country": "USA",
            "phone": "(971) 555-2050"
          },
          "email": "layla.watson@company.com"
        },
        {
          "first_name": "Nathan",
          "last_name": "Jackson",
          "address": {
            "street": "7279 Colonial Circle",
            "city": "Frogville",
            "state": "Kansas",
            "zip": "67777-0287",
            "country": "USA",
            "phone": "(785) 555-8893"
          },
          "email": "nathan.jackson@company.com"
        },
        {
          "first_name": "Sebastian",
          "last_name": "Martinez",
          "address": {
            "street": "9355 Foggy Green",
            "city": "Co-Operative",
            "state": "Utah",
            "zip": "84241-5309",
            "country": "USA",
            "phone": "(385) 555-7683"
          },
          "email": "sebastian.martinez@company.com"
        },
        {
          "first_name": "Julia",
          "last_name": "Kelly",
          "address": {
            "street": "9236 Quiet Round",
            "city": "Vopolo Havoka",
            "state": "Missouri",
            "zip": "64519-3548",
            "country": "USA",
            "phone": "(417) 555-8373"
          },
          "email": "julia.kelly@company.com"
        },
        {
          "first_name": "Alexa",
          "last_name": "Stewart",
          "address": {
            "street": "4721 Noble Maze",
            "city": "Kalifornsky",
            "state": "Nevada",
            "zip": "89270-4436",
            "country": "USA",
            "phone": "(702) 555-8425"
          },
          "email": "alexa.stewart@company.com"
        },
        {
          "first_name": "Zachary",
          "last_name": "Rogers",
          "address": {
            "street": "1846 Broad Log By-pass",
            "city": "Bucksnort",
            "state": "Indiana",
            "zip": "47082-9236",
            "country": "USA",
            "phone": "(317) 555-2631"
          },
          "email": "zachary.rogers@company.com"
        },
        {
          "first_name": "Hudson",
          "last_name": "Taylor",
          "address": {
            "street": "7277 Broad Island",
            "city": "Clappertown",
            "state": "South Carolina",
            "zip": "29693-1651",
            "country": "USA",
            "phone": "(843) 555-3284"
          },
          "email": "hudson.taylor@company.com"
        },
        {
          "first_name": "Hunter",
          "last_name": "Butler",
          "address": {
            "street": "9874 Quiet Isle",
            "city": "Paydown",
            "state": "Connecticut",
            "zip": "06323-9300",
            "country": "USA",
            "phone": "(475) 555-6713"
          },
          "email": "hunter.butler@company.com"
        },
        {
          "first_name": "Julian",
          "last_name": "Carter",
          "address": {
            "street": "116 Umber Way",
            "city": "Sinker",
            "state": "Arizona",
            "zip": "85295-6386",
            "country": "USA",
            "phone": "(520) 555-5159"
          },
          "email": "julian.carter@company.com"
        },
        {
          "first_name": "Allison",
          "last_name": "Lee",
          "address": {
            "street": "2139 Dewy Autumn Orchard",
            "city": "Wax",
            "state": "Florida",
            "zip": "34109-9657",
            "country": "USA",
            "phone": "(863) 555-4371"
          },
          "email": "allison.lee@company.com"
        },
        {
          "first_name": "Allison",
          "last_name": "Hernandez",
          "address": {
            "street": "4817 Fallen Swale",
            "city": "Louisiana",
            "state": "District of Columbia",
            "zip": "20084-2834",
            "country": "USA",
            "phone": "(202) 555-7072"
          },
          "email": "allison.hernandez@company.com"
        },
        {
          "first_name": "Joseph",
          "last_name": "Allen",
          "address": {
            "street": "116 Umber Way",
            "city": "Sinker",
            "state": "Arizona",
            "zip": "85295-6386",
            "country": "USA",
            "phone": "(520) 555-5159"
          },
          "email": "joseph.allen@company.com"
        },
        {
          "first_name": "Samantha",
          "last_name": "Smith",
          "address": {
            "street": "2718 Cozy Range",
            "city": "Mousie",
            "state": "Montana",
            "zip": "59984-7576",
            "country": "USA",
            "phone": "(406) 555-0326"
          },
          "email": "samantha.smith@company.com"
        },
        {
          "first_name": "Gabriel",
          "last_name": "Gomez",
          "address": {
            "street": "7009 Sunny Elk By-pass",
            "city": "Herculaneum",
            "state": "Louisiana",
            "zip": "70799-1187",
            "country": "USA",
            "phone": "(225) 555-5532"
          },
          "email": "gabriel.gomez@company.com"
        },
        {
          "first_name": "Henry",
          "last_name": "Jones",
          "address": {
            "street": "2615 Quiet Path",
            "city": "Scrappy Corner",
            "state": "Arkansas",
            "zip": "72480-0327",
            "country": "USA",
            "phone": "(479) 555-0211"
          },
          "email": "henry.jones@company.com"
        },
        {
          "first_name": "Hannah",
          "last_name": "Ramirez",
          "address": {
            "street": "6001 Noble Centre",
            "city": "Java",
            "state": "South Carolina",
            "zip": "29828-4809",
            "country": "USA",
            "phone": "(803) 555-6337"
          },
          "email": "hannah.ramirez@company.com"
        },
        {
          "first_name": "Olivia",
          "last_name": "Rodriguez",
          "address": {
            "street": "5547 Shady Robin Farm",
            "city": "Twentythree",
            "state": "North Carolina",
            "zip": "28393-3677",
            "country": "USA",
            "phone": "(980) 555-9354"
          },
          "email": "olivia.rodriguez@company.com"
        },
        {
          "first_name": "Madison",
          "last_name": "Nguyen",
          "address": {
            "street": "9428 Jagged Grove",
            "city": "Spirit Lake",
            "state": "Rhode Island",
            "zip": "02906-5528",
            "country": "USA",
            "phone": "(401) 555-8783"
          },
          "email": "madison.nguyen@company.com"
        },
        {
          "first_name": "David",
          "last_name": "Lee",
          "address": {
            "street": "8695 High Horse Carrefour",
            "city": "Nashville",
            "state": "District of Columbia",
            "zip": "20012-7697",
            "country": "USA",
            "phone": "(202) 555-5209"
          },
          "email": "david.lee@company.com"
        },
        {
          "first_name": "Justin",
          "last_name": "Morris",
          "address": {
            "street": "7585 Sleepy Parkway",
            "city": "Kilowatt",
            "state": "Minnesota",
            "zip": "55105-3263",
            "country": "USA",
            "phone": "(507) 555-0207"
          },
          "email": "justin.morris@company.com"
        },
        {
          "first_name": "Autumn",
          "last_name": "Barnes",
          "address": {
            "street": "6104 Old Front",
            "city": "Goobertown",
            "state": "New Hampshire",
            "zip": "03553-1592",
            "country": "USA",
            "phone": "(603) 555-9004"
          },
          "email": "autumn.barnes@company.com"
        },
        {
          "first_name": "Gabriella",
          "last_name": "Brooks",
          "address": {
            "street": "9690 Easy Spring Thicket",
            "city": "Dowdy",
            "state": "West Virginia",
            "zip": "24724-6172",
            "country": "USA",
            "phone": "(304) 555-2786"
          },
          "email": "gabriella.brooks@company.com"
        },
        {
          "first_name": "Taylor",
          "last_name": "Perez",
          "address": {
            "street": "2854 Cotton Island",
            "city": "Brian Head",
            "state": "Alabama",
            "zip": "36016-8269",
            "country": "USA",
            "phone": "(256) 555-6847"
          },
          "email": "taylor.perez@company.com"
        },
        {
          "first_name": "Luis",
          "last_name": "Jackson",
          "address": {
            "street": "4912 Lost Brook Promenade",
            "city": "High Health",
            "state": "Florida",
            "zip": "34389-3438",
            "country": "USA",
            "phone": "(904) 555-7711"
          },
          "email": "luis.jackson@company.com"
        },
        {
          "first_name": "Levi",
          "last_name": "Williams",
          "address": {
            "street": "4912 Lost Brook Promenade",
            "city": "High Health",
            "state": "Florida",
            "zip": "34389-3438",
            "country": "USA",
            "phone": "(904) 555-7711"
          },
          "email": "levi.williams@company.com"
        },
        {
          "first_name": "Mackenzie",
          "last_name": "Johnson",
          "address": {
            "street": "961 Golden Crescent",
            "city": "Quinapoxet",
            "state": "Tennessee",
            "zip": "37314-3596",
            "country": "USA",
            "phone": "(865) 555-9904"
          },
          "email": "mackenzie.johnson@company.com"
        },
        {
          "first_name": "Easton",
          "last_name": "Reyes",
          "address": {
            "street": "4660 High Downs",
            "city": "Offer",
            "state": "Massachusetts",
            "zip": "01390-7301",
            "country": "USA",
            "phone": "(351) 555-9202"
          },
          "email": "easton.reyes@company.com"
        },
        {
          "first_name": "Austin",
          "last_name": "Cook",
          "address": {
            "street": "1223 Red Embers Expressway",
            "city": "Advance",
            "state": "Montana",
            "zip": "59254-1467",
            "country": "USA",
            "phone": "(406) 555-1801"
          },
          "email": "austin.cook@company.com"
        },
        {
          "first_name": "Eli",
          "last_name": "Bailey",
          "address": {
            "street": "8474 Red Circuit",
            "city": "Hustler",
            "state": "Delaware",
            "zip": "19765-0566",
            "country": "USA",
            "phone": "(302) 555-0234"
          },
          "email": "eli.bailey@company.com"
        },
        {
          "first_name": "Gabriel",
          "last_name": "Ortiz",
          "address": {
            "street": "7009 Sunny Elk By-pass",
            "city": "Herculaneum",
            "state": "Louisiana",
            "zip": "70799-1187",
            "country": "USA",
            "phone": "(225) 555-5532"
          },
          "email": "gabriel.ortiz@company.com"
        },
        {
          "first_name": "Julian",
          "last_name": "Williams",
          "address": {
            "street": "2514 Thunder Square",
            "city": "Confidence",
            "state": "New Jersey",
            "zip": "07454-7884",
            "country": "USA",
            "phone": "(551) 555-6031"
          },
          "email": "julian.williams@company.com"
        },
        {
          "first_name": "Nathan",
          "last_name": "Edwards",
          "address": {
            "street": "4783 Merry Sky Glade",
            "city": "Blowville",
            "state": "Wyoming",
            "zip": "83099-4319",
            "country": "USA",
            "phone": "(307) 555-7524"
          },
          "email": "nathan.edwards@company.com"
        },
        {
          "first_name": "Jackson",
          "last_name": "Barnes",
          "address": {
            "street": "8616 Heather Glade",
            "city": "Corkscrew",
            "state": "Mississippi",
            "zip": "38785-5317",
            "country": "USA",
            "phone": "(601) 555-9985"
          },
          "email": "jackson.barnes@company.com"
        },
        {
          "first_name": "Violet",
          "last_name": "Perez",
          "address": {
            "street": "9546 Middle Passage",
            "city": "Beans Corner",
            "state": "Connecticut",
            "zip": "06257-5597",
            "country": "USA",
            "phone": "(860) 555-6030"
          },
          "email": "violet.perez@company.com"
        },
        {
          "first_name": "Melanie",
          "last_name": "Scott",
          "address": {
            "street": "8157 Silver Heath",
            "city": "Steam Corners",
            "state": "Connecticut",
            "zip": "06278-3936",
            "country": "USA",
            "phone": "(860) 555-7560"
          },
          "email": "melanie.scott@company.com"
        },
        {
          "first_name": "Jason",
          "last_name": "Taylor",
          "address": {
            "street": "920 Rocky Glen",
            "city": "Scuppernong",
            "state": "Utah",
            "zip": "84818-7141",
            "country": "USA",
            "phone": "(385) 555-3593"
          },
          "email": "jason.taylor@company.com"
        },
        {
          "first_name": "Madelyn",
          "last_name": "James",
          "address": {
            "street": "4947 Middle Estates",
            "city": "Reduction",
            "state": "Rhode Island",
            "zip": "02881-3575",
            "country": "USA",
            "phone": "(401) 555-8420"
          },
          "email": "madelyn.james@company.com"
        },
        {
          "first_name": "Thomas",
          "last_name": "Bailey",
          "address": {
            "street": "9797 Noble Hills Trail",
            "city": "Iron Horse",
            "state": "Pennsylvania",
            "zip": "16906-7969",
            "country": "USA",
            "phone": "(267) 555-3375"
          },
          "email": "thomas.bailey@company.com"
        },
        {
          "first_name": "Isabelle",
          "last_name": "Butler",
          "address": {
            "street": "9386 Crystal Oak Beach",
            "city": "Bacon",
            "state": "Maryland",
            "zip": "21997-5878",
            "country": "USA",
            "phone": "(240) 555-5122"
          },
          "email": "isabelle.butler@company.com"
        },
        {
          "first_name": "Jace",
          "last_name": "Ortiz",
          "address": {
            "street": "7851 Rustic Canyon",
            "city": "Convent",
            "state": "Minnesota",
            "zip": "55320-7161",
            "country": "USA",
            "phone": "(612) 555-1693"
          },
          "email": "jace.ortiz@company.com"
        },
        {
          "first_name": "Jackson",
          "last_name": "Foster",
          "address": {
            "street": "7009 Sunny Elk By-pass",
            "city": "Herculaneum",
            "state": "Louisiana",
            "zip": "70799-1187",
            "country": "USA",
            "phone": "(225) 555-5532"
          },
          "email": "jackson.foster@company.com"
        },
        {
          "first_name": "Madison",
          "last_name": "Campbell",
          "address": {
            "street": "8787 Velvet Deer Via",
            "city": "First Cliff",
            "state": "North Carolina",
            "zip": "27141-0975",
            "country": "USA",
            "phone": "(252) 555-2998"
          },
          "email": "madison.campbell@company.com"
        },
        {
          "first_name": "Logan",
          "last_name": "Carter",
          "address": {
            "street": "9683 Silent Private",
            "city": "Fate",
            "state": "Alabama",
            "zip": "36269-7910",
            "country": "USA",
            "phone": "(251) 555-5530"
          },
          "email": "logan.carter@company.com"
        },
        {
          "first_name": "Bentley",
          "last_name": "Mitchell",
          "address": {
            "street": "2854 Cotton Island",
            "city": "Brian Head",
            "state": "Alabama",
            "zip": "36016-8269",
            "country": "USA",
            "phone": "(256) 555-6847"
          },
          "email": "bentley.mitchell@company.com"
        },
        {
          "first_name": "Samuel",
          "last_name": "Watson",
          "address": {
            "street": "3759 Grand Timber Concession",
            "city": "Zulu",
            "state": "Kentucky",
            "zip": "40570-2685",
            "country": "USA",
            "phone": "(859) 555-0145"
          },
          "email": "samuel.watson@company.com"
        },
        {
          "first_name": "Camila",
          "last_name": "Watson",
          "address": {
            "street": "9570 Lazy Oak Farm",
            "city": "Hoosick",
            "state": "South Carolina",
            "zip": "29606-1498",
            "country": "USA",
            "phone": "(843) 555-1545"
          },
          "email": "camila.watson@company.com"
        }
      ];
      if (t.records.length > 0) {
        t.prepareData(1, 10);
      }
    }*/

    if (t.footerButtons.length > 0) {
      // create elements
      for (const item of t.footerButtons) {
        const btn = document.createElement('vaadin-button');
        btn.setAttribute('part', item.part);
        btn.setAttribute('theme', item.theme);
        if (item?.icon !== '') {
          const icon = document.createElement('vaadin-icon');
          icon.setAttribute("src", UIIcons[item.icon]);
          btn.appendChild(icon);
        }
        if (item?.text !== '') {
          btn.appendChild(document.createTextNode(item.text));
        }
        if (item?.tooltip !== '') {
          const tooltip = document.createElement('vaadin-tooltip');
          tooltip.setAttribute("slot", "tooltip");
          tooltip.setAttribute("text", item.tooltip);
          btn.appendChild(tooltip);
        }
        if (item?.disabled === true) {
          btn.disabled = true;
        }
        t.elContainerFooter.appendChild(btn);
      }
      //set event listeners
      const itemsFireClick = t.elContainerFooter.querySelectorAll('vaadin-button');
      // console.log(itemsFireClick)
      if (itemsFireClick.length > 0) {
        await t.uiFns.asyncForEach(itemsFireClick, async (item: Button) => {
          //console.log(item.innerText);
          item.addEventListener('click', t._buttonClick.bind(t, item.part[0]), { passive: true });
        });
      }
    }
    //if (Build.isDev) {
    //  t.setButtonDisplay('test-1', 'hidden');
    //  t.setButtonState('test-1', 'enabled');
    //}
    //await t.elPagebar.setButtonDisplay('add', 'hidden');
  }

  private async _buttonClick(button: string) {
    let t = this;
    t.pagebarChange({ type: 'button', element: button, data: null });
    return;
  }

  @Method()
  async setData(obj: any) {
    let t = this;
    //console.log('setData', obj);
    //const totalRecords: number = t.records.length;
    t.elPagebar.args = {
      currentPage: obj.currentPage,
      recordsPerPage: obj.recordsPerPage,
      totalRecords: obj.totalRecords
    };
    t.pxDataTable.data = obj.records;
    t.afterLoad();
    return;
  }

  @Method()
  async afterLoad() {
    let t = this;
    t.elLoadingIndicator.setVisible(false);
  }

  @Method()
  async beforeLoad() {
    let t = this;
    //console.log('grid-beforeLoad')
    await t.pxDataTable.clearSelectedRows();
    await t.elLoadingIndicator.setVisible(true);
    //await t.pxDataTable.setData([]);
    return;
  }

  @Method()
  async clearSelectedRows() {
    let t = this;
    //console.log('grid-clearSelectedRows')
    await t.pxDataTable.clearSelectedRows();
  }

  @Method()
  async clearSortData() {
    let t = this;
    t.sorts = [];
    await t.pxDataTable.clearSortData();
  }

  @Method()
  async getSelectedRows() {
    let t = this;
    return await t.pxDataTable.getSelectedRows();
  }

  @Method()
  async setButtonDisplay(part: string, state: string) {
    let t = this;
    const btn: Button = t.elContainerFooter.querySelector('vaadin-button[part=' + part + ']');
    if (btn) {
      if (state === 'hidden') {
        btn.hidden = true;
      } else {
        btn.hidden = false;
      }
    }
  }

  @Method()
  async setButtonState(part: string, state: string) {
    let t = this;
    const btn: Button = t.elContainerFooter.querySelector('vaadin-button[part=' + part + ']');
    if (btn) {
      if (state === 'enabled') {
        btn.disabled = false;
      } else {
        btn.disabled = true;
      }
    }
  }

  async onRowClick(data: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log('onRowClick');
    //}
    if (data.select) {
      await t.elPagebar.setButtonState('del', 'enabled');
      await t.elPagebar.setButtonState('edit', 'enabled');
      t.uiDataSourceEvent.emit({ type: 'row-select', element: null, data });
      //if (Build.isDev) {
      //console.log({ type: 'row-select-click', element: null, data });
      //}
    }
  }

  async onRowDblClick(data: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log('onRowDblClick');
    //}
    if (data.select) {
      await t.elPagebar.setButtonState('del', 'enabled');
      await t.elPagebar.setButtonState('edit', 'enabled');
      t.uiDataSourceEvent.emit({ type: 'row-dblclick', element: null, data });
      //if (Build.isDev) {
      //console.log({ type: 'row-select-dblclick', element: null, data });
      //}
    }
  }

  async onSortCellClick(data: any) {
    let t = this;
    if (t.isSortMultiple) {
      const idx: number = await t.uiFns.getIndex(t.sorts, 'field', data.field);
      if (idx === -1) {
        t.sorts.push(data);
      } else {
        if (data.dir === '') {
          t.sorts.splice(idx, 1);
        } else {
          t.sorts[idx].dir = data.dir;
        }
      }
      t.uiDataSourceEvent.emit({ type: 'col-sort', element: null, data: t.sorts });
    } else {
      if (data.dir === '') {
        t.uiDataSourceEvent.emit({ type: 'col-sort', element: null, data: [] });
      } else {
        t.uiDataSourceEvent.emit({ type: 'col-sort', element: null, data });
      }
    }
  }

  async onClearSelections(triggerEvent: boolean) {
    let t = this;
    //if (Build.isDev) {
    //  console.log('onClearSelections:', triggerEvent);
    //}
    await t.elPagebar.setButtonState('del', 'disabled');
    await t.elPagebar.setButtonState('edit', 'disabled');
    if (triggerEvent) {
      t.uiDataSourceEvent.emit({ type: 'clear-row-select', element: null, data: null });
      //if (Build.isDev) {
      //  console.log({ type: 'clear-row-select', element: null, data: null });
      //}
    }
  }

  async pagebarChange(obj: any) {
    //console.log('pagebarChange', obj)
    let t = this;
    t.uiDataSourceEvent.emit(obj);
    //if (Build.isDev) {
    //  console.log(obj);
    //}

    if (Build.isDev) {
      if (obj.type === 'button') {
        switch (obj.element) {
          case 'first': {
            t.beforeLoad();
            await t.uiFns.delay(2000);
            await t.prepareData(1, obj.args.recordsPerPage);
            break;
          }
          case 'prev': {
            t.beforeLoad();
            await t.uiFns.delay(2000);
            await t.prepareData((obj.args.currentPage - 1), obj.args.recordsPerPage);
            break;
          }
          case 'next': {
            t.beforeLoad();
            await t.uiFns.delay(2000);
            await t.prepareData((obj.args.currentPage + 1), obj.args.recordsPerPage);
            break;
          }
          case 'last': {
            t.beforeLoad();
            await t.uiFns.delay(2000);
            await t.prepareData(Math.ceil(t.records.length / obj.args.recordsPerPage), obj.args.recordsPerPage);
            break;
          }
          case 'refresh': {
            t.beforeLoad();
            await t.uiFns.delay(2000);
            await t.prepareData(obj.args.currentPage, obj.args.recordsPerPage);
            break;
          }
          default: {

            break;
          }
        }
      }
      if (obj.type === 'change-currentPage') {
        t.beforeLoad();
        await t.uiFns.delay(1000);
        await t.prepareData(parseInt(obj.data, 10), obj.args.recordsPerPage);
      }
      if (obj.type === 'change-recordsPerPage') {
        t.beforeLoad();
        await t.uiFns.delay(1000);
        obj.args.recordsPerPage = parseInt(obj.data, 10);
        await t.prepareData(1, obj.args.recordsPerPage);
      }
    }

  }

  // sadece dev mod için yazıldı
  async prepareData(currentPage: number, recordsPerPage: number) {
    let t = this;
    //if (Build.isDev) {
    //console.log('prepData', currentPage, recordsPerPage);
    //}
    const totalRecords: number = t.records.length;
    t.elPagebar.args = {
      currentPage,
      recordsPerPage,
      totalRecords
    };
    //await t.pxDataTable.setData(t.records.slice((currentPage - 1) * recordsPerPage, Math.min(currentPage * recordsPerPage, totalRecords)));
    const data: any = t.records.slice((currentPage - 1) * recordsPerPage, Math.min(currentPage * recordsPerPage, totalRecords));
    //console.log('prepData', currentPage, recordsPerPage, data);
    t.pxDataTable.data = data;
    //if (Build.isDev) {
    //  console.log('prepData', currentPage, recordsPerPage, data);
    //}
    t.afterLoad();
    return;
  }

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('Render|', t.myElement.tagName.toLowerCase());
    //}
    let gridHeight: string = 'calc(100% - 0px)';
    let dtHeight: string = 'calc(100% - 0px)';
    let styleHeader: any = { height: "50px", width: 'calc(100% - 2px)', border: "1px solid var(--lumo-contrast-20pct)", borderBottom: "none" };
    let styleFooter: any = { height: "50px", width: 'calc(100% - 2px)', border: "1px solid var(--lumo-contrast-20pct)", borderTop: "none" };
    if (t.isShowHeader) {
      if (t.isShowFooter) {
        gridHeight = 'calc(100% - 102px)';
        dtHeight = 'calc(100% - 2px)';
      } else {
        gridHeight = 'calc(100% - 51px)';
        dtHeight = 'calc(100% - 1px)';
        styleFooter = { display: "none" };
      }
    } else {
      styleHeader = { display: "none" };
      if (t.isShowFooter) {
        gridHeight = 'calc(100% - 51px)';
        dtHeight = 'calc(100% - 1px)';
      } else {
        styleFooter = { display: "none" };
      }
    }
    return (
      <div style={{ height: t.height, width: t.width }} >
        <ui-loading-indicator
          ref={(el) => t.elLoadingIndicator = el as unknown as UILoadingIndicator}
          text='Lüften bekleyiniz...'
        >
        </ui-loading-indicator>
        <div class="flex flex--col" style={{ height: "100%", width: "100%" }} >
          <div class="flex" ref={(el) => t.elContainerHeader = el as HTMLDivElement}
            style={styleHeader}>
            <ui-pagebar ref={(el) => t.elPagebar = el as unknown as UIPagebar}
              on-uiPagebarEvent={(e) => { t.pagebarChange(e.detail); }}
              style={{ height: "100%", width: "100%" }}
            ></ui-pagebar>
          </div>
          <div style={{ height: gridHeight, width: "100%" }}>
            <px-data-table
              ref={(el) => t.pxDataTable = el as unknown as PxDataTable}
              style={{ overflowY: "auto", display: "block", height: dtHeight, borderTop: t.tableBorderTop, borderBottom: t.tableBorderBottom, borderLeft: t.tableBorderLeft, borderRight: t.tableBorderRight }}
              class="data-table"
              columnRenderer={t.columnRenderer}
              columns={t.columns}
              data={[]}
              clsTable="table--small"
              isSelectAll={t.isSelectAll}
              isSortable={t.isSortable}
              isSortMultiple={t.isSortMultiple}
              selectionMode={t.selectionMode}
              onPxDataTableRowClick={(e) => { t.onRowClick(e.detail); }}
              onPxDataTableRowDblClick={(e) => { t.onRowDblClick(e.detail); }}
              onPxDataTableClearSelections={() => { t.onClearSelections(true); }}
              onPxDataTableSortCellClick={(e) => { t.onSortCellClick(e.detail); }}
            ></px-data-table>
          </div>
          <div ref={(el) => t.elContainerFooter = el as HTMLDivElement} style={styleFooter}>
          </div>
        </div>
      </div>
    );
  }
}
