import { Component, Element, Event, EventEmitter, h, Method, Prop } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';
//px-components
import '@vaadin/button';
import { PxDataTable } from '../px-data-table/px-data-table';
//ui-components
//import { Button } from '@vaadin/button';
import { UILoadingIndicator } from '../ui-loading-indicator/ui-loading-indicator';

import { debounce } from "ts-debounce";

@Component({
  tag: 'ui-grid-editable',
  styleUrl: 'ui-grid.scss',
  shadow: true
})
export class UIGridEditable {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'uiDataSourceEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uiDataSourceEvent: EventEmitter;

  @Prop() args: any;
  @Prop() columnRenderer: string = 'default';
  @Prop() height: string = '600px';
  @Prop() width: string = '100%';
  @Prop() border: string = "1px solid var(--lumo-contrast-20pct)";
  @Prop() isFreeze: boolean = false;//true ise satır eklenemiyor
  @Prop() isFillCell: boolean = true;
  @Prop({ mutable: true }) columns: any = [];/* [
    { label: "", dataIndex: 'id', style: { width: "20px", textAlign: "right", display: "none" } },
    { label: "", dataIndex: 'id2', style: { width: "20px", textAlign: "right", display: "none" } },
    { label: "#", dataIndex: 'sira', style: { width: "25px", maxWidth: "44px", textAlign: "right", fontWeight: "bold" }, type: 'icon', options: { autoIncrement: true, icon: 'vaadin__hash' } },
    { label: "+", dataIndex: 'add', style: { width: "24px", maxWidth: "44px", textAlign: "center", color: "green", paddingLeft: "10px", paddingRight: "10px" }, type: 'icon', options: { icon: 'vaadin__plus_circle' } },
    { label: "-", dataIndex: 'del', style: { width: "24px", maxWidth: "44px", textAlign: "center", color: "red", paddingLeft: "10px", paddingRight: "10px" }, type: 'icon', options: { icon: 'vaadin__minus_circle' } },
    { label: "Stok Kodu", dataIndex: 'stokkodu', style: { width: "200px" }, type: 'search', options: { placeholder: "" } },
    { label: "Stok Adı", dataIndex: 'stokadi', style: { width: "300px" }, type: 'text', options: { readonly: true } },
    //{ label: "Hesap Kodu", dataIndex: 'bankakodu', style: { width: "160px" }, type: 'search', options: { placeholder: "" } },
    //{ label: "Hesap Adı", dataIndex: 'hesapadi', style: { width: "250px" }, type: 'text', options: { readonly: true } },
    { label: "Miktar", dataIndex: 'miktar', style: { width: "100px", textAlign: "right" }, type: 'number', options: { min: 0 } },
    { label: "Fiyat", dataIndex: 'fiyat', style: { width: "150px", textAlign: "right" }, type: 'money', options: { min: 0, symbol: '₺', clearButtonVisible: true } },
    { label: "Tutar1", dataIndex: 'tutar1', style: { width: "150px", textAlign: "right", display: "none" }, type: 'money', options: { symbol: '₺' } },//, display: "none"
    { label: "İsk %", dataIndex: 'iskontooran', style: { width: "80px", textAlign: "right" }, type: 'number', options: { min: 0 } },
    { label: "İsk ₺", dataIndex: 'iskontotutar', style: { width: "150px", textAlign: "right" }, type: 'money', options: { symbol: '₺' } },
    { label: "Tutar", dataIndex: 'tutar', style: { width: "150px", textAlign: "right" }, type: 'money', options: { symbol: '₺' } },
    { label: "KDV %", dataIndex: 'kdvoran', style: { width: "80px", textAlign: "right" }, type: 'number', options: { min: 0 } },
    { label: "KDV ₺", dataIndex: 'kdvtutar', style: { width: "150px", textAlign: "right" }, type: 'money', options: { symbol: '₺' } },
    { label: "Toplam", dataIndex: 'toplam', style: { width: "150px", textAlign: "right" }, type: 'money', options: { symbol: '₺' } },
    //{ label: "pb", dataIndex: 'parabirimiId_lookup_', style: { width: "50px", textAlign: "left", display: "none" }, type: 'combo', store: [] },
    //{ label: "Tarih", dataIndex: 'tarih', style: { width: "130px" }, type: 'date' },
    //{ label: "Toggle", dataIndex: 'secim', style: { width: "80px", textAlign: "center" }, type: 'toggle', options: {} },
    //{ label: "Cevap", dataIndex: 'cevap', style: { width: "150px" }, type: 'radio', options: { answertype: "Evet-Hayır", maxlength: 50 } },
    //{ label: "Combo", dataIndex: 'sec', style: { width: "150px" }, type: 'combo', options: { data: ['Chrome', 'Edge', 'Firefox', 'Safari'] } },
    //{ label: "Makbuz No", dataIndex: 'makbuzno', style: { width: "150px" }, type: 'text', options: { maxlength: 50 } },
    //{ label: "Açıklama", dataIndex: 'aciklama', style: { width: "100%", borderRiight: 'none' }, type: 'text', options: { maxlength: 250 } }
  ];*/
  @Prop() calculations: any[] = []/* [
    { dataIndex: 'tutar1', proc: '*', fields: ['miktar', 'fiyat'] },
    { dataIndex: 'iskontotutar', proc: '%', fields: ['tutar1', 'iskontooran'] },
    { dataIndex: 'tutar', proc: '-', fields: ['tutar1', 'iskontotutar'] },
    { dataIndex: 'kdvtutar', proc: '%', fields: ['tutar', 'kdvoran'] },
    { dataIndex: 'toplam', proc: '+', fields: ['tutar', 'kdvtutar'] }
  ];*/
  @Prop() totals: any[] = [];

  uiFns: UIFunctions = new UIFunctions();

  pxDataTable: PxDataTable;
  elLoadingIndicator: UILoadingIndicator;

  records: any[] = [];

  async componentWillLoad() {
    let t = this;
    /*
    if (Build.isDev) {
      t.columns = [
        { label: "", dataIndex: 'id', style: { width: "20px", textAlign: "right", display: "none" } },
        { label: "", dataIndex: 'id2', style: { width: "20px", textAlign: "right", display: "none" } },
        { label: "#", dataIndex: 'sira', style: { width: "25px", maxWidth: "44px", textAlign: "right", fontWeight: "bold" }, type: 'icon', options: { autoIncrement: true, icon: 'vaadin__hash' } },
        { label: "+", dataIndex: 'add', style: { width: "24px", maxWidth: "44px", textAlign: "center", color: "green", paddingLeft: "10px", paddingRight: "10px" }, type: 'icon', options: { icon: 'vaadin__plus_circle' } },
        { label: "-", dataIndex: 'del', style: { width: "24px", maxWidth: "44px", textAlign: "center", color: "red", paddingLeft: "10px", paddingRight: "10px" }, type: 'icon', options: { icon: 'vaadin__minus_circle' } },
        { label: "Stok Kodu", dataIndex: 'stokkodu', style: { width: "200px" }, type: 'search', options: { placeholder: "" } },
        { label: "Stok Adı", dataIndex: 'stokadi', style: { width: "300px" }, type: 'text', options: { readonly: true } },
        //{ label: "Hesap Kodu", dataIndex: 'bankakodu', style: { width: "160px" }, type: 'search', options: { placeholder: "" } },
        //{ label: "Hesap Adı", dataIndex: 'hesapadi', style: { width: "250px" }, type: 'text', options: { readonly: true } },
        { label: "Miktar", dataIndex: 'miktar', style: { width: "100px", textAlign: "right" }, type: 'number', options: { min: 0 } },
        { label: "Fiyat", dataIndex: 'fiyat', style: { width: "150px", textAlign: "right" }, type: 'money', options: { min: 0, symbol: '₺', clearButtonVisible: true } },
        { label: "Tutar1", dataIndex: 'tutar1', style: { width: "150px", textAlign: "right", display: "none" }, type: 'money', options: { symbol: '₺' } },//, display: "none"
        { label: "İsk %", dataIndex: 'iskontooran', style: { width: "80px", textAlign: "right" }, type: 'number', options: { min: 0 } },
        { label: "İsk ₺", dataIndex: 'iskontotutar', style: { width: "150px", textAlign: "right" }, type: 'money', options: { symbol: '₺' } },
        { label: "Tutar", dataIndex: 'tutar', style: { width: "150px", textAlign: "right" }, type: 'money', options: { symbol: '₺' } },
        { label: "KDV %", dataIndex: 'kdvoran', style: { width: "80px", textAlign: "right" }, type: 'number', options: { min: 0 } },
        { label: "KDV ₺", dataIndex: 'kdvtutar', style: { width: "150px", textAlign: "right" }, type: 'money', options: { symbol: '₺' } },
        { label: "Toplam", dataIndex: 'toplam', style: { width: "150px", textAlign: "right" }, type: 'money', options: { symbol: '₺' } },
        { label: "pb", dataIndex: 'parabirimiId_lookup_', style: { width: "50px", textAlign: "left", display: "none" }, type: 'combo', store: [] },
        { label: "Tarih", dataIndex: 'tarih', style: { width: "140px" }, type: 'date' },
        //{ label: "Toggle", dataIndex: 'secim', style: { width: "80px", textAlign: "center" }, type: 'toggle', options: {} },
        //{ label: "Cevap", dataIndex: 'cevap', style: { width: "150px" }, type: 'radio', options: { answertype: "Evet-Hayır", maxlength: 50 } },
        //{ label: "Combo", dataIndex: 'sec', style: { width: "150px" }, type: 'combo', options: { data: ['Chrome', 'Edge', 'Firefox', 'Safari'] } },
        //{ label: "Makbuz No", dataIndex: 'makbuzno', style: { width: "150px" }, type: 'text', options: { maxlength: 50 } },
        //{ label: "Açıklama", dataIndex: 'aciklama', style: { width: "100%", borderRiight: 'none' }, type: 'text', options: { maxlength: 250 } }
      ];
      t.calculations = [
        { dataIndex: 'tutar1', proc: '*', fields: ['miktar', 'fiyat'] },
        { dataIndex: 'iskontotutar', proc: '%', fields: ['tutar1', 'iskontooran'] },
        { dataIndex: 'tutar', proc: '-', fields: ['tutar1', 'iskontotutar'] },
        { dataIndex: 'kdvtutar', proc: '%', fields: ['tutar', 'kdvoran'] },
        { dataIndex: 'toplam', proc: '+', fields: ['tutar', 'kdvtutar'] }
      ];
    }
    */
    if (typeof t.columns === 'string') {
      t.columns = JSON.parse(t.columns);
    }
    let tmpObj: any = await t.getNewRow(false);
    t.records = [...t.records, tmpObj];
    //console.log('componentWillLoad', t.records)
  }

  async componentDidLoad() {
    //let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    //if (t.hiddenPageBarButtons.length > 0) {
    //  for (const item of t.hiddenPageBarButtons) {
    //    await t.elPagebar.setButtonDisplay(item, 'hidden');
    //  }
    //}
    //if (Build.isDev) {
    //t.pxDataTable.addRow({ sira: '', kasakodu: '123', kasaadi: '456', borc: '800', tarih: '2023-10-23', cevap: 'Evet', sec: 'Edge', secim: true, aciklama: 'test-1' });
    //t.pxDataTable.addRow({ sira: '', kasakodu: '456', kasaadi: '789', borc: '500', tarih: '2023-10-20', cevap: 'Hayır', sec: 'Edge', secim: true, aciklama: 'test2' });
    //t.pxDataTable.addRow({ sira: '', kasakodu: '' });
    //}
    //if (Build.isDev) {
    //  t.setButtonDisplay('test-1', 'hidden');
    //  t.setButtonState('test-1', 'enabled');
    //}
    //await t.elPagebar.setButtonDisplay('add', 'hidden');
  }

  @Method()
  async getData() {
    let t = this;
    let moneyColumns: string[] = [];
    for (const col of t.columns) {
      if (col?.type === 'money' && moneyColumns.indexOf(col.dataIndex) === -1) {
        moneyColumns.push(col.dataIndex);
      }
    }
    if (moneyColumns.length > 0) {
      for (const row of t.records) {
        for (const col of moneyColumns) {
          if (row[col] === '') {
            row[col] = 0;
          }
          if (typeof row[col] === 'string') {
            row[col] = parseFloat(row[col]);
          }
        }
      }
    }
    return t.records;
  }

  @Method()
  async setSelectedCellValue(val: any) {
    let t = this;
    return await t.pxDataTable.setSelectedCellValue(val);
  }

  @Method()
  async setData(obj: any) {
    let t = this;
    t.records = obj.records;
    await t.beforeLoad();
    //console.log('setData-1')
    t.pxDataTable.data = [];
    await t.uiFns.delay(100);
    //console.log('setData-2')
    t.pxDataTable.data = t.records;
    await t.afterLoad();
    //console.log('setData-3')
    return;
  }

  @Method()
  async afterLoad() {
    let t = this;
    t.elLoadingIndicator.setVisible(false);
  }

  @Method()
  async beforeLoad() {
    let t = this;
    await t.pxDataTable.clearSelectedRows();
    await t.elLoadingIndicator.setVisible(true);
    //await t.pxDataTable.setData([]);
    return;
  }

  async onCalcChange(data: any) {
    let t = this;
    const debounced = debounce(
      (datadeounced) => {
        t.uiDataSourceEvent.emit({ type: 'calc-change', element: null, data: datadeounced });
        //console.log('onCalcChange:', datadeounced);
      }
      , 20,
      // The maximum time func is allowed to be delayed before it's invoked:
      { maxWait: 50 }
    );
    debounced(data);
  }

  async onTotalsChange(data: any) {
    let t = this;
    const debounced = debounce(
      (datadeounced) => {
        t.uiDataSourceEvent.emit({ type: 'total-change', element: null, data: datadeounced });
        //console.log('total-change:', datadeounced);
      }
      , 20,
      // The maximum time func is allowed to be delayed before it's invoked:
      { maxWait: 50 }
    );
    debounced(data);
  }

  async onColumnChange(data: any) {
    let t = this;
    //console.log('onColumnChange', data)
    const debounced = debounce(
      (datadeounced, index) => {
        if (t.records[index][datadeounced.column.dataIndex] !== datadeounced.value) {
          //console.log('onColumnChange', datadeounced, t.records, idx);
          t.records[index][datadeounced.column.dataIndex] = datadeounced.value;
        }
      }
      , 10,
      // The maximum time func is allowed to be delayed before it's invoked:
      { maxWait: 20 }
    );
    const idx: number = await t.uiFns.getIndex(t.records, '_part_', data.row._part_);
    //console.log('idx', idx)
    if (idx !== -1) {
      //if (t.records[idx][data.column.dataIndex] !== data.value) {
      debounced(data, idx);
      //}
    }
    //debounced(data);
  }

  async onColumnButtonClick(data: any) {
    let t = this;
    //console.log('onColumnButtonClick', data, t.isfreeze);
    if (t.isFreeze === false) {
      if (data.column.dataIndex === 'add') {
        return await t.getNewRow(true);
      }
    }
    if (data.column.dataIndex === 'del') {
      let autoIncFieldDataIndex: string = '';
      for (const item of t.columns) {
        if (item?.options?.autoIncrement) {
          autoIncFieldDataIndex = item.dataIndex;
        }
      }
      const idx: number = await t.uiFns.getIndex(t.records, '_part_', data.row._part_);
      if (idx !== -1) {
        t.records.splice(idx, 1);
        for (let i = idx; i < t.records.length; i++) {
          //if (typeof t.records[i].sira !== 'undefined') {
          //  t.records[i].sira -= 1;
          //}
          if (typeof t.records[i][autoIncFieldDataIndex] !== 'undefined') {
            t.records[i][autoIncFieldDataIndex] -= 1;
          }
        }
        if (t.records.length > 0) {
          t.pxDataTable.data = [];
          await t.uiFns.delay(100);
          t.pxDataTable.data = t.records;
        } else {
          await t.getNewRow(true);
        }
      }
      //console.log('del-row:', t.records);
      return;
    }
    if (data.column.type === 'search') {
      t.uiDataSourceEvent.emit({ type: 'row-search', element: null, data });
      //console.log('onColumnButtonClick', data);
      return;
    }
  }

  async onRowClick(data: any) {
    let t = this;
    //if (Build.isDev) {
    //  console.log('onRowClick');
    //}
    if (data.select) {
      t.uiDataSourceEvent.emit({ type: 'row-select', element: null, data });
      //if (Build.isDev) {
      //  console.log({ type: 'row-select', element: null, data });
      //}
    }
  }

  async onClearSelections(triggerEvent: boolean) {
    let t = this;
    //if (Build.isDev) {
    //  console.log('onClearSelections:', triggerEvent);
    //}
    if (triggerEvent) {
      t.uiDataSourceEvent.emit({ type: 'clear-row-select', element: null, data: null });
      //if (Build.isDev) {
      //  console.log({ type: 'clear-row-select', element: null, data: null });
      //}
    }
  }

  async getNewRow(append: boolean = false) {
    let t = this;
    let tmpObj: any = {};
    for (const item of t.columns) {
      if (item?.options?.autoIncrement) {
        tmpObj[item.dataIndex] = t.records.length + 1;
      } else {
        tmpObj[item.dataIndex] = '';
      }
    }
    if (append) {
      t.records = [...t.records, tmpObj];
      t.pxDataTable.data = t.records;
    }
    //if (Build.isDev) {
    //console.log('getNewRow', tmpObj);
    //}
    return tmpObj;
  }

  render() {
    let t = this;
    let gridHeight: string = 'calc(100% - 0px)';
    let dtHeight: string = 'calc(100% - 2px)';
    let tableStyle: any = { overflowY: "auto", display: "block", height: dtHeight, border: t.border };
    //if (Build.isDev) {
    //  console.log('Render|', t.myElement.tagName.toLowerCase());
    //}
    return (
      <div style={{ height: t.height, width: t.width }} >
        <ui-loading-indicator
          ref={(el) => t.elLoadingIndicator = el as unknown as UILoadingIndicator}
          text='Lüften bekleyiniz...'
        >
        </ui-loading-indicator>
        <div class="flex flex--col" style={{ height: "100%", width: "100%" }} >
          <div style={{ height: gridHeight, width: "100%" }}>
            <px-data-table
              ref={(el) => t.pxDataTable = el as unknown as PxDataTable}
              style={tableStyle}
              class="data-table"
              columnRenderer={t.columnRenderer}
              columns={t.columns}
              calculations={t.calculations}
              totals={t.totals}
              data={t.records}
              clsTable="table--small"
              isSelectAll={false}
              isEditable={true}
              isFillCell={t.isFillCell}
              selectionMode="cell"
              onPxDataTableRowClick={(e) => { t.onRowClick(e.detail); }}
              onPxDataTableClearSelections={() => { t.onClearSelections(true); }}
              onPxDataTableCalcChange={(e) => { t.onCalcChange(e.detail); }}
              onPxDataTableTotalsChange={(e) => { t.onTotalsChange(e.detail); }}
              onPxDataTableColumnChange={(e) => { t.onColumnChange(e.detail); }}
              onPxDataTableButtonClick={(e) => { t.onColumnButtonClick(e.detail); }}
            ></px-data-table>
          </div>
        </div>
      </div>
    );
  }
}
