import { Build, Component, Element, Event, EventEmitter, h, Method, Prop } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';
//import { ReportViewer, ReportDesigner, Core, PdfExport, HtmlExport, XlsxExport, TabularDataExport } from "@mescius/activereportsjs";
import { /*Core,*/ ReportViewer } from "@mescius/activereportsjs";
import { Designer } from "@mescius/activereportsjs/reportdesigner";
import { Viewer } from "@mescius/activereportsjs/reportviewer";
import * as UIIcons from '../ui-icons';
import { Icon } from '@vaadin/icon';
import '@vaadin/button';
import '@vaadin/dialog';
import '@vaadin/icon';
import { Dialog, DialogOverlay } from '@vaadin/dialog';
@Component({
  tag: 'app-report-designer',
  styleUrls: ['../../../node_modules/@mescius/activereportsjs/styles/ar-js-ui.css'
    , '../../../node_modules/@mescius/activereportsjs/styles/ar-js-viewer.css'
    , '../../../node_modules/@mescius/activereportsjs/styles/ar-js-designer.css'
    , 'app-report-designer.scss'],
  shadow: false
})
export class AppReportDesigner {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Prop() args: any = {};

  uiFns: UIFunctions = new UIFunctions();

  container: HTMLDivElement;
  cntDesigner: HTMLDivElement;
  cntViewer: HTMLDivElement;
  designer: Designer;
  viewer: Viewer;
  dialogFM: any;
  currentReportPath: string = '';

  CPLReport: any = { "Name": "", "Width": "2pt", "Layers": [{ "Name": "default" }], "CustomProperties": [{ "Name": "DisplayType", "Value": "Page" }, { "Name": "SizeType", "Value": "Default" }, { "Name": "CollapseWhiteSpace", "Value": "True" }], "Version": "7.6.0", "Page": { "PageWidth": "8.5in", "PageHeight": "11in", "RightMargin": "0in", "LeftMargin": "0in", "TopMargin": "0in", "BottomMargin": "0in", "Columns": 1, "ColumnSpacing": "0.5in", "PaperOrientation": "Portrait" }, "ReportSections": [{ "Type": "Continuous", "Name": "ContinuousSection1", "Page": { "PageWidth": "21cm", "PageHeight": "29.7cm", "RightMargin": "2.5cm", "LeftMargin": "2.5cm", "TopMargin": "2.5cm", "BottomMargin": "2.5cm", "Columns": 1, "ColumnSpacing": "0cm", "PaperOrientation": "Portrait" }, "Width": "2pt", "Body": { "Height": "2pt" } }] };
  resolveFunc = null;

  currentPage: number = 1;
  recordsPerPage: number = 25;
  totalRecords: number = 0;
  baseRequestParams: any;
  currentReport: any = null;

  async componentDidLoad() {
    let t = this;
    //Core.setLicenseKey("856515978924416#B1KZOQop6SlNmc9YDW8gGdMdTWCFnQvBlSQp5TwJkcwsyVS56NMpGTwYHVBNTd6N5NxFmZrUEewJUMz2kQOREOxMlMENlb4kmV5MjTRd4dF3mdyEzUVdjcMllSrUXMGRGWtRTdzU6dTRXV8MWN7hlajREUzcXYSd4Q8EkcwNVeURmZPlEMZZEav4WQzYUO0FkQzNGWpdVNnxUYysCaSJDOKNTerdHS95WN8lnbON6NLplZoJTaMpVcZpHSwcjcuVGZzw6aGhTSohFWy8UVrhVbqlVVWtEdXJXcWFkWFx4T8dEd4lza09UT6kVexlWU6tWV63mYO56dSBnU8tiQZl7LsJlNz5Gd9hmVQRFSyUFZC36dwMkI0IyUiwiIwEkQygTNFFjI0ICSiwiNzcTOzMTO6gTM0IicfJye#4Xfd5nIWx4SOJiOiMkIsISNWByUKRncvBXZSVmdpR7YBJiOi8kI1tlOiQmcQJCLiMDN4ETOwACMzQDM4IDMyIiOiQncDJCLiMXduMXdpN6cl5mLqwCcvRnLzVXajNXZt9iKs2WauMXdpN6cl5mLqwSbvNmLzVXajNXZt9iKsI7au26YuMXdpN6cl5mLqwCcq9ybj9yc5l6YzVWbuoiI0IyctRkIsIycrJXYQBCbl3mSiojIh94QiwiI6EDN4ITO8cTO5ETN6UDOiojIklkIs4XXbpjInxmZiwSZzxWYmpjIyNHZisnOiwmbBJye0ICRiwiI34TUVpUN9RkeMJWUsF4dBZDUBZHSyNDZ5wkTR9WWQJmYwxkNIJ4RNlnNpdTZIFWbkNWVLBjWypXOnV7SOVlNQNWSuRuN");
    t.baseRequestParams = {
      emitter: t.appEvent, controller: '', tagName: t.myElement.tagName.toLowerCase(), idx: t.args.idx, recordsPerPage: t.recordsPerPage
    };
    const appBarSettings = {
      visible: true, // Show the app bar
      homeTab: {
        visible: true, // Show the home tab
      },
      contextActionsTab: {
        visible: true, // Hide the context actions tab
      },
      parametersTab: {
        visible: true, // Hide the parameters tab
      },
    };
    const toolBarSettings = {
      visible: true, // Show the toolbar
    };
    const menuSettings = {
      visible: true, // Show the main menu
      toolBox: {
        visible: true, // Hide the Tool Box
      },
      documentExplorer: {
        visible: true, // Show the Document Explorer
      },
      groupEditor: {
        visible: true, // Hide the Group Editor
      },
      layerEditor: {
        visible: true, // Hide the Layer Editor
      },
      logo: {
        visible: false, // Hide the logo
      }
    };
    const dataTabSettings = {
      dataTab: {
        visible: true, // Hide the Data tab
      },
    };
    const propertyGridSettings = {
      propertiesTab: {
        visible: true, // Show the Properties tab
      },
      mode: "Advanced", // Set the Property Grid mode to "Advanced" or "Basic"
    };
    const statusBarSettings = {
      visible: true, // Hide the status bar
    };
    const themeConfig = {
      initialTheme: "System",
      themeSelector: {
        isEnabled: true,
        availableThemes: ["System", "Default", "DefaultDark", "DarkOled"]
      }
    };
    //const libraries = [
    //  {
    //    id: 'reports/ReportLib.rdlx-json',
    //    name: 'ReportLib',
    //    displayName: "Sales Data Visualizers"
    //  }
    //];
    const designerConfig: any = {
      language: 'en',
      appBar: appBarSettings,
      toolBar: toolBarSettings,
      menu: menuSettings,
      data: dataTabSettings,
      propertyGrid: propertyGridSettings,
      statusBar: statusBarSettings,
      themeConfig: themeConfig,
      //reportPartsLibraries: libraries,
    };
    t.designer = new Designer(t.cntDesigner, designerConfig);
    t.designer.setActionHandlers({
      onCreate: () => {
        //@@info: yeni deyince kaydedileceği klasör ve rapor dosya adı sorulacak
        //const reportId = `NewReport${++this.counter}`;
        //return Promise.resolve({
        //  definition: t.CPLReport,
        //  id: reportId,
        //  displayName: reportId,
        //});
        ////return Promise.resolve({ id: "reports/blank.rdlx-json", displayName: "NewReport" });
        return new Promise(async function (resolve) {
          t.resolveFunc = resolve;
          t.dialogFM = await t.generateDialog({
            id: "dialogLookup_create",
            headerTitle: "Yeni Rapor",
            draggable: true,
            modeless: false,
            resizable: false,
            noCloseOnEsc: true,
            noCloseOnOutsideClick: true,
            buttons: [
              //{ title: 'Kaydet', theme: 'primary', icon: 'fa__check', event: 'save', element: 'btnOkCreate' },
              { title: 'Vazgeç', theme: 'primary contrast', icon: 'vaadin__arrow_backward', event: 'cancel', element: 'btnCancelCreate' }
            ]
          });
        });
      },
      onOpen: () => {
        return new Promise(async function (resolve) {
          t.resolveFunc = resolve;
          t.dialogFM = await t.generateDialog({
            id: "dialogLookup_create",
            headerTitle: "Rapor Seç",
            draggable: true,
            modeless: false,
            resizable: false,
            noCloseOnEsc: true,
            noCloseOnOutsideClick: true,
            buttons: [
              { title: 'Seç', theme: 'primary', icon: 'fa__check', event: 'save', element: 'btnOkSelect' },
              { title: 'Vazgeç', theme: 'primary contrast', icon: 'vaadin__arrow_backward', event: 'cancel', element: 'btnCancelSelect' }
            ]
          });
        });
      },
      onRender: (report) => {
        t.cntViewer.style.display = "";
        t.cntDesigner.style.display = "none";
        t.viewer.open(report.definition);
        return Promise.resolve();
      },
      onSave: (info) => {
        //console.log('onSave-info', info, 'currentReportPath:', t.currentReportPath);
        return new Promise(async function (resolve) {
          t.resolveFunc = resolve;
          info.definition.Name = info.id;
          info.displayName = info.id;
          t.appEvent.emit({
            event: 'sendData'
            , component: t.myElement.tagName.toLowerCase()
            , idx: t.args.idx
            , data: info
            , method: 'post'
          });
        });
        //if (t.currentReportPath !== '') {
        //  const reportId: string = info.definition.Name;
        //  t.appEvent.emit({
        //    event: 'sendData'
        //    , component: t.myElement.tagName.toLowerCase()
        //    , idx: t.args.idx
        //    , data: info
        //    , method: 'post'
        //  });
        //  //const reportId = info.id || `NewReport${++this.counter}`;
        //  //t.reportStorage.set(reportId, info.definition);
        //  return Promise.resolve({ displayName: reportId });
        //} else {
        //  t.rendererEvent.emit({
        //    event: 'showNotification'
        //    , component: t.myElement.tagName.toLowerCase()
        //    , idx: t.args.idx
        //    , message: 'Yeni bir rapor oluşturmalı ya da bir raporu açmalısınız.'
        //  });
        //  return Promise.resolve(null);
        //}
      },
      onSaveAs: (info) => {
        //@@info: farklı kaydet deyince kaydedileceği klasör ve rapor dosya adı sorulacak
        //console.log('onSaveAs-info', info);
        return new Promise(async function (resolve) {
          t.resolveFunc = resolve;
          const reportId = `NewReport_${await t.uiFns.dateCurrent()}`;
          info.definition.Name = reportId;
          info.displayName = reportId;
          info.id = reportId;
          t.appEvent.emit({
            event: 'sendData'
            , component: t.myElement.tagName.toLowerCase()
            , idx: t.args.idx
            , data: info
            , method: 'post'
          });
        });
        //if (t.currentReportPath !== '') {
        //  const reportId = `NewReport_${await t.uiFns.dateCurrent()}`;
        //  //t.reportStorage.set(reportId, info.definition);
        //  return Promise.resolve({ id: reportId, displayName: reportId });
        //} else {
        //  t.rendererEvent.emit({
        //    event: 'showNotification'
        //    , component: t.myElement.tagName.toLowerCase()
        //    , idx: t.args.idx
        //    , message: 'Yeni bir rapor oluşturmalı ya da bir raporu açmalısınız.'
        //  });
        //  return Promise.resolve(null);
        //}
      },
    });
    //await t.designer.setReport({ id: "reports/blank.rdlx-json", displayName: "NewReport" });
    //t.designer.getPanelsAPI().then(function (panelsApi) {
    //  panelsApi.menu.open("libraries-list");
    //});
    //const btnInfo: HTMLButtonElement = t.cntDesigner.querySelector('button[title="About"]');
    //if (btnInfo) {
    //  btnInfo.style.display = "none";
    //}
    t.viewer = new ReportViewer.Viewer(t.cntViewer, { language: 'en', theme: 'Default' });
    var designButton = {
      key: "$openDesigner",
      text: "Edit in Designer",
      iconCssClass: 'fa__pencil',
      enabled: true,
      action: () => {
        t.cntViewer.style.display = "none";
        t.cntDesigner.style.display = "";
      },
    };
    t.viewer.toolbar.addItem(designButton);
    t.viewer.toolbar.updateLayout({
      default: [
        "$openDesigner",
        "$split",
        "$navigation",
        "$split",
        "$refresh",
        "$split",
        "$history",
        "$split",
        "$zoom",
        "$fullscreen",
        "$split",
        "$print",
        "$split",
        "$singlepagemode",
        "$continuousmode",
        "$galleymode",
      ],
    });
    const btnEditDesigner: HTMLButtonElement = t.cntViewer.querySelector('button[title="Edit in Designer"]');
    if (btnEditDesigner) {
      const icon: Icon = document.createElement("vaadin-icon");
      icon.src = UIIcons['fa__pencil'];
      icon.style.height = "16px";
      icon.style.width = "16px";
      btnEditDesigner.firstChild.appendChild(icon);
    }
  }

  async disconnectedCallback() {
    window.onbeforeunload = null;
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
    //}
    switch (obj.element) {
      case 'folderTree': {
        t.dialogFM.fm.setItemData(obj);
        break;
      }
      default: {
        //if (Build.isDev) {
        console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
        //}
        break;
      }
    }
  }

  @Method()
  async callbackRequestData(obj: any, resp: any) {
    let t = this;
    if (Build.isDev) {
      console.log('CBRD|', t.myElement.tagName.toLowerCase(), obj, resp);
    }
    switch (resp?.type) {
      case 'addFolder': {
        t.appEvents({ type: 'loadData', element: 'folderTree' });
        break;
      }
      case 'deleteItem': {
        t.appEvents({ type: 'loadData', element: 'folderTree' });
        break;
      }
      case 'getReportFile': {
        let definition: any = JSON.parse(JSON.stringify(resp.content));
        t.currentReportPath = resp.path;
        const reportId: string = definition.Name;
        t.resolveFunc({
          definition,
          id: reportId,
          displayName: reportId,
        });
        t.resolveFunc = null;
        t.dialogFM.dialog.opened = false;
        t.dialogFM.dialog.remove();
        break;
      }
      case 'renameFile': {
        t.appEvents({ type: 'loadData', element: 'folderTree' });
        break;
      }
      case 'renameFolder': {
        t.appEvents({ type: 'loadData', element: 'folderTree' });
        break;
      }
      case 'saveReport': {
        let definition: any = JSON.parse(JSON.stringify(resp.content));
        t.currentReportPath = resp.path;
        const reportId: string = definition.Name;
        t.resolveFunc({
          definition,
          id: reportId,
          displayName: reportId,
        });
        t.resolveFunc = null;
        break;
      }
      default:
        break;
    }
  }

  @Method()
  async resize(height: number) {
    let t = this;
    //console.log('resize|', t.myElement.tagName.toLowerCase(), height, t.cacheData);
    t.cntDesigner.style.height = (height - 2) + 'px';
  }

  async appEvents(obj: any) {
    let t = this;
    //console.log(t.myElement.tagName.toLowerCase(), 'appEvents', obj);
    switch (obj.type) {
      case 'button': {
        switch (obj.element) {
          case 'btnCancelCreate': {
            t.dialogFM.dialog.opened = false;
            t.dialogFM.dialog.remove();
            t.resolveFunc(null);
            t.resolveFunc = null;
            t.currentReport = null;
            break;
          }
          case 'btnCancelSelect': {
            t.dialogFM.dialog.opened = false;
            t.dialogFM.dialog.remove();
            t.resolveFunc(null);
            t.resolveFunc = null;
            t.currentReport = null;
            break;
          }
          case 'btnOkSelect': {
            const items = await t.dialogFM.fm.getSelectedItem();
            if (items.length > 0) {
              t.appEvent.emit({
                event: 'sendData'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , data: { path: items[0].relativePath }
                , route: 'getReportFile'
                , method: 'post'
              });
            }
            break;
          }
        }
        break;
      }
      case 'addFile': {
        //console.log(t.myElement.tagName.toLowerCase(), 'appEvents', obj);
        t.currentReport = obj;
        let definition: any = JSON.parse(JSON.stringify(t.CPLReport));
        t.currentReportPath = obj.selectedFolder.path;
        const reportId: string = obj.name;
        definition.Name = reportId;
        t.resolveFunc({
          definition: definition,
          id: reportId,
          displayName: reportId,
        });
        t.resolveFunc = null;
        t.dialogFM.dialog.opened = false;
        t.dialogFM.dialog.remove();

        t.appEvent.emit({
          event: 'sendData'
          , component: t.myElement.tagName.toLowerCase()
          , idx: t.args.idx
          , data: { folder: obj.selectedFolder.path, name: obj.name, definition }
          , route: obj.type
          , method: 'post'
        });
        break;
      }
      case 'addFolder': {
        t.appEvent.emit({
          event: 'sendData'
          , component: t.myElement.tagName.toLowerCase()
          , idx: t.args.idx
          , data: { folder: obj.selectedFolder.path, name: obj.name }
          , route: obj.type
          , method: 'post'
        });
        break;
      }
      case 'deleteItem': {
        t.appEvent.emit({
          event: 'sendData'
          , component: t.myElement.tagName.toLowerCase()
          , idx: t.args.idx
          , data: { folder: obj.folder.path, file: obj.file }
          , route: obj.type
          , method: 'post'
        });
        break;
      }
      case 'renameFile': {
        //console.log(t.myElement.tagName.toLowerCase(), 'appEvents', obj);
        t.appEvent.emit({
          event: 'sendData'
          , component: t.myElement.tagName.toLowerCase()
          , idx: t.args.idx
          , data: { file: obj.selectedFile.relativePath, name: obj.name }
          , route: obj.type
          , method: 'post'
        });
        break;
      }
      case 'renameFolder': {
        //console.log(t.myElement.tagName.toLowerCase(), 'appEvents', obj);
        t.appEvent.emit({
          event: 'sendData'
          , component: t.myElement.tagName.toLowerCase()
          , idx: t.args.idx
          , data: { folder: obj.selectedFolder.path, name: obj.name }
          , route: obj.type
          , method: 'post'
        });
        break;
      }
      case 'loadData': {
        switch (obj.element) {
          case 'folderTree': {
            await t.uiFns.requestData(t.baseRequestParams, { element: obj.element, elementName: obj.element, route: 'loadFolder', currentPage: 1, where: obj.where, order: [] });
            break;
          }
        }
        break;
      }
    }
  }

  async generateDialog(options: any) {
    let t = this;
    const dialog: Dialog = document.createElement('vaadin-dialog');
    dialog.id = options?.id || 'dialog';
    dialog.ariaLabel = options?.id || 'dialog';
    dialog.headerTitle = options?.headerTitle || '';
    dialog.draggable = options?.draggable || true;
    dialog.modeless = options?.modeless || false;
    dialog.resizable = options?.resizable || false;
    dialog.noCloseOnEsc = options?.noCloseOnEsc || true;
    dialog.noCloseOnOutsideClick = options?.noCloseOnOutsideClick || true;
    //dialog.opened = options?.opened || false;
    dialog.opened = true;
    t.container.append(dialog);
    const vLayout = document.createElement('vaadin-vertical-layout');
    const vLayout2 = document.createElement('vaadin-vertical-layout');

    const fm = document.createElement('ui-file-manager');
    fm.setAttribute("style", 'width: 1200px;height:400px;');
    fm.args = { clientHeight: 400 };
    fm.addEventListener('uiDataSourceEvent', async (event: CustomEvent) => {
      t.appEvents(event.detail);
    });
    vLayout2.appendChild(fm);

    vLayout2.style.alignItems = "stretch";
    vLayout.appendChild(vLayout2);
    vLayout.setAttribute("theme", "spacing");
    vLayout.style.width = "1200px";
    vLayout.style.maxWidth = "100%";
    vLayout.style.alignItems = "stretch";
    const overlay = await document.body.querySelector("vaadin-dialog-overlay[aria-label='" + dialog.ariaLabel + "']");
    (overlay as DialogOverlay).style.userSelect = "none";
    overlay.appendChild(vLayout);
    if ((options?.buttons || []).length > 0) {
      const div = document.createElement('div');
      div.setAttribute("slot", "footer");
      for (const item of options?.buttons) {
        const btn = document.createElement('vaadin-button');
        btn.setAttribute("theme", item.theme);
        btn.appendChild(document.createTextNode(item.title));
        const iconButton = document.createElement('vaadin-icon');
        iconButton.setAttribute("slot", "prefix");
        iconButton.setAttribute("src", UIIcons[item.icon]);
        btn.appendChild(iconButton);
        div.appendChild(btn);
        btn.addEventListener('click'
          , async () => {
            t.appEvents.apply(t, [{
              type: 'button', element: item.element
              , data: []
            }]);
          }
          , { passive: true }
        );
        if (item.event === 'save') {
          item.ref = btn;
        }
      }
      overlay.appendChild(div);
    }
    const footer = overlay.shadowRoot.querySelector('footer');
    footer.setAttribute("style", "display:flex !important");

    return { dialog, fm };
  }

  render() {
    let t = this;
    const clientHeight: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 2) + 'px' : '600px';
    return (<div ref={(el) => t.container = el as HTMLDivElement}>
      <div ref={(el) => t.cntDesigner = el as HTMLDivElement} style={{ width: "calc(100vw - 16px)", height: clientHeight }}></div>
      <div ref={(el) => t.cntViewer = el as HTMLDivElement} style={{ width: "calc(100vw - 16px)", height: clientHeight, display: "none" }}></div>
    </div>
    );
  }
}
