import { Component, Element, Event, EventEmitter, h, Method, Prop, Watch } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';

import '@vaadin/button';
import '@vaadin/combo-box';
import '@vaadin/grid';
import '@vaadin/icon';
import '@vaadin/integer-field';
import '@vaadin/menu-bar';
import '@vaadin/scroller';
import '@vaadin/tooltip';
import * as UIIcons from '../ui-icons';

import { Button } from '@vaadin/button';
import { ComboBox } from '@vaadin/combo-box';
import { IntegerField } from '@vaadin/integer-field';
import { debounce } from "ts-debounce";

@Component({
  tag: 'ui-pagebar',
  styleUrl: 'ui-pagebar.scss',
  shadow: true
})
export class UIPagebar {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'uiPagebarEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uiPagebarEvent: EventEmitter;

  @Prop() args: any;

  uiFns: UIFunctions = new UIFunctions();

  currentPage: number = 0;
  recordsPerPage: number = 25;
  totalPage: number = 0;
  totalRecords: number = 0;
  firstRecordOnPage: number = 0;
  lastRecordOnPage: number = 0;

  preRPP: string = '-1';

  container: HTMLDivElement;
  btnFirst: Button;
  btnPrev: Button;
  btnNext: Button;
  btnLast: Button;
  btnRefresh: Button;
  btnAdd: Button;
  btnEdit: Button;
  btnDelete: Button;
  btnFilter: Button;
  fieldCurrentPage: IntegerField;
  containerTotalPage: HTMLLabelElement;
  fieldRecordsPerPage: ComboBox;
  containerSummary: HTMLLabelElement;

  async componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    const itemsFireClick = t.container.querySelectorAll('vaadin-button');
    // console.log(itemsFireClick)
    if (itemsFireClick.length > 0) {
      await t.uiFns.asyncForEach(itemsFireClick, async (item: Button) => {
        //console.log(item.innerText);
        item.addEventListener('click', t._buttonClick.bind(t, item.part[0]), { passive: true });
      });
    }
    await t.setCurrentStatus();
  }

  @Method()
  async setButtonDisplay(btn: string, state: string) {
    let t = this;
    if (btn === 'pager') {
      if (state === 'hidden') {
        let sepCount: number = 0;
        for (let i: number = 0; i < t.container.childElementCount; i++) {
          if (sepCount < 5) {
            if ((t.container.childNodes[i] as HTMLElement).classList.contains('seperator-vertical')) {
              sepCount++;
            }
            if (!(t.container.childNodes[i] as HTMLElement).classList.contains('hidden')) {
              (t.container.childNodes[i] as HTMLElement).classList.add('hidden');
            }
          }
        }
        t.containerSummary.hidden = true;
      } else {
        let sepCount: number = 0;
        for (let i: number = 0; i < t.container.childElementCount; i++) {
          if (sepCount < 5) {
            if ((t.container.childNodes[i] as HTMLElement).classList.contains('seperator-vertical')) {
              sepCount++;
            }
            if ((t.container.childNodes[i] as HTMLElement).classList.contains('hidden')) {
              (t.container.childNodes[i] as HTMLElement).classList.remove('hidden');
            }
          }
        }
        t.containerSummary.hidden = false;
      }
    }
    if (btn === 'add') {
      if (state === 'hidden') {
        t.btnAdd.hidden = true;
      } else {
        t.btnAdd.hidden = false;
      }
    }
    if (btn === 'del') {
      if (state === 'hidden') {
        t.btnDelete.hidden = true;
      } else {
        t.btnDelete.hidden = false;
      }
    }
    if (btn === 'edit') {
      if (state === 'hidden') {
        t.btnEdit.hidden = true;
      } else {
        t.btnEdit.hidden = false;
      }
    }
    if (btn === 'filter') {
      let elSplitter: HTMLDivElement = t.btnFilter.parentElement.previousSibling.childNodes[0] as HTMLDivElement;
      if (state === 'hidden') {
        t.btnFilter.hidden = true;
        elSplitter.hidden = true;
      } else {
        t.btnFilter.hidden = false;
        elSplitter.hidden = false;
      }
    }
  }

  @Method()
  async setButtonState(btn: string, state: string) {
    let t = this;
    if (btn === 'del') {
      if (state === 'enabled') {
        t.btnDelete.disabled = false;
      } else {
        t.btnDelete.disabled = true;
      }
    }
    if (btn === 'edit') {
      if (state === 'enabled') {
        t.btnEdit.disabled = false;
      } else {
        t.btnEdit.disabled = true;
      }
    }
  }

  @Watch('args')
  async _setArgs() {
    let t = this;
    if (t.args) {
      for (const arg in t.args) {
        t[arg] = t.args[arg];
      }
    }
    t.setCurrentStatus();
  }

  private async setCurrentStatus() {
    let t = this;
    //console.log('SCS|', t.myElement.tagName.toLowerCase(), t.currentPage);
    t.btnFirst.disabled = false;
    t.btnPrev.disabled = false;
    t.fieldCurrentPage.disabled = false;
    t.btnNext.disabled = false;
    t.btnLast.disabled = false;
    t.fieldRecordsPerPage.disabled = false;
    t.btnRefresh.disabled = false;
    if (t.currentPage === 0 || t.totalRecords === 0) {
      t.btnFirst.disabled = true;
      t.btnPrev.disabled = true;
      t.fieldCurrentPage.disabled = true;
      t.btnNext.disabled = true;
      t.btnLast.disabled = true;
      t.fieldRecordsPerPage.disabled = true;
      //t.btnRefresh.disabled = true;
    }
    if (t.currentPage === 1) {
      t.btnFirst.disabled = true;
      t.btnPrev.disabled = true;
    }
    if (t.currentPage >= 1) {
      t.totalPage = Math.ceil(t.totalRecords / t.recordsPerPage);
      t.fieldCurrentPage.value = t.currentPage.toString();
      t.fieldCurrentPage.min = 1;
      t.fieldCurrentPage.max = t.totalPage;
      t.containerTotalPage.innerHTML = '/ ' + t.totalPage;
      t.fieldRecordsPerPage.value = t.recordsPerPage.toString();
      t.firstRecordOnPage = ((t.currentPage - 1) * t.recordsPerPage) + 1;
      t.lastRecordOnPage = (t.currentPage < t.totalPage) ? (t.currentPage * t.recordsPerPage) : t.totalRecords;
      t.containerSummary.innerHTML = t.firstRecordOnPage + ' - ' + t.lastRecordOnPage + ' / ' + t.totalRecords;
    }
    if (t.currentPage === t.totalPage) {
      t.btnNext.disabled = true;
      t.btnLast.disabled = true;
    }
  }

  private async _buttonClick(button: string) {
    let t = this;
    t.uiPagebarEvent.emit({ type: 'button', element: button, data: null, args: t.args });
  }

  private async _currentPageChange(evt: CustomEvent) {
    let t = this;
    if ((evt.target as IntegerField).hasAttribute('focused')
      && evt.detail.value !== ''
      && evt.detail.value !== '0'
      && evt.detail.value >= t.fieldCurrentPage.min
      && evt.detail.value <= t.fieldCurrentPage.max) {
      const debounced = debounce(
        (value) => {
          //console.log(t.fieldCurrentPage.min, t.fieldCurrentPage.max);
          t.uiPagebarEvent.emit({ type: 'change-currentPage', element: null, data: value, args: t.args });
        }
        , 200,
        // The maximum time func is allowed to be delayed before it's invoked:
        { maxWait: 400 }
      );
      debounced(evt.detail.value);
    }
  }

  private async _rppChange(evt: any) {
    let t = this;
    if (evt.value !== '' && evt.value !== '-1' && t.preRPP !== '-1') {
      t.uiPagebarEvent.emit({ type: 'change-recordsPerPage', element: null, data: evt.value, args: t.args });
    }
    t.preRPP = evt.value;
  }

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('Render|', t.myElement.tagName.toLowerCase());
    //}<div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
    return (
      <vaadin-scroller scroll-direction="horizontal">
        <div class="flex flex--justify" style={{ height: "50px", width: "100%" }}>
          <div class="flex flex--row flex--center flex--stretch" ref={(el) => t.container = el as HTMLDivElement} style={{ height: "100%" }}>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-button theme="icon tertiary large" part="first"
                ref={(el: Button) => t.btnFirst = el as Button}>
                <vaadin-icon src={UIIcons['fa__angles_left']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="İlk Sayfa"></vaadin-tooltip>
              </vaadin-button>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-button theme="icon tertiary large" part="prev"
                ref={(el: Button) => t.btnPrev = el as Button}>
                <vaadin-icon src={UIIcons['fa__angle_left']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Önceki Sayfa"></vaadin-tooltip>
              </vaadin-button>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} ><div class="seperator-vertical"></div></div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} ><label slot="label" class="pager-paper">Sayfa</label></div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-integer-field
                ref={(el: IntegerField) => t.fieldCurrentPage = el as IntegerField}
                style={{ width: "40px" }}
                on-value-changed={(e: CustomEvent) => { t._currentPageChange(e); }}
                theme="align-right small"
                min="0"
                max="0"
              ></vaadin-integer-field>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <label slot="label" class="pager-paper"
                ref={(el) => t.containerTotalPage = el as HTMLLabelElement}>/ 0</label>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} ><div class="seperator-vertical"></div></div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-button theme="icon tertiary large" part="next"
                ref={(el: Button) => t.btnNext = el as Button}>
                <vaadin-icon src={UIIcons['fa__angle_right']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Sonraki Sayfa"></vaadin-tooltip>
              </vaadin-button>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-button theme="icon tertiary large" part="last"
                ref={(el: Button) => t.btnLast = el as Button}>
                <vaadin-icon src={UIIcons['fa__angles_right']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Son Sayfa"></vaadin-tooltip>
              </vaadin-button>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} ><div class="seperator-vertical"></div></div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-combo-box
                ref={(el: ComboBox) => t.fieldRecordsPerPage = el as ComboBox}
                style={{ width: "85px" }}
                on-value-changed={(e) => { t._rppChange(e.detail); }}
                theme="align-right small"
                value="-1"
                items={['10', '25', '50', '100']}
              ></vaadin-combo-box>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} ><div class="seperator-vertical"></div></div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-button theme="icon tertiary large" part="refresh"
                ref={(el: Button) => t.btnRefresh = el as Button}>
                <vaadin-icon src={UIIcons['fa__rotate']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Yenile"></vaadin-tooltip>
              </vaadin-button>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} ><div class="seperator-vertical"></div></div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-button theme="icon tertiary large" part="add"
                ref={(el: Button) => t.btnAdd = el as Button}>
                <vaadin-icon src={UIIcons['fa__file_circle_plus']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Ekle"></vaadin-tooltip>
              </vaadin-button>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-button theme="icon tertiary large" part="edit" disabled
                ref={(el: Button) => t.btnEdit = el as Button}>
                <vaadin-icon src={UIIcons['fa__file_pen']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Düzenle"></vaadin-tooltip>
              </vaadin-button>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-button theme="icon tertiary large" part="delete" disabled
                ref={(el: Button) => t.btnDelete = el as Button}>
                <vaadin-icon src={UIIcons['fa__trash']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Sil"></vaadin-tooltip>
              </vaadin-button>
            </div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} ><div class="seperator-vertical"></div></div>
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
              <vaadin-button theme="icon tertiary large" part="filter"
                ref={(el: Button) => t.btnFilter = el as Button}>
                <vaadin-icon src={UIIcons['fa__filter']} slot="prefix"></vaadin-icon>
                <vaadin-tooltip slot="tooltip" text="Filtrele"></vaadin-tooltip>
              </vaadin-button>
            </div>
          </div>
          <div class="flex flex--col flex--center flex--middle" style={{ height: "100%" }} >
            <div class="flex flex--row flex--center flex--stretch" style={{}}>
              <label slot="label" class="pager-paper-centered"
                ref={(el) => t.containerSummary = el as HTMLLabelElement}>0 - 0 / 0</label>
            </div>
          </div>
        </div >
      </vaadin-scroller>
    );
  }
}
