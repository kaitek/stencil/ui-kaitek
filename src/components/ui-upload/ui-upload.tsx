import { Component, Element, Event, EventEmitter, Prop, Watch, h } from '@stencil/core';
import '@vaadin/icon';
import '@vaadin/upload';
import * as UIIcons from '../ui-icons';
import type {
  Upload, UploadAbortEvent, UploadBeforeEvent, UploadErrorEvent, UploadFileRejectEvent
  , UploadFilesChangedEvent, UploadMaxFilesReachedChangedEvent
  , UploadProgressEvent, UploadRequestEvent, UploadResponseEvent
  , UploadRetryEvent, UploadStartEvent, UploadSuccessEvent
} from '@vaadin/upload';

@Component({
  tag: 'ui-upload',
  styleUrl: 'ui-upload.scss',
  shadow: true
})
export class UIUpload {
  @Element() myElement: HTMLElement;

  @Prop({ attribute: 'dataIndex', reflect: true }) dataIndex: string = "";
  @Prop({ attribute: 'value', reflect: true }) value: any = null;
  @Prop({ attribute: 'nodrop', reflect: true }) nodrop: boolean = false;
  @Prop({ attribute: 'maxFilesReached', reflect: true }) maxFilesReached: boolean = false;
  @Prop() method: string = "PUT";
  @Prop() target: string = "upload-handler";
  @Prop() viewTarget: string = "uploads/";
  @Prop() headers: object | string = {};
  @Prop() timeout: number = 0;
  @Prop() maxFiles: number = 1;
  @Prop() accept: string = "application/pdf,.pdf";
  @Prop() maxFileSize: number = Infinity;
  @Prop() formDataName: string = "file";
  @Prop() noAuto: boolean = true;
  @Prop() withCredentials: boolean = false;
  @Prop() thumbnail: any = null;
  @Prop() i18n: object = {
    dropFiles: {
      one: 'Dosyayı buraya bırakabilirsiniz',
      many: 'Dosyaları buraya bırakabilirsiniz',
    },
    addFiles: {
      one: 'Dosya Yükle...',
      many: 'Dosyaları Yükle...',
    },
    error: {
      tooManyFiles: 'Fazla sayıda dosya.',
      fileIsTooBig: 'Dosya boyutu yüksek.',
      incorrectFileType: 'Yüklediğiniz dosya geçerli bir fotmatta değil.',
    },
    uploading: {
      status: {
        connecting: 'Bağlanıyor...',
        stalled: 'Durdu',
        processing: 'Dosya işleniyor...',
        held: 'Sıraya alındı',
      },
      remainingTime: {
        prefix: 'kalan süre: ',
        unknown: 'kalan süre bilinmiyor',
      },
      error: {
        serverUnavailable: 'Yükleme başarısız oldu, lütfen daha sonra tekrar deneyiniz',
        unexpectedServerError: 'Sunucu hatası nedeniyle yükleme başarısız oldu',
        forbidden: 'Sunucu dosya yüklemeye izin vermiyor',
      },
    },
    file: {
      retry: 'Yeniden dene',
      start: 'Başla',
      remove: 'Kaldır',
    },
    units: {
      size: ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
    },
  };

  @Event({
    eventName: 'fileReject',
    composed: true,
    bubbles: true,
  }) fileReject: EventEmitter;

  @Event({
    eventName: 'filesChanged',
    composed: true,
    bubbles: true,
  }) filesChanged: EventEmitter;

  @Event({
    eventName: 'maxFilesReachedChanged',
    composed: true,
    bubbles: true,
  }) maxFilesReachedChanged: EventEmitter;

  @Event({
    eventName: 'uploadBefore',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uploadBefore: EventEmitter;

  @Event({
    eventName: 'uploadStart',
    composed: true,
    bubbles: true,
  }) uploadStart: EventEmitter;

  @Event({
    eventName: 'uploadProgress',
    composed: true,
    bubbles: true,
  }) uploadProgress: EventEmitter;

  @Event({
    eventName: 'uploadResponse',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uploadResponse: EventEmitter;

  @Event({
    eventName: 'uploadSuccess',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uploadSuccess: EventEmitter;

  @Event({
    eventName: 'uploadError',
    composed: true,
    //cancelable: true,
    bubbles: true,
  }) uploadError: EventEmitter;

  @Event({
    eventName: 'uploadRequest',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uploadRequest: EventEmitter;

  @Event({
    eventName: 'uploadRetry',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uploadRetry: EventEmitter;

  @Event({
    eventName: 'uploadAbort',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) uploadAbort: EventEmitter;

  @Event({
    eventName: 'fileDelete',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) fileDelete: EventEmitter;

  @Event({
    eventName: 'fileOpen',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) fileOpen: EventEmitter;

  @Event({
    eventName: 'fileView',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) fileView: EventEmitter;

  uploadFile: Upload;
  viewFile: HTMLDivElement;
  imgViewer: HTMLImageElement;

  componentDidLoad() {
    //let t = this;
    //t.uploadFile.nodeValue = 'vscode-keyboard-shortcuts-windows.pdf';
  }

  @Watch('value')
  setValue(value: any) {
    let t = this;
    //console.log('setValue', value);
    if (value === '') {
      if (t.uploadFile.files.length > 0) {
        t.uploadFile.files = [];
      }
      if (t.uploadFile.classList.contains('hidden')) {
        t.uploadFile.classList.remove('hidden');
      }
      if (!t.viewFile.classList.contains('hidden')) {
        t.viewFile.classList.add('hidden');
      }
    } else {
      if (!t.uploadFile.classList.contains('hidden')) {
        t.uploadFile.classList.add('hidden');
      }
      if (t.viewFile.classList.contains('hidden')) {
        t.viewFile.classList.remove('hidden');
      }
      t.fileView.emit({ file: t.value, target: t.viewTarget });
    }
  }

  @Watch('thumbnail')
  setViewer(value: any) {
    let t = this;
    t.imgViewer.src = "data:image/png;base64," + value;
  }

  render() {
    let t = this;
    return (
      <div>
        <label class="label" htmlFor="uploadFile">Avatar</label>
        <div class="flex flex--col" style={{ height: "100%", width: "100%" }}>
          <vaadin-upload
            class=""
            ref={(el) => t.uploadFile = el as Upload}
            id="uploadFile"
            nodrop={t.nodrop}
            target={t.target}
            method={t.method}
            headers={t.headers}
            timeout={t.timeout}
            max-files={t.maxFiles}
            max-files-reached={t.maxFilesReached}
            accept={t.accept}
            max-file-size={t.maxFileSize}
            form-data-name={t.formDataName}
            no-auto={t.noAuto}
            with-credentials={t.withCredentials}
            i18n={t.i18n}
            on-file-reject={async (event: UploadFileRejectEvent) => {
              t.fileReject.emit(event.detail);
              //console.log('file-reject', event.detail);
            }}
            on-files-changed={async (event: UploadFilesChangedEvent) => {
              t.filesChanged.emit(event.detail);
              //console.log('files-changed', event.detail);
            }}
            on-max-files-reached-changed={async (event: UploadMaxFilesReachedChangedEvent) => {
              t.maxFilesReachedChanged.emit(event.detail);
              //console.log('max-files-reached-changed', event.detail.value);
            }}
            on-upload-before={async (event: UploadBeforeEvent) => {
              t.uploadBefore.emit(event.detail);
              //console.log('upload-before', event.detail);
            }}
            on-upload-start={async (event: UploadStartEvent) => {
              t.uploadStart.emit(event.detail);
              //console.log('upload-start', event.detail);
            }}
            on-upload-progress={async (event: UploadProgressEvent) => {
              t.uploadProgress.emit(event.detail);
              //console.log('upload-progress', event.detail);
            }}
            on-upload-success={async (event: UploadSuccessEvent) => {
              t.uploadSuccess.emit(event.detail);
              //console.log('upload-success', event.detail);
            }}
            on-upload-error={async (event: UploadErrorEvent) => {
              t.uploadError.emit(event.detail);
              //console.log('upload-error', event.detail);
            }}
            on-upload-request={async (event: UploadRequestEvent) => {
              t.uploadRequest.emit(event.detail);
              //console.log('upload-request', event.detail);
            }}
            on-upload-response={async (event: UploadResponseEvent) => {
              t.uploadResponse.emit(event.detail);
              //console.log('upload-response', event.detail);
            }}
            on-upload-retry={async (event: UploadRetryEvent) => {
              t.uploadRetry.emit(event.detail);
              //console.log('upload-retry', event.detail);
            }}
            on-upload-abort={async (event: UploadAbortEvent) => {
              t.uploadAbort.emit(event.detail);
              //console.log('upload-abort', event.detail, event);
            }}
          ></vaadin-upload>
          <div class="viewer" ref={(el) => t.viewFile = el as HTMLDivElement}>
            <div class="flex flex--row" style={{ height: "100%", width: "100%" }}>
              <div style={{ height: "100%", width: "60px" }}>
                <div class="flex flex--col" style={{ height: "100%", width: "100%" }}>
                  <vaadin-button theme="icon" aria-label="file_open"
                    on-click={() => {
                      t.fileOpen.emit({ file: t.value, target: t.viewTarget });
                    }}
                  >
                    <vaadin-icon src={UIIcons['fa__magnifying_glass']} slot="prefix"></vaadin-icon>
                    <vaadin-tooltip slot="tooltip" text="Aç"></vaadin-tooltip>
                  </vaadin-button>
                  <vaadin-button theme="icon" aria-label="file_delete"
                    on-click={() => {
                      t.fileDelete.emit({ file: t.value, target: t.viewTarget });
                    }}
                  >
                    <vaadin-icon src={UIIcons['fa__square_minus']} slot="prefix"></vaadin-icon>
                    <vaadin-tooltip slot="tooltip" text="Sil"></vaadin-tooltip>
                  </vaadin-button>
                </div>
              </div>
              <div class="flex flex--row flex--center flex--stretch" style={{ height: "100%", width: "100%" }}>
                <img ref={(el) => t.imgViewer = el as HTMLImageElement}></img>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}
