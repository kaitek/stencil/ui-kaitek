import { Build, Component, Element, Event, EventEmitter, Method, Prop, h } from '@stencil/core';

import { UIFunctions } from '../ui-functions/ui-functions';
import * as UIIcons from '../ui-icons';
import { UITree } from '../ui-tree/ui-tree';

import { Button } from '@vaadin/button';

import '@vaadin/button';
import '@vaadin/horizontal-layout';
import '@vaadin/icon';
import '@vaadin/vertical-layout';

@Component({
  tag: 'app-permission',
  styleUrl: 'app-permission.scss',
  shadow: true
})
export class AppPermission {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Prop() args: any = { idx: 1, clientHeight: 600 };
  //isDev: boolean = false;

  uiFns: UIFunctions = new UIFunctions();

  contentContainer: HTMLDivElement;
  tabForm: HTMLDivElement;
  userPermissionTree: UITree;
  menuTree: UITree;
  btnClose: Button;

  menuTreeData: any[] = [];
  permissionTreeData: any[] = [];
  baseRequestParams: any;

  async componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    t.baseRequestParams = {
      emitter: t.appEvent, controller: '', tagName: t.myElement.tagName.toLowerCase(), idx: t.args.idx, recordsPerPage: 9999
    };
    //t.appEvent.emit({ event: 'componentDidLoad', component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
    //const els = document.getElementsByTagName('html');
    //t.isDev = typeof els[0].attributes["isdev"] !== 'undefined';
    await t.uiFns.requestData(t.baseRequestParams, { element: t.menuTree, elementName: 'menuTree', route: 'filtered-records', currentPage: 1, where: [], order: [] });
    //if (Build.isDev) {
    //  await t.uiFns.delay(500);
    //  await t.dataGrid.afterLoad();
    //}
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    if (Build.isDev) {
      console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
    }
    switch (obj.element) {
      case 'menuTree': {
        t.menuTreeData = obj.records;
        await t.menuTree.load(t.menuTreeData);
        break;
      }
      case 'userPermissionTree': {
        t.permissionTreeData = obj.records;
        await t.userPermissionTree.load(t.permissionTreeData);
        break;
      }
      default: {
        if (Build.isDev) {
          console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
        }
        break;
      }
    }
  }

  @Method()
  async resize(height: number) {
    let t = this;
    //console.log('resize|', t.myElement.tagName.toLowerCase(), height);
    t.tabForm.style.height = (height - 2) + 'px';
  }

  async appEvents(obj: any) {
    let t = this;
    if (Build.isDev) {
      console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
    }
    switch (obj.type) {
      case 'button': {
        switch (obj.element) {
          case 'close': {
            t.rendererEvent.emit({
              event: 'close'
              , component: t.myElement.tagName.toLowerCase()
              , idx: t.args.idx
            });
            break;
          }
          default: {
            console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            break;
          }
        }
        break;
      }
      default: {
        console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
        break;
      }
    }
  }

  render() {
    let t = this;
    const clientHeight: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 2) + 'px' : '600px';
    let formHeight: string = 'calc(100% - 0px)';
    let styleHeader: any = { height: "50px", width: 'calc(100% - 2px)', border: "1px solid var(--lumo-contrast-20pct)" };
    formHeight = 'calc(100% - 53px)';
    return (
      <div ref={(el) => t.contentContainer = el as HTMLDivElement}>
        <div ref={(el) => t.tabForm = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <div //ref={(el) => t.elContainerHeader = el as HTMLDivElement}
            style={styleHeader}>
            <vaadin-horizontal-layout
              class="flex--middle flex--wrap flex--right"
              theme="spacing"
              style={{ height: "100%", paddingLeft: "10px", paddingRight: "10px" }}
            >
              <vaadin-button theme="primary contrast" ref={(el) => t.btnClose = el as Button}
                on-click={() => {
                  t.btnClose.disabled = true;
                  t.appEvents({ type: 'button', element: 'close' });
                }}>
                <vaadin-icon src={UIIcons['fa__square_xmark']} slot="prefix"></vaadin-icon>
                Kapat
              </vaadin-button>
            </vaadin-horizontal-layout>
          </div>
          <div style={{ height: formHeight, width: "calc(100% - 2px)", border: "1px solid var(--lumo-contrast-20pct)", borderTop: "none" }} >
            <div class="flex flex--row" style={{ height: "100%", width: "100%" }}>
              <div style={{ height: "100%", width: "40%", paddingLeft: "0px" }}>
                <ui-tree
                  ref={(el) => t.menuTree = el as unknown as UITree}
                  height="100%" width="100%"
                  isCheckableNode={true}
                  on-uiTreeNodeSelectEvent={async (e: CustomEvent) => {
                    await t.uiFns.requestData(t.baseRequestParams, {
                      element: t.userPermissionTree, elementName: 'userPermissionTree', route: 'getPermissionData', currentPage: 1, where: [{
                        condition: "=",
                        fields: ["menu_id"],
                        keyword: e.detail.menu_id
                      }], order: []
                    });
                  }}
                ></ui-tree>
              </div>
              <div style={{ height: "100%", width: "60%", paddingLeft: "10px", overflowY: "auto", borderLeft: "1px solid var(--lumo-contrast-20pct)" }}>
                <ui-tree
                  ref={(el) => t.userPermissionTree = el as unknown as UITree}
                  height="100%" width="100%"
                  columns={[
                    { header: "Kullanıcı", dataIndex: 'email', width: 500 },
                    { header: "Gör", dataIndex: 'p_menu', width: 100, align: 'center' },
                    { header: "Ekle", dataIndex: 'p_add', width: 100, align: 'center' },
                    { header: "Düzenle", dataIndex: 'p_edit', width: 100, align: 'center' },
                    { header: "Sil", dataIndex: 'p_del', width: 100, align: 'center' },
                    //{ header: "Özel Hak", dataIndex: 'custom_right', width: 100, align: 'center', triggerClick: true }
                  ]}
                ></ui-tree>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
