//import { IListItem } from "./ui-gridDataSource-interface";

export interface IGridDataSource {
  currentPage: number,
  recordsPerPage: number,
  totalRecords: number,
  records: any[]
}

export class GridDataSource {
  currentPage: number;
  recordsPerPage: number;
  totalRecords: number;
  records: any[];
  constructor(obj: IGridDataSource) {
    this.currentPage = obj.currentPage;
    this.recordsPerPage = obj.recordsPerPage;
    this.totalRecords = obj.totalRecords;
    this.records = obj.records;
  }
}
