import { Build, Component, Element, Event, EventEmitter, h, Method, Prop } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';
import { IQueryBuilderOptions, QueryBuilderOptions } from '../px-interfaces';
import { UIQueryBuilder } from '../ui-query-builder/ui-query-builder';
import { UILoadingIndicator } from '../ui-loading-indicator/ui-loading-indicator';
import { /*Core,*/ ReportViewer } from "@mescius/activereportsjs";
import * as UIIcons from '../ui-icons';
import { Icon } from '@vaadin/icon';
import '@vaadin/icon';

import { Viewer } from "@mescius/activereportsjs/reportviewer";
@Component({
  tag: 'app-report-viewer',
  styleUrls: ['../../../node_modules/@mescius/activereportsjs/styles/ar-js-ui.css'
    , '../../../node_modules/@mescius/activereportsjs/styles/ar-js-viewer.css'
    , 'app-report-viewer.scss'],
  shadow: false
})
export class AppReportViewer {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Prop() args: any = {};

  uiFns: UIFunctions = new UIFunctions();

  container: HTMLDivElement;
  cntQueryBuilder: HTMLDivElement;
  cntViewer: HTMLDivElement;

  elLoadingIndicator: UILoadingIndicator;

  qb: UIQueryBuilder;
  options: IQueryBuilderOptions;
  viewer: Viewer;

  baseRequestParams: any;
  script: any;

  async componentWillLoad() {
    let t = this;
    if (t.args.script !== '') {
      t.script = JSON.parse(t.args.script);
    }
    //console.log(t.myElement.tagName.toLowerCase(), 'componentWillLoad:', t.args, t.script);
    t.options = t.args?.options || new QueryBuilderOptions({
      filters: [

      ]
      , rules: [

      ]
    });
  }

  async componentDidLoad() {
    let t = this;
    if (Build.isDev) {
      console.log('CDL|', t.myElement.tagName.toLowerCase());
    }
    //Core.setLicenseKey("856515978924416#B1KZOQop6SlNmc9YDW8gGdMdTWCFnQvBlSQp5TwJkcwsyVS56NMpGTwYHVBNTd6N5NxFmZrUEewJUMz2kQOREOxMlMENlb4kmV5MjTRd4dF3mdyEzUVdjcMllSrUXMGRGWtRTdzU6dTRXV8MWN7hlajREUzcXYSd4Q8EkcwNVeURmZPlEMZZEav4WQzYUO0FkQzNGWpdVNnxUYysCaSJDOKNTerdHS95WN8lnbON6NLplZoJTaMpVcZpHSwcjcuVGZzw6aGhTSohFWy8UVrhVbqlVVWtEdXJXcWFkWFx4T8dEd4lza09UT6kVexlWU6tWV63mYO56dSBnU8tiQZl7LsJlNz5Gd9hmVQRFSyUFZC36dwMkI0IyUiwiIwEkQygTNFFjI0ICSiwiNzcTOzMTO6gTM0IicfJye#4Xfd5nIWx4SOJiOiMkIsISNWByUKRncvBXZSVmdpR7YBJiOi8kI1tlOiQmcQJCLiMDN4ETOwACMzQDM4IDMyIiOiQncDJCLiMXduMXdpN6cl5mLqwCcvRnLzVXajNXZt9iKs2WauMXdpN6cl5mLqwSbvNmLzVXajNXZt9iKsI7au26YuMXdpN6cl5mLqwCcq9ybj9yc5l6YzVWbuoiI0IyctRkIsIycrJXYQBCbl3mSiojIh94QiwiI6EDN4ITO8cTO5ETN6UDOiojIklkIs4XXbpjInxmZiwSZzxWYmpjIyNHZisnOiwmbBJye0ICRiwiI34TUVpUN9RkeMJWUsF4dBZDUBZHSyNDZ5wkTR9WWQJmYwxkNIJ4RNlnNpdTZIFWbkNWVLBjWypXOnV7SOVlNQNWSuRuN");
    t.baseRequestParams = {
      emitter: t.appEvent, controller: '', tagName: t.myElement.tagName.toLowerCase(), idx: t.args.idx, recordsPerPage: 9999
    };
    await t.uiFns.requestData(t.baseRequestParams, { element: t.cntQueryBuilder, elementName: 'queryBuilder', route: 'query-builder', currentPage: 1, where: [], order: [], report: (t.script?.report || '') });
  }

  @Method()
  async resize(height: number) {
    let t = this;
    //console.log('resize|', t.myElement.tagName.toLowerCase(), height);
    t.container.style.height = (height - 2) + 'px';
    t.cntQueryBuilder.style.height = (height - 2) + 'px';
    t.cntViewer.style.height = (height - 2) + 'px';
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
    //}
    switch (obj.element) {
      case 'queryBuilder': {
        t.options = t.args?.options || new QueryBuilderOptions({
          filters: JSON.parse(obj.filters)
          , rules: JSON.parse(obj.rules)
        });
        const argument = {
          options: t.options
          , statement: (t.script?.statement || 'named')
        };
        const qb = document.createElement('ui-query-builder');
        //qb.setAttribute("style", 'width: 1200px;height:400px;');
        qb.args = argument;
        qb.addEventListener('pxQueryBuilderConditionChange', async (event: CustomEvent) => {
          t.appEvents({ type: 'condition_change', data: event.detail });
        });
        t.cntQueryBuilder.appendChild(qb);
        t.qb = qb as unknown as UIQueryBuilder;
        break;
      }
      case 'viewer': {
        const exportsSettings: any = {
          pdf: {
            title: obj.title,
            author: "Kaitek",
            subject: "Web Reporting",
            //keywords: "reporting, sample",
            //userPassword: "pwd",
            //ownerPassword: "ownerPwd",
            //printing: "none",
            copying: false,
            modifying: false,
            annotating: false,
            contentAccessibility: false,
            documentAssembly: false,
            pdfVersion: "1.7",
            //autoPrint: true,
            filename: obj.filename + ".pdf",
          },
          html: {
            title: obj.title,
            filename: obj.filename + ".html",
            autoPrint: true,
            multiPage: true,
            embedImages: "external",
            outputType: "html",
          },
          "tabular-data": {
            title: obj.title,
            filename: obj.filename + ".csv",
          },
          xlsx: {
            title: obj.title,
            creator: "Kaitek",
            filename: obj.filename + ".xlsx",
            outputType: "xlsx",
          }
        };
        t.viewer = new ReportViewer.Viewer(t.cntViewer, { language: 'en', theme: 'System', ExportsSettings: exportsSettings });
        //t.viewer.theme = "System";
        t.viewer.availableExports = ["pdf", "xlsx", "tabular-data"];
        const filterButton = {
          key: "$filter",
          iconCssClass: "fa__filter",
          text: "Filtrele",
          enabled: true,
          action: function () {
            t.qb.setButtonStatus('filter_records', false);
            t.cntQueryBuilder.style.display = "block";
            t.cntViewer.style.display = "none";
            t.viewer.dispose();
            t.viewer = null;
          },
        };
        t.viewer.toolbar.addItem(filterButton);
        //const x = await t.viewer.toolbar.getDefaultToolbarItems();
        //console.log(x)
        t.viewer.toolbar.updateLayout({
          //default: ["$filter", "$split", "$navigation", "$split", "$refresh", "$split", "$history", "$split", "$mousemode", "$zoom", "$fullscreen", "$split", "$print", "$split", "$singlepagemode", "$continuousmode", "$galleymode"],
          default: ["$filter", "$split", "$navigation", "$split", "$refresh", "$split", "$mousemode", "$zoom", "$split", "$print", "$split", "$singlepagemode", "$continuousmode", "$galleymode"],
          fullscreen: ["$navigation", "$split", "$fullscreen"],
          mobile: ["$filter", "$split", "$navigation", "$split"],
        });
        const btnFilter: HTMLButtonElement = t.cntViewer.querySelector('button[title="Filtrele"]');
        if (btnFilter) {
          const icon: Icon = document.createElement("vaadin-icon");
          icon.src = UIIcons['fa__filter'];
          icon.style.height = "16px";
          icon.style.width = "16px";
          btnFilter.firstChild.appendChild(icon);
        }
        //const btnFullscreen: HTMLButtonElement = t.cntViewer.querySelector('button[title="Toggle Fullscreen"]');
        //if (btnFullscreen) {
        //  btnFullscreen.addEventListener('click'
        //    , async () => {
        //      const btnExitFullscreen: HTMLButtonElement = t.cntViewer.querySelector('button[title="Toggle Fullscreen"]');
        //      console.log('btnFullscreen', t.viewer, btnExitFullscreen)
        //      if (btnExitFullscreen) {
        //        btnExitFullscreen.addEventListener('click'
        //          , async () => {
        //            console.log('btnExitFullscreen', t.viewer)
        //          }
        //          //, { passive: true }
        //        );
        //      }
        //    }
        //    //, { passive: true }
        //  );
        //}
        //const report = new Core.PageReport({ language: 'en' });
        //await report.load("/reports/CustomersTable.rdlx-json");
        const report = await JSON.parse(obj.report);
        report.DataSources[0].ConnectionProperties.ConnectString = "jsondata=" + JSON.stringify(obj.records);
        t.viewer.open(report);
        t.cntQueryBuilder.style.display = "none";
        t.cntViewer.style.display = "block";
        t.elLoadingIndicator.setVisible(false);
        break;
      }
    }
  }

  async appEvents(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //  console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
    //}
    switch (obj.type) {
      case 'condition_change': {
        //console.log(t.myElement.tagName.toLowerCase(), 'condition_change', obj);
        t.elLoadingIndicator.setVisible(true);
        await t.uiFns.requestData(t.baseRequestParams, { element: t.cntQueryBuilder, elementName: 'viewer', route: 'report-data', where: obj.data, currentPage: 1, order: [], report: (t.script?.report || '') });
        break;
      }
      default: {
        console.log(t.myElement.tagName.toLowerCase(), 'appEvents', obj);
        break;
      }
    }
  }

  render() {
    let t = this;
    const clientHeight: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 2) + 'px' : '600px';
    return (<div ref={(el) => t.container = el as HTMLDivElement}>
      <ui-loading-indicator
        ref={(el) => t.elLoadingIndicator = el as unknown as UILoadingIndicator}
        text='Lüften bekleyiniz...'
      >
      </ui-loading-indicator>
      <div ref={(el) => t.cntQueryBuilder = el as HTMLDivElement} style={{ width: "calc(100vw - 16px)", height: clientHeight }}>

      </div>
      <div ref={(el) => t.cntViewer = el as HTMLDivElement} style={{ width: "calc(100vw - 16px)", height: clientHeight, display: "none" }}>

      </div>
    </div>
    );
  }
}
