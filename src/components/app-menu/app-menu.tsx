import { Build, Component, Element, Event, EventEmitter, Method, Prop, h } from '@stencil/core';

import { DataTableColumn } from '../px-interfaces';
import { UIFunctions } from '../ui-functions/ui-functions';
import { UIGrid } from '../ui-grid/ui-grid';
import * as UIIcons from '../ui-icons';
import { UITree } from '../ui-tree/ui-tree';

import { Button } from '@vaadin/button';
import { Icon } from '@vaadin/icon';
import { TextField } from '@vaadin/text-field';

import '@vaadin/button';
import '@vaadin/form-layout';
import '@vaadin/horizontal-layout';
import '@vaadin/icon';
import '@vaadin/integer-field';
import '@vaadin/text-area';
import '@vaadin/text-field';
import '@vaadin/vertical-layout';

import type { FormLayout, FormLayoutResponsiveStep } from '@vaadin/form-layout';

@Component({
  tag: 'app-menu',
  styleUrl: 'app-menu.scss',
  shadow: true
})
export class AppMenu {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Prop() args: any = { idx: 1, clientHeight: 700 };
  //isDev: boolean = false;

  uiFns: UIFunctions = new UIFunctions();

  contentContainer: HTMLDivElement;
  tabForm: HTMLDivElement;
  formLayout: FormLayout;
  formLayoutCustomRight: FormLayout;
  iconItem: Icon;
  menuTree: UITree;
  customRights: UIGrid;
  btnAdd: Button;
  btnClose: Button;
  btnDelete: Button;
  btnSave: Button;
  btnAddCustomRight: Button;
  btnDeleteCustomRight: Button;
  btnSaveCustomRight: Button;

  fElCrFieldName: TextField;
  fElCrFieldLabel: TextField;
  fElCrFieldType: TextField;
  fElCrFieldValue: TextField;

  menuTreeData: any[] = [];
  customRightData: any[] = [];
  selectedTreeNode: any = {};
  selectedCustomRight: any = {};

  isInvalid: boolean = false;
  baseRequestParams: any;

  async componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    t.baseRequestParams = {
      emitter: t.appEvent, controller: '', tagName: t.myElement.tagName.toLowerCase(), idx: t.args.idx, recordsPerPage: 9999
    };
    //t.appEvent.emit({ event: 'componentDidLoad', component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
    //const els = document.getElementsByTagName('html');
    //t.isDev = typeof els[0].attributes["isdev"] !== 'undefined';
    await t.uiFns.requestData(t.baseRequestParams, { element: t.menuTree, elementName: 'menuTree', route: 'filtered-records', currentPage: 1, where: [], order: [] });
    //if (Build.isDev || t.isDev) {
    //  await t.uiFns.delay(500);
    //  await t.dataGrid.afterLoad();
    //}
    if (Build.isDev) {
      t.menuTreeData = [
        {
          "id": 3,
          "created_user": "Admin",
          "created_at": "2024-01-11T14:00:08.067Z",
          "updated_user": "Admin",
          "updated_at": "2024-01-11T14:00:08.067Z",
          "delete_user": null,
          "deleted_at": null,
          "alignment": 1,
          "parent_id": "",
          "menu_id": "parameters",
          "title": "Messages",
          "icon": "vaadin__envelope",
          "part": "",
          "class": "",
          "script": "",
          "custom_right": [],
          "version": 0,
          "text": "Messages",
          "data": [
            {
              "id": 4,
              "created_user": "Admin",
              "created_at": "2024-01-11T14:00:08.067Z",
              "updated_user": "Admin",
              "updated_at": "2024-01-11T14:00:08.067Z",
              "delete_user": null,
              "deleted_at": null,
              "alignment": 1,
              "parent_id": "parameters",
              "menu_id": "parameters|inbox",
              "title": "Inbox",
              "icon": "vaadin__inbox",
              "part": "app-inbox",
              "class": "item-fire-click",
              "script": "",
              "custom_right": [],
              "version": 0,
              "text": "Inbox"
            },
            {
              "id": 5,
              "created_user": "Admin",
              "created_at": "2024-01-11T14:00:08.067Z",
              "updated_user": "Admin",
              "updated_at": "2024-01-23T11:40:22.904Z",
              "delete_user": null,
              "deleted_at": null,
              "alignment": 2,
              "parent_id": "parameters",
              "menu_id": "parameters|sent",
              "title": "Sent",
              "icon": "vaadin__paperplane",
              "part": "app-sent",
              "class": "item-fire-click",
              "script": "",
              "custom_right": [],
              "version": 0,
              "text": "Sent"
            },
            {
              "id": 6,
              "created_user": "Admin",
              "created_at": "2024-01-11T14:00:08.067Z",
              "updated_user": "Admin",
              "updated_at": "2024-01-23T11:45:34.618Z",
              "delete_user": null,
              "deleted_at": null,
              "alignment": 3,
              "parent_id": "parameters",
              "menu_id": "parameters|trash",
              "title": "Trash",
              "icon": "vaadin__trash",
              "part": "app-trash",
              "class": "item-fire-click",
              "script": "",
              "custom_right": [],
              "version": 0,
              "text": "Trash"
            },
            {
              "id": 68,
              "created_user": "Admin",
              "created_at": "2024-01-18T05:55:43.348Z",
              "updated_user": "Admin",
              "updated_at": "2024-01-23T09:59:17.463Z",
              "delete_user": null,
              "deleted_at": null,
              "alignment": 5,
              "parent_id": "parameters",
              "menu_id": "parameters|123",
              "title": "dfgfdg13",
              "icon": "vaadin__inbox",
              "part": "56546",
              "class": "",
              "script": "",
              "custom_right": [
                {
                  "fieldName": "22",
                  "fieldType": "44",
                  "fieldLabel": "33",
                  "fieldValue": "55"
                },
                {
                  "fieldName": "33",
                  "fieldType": "55",
                  "fieldLabel": "44",
                  "fieldValue": "66"
                }
              ],
              "version": 0,
              "text": "dfgfdg13"
            }
          ]
        },
        {
          "id": 7,
          "created_user": "Admin",
          "created_at": "2024-01-11T14:00:08.067Z",
          "updated_user": "Admin",
          "updated_at": "2024-01-11T14:00:08.067Z",
          "delete_user": null,
          "deleted_at": null,
          "alignment": 999,
          "parent_id": "",
          "menu_id": "administration",
          "title": "Yönetim",
          "icon": "vaadin__cog",
          "part": "",
          "class": "",
          "script": "",
          "custom_right": [],
          "version": 0,
          "text": "Yönetim",
          "data": [
            {
              "id": 8,
              "created_user": "Admin",
              "created_at": "2024-01-11T14:00:08.067Z",
              "updated_user": "Admin",
              "updated_at": "2024-01-11T14:00:08.067Z",
              "delete_user": null,
              "deleted_at": null,
              "alignment": 1,
              "parent_id": "administration",
              "menu_id": "administration|user",
              "title": "Kullanıcılar",
              "icon": "vaadin__group",
              "part": "app-user",
              "class": "item-fire-click",
              "script": "",
              "custom_right": [],
              "version": 0,
              "text": "Kullanıcılar"
            },
            {
              "id": 9,
              "created_user": "Admin",
              "created_at": "2024-01-11T14:00:08.067Z",
              "updated_user": "Admin",
              "updated_at": "2024-01-11T14:00:08.067Z",
              "delete_user": null,
              "deleted_at": null,
              "alignment": 2,
              "parent_id": "administration",
              "menu_id": "administration|permission",
              "title": "Yetkiler",
              "icon": "vaadin__key",
              "part": "app-permission",
              "class": "item-fire-click",
              "script": "",
              "custom_right": [],
              "version": 0,
              "text": "Yetkiler"
            },
            {
              "id": 10,
              "created_user": "Admin",
              "created_at": "2024-01-11T14:00:08.067Z",
              "updated_user": "Admin",
              "updated_at": "2024-01-18T11:02:45.654Z",
              "delete_user": null,
              "deleted_at": null,
              "alignment": 3,
              "parent_id": "administration",
              "menu_id": "administration|menu",
              "title": "Menü",
              "icon": "vaadin__file_tree",
              "part": "app-menu",
              "class": "item-fire-click",
              "script": "",
              "custom_right": [],
              "version": 0,
              "text": "Menü"
            }
          ]
        }];
      await t.menuTree.load(t.menuTreeData);
      t.customRights.setData({ currentPage: 1, recordsPerPage: 9999, totalRecords: 9999, records: t.customRightData });
    }
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //  console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
    //}
    switch (obj.element) {
      case 'menuTree': {
        t.menuTreeData = obj.records;
        await t.menuTree.load(t.menuTreeData);
        break;
      }
      default: {
        if (Build.isDev) {
          console.log(t.myElement.tagName.toLowerCase(), 'setItemData:', obj);
        }
        break;
      }
    }
  }

  @Method()
  async callbackRequestData(obj: any, resp: any) {
    let t = this;
    if (Build.isDev) {
      console.log('CBRD|', t.myElement.tagName.toLowerCase(), obj, resp);
    }
    await t.uiFns.requestData(t.baseRequestParams, { element: t.menuTree, elementName: 'menuTree', route: 'filtered-records', currentPage: 1, where: [], order: [] });
    return await t.appEvents({ type: 'button', element: 'add' });
  }

  @Method()
  async resize(height: number) {
    let t = this;
    //console.log('resize|', t.myElement.tagName.toLowerCase(), height);
    t.tabForm.style.height = (height - 2) + 'px';
  }

  async appEvents(obj: any) {
    let t = this;
    if (Build.isDev) {
      console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
    }
    switch (obj.type) {
      case 'button': {
        switch (obj.element) {
          case 'add': {
            //t.appEvent.emit({ event: 'btn-' + obj.element, component: t.myElement.tagName.toLowerCase(), idx: t.args.idx });
            t.btnAdd.disabled = false;
            t.btnClose.disabled = false;
            t.btnSave.disabled = false;
            await t.menuTree.load(t.menuTreeData);
            await t.setFormValues({});
            t.btnAddCustomRight.disabled = false;
            t.btnDeleteCustomRight.disabled = true;
            t.btnSaveCustomRight.disabled = false;
            t.fElCrFieldName.value = '';
            t.fElCrFieldLabel.value = '';
            t.fElCrFieldType.value = '';
            t.fElCrFieldValue.value = '';
            t.selectedCustomRight = {};
            break;
          }
          case 'edit': {
            t.btnClose.disabled = false;
            t.btnSave.disabled = false;
            //await t.dataGrid.beforeLoad();
            //await t.uiFns.delay(250);
            await t.menuTree.load(t.menuTreeData);
            //await t.dataGrid.afterLoad();
            break;
          }
          case 'delete': {
            t.appEvent.emit({
              event: 'sendData'
              , component: t.myElement.tagName.toLowerCase()
              , idx: t.args.idx
              , data: t.selectedTreeNode
              , id: t.selectedTreeNode.id
              , method: 'delete'
            });
            break;
          }
          case 'refresh': {
            await t.uiFns.requestData(t.baseRequestParams, { element: t.menuTree, elementName: 'menuTree', route: 'filtered-records', currentPage: 1, where: [], order: [] });
            break;
          }
          case 'save': {
            const values: any = await t.getFormValues();
            if (!t.isInvalid) {
              t.appEvent.emit({
                event: 'sendData'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , data: values
                , method: (typeof values.id !== 'undefined' && values.id !== '' ? 'patch' : 'post')
              });
            } else {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Gerekli alanları eksiksiz olarak doldurunuz.'
              });
              t.btnAdd.disabled = false;
              t.btnClose.disabled = false;
              t.btnDelete.disabled = false;
              t.btnSave.disabled = false;
              t.btnAddCustomRight.disabled = false;
              t.btnDeleteCustomRight.disabled = true;
              t.btnSaveCustomRight.disabled = false;
            }
            break;
          }
          case 'close': {
            t.rendererEvent.emit({
              event: 'close'
              , component: t.myElement.tagName.toLowerCase()
              , idx: t.args.idx
            });
            break;
          }
          case 'save_customRight': {
            t.fElCrFieldName.focus();
            t.fElCrFieldName.blur();
            t.fElCrFieldLabel.focus();
            t.fElCrFieldLabel.blur();
            t.fElCrFieldType.focus();
            t.fElCrFieldType.blur();
            t.fElCrFieldValue.focus();
            t.fElCrFieldValue.blur();
            if ((t.fElCrFieldName.value !== '' && !t.fElCrFieldName.invalid)
              && (t.fElCrFieldLabel.value !== '' && !t.fElCrFieldLabel.invalid)
              && (t.fElCrFieldType.value !== '' && !t.fElCrFieldType.invalid)
              && (t.fElCrFieldValue.value !== '' && !t.fElCrFieldValue.invalid)
            ) {
              t.customRightData = [...t.customRightData, { fieldName: t.fElCrFieldName.value, fieldLabel: t.fElCrFieldLabel.value, fieldType: t.fElCrFieldType.value, fieldValue: t.fElCrFieldValue.value }];
              await t.customRights.beforeLoad();
              await t.uiFns.delay(200);
              t.customRights.setData({ currentPage: 1, recordsPerPage: 9999, totalRecords: 9999, records: t.customRightData });
            } else {
              t.rendererEvent.emit({
                event: 'showNotification'
                , component: t.myElement.tagName.toLowerCase()
                , idx: t.args.idx
                , message: 'Gerekli alanları eksiksiz olarak doldurunuz.'
              });
            }
            t.btnAddCustomRight.disabled = false;
            t.btnDeleteCustomRight.disabled = true;
            t.btnSaveCustomRight.disabled = false;
            if (!t.fElCrFieldName.invalid && !t.fElCrFieldLabel.invalid && !t.fElCrFieldType.invalid && !t.fElCrFieldValue.invalid) {
              t.fElCrFieldName.value = '';
              t.fElCrFieldLabel.value = '';
              t.fElCrFieldType.value = '';
              t.fElCrFieldValue.value = '';
            }
            t.selectedCustomRight = {};
            break;
          }
          case 'add_customRight': {
            await t.customRights.beforeLoad();
            await t.uiFns.delay(200);
            await t.customRights.afterLoad();
            t.btnAddCustomRight.disabled = false;
            t.btnDeleteCustomRight.disabled = true;
            t.btnSaveCustomRight.disabled = false;
            t.fElCrFieldName.value = '';
            t.fElCrFieldLabel.value = '';
            t.fElCrFieldType.value = '';
            t.fElCrFieldValue.value = '';
            t.selectedCustomRight = {};
            break;
          }
          case 'delete_customRight': {
            const idx: number = await t.uiFns.getIndex(t.customRightData, 'fieldName|fieldType|fieldValue', t.selectedCustomRight.fieldName + '|' + t.selectedCustomRight.fieldType + '|' + t.selectedCustomRight.fieldValue);
            if (idx !== -1) {
              t.customRightData.splice(idx, 1);
              t.customRights.setData({ currentPage: 1, recordsPerPage: 9999, totalRecords: 9999, records: [] });
              await t.customRights.beforeLoad();
              await t.uiFns.delay(200);
              t.customRights.setData({ currentPage: 1, recordsPerPage: 9999, totalRecords: 9999, records: t.customRightData });
            }
            t.btnAddCustomRight.disabled = false;
            t.btnSaveCustomRight.disabled = false;
            break;
          }
          default: {
            console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
            break;
          }
        }
        break;
      }
      case 'clear-row-select': {
        t.btnDeleteCustomRight.disabled = true;
        t.fElCrFieldName.value = '';
        t.fElCrFieldLabel.value = '';
        t.fElCrFieldType.value = '';
        t.fElCrFieldValue.value = '';
        t.selectedCustomRight = {};
        break;
      }
      case 'row-select': {
        t.btnDeleteCustomRight.disabled = false;
        t.fElCrFieldName.value = obj.data.fieldName;
        t.fElCrFieldLabel.value = obj.data.fieldLabel;
        t.fElCrFieldType.value = obj.data.fieldType;
        t.fElCrFieldValue.value = obj.data.fieldValue;
        t.selectedCustomRight = obj.data;
        break;
      }
      default: {
        console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
        break;
      }
    }
  }

  private async getFormValues() {
    let t = this;
    const formElements: NodeListOf<ChildNode> = await t.uiFns.getFormElements(t.formLayout);
    t.isInvalid = false;
    const preData: any = Object.assign({}, t.selectedTreeNode);
    await t.uiFns.asyncForEach(formElements, async (item: any) => {
      if (typeof item.getAttribute === 'function') {
        const dataIndex: string = item.getAttribute('dataIndex');
        if (dataIndex !== null && dataIndex !== '') {
          if (typeof item.focus === 'function') {
            item.focus();
          }
          if (typeof item.blur === 'function') {
            item.blur();
          }
          if (item.invalid) {
            t.isInvalid = true;
          }
          const tagName: string = item.tagName.toLowerCase();
          if (tagName === "vaadin-integer-field") {
            t.selectedTreeNode[dataIndex] = parseInt(item.value, 10);
          } else {
            t.selectedTreeNode[dataIndex] = item.value;
          }
        }
      }
    });
    if (!t.isInvalid) {
      t.selectedTreeNode.custom_right = t.customRightData;
    } else {
      t.selectedTreeNode = preData;
    }
    return t.selectedTreeNode;
  }

  private async setFormValues(data: any) {
    let t = this;
    t.selectedTreeNode = data;
    const formElements: NodeListOf<ChildNode> = await t.uiFns.getFormElements(t.formLayout);
    let isFormClear: boolean = true;
    await t.uiFns.asyncForEach(formElements, async (item: any) => {
      if (typeof item.getAttribute === 'function') {
        const dataIndex: string = item.getAttribute('dataIndex');
        if (dataIndex !== null && dataIndex !== '') {
          if (typeof data[dataIndex] !== 'undefined') {
            isFormClear = false;
            if (data[dataIndex] !== null) {
              item.value = data[dataIndex];
            } else {
              item.value = '';
              item.removeAttribute('invalid');
            }
          } else {
            item.value = '';
            item.removeAttribute('invalid');
          }
        }
      }
    });
    if (typeof data.custom_right !== 'undefined') {
      t.customRightData = data.custom_right;
    } else {
      t.customRightData = [];
    }
    await t.customRights.beforeLoad();
    await t.uiFns.delay(200);
    t.customRights.setData({ currentPage: 1, recordsPerPage: 9999, totalRecords: 9999, records: t.customRightData });
    if (!isFormClear) {
      t.btnDelete.disabled = false;
    }
    t.fElCrFieldName.removeAttribute('invalid');
    t.fElCrFieldLabel.removeAttribute('invalid');
    t.fElCrFieldType.removeAttribute('invalid');
    t.fElCrFieldValue.removeAttribute('invalid');
  }

  private responsiveSteps: FormLayoutResponsiveStep[] = [
    // Use one column by default
    { minWidth: 0, columns: 1 },
    // Use two columns, if layout's width exceeds 500px
    { minWidth: '300px', columns: 3 },
  ];

  private responsiveStepsCustomRight: FormLayoutResponsiveStep[] = [
    // Use one column by default
    { minWidth: 0, columns: 1 },
    // Use two columns, if layout's width exceeds 500px
    { minWidth: '300px', columns: 4 },
  ];

  render() {
    let t = this;
    const clientHeight: string = typeof t.args !== 'undefined' && typeof t.args.clientHeight !== 'undefined' ? (t.args.clientHeight - 2) + 'px' : '600px';
    let formHeight: string = 'calc(100% - 0px)';
    let styleHeader: any = { height: "50px", width: 'calc(100% - 2px)', border: "1px solid var(--lumo-contrast-20pct)" };
    formHeight = 'calc(100% - 53px)';
    return (
      <div ref={(el) => t.contentContainer = el as HTMLDivElement}>
        <div ref={(el) => t.tabForm = el as HTMLDivElement} class="content" style={{ height: clientHeight }}>
          <div //ref={(el) => t.elContainerHeader = el as HTMLDivElement}
            style={styleHeader}>
            <vaadin-horizontal-layout
              class="flex--middle flex--wrap flex--right"
              theme="spacing"
              style={{ height: "100%", paddingLeft: "10px", paddingRight: "10px" }}
            >
              <vaadin-button theme="primary" ref={(el) => t.btnSave = el as Button}
                on-click={() => {
                  t.btnAdd.disabled = true;
                  t.btnClose.disabled = true;
                  t.btnDelete.disabled = true;
                  t.btnSave.disabled = true;
                  t.btnAddCustomRight.disabled = true;
                  t.btnDeleteCustomRight.disabled = true;
                  t.btnSaveCustomRight.disabled = true;
                  t.appEvents({ type: 'button', element: 'save' });
                }}>
                <vaadin-icon src={UIIcons['fa__check']} slot="prefix"></vaadin-icon>
                Kaydet
              </vaadin-button>
              <vaadin-button theme="primary success" ref={(el) => t.btnAdd = el as Button}
                on-click={() => {
                  t.btnAdd.disabled = true;
                  t.btnClose.disabled = true;
                  t.btnDelete.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'add' });
                }}>
                <vaadin-icon src={UIIcons['fa__file']} slot="prefix"></vaadin-icon>
                Yeni
              </vaadin-button>
              <vaadin-button theme="primary error" ref={(el) => t.btnDelete = el as Button}
                disabled
                on-click={() => {
                  t.btnAdd.disabled = true;
                  t.btnClose.disabled = true;
                  t.btnDelete.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'delete' });
                }}>
                <vaadin-icon src={UIIcons['fa__trash']} slot="prefix"></vaadin-icon>
                Sil
              </vaadin-button>
              <vaadin-button theme="primary contrast" ref={(el) => t.btnClose = el as Button}
                on-click={() => {
                  t.btnAdd.disabled = true;
                  t.btnClose.disabled = true;
                  t.btnDelete.disabled = true;
                  t.btnSave.disabled = true;
                  t.appEvents({ type: 'button', element: 'close' });
                }}>
                <vaadin-icon src={UIIcons['fa__square_xmark']} slot="prefix"></vaadin-icon>
                Kapat
              </vaadin-button>
            </vaadin-horizontal-layout>
          </div>
          <div style={{ height: formHeight, width: "calc(100% - 2px)", border: "1px solid var(--lumo-contrast-20pct)", borderTop: "none" }} >
            <div class="flex flex--row" style={{ height: "100%", width: "100%" }}>
              <div style={{ height: "100%", width: "15%", paddingLeft: "0px" }}>
                <ui-tree
                  ref={(el) => t.menuTree = el as unknown as UITree}
                  height="100%" width="100%"
                  isCheckableNode={true}
                  on-uiTreeNodeSelectEvent={async (e: CustomEvent) => {
                    await t.setFormValues(e.detail);
                  }}
                ></ui-tree>
              </div>
              <div style={{ height: "100%", width: "45%", overflowY: "auto", borderLeft: "1px solid var(--lumo-contrast-20pct)" }}>
                <vaadin-form-layout ref={(el) => t.formLayout = el as FormLayout} responsiveSteps={t.responsiveSteps} style={{ padding: "10px" }}>
                  <vaadin-text-field
                    label="ID"
                    dataIndex="id"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>
                  <vaadin-text-field
                    label="version"
                    dataIndex="version"
                    max-length="50"
                    hidden
                  ></vaadin-text-field>
                  <vaadin-integer-field
                    label="Sıralama"
                    step-buttons-visible
                    min="0"
                    max="999"
                    value="0"
                    dataIndex="alignment"
                    required
                    helper-text="Menünün kendi seviyesi içindeki sıra değeri."></vaadin-integer-field>
                  <vaadin-text-field
                    label="Önceki Menü Id"
                    dataIndex="parent_id"
                    max-length="50"
                    helper-text="Parent Id değeri. Ana menü girdisinde boş olabilir."></vaadin-text-field>
                  <vaadin-text-field
                    label="Menü Id"
                    dataIndex="menu_id"
                    max-length="50"
                    required
                    helper-text="Menu Id değeri."></vaadin-text-field>
                  <vaadin-text-field
                    colspan="3"
                    label="Başlık"
                    dataIndex="title"
                    max-length="50"
                    required helper-text="Menü başlığı."></vaadin-text-field>
                  <vaadin-text-field
                    label="Uygulama Adı"
                    dataIndex="part"
                    max-length="50"
                    helper-text="Menüye basıldığında çalıştırılacak olan ui-kaitek bileşeni adı."></vaadin-text-field>
                  <vaadin-text-field
                    label="Simge"
                    dataIndex="icon"
                    max-length="50"
                    helper-text="Menü simge adı."
                    clear-button-visible
                    on-value-changed={(e: CustomEvent) => {
                      t.iconItem.src = UIIcons[e.detail.value];
                    }}
                  ><vaadin-icon ref={(el) => t.iconItem = el as Icon} slot="prefix" icon=""></vaadin-icon></vaadin-text-field>
                  <vaadin-text-field
                    label="Class"
                    dataIndex="class"
                    max-length="50"
                    helper-text="Menüye basıldığında olay tetiklenmesi için item-fire-click."></vaadin-text-field>
                  <vaadin-text-area
                    label="Komut"
                    dataIndex="script"
                    colspan="3"
                    min-length="5"
                    max-length="250"
                    helper-text="Menüye basıldığında çalıştırılacak olan komut."
                  ></vaadin-text-area>
                </vaadin-form-layout>
              </div>
              <div style={{ height: "100%", width: "40%" }}>
                <div class="flex flex--col" style={{ height: "100%", width: "100%" }}>
                  <div style={{ height: "70%", width: "100%" }}>
                    <ui-grid
                      ref={(el) => t.customRights = el as unknown as UIGrid}
                      style={{ border: "none" }}
                      tableBorderRight="none"
                      tableBorderTop="none"
                      columns={[
                        new DataTableColumn({ label: 'Adı', dataIndex: 'fieldName', style: { width: "25%" } })
                        , new DataTableColumn({ label: 'Başlık', dataIndex: 'fieldLabel', style: { width: "25%" } })
                        , new DataTableColumn({ label: 'Tipi', dataIndex: 'fieldType', style: { width: "25%" } })
                        , new DataTableColumn({ label: 'Değeri', dataIndex: 'fieldValue', style: { width: "25%" } })
                      ]}
                      height='calc(100%)'
                      isShowFooter={false}
                      isShowHeader={false}
                      on-uiDataSourceEvent={(e: CustomEvent) => t.appEvents(e.detail)}
                    ></ui-grid>
                  </div>
                  <div style={{ height: "30%", width: "100%" }}>
                    <vaadin-form-layout ref={(el) => t.formLayoutCustomRight = el as FormLayout} responsiveSteps={t.responsiveStepsCustomRight} style={{ padding: "10px", paddingLeft: "10px" }}>
                      <vaadin-text-field
                        ref={(el) => t.fElCrFieldName = el as TextField}
                        label="Adı"
                        dataIndex="fieldName"
                        max-length="50"
                        required
                        helper-text="Uygulamada geçen değişken adı.">
                      </vaadin-text-field>
                      <vaadin-text-field
                        ref={(el) => t.fElCrFieldLabel = el as TextField}
                        label="Başlık"
                        dataIndex="fieldLabel"
                        max-length="50"
                        required
                        helper-text="Yetkilendirmede geçen başlık değeri.">
                      </vaadin-text-field>
                      <vaadin-text-field
                        ref={(el) => t.fElCrFieldType = el as TextField}
                        label="Tipi"
                        dataIndex="fieldType"
                        max-length="50"
                        required
                        helper-text="Değişkenin tipi.">
                      </vaadin-text-field>
                      <vaadin-text-field
                        ref={(el) => t.fElCrFieldValue = el as TextField}
                        label="Değeri"
                        dataIndex="fieldValue"
                        max-length="50"
                        required
                        helper-text="Değişkenin ön tanımlı değeri.">
                      </vaadin-text-field>
                    </vaadin-form-layout>
                    <vaadin-horizontal-layout
                      class="flex--middle flex--wrap flex--right"
                      theme="spacing"
                      style={{ height: "50px", paddingLeft: "10px", paddingRight: "10px" }}
                    >
                      <vaadin-button theme="primary" ref={(el) => t.btnSaveCustomRight = el as Button}
                        on-click={() => {
                          t.btnAddCustomRight.disabled = true;
                          t.btnDeleteCustomRight.disabled = true;
                          t.btnSaveCustomRight.disabled = true;
                          t.appEvents({ type: 'button', element: 'save_customRight' });
                        }}>
                        <vaadin-icon src={UIIcons['fa__square_plus']} slot="prefix"></vaadin-icon>
                        Ekle
                      </vaadin-button>
                      <vaadin-button theme="primary success" ref={(el) => t.btnAddCustomRight = el as Button}
                        on-click={() => {
                          t.btnAddCustomRight.disabled = true;
                          t.btnDeleteCustomRight.disabled = true;
                          t.btnSaveCustomRight.disabled = true;
                          t.appEvents({ type: 'button', element: 'add_customRight' });
                        }}>
                        <vaadin-icon src={UIIcons['fa__file']} slot="prefix"></vaadin-icon>
                        Yeni
                      </vaadin-button>
                      <vaadin-button theme="primary error" ref={(el) => t.btnDeleteCustomRight = el as Button}
                        disabled
                        on-click={() => {
                          t.btnAddCustomRight.disabled = true;
                          t.btnDeleteCustomRight.disabled = true;
                          t.btnSaveCustomRight.disabled = true;
                          t.appEvents({ type: 'button', element: 'delete_customRight' });
                        }}>
                        <vaadin-icon src={UIIcons['fa__square_minus']} slot="prefix"></vaadin-icon>
                        Sil
                      </vaadin-button>
                    </vaadin-horizontal-layout>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
