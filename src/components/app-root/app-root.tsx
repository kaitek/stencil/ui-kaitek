import { Build, Component, Element, Event, EventEmitter, h, Method, Prop } from '@stencil/core';

import { UIFunctions } from '../ui-functions/ui-functions';
import * as UIIcons from '../ui-icons';
//import { UILoadingIndicator } from '../ui-loading-indicator/ui-loading-indicator';

import { AppLayout } from '@vaadin/app-layout/vaadin-app-layout';
import { DrawerToggle } from '@vaadin/app-layout/vaadin-drawer-toggle';
import { ConfirmDialog } from '@vaadin/confirm-dialog';
//import { Dialog } from '@vaadin/dialog';
//import { dialogFooterRenderer, dialogHeaderRenderer, dialogRenderer } from '@vaadin/dialog/lit.js';
import { Icon } from '@vaadin/icon';
import { MenuBar } from '@vaadin/menu-bar/vaadin-menu-bar';
import { Notification } from '@vaadin/notification/vaadin-notification';
import { SideNav } from '@vaadin/side-nav/vaadin-side-nav';
import { SideNavItem } from '@vaadin/side-nav/vaadin-side-nav-item';
import { Tab } from '@vaadin/tabs/vaadin-tab';
import { Tabs } from '@vaadin/tabs/vaadin-tabs';

import '@vaadin/app-layout';
import '@vaadin/app-layout/vaadin-drawer-toggle.js';
import '@vaadin/button';
import '@vaadin/confirm-dialog';
import '@vaadin/dialog';
import '@vaadin/horizontal-layout';
import '@vaadin/icon';
import '@vaadin/menu-bar';
import '@vaadin/message-list';
import '@vaadin/notification';
import '@vaadin/scroller';
import '@vaadin/side-nav';
import '@vaadin/tabs';
import '@vaadin/tooltip';
import '@vaadin/vertical-layout';

//import type { DialogOpenedChangedEvent } from '@vaadin/dialog';
//import type { ConfirmDialogOpenedChangedEvent } from '@vaadin/confirm-dialog';
//import type { NotificationPosition } from '@vaadin/notification';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.scss',
  shadow: false//theme switcher çalışmıyor
})
export class AppRoot {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  //@Prop({ attribute: 'theme', reflect: true }) theme: string = 'dark';
  @Prop({ attribute: 'theme', reflect: true }) itemsSideBar: any = [];
  //isDev: boolean = false;

  uiFns: UIFunctions = new UIFunctions();

  elAppLayout: AppLayout;
  elSideNav: SideNav;
  elDrawerToggle: DrawerToggle;
  navbarContainer: HTMLDivElement;
  navbarShortcutsContainer: HTMLDivElement;
  navbarInfoContainer: HTMLDivElement;
  contentContainer: HTMLDivElement;
  containerMain: HTMLDivElement;
  elMenuBar: MenuBar;
  navbarTabs: Tabs;
  confirmDialog: ConfirmDialog;
  contentConfirmDialog: HTMLDivElement;
  notificationPos: 'top-stretch' | 'top-start' | 'top-center' | 'top-end' | 'middle' | 'bottom-start' | 'bottom-center' | 'bottom-end' | 'bottom-stretch' = 'top-stretch';
  storageKey: string = 'theme-preference';
  currentTheme: any;
  //weatherDialog: Dialog;
  //elLoadingIndicator: UILoadingIndicator;
  //ifrmMainScreen: HTMLIFrameElement;

  dialogOpened: boolean = false;
  lastOpenedDialog: string = '';
  lastOpenedDialogResult: string = '';
  arrTabItemsIndex: string[] = [];
  __boundResizeListener: any;
  timerResize: any = null;

  async connectedCallback() {
    let t = this;
    t.__boundResizeListener = t._resize.bind(t);
    window.addEventListener('resize', t.__boundResizeListener);
  }

  async componentWillLoad() {
    let t = this;
    // console.log('CWL|', t.myElement.tagName.toLowerCase());
    t.currentTheme = { value: await t.getColorPreference() };
    //console.log(t.currentTheme)
    //const els = document.getElementsByTagName('html');
    //t.isDev = typeof els[0].attributes["isdev"] !== 'undefined';
    if (Build.isDev) {
      t.itemsSideBar = [
        {
          class: ""
          , icon: "vaadin__m_folder"
          , part: ""
          , title: "Messages"
          , children: [
            {
              class: "item-fire-click"
              , icon: "fa__inbox"
              , part: "app-inbox"
              , title: "Inbox"
            }
            , {
              class: "item-fire-click"
              , icon: "fa__paper_plane"
              , part: "app-sent"
              , title: "Sent"
            }
            , {
              class: "item-fire-click"
              , icon: "fa__trash"
              , part: "app-trash"
              , title: "Trash"
            }
            , {
              class: "item-fire-click"
              , icon: "fa__table_list"
              , part: "app-report-viewer"
              , title: "Banka Listesi"
              , script: JSON.stringify({ report: "CustomersTable", statement: "named" })
            }
            , {
              class: "item-fire-click"
              , icon: "fa__table_list"
              , part: "app-report-customer"
              , title: "Müşteri Listesi"
              , script: JSON.stringify({ report: "CustomersTable" })
            }
          ]
        }
        , {
          class: ""
          , icon: "fa__user_shield"
          , part: ""
          , title: "Yönetim"
          , children: [
            {
              class: "item-fire-click"
              , icon: "fa__users"
              , part: "app-user"
              , title: "Kullanıcılar"
            }
            , {
              class: "item-fire-click"
              , icon: "fa__key"
              , part: "app-permission"
              , title: "Yetkiler"
            }
            , {
              class: "item-fire-click"
              , icon: "fa__folder_tree"
              , part: "app-menu"
              , title: "Menü"
            }
          ]
        }
      ];
    }
  }

  async componentDidLoad() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
    // console.log(t.elSideNav);
    t.reflectPreference();

    window
      .matchMedia('(prefers-color-scheme: dark)')
      .addEventListener('change', ({ matches: isDark }) => {
        t.currentTheme.value = isDark ? 'dark' : 'light';
        t.setPreference();
      });
    //const themeIcon: string = t.currentTheme.value === 'dark' ? 'vaadin__moon_o' : 'vaadin__sun_o';
    //@@todo: notification child count=0 ise children olmayacak
    const itemsMenuBar: any[] = [
      //{
      //  component: await this.createMenuBarItem('vaadin__bell_o', '3', false),
      //  tooltip: 'Bildirimler',
      //  part: 'btn-notification',
      //  children: [
      //    {
      //      component: await this.createMessageListHeader(3),
      //      part: 'notification-header'
      //    },
      //    {
      //      component: await this.createMessageListItem('Matt Mambo', 'Linsey, could you check if the details...', '2023-01-01 20:00:00'),
      //      part: 'notificationId:1'
      //    },
      //    {
      //      component: await this.createMessageListItem('Halil Ibrahim Karaalp', 'Linsey, could you check if the details...', '2023-01-01 20:00:00'),
      //      part: 'notificationId:2'
      //    },
      //    {
      //      component: await this.createMessageListItem('Matt Mambo', 'Linsey, could you check if the details...', '2023-01-01 20:00:00'),
      //      part: 'notificationId:3'
      //    },
      //    {
      //      component: await this.createMessageListReadAll()
      //      , part: 'btn-notification-read-all'
      //    }
      //  ],
      //},
      //{
      //  component: await this.createMenuBarItem('theme-switcher', '', false),//themeIcon
      //  tooltip: 'Temayı Değiştir',
      //  part: 'btn-change-theme'
      //},
      //{
      //  component: await this.createMenuBarItem('vaadin__grid_big_o', '', false),
      //  children: [
      //    {
      //      text: 'On social media',
      //      children: [{ text: 'Facebook' }, { text: 'Twitter' }, { text: 'Instagram' }],
      //    },
      //    { text: 'By email' },
      //    { text: 'Get link' },
      //  ],
      //  tooltip: 'Kısayollar',
      //  part: ''
      //},
      {
        component: await this.createMenuBarItem('vaadin__m_account', '', false),
        children: [
          {
            component: await this.createMenuBarItem('vaadin__m_card_account_details', 'Profil', true)
            , part: 'app-profile'
            , title: "Profil"
          },
          //{
          //  component: await this.createMenuBarItem('vaadin__envelope_o', 'Gelen Kutusu', true)
          //  , part: 'app-inbox'
          //},
          //{
          //  component: await this.createMenuBarItem('vaadin__comment_o', 'Chat', true)
          //  , part: 'app-chat'
          //},
          //{ component: 'hr' },
          //{
          //  component: await this.createMenuBarItem('vaadin__cog_o', 'Ayarlar', true)
          //  , part: 'app-settings'
          //  , title: "Ayarlar"
          //},
          //{
          //  component: await this.createMenuBarItem('vaadin__question_circle_o', 'Yardım', true)
          //  , part: 'app-help'
          //},
          { component: 'hr' },
          {
            component: await this.createMenuBarItem('vaadin__m_logout', 'Çıkış', true)
            , part: 'btn-sign-out'
            , title: "Çıkış"
          }
        ],
        tooltip: '',
        part: ''
      }
    ];
    t.elMenuBar.items = itemsMenuBar;
    const itemsFireClick = t.elSideNav.querySelectorAll('.item-fire-click');
    // console.log(itemsFireClick)
    if (itemsFireClick.length > 0) {
      await t.uiFns.asyncForEach(itemsFireClick, async (item: SideNavItem) => {
        //console.log(item, item["arg"]);
        const perm: any = {
          custom_right: item["arg"].custom_right
          , menu_id: item["arg"].menu_id
          , add: item["arg"].p_add
          , del: item["arg"].p_del
          , edit: item["arg"].p_edit
          , menu: item["arg"].p_menu
        };
        item.addEventListener('click', t._menuItemClicked.bind(t, item.part[0], item.innerText, perm, item), { passive: true });
      });
    }
    //const itemBackButton = t.navbarTabs.shadowRoot.querySelectorAll('div[part=back-button]');
    //if (itemBackButton.length > 0) {
    //  (itemBackButton[0] as HTMLDivElement).style.paddingTop = "4px";
    //}
    //const itemForwardButton = t.navbarTabs.shadowRoot.querySelectorAll('div[part=forward-button]');
    //if (itemForwardButton.length > 0) {
    //  (itemForwardButton[0] as HTMLDivElement).style.paddingTop = "4px";
    //}
    //t.weatherDialog.opened = true;
    //await t.elLoadingIndicator.setVisible(true);
    /*
    const targetNode = t.ifrmMainScreen;
    const config = {
      attributes: true,
      attributeFilter: ['src'],
      attributeOldValue: true,
      characterData: false,
      characterDataOldValue: false,
      childList: true,
      subtree: true
    };

    // Callback function to execute when mutations are observed
    const callback = (mutationList, observer) => {
      for (const mutation of mutationList) {
        if (mutation.type === "childList") {
          console.log("A child node has been added or removed.");
        } else if (mutation.type === "attributes") {
          console.log(`The ${mutation.attributeName} attribute was modified.`);
        }
      }
    };

    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);

    t.ifrmMainScreen.src = "https://www.marinetraffic.com/en/ais/embed?zoom=15&centery=40.8448&centerx=29.2650&maptype=1&notation=true&shownames=true&mmsi=0&fleet=&remember=true";
    // Later, you can stop observing
    observer.disconnect();
    */
    t.elAppLayout.setAttribute("overlay", "true");
    if (t.elDrawerToggle.slot === 'navbar-bottom') {
      t.navbarContainer.style.width = "calc(100vw - 50px)";
      t.navbarShortcutsContainer.style.width = "calc(100vw - 50px)";
      t.navbarTabs.style.width = "calc(100vw - 50px)";
    }
  }

  @Method()
  async callbackRequestData(obj: any, resp: any) {
    let t = this;
    const el: any = t.contentContainer.querySelector(obj.component + "[id='content-" + obj.idx + "']");
    //console.log('callbackRequestData', obj, el);
    if (el) {
      await el.callbackRequestData(obj, resp);
    }
  }

  @Method()
  async setItemData(obj: any) {
    let t = this;
    const el: any = t.contentContainer.querySelector(obj.component + "[id='content-" + obj.idx + "']");
    //console.log('setItemData', obj, el);
    if (el) {
      el.setItemData(obj);
    }
  }

  @Method()
  async showNotification(message: string) {
    let t = this;
    setTimeout(() => {
      Notification.show(message, { position: t.notificationPos, duration: 5000, theme: "error" });
    }, 100);
  }

  async _menuItemClicked(appName: string, title: string, perm: any, obj: any) {
    let t = this;
    if (Build.isDev) {
      console.log('_menuItemClicked:', appName, obj?.arg);
    }
    const d = new Date();
    const idx = d.getTime().toString();
    const tabItem = document.createElement('vaadin-tab');
    tabItem.addEventListener('click', t._tabItemClick.bind(t, idx, tabItem), { passive: true });
    tabItem.setAttribute("id", "tab-" + idx);
    tabItem.appendChild(document.createTextNode(title));
    const spanItem = document.createElement('span');
    const iconItemClose = document.createElement('vaadin-icon');
    iconItemClose.setAttribute("class", "taskbar-close-icon");
    iconItemClose.setAttribute("src", UIIcons["fa__square_xmark"]);
    iconItemClose.addEventListener('click', t._menuItemClosed.bind(t, idx), { passive: true });
    spanItem.appendChild(iconItemClose);
    tabItem.appendChild(spanItem);
    t.navbarTabs.appendChild(tabItem);
    const tabs = t.navbarTabs.querySelectorAll("vaadin-tab");
    t.navbarTabs.selected = tabs.length - 1;
    //@@info: menü objesi içinde açılacak komponenetin adı yazmalı verimot-users gibi
    //@@info: bütün komponentler idx property içerecek, buraya gönderilen değer sunucu requestine de gidecek
    //  , dönüşte de nereye callback olacağı bu değere göre belirlenecek
    //console.log(t.contentContainer.clientHeight, t.contentContainer.offsetHeight, t.contentContainer.scrollHeight);
    await t.hideAllContainers();
    const contentItem: any = document.createElement(appName);
    contentItem.setAttribute("class", "content");
    contentItem.setAttribute("id", "content-" + idx);
    contentItem.args = { idx: idx, clientHeight: t.contentContainer.clientHeight, perm, script: (typeof obj.arg.script !== 'undefined' ? obj.arg.script : '') };
    //contentItem.appendChild(document.createTextNode("content-" + idx));
    t.contentContainer.appendChild(contentItem);
    (async () => {
      await customElements.whenDefined(appName);
      const root = document.querySelector(appName + "[id='content-" + idx + "']");
      root.addEventListener('rendererEvent', (event: CustomEvent) => {
        switch (event?.detail?.event) {
          case 'close': {
            t._menuItemClosed(event.detail.idx);
            break;
          }
          case 'showNotification': {
            t.showNotification(event.detail.message);
            break;
          }
          default: {
            console.log('rendererEvent', event);
            break;
          }
        }
      });
    })();
    t.arrTabItemsIndex.push(idx);
    t.containerMain.classList.add("hidden");
    t.elAppLayout.removeAttribute("drawer-opened");
  }

  async _tabItemClick(id, tabItem: Tab) {
    let t = this;
    if (t.navbarTabs.selected !== -1) {
      const cIdx: number = t.arrTabItemsIndex.indexOf(id);
      if (cIdx !== -1 && cIdx === t.navbarTabs.selected) {
        tabItem.setAttribute("aria-selected", "false");
        tabItem.removeAttribute("focused");
        tabItem.removeAttribute("selected");
        await t.hideAllContainers();
        //console.log('_tabItemClick:', id, t.navbarTabs.selected, tabItem);
        setTimeout(() => {
          t.navbarTabs.selected = -1;
        }, 100);
        t.containerMain.classList.remove("hidden");
      }
    }
  }

  async _menuItemClosed(id: string) {
    let t = this;
    //console.log('_menuItemClosed:', t, id);
    const cIdx: number = t.arrTabItemsIndex.indexOf(id);
    if (cIdx !== -1) {
      t.arrTabItemsIndex.splice(cIdx, 1);
    }
    const tab = t.navbarTabs.querySelector("vaadin-tab[id='tab-" + id + "']");
    if (tab) {
      t.navbarTabs.removeChild(tab);
    }
    t.navbarTabs.selected = -1;
    const container = t.contentContainer.querySelector("[id='content-" + id + "']");
    if (container) {
      t.contentContainer.removeChild(container);
    }
    t.containerMain.classList.remove("hidden");
  }

  private async hideAllContainers() {
    let t = this;
    //açık olan içerikler gizleniyor
    const contentContainers = t.contentContainer.querySelectorAll('.content');
    if (contentContainers.length > 0) {
      await t.uiFns.asyncForEach(contentContainers, async (item: SideNavItem) => {
        if (!item.classList.contains('hidden')) {
          item.classList.add('hidden');
        }
      });
    }
  }

  private async resizeAllContainers(height: number) {
    let t = this;
    //açık olan içerikler gizleniyor
    const contentContainers = t.contentContainer.querySelectorAll('.content');
    if (contentContainers.length > 0) {
      await t.uiFns.asyncForEach(contentContainers, async (item: any) => {
        //console.log(typeof item.resize, item)
        if (typeof item.resize === 'function') {
          await item.resize(height);
        }
        //if (!item.classList.contains('hidden')) {
        //  item.classList.add('hidden');
        //}
      });
    }
  }

  private showContainer(id: string) {
    let t = this;
    const contentContainer = t.contentContainer.querySelector("[id='content-" + id + "']");
    if (contentContainer) {
      if (contentContainer.classList.contains('hidden')) {
        contentContainer.classList.remove("hidden");
      }
      t.containerMain.classList.add("hidden");
    }
  }

  //async _layoutOverlayChanged(expanded: boolean) {
  //  //let t = this;
  //  //console.log('_layoutOverlayChanged-expanded', expanded);
  //  if (expanded) {
  //
  //  } else {
  //
  //  }
  //}

  async _sideBarItemChanged(e: CustomEvent) {
    const item: SideNavItem = e.target as SideNavItem;
    if ((item.firstChild as HTMLElement).tagName === 'VAADIN-ICON') {
      if (e.detail.value) {
        if ((item.firstChild as Icon).src === UIIcons['vaadin__m_folder']) {
          (item.firstChild as Icon).src = UIIcons['vaadin__m_folder_open'];
        }
      } else {
        if ((item.firstChild as Icon).src === UIIcons['vaadin__m_folder_open']) {
          (item.firstChild as Icon).src = UIIcons['vaadin__m_folder'];
        }
      }
    }
  }

  async _tabItemChanged() {
    let t = this;
    await t.hideAllContainers();
    //console.log('_tabItemChanged:', t.navbarTabs.selected);
    const tab = t.navbarTabs.querySelector("vaadin-tab[aria-selected='true']");
    if (tab) {
      t.showContainer(tab.id.split('tab-').join(''));
    }
  }

  async _menuBarItemClicked(obj: any) {
    let t = this;
    if (typeof obj.part === 'undefined') {
      obj.part = '';
    }
    //console.log('_menuBarItemClicked---', obj, t)
    switch (obj.part) {
      case 'btn-notification': {

        break;
      }
      //case 'btn-change-theme': {
      //  t._themeToggle(obj);
      //  break;
      //}
      case 'app-profile': {
        const perm: any = {
          custom_right: []
          , menu_id: 1
          , add: 1
          , del: 1
          , edit: 1
          , menu: 1
        }
        t._menuItemClicked(obj.part, obj.title, perm, obj);
        break;
      }
      case 'btn-inbox': {

        break;
      }
      case 'btn-chat': {

        break;
      }
      case 'app-settings': {
        const perm: any = {
          custom_right: []
          , menu_id: 1
          , add: 1
          , del: 1
          , edit: 1
          , menu: 1
        }
        t._menuItemClicked(obj.part, obj.title, perm, obj);
        //t.contentConfirmDialog.innerHTML = "Programdan çıkış yapmak istediğinize emin misiniz?---";
        //t.confirmDialog.setAttribute('part', obj.part);
        //t.confirmDialog.header = "Programdan Çıkış";
        //t.confirmDialog.opened = true;
        break;
      }
      case 'btn-help': {

        break;
      }
      case 'btn-sign-out': {
        t.contentConfirmDialog.innerHTML = "Oturumunuzu sonlandırmak istediğinize emin misiniz?";
        t.confirmDialog.setAttribute('part', obj.part);
        t.confirmDialog.header = "Oturumu Sonlandırma";
        t.confirmDialog.opened = true;
        break;
      }
      default: {
        console.log('_menuBarItemClicked:', obj.part);
        break;
      }
    }
  }

  async _themeToggle() {//obj: any
    let t = this;
    const els = document.getElementsByTagName('html');
    if (els.length > 0) {
      const el = els[0];
      await t.uiFns.asyncForEach(el.attributes, async (item: any) => {
        if (item.name === 'theme') {
          if (item.value === 'dark') {
            item.value = 'light';
          } else {
            item.value = 'dark';
          }
          t.currentTheme.value = item.value;
          t.setPreference();
        }
      });
    }
  }

  async getColorPreference() {
    let t = this;
    if (localStorage.getItem(t.storageKey)) {
      return localStorage.getItem(t.storageKey);
    } else {
      return window.matchMedia('(prefers-color-scheme: dark)').matches
        ? 'dark'
        : 'light';
    }
  }

  async reflectPreference() {
    let t = this;
    document.firstElementChild
      .setAttribute('theme', t.currentTheme.value);
    //t.btnTheme.setAttribute('aria-label', t.currentTheme.value);
  }

  async setPreference() {
    let t = this;
    localStorage.setItem(t.storageKey, t.currentTheme.value);
    t.reflectPreference();
  }

  private async _resize() {
    let t = this;
    if (t.timerResize !== null) {
      clearTimeout(t.timerResize);
      t.timerResize = null;
    }
    t.timerResize = setTimeout(async () => {
      t.elAppLayout.setAttribute("overlay", "true");
      if (t.elDrawerToggle.slot === 'navbar-bottom') {
        t.navbarContainer.style.width = "calc(100vw - 50px)";
        t.navbarShortcutsContainer.style.width = "calc(100vw - 50px)";
        t.navbarTabs.style.width = "calc(100vw - 50px)";
      }
      //console.log('t.contentContainer.clientHeight:', t.contentContainer.clientHeight)
      await t.resizeAllContainers(t.contentContainer.clientHeight);
    }, 100);
  }
  private async createMenuBarItem(iconName: string, text: string, isChild: boolean = false) {
    const item = document.createElement('vaadin-menu-bar-item');
    const icon = document.createElement('vaadin-icon');

    if (isChild) {
      icon.style.width = 'var(--lumo-icon-size-m)';
      icon.style.height = 'var(--lumo-icon-size-m)';
      icon.style.marginRight = 'var(--lumo-space-s)';
    } else {
      icon.style.width = 'var(--lumo-size-s)';
      icon.style.height = 'var(--lumo-size-s)';
    }
    if (text === 'Profil') {
      item.setAttribute("disabled", "true");
    }
    if (text === 'Çıkış') {
      item.className = "bg-error text-primary-contrast";
    }
    if (iconName === 'copy') {
      item.setAttribute('aria-label', 'duplicate');
    }
    if (iconName === 'theme-switcher') {
      const svg = 'data:image/svg+xml;base64,PHN2ZyBjbGFzcz0ic3VuLWFuZC1tb29uIiBhcmlhLWhpZGRlbj0idHJ1ZSIgd2lkdGg9IjI0IiBoZWlnaHQ9IjI0IiB2aWV3Qm94PSIwIDAgMjQgMjQiPgogICAgICAgIDxtYXNrIGNsYXNzPSJtb29uIiBpZD0ibW9vbi1tYXNrIj4KICAgICAgICAgIDxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9IndoaXRlIiAvPgogICAgICAgICAgPGNpcmNsZSBjeD0iMjQiIGN5PSIxMCIgcj0iNiIgZmlsbD0iYmxhY2siIC8+CiAgICAgICAgPC9tYXNrPgogICAgICAgIDxjaXJjbGUgY2xhc3M9InN1biIgY3g9IjEyIiBjeT0iMTIiIHI9IjYiIG1hc2s9InVybCgjbW9vbi1tYXNrKSIgZmlsbD0iY3VycmVudENvbG9yIiAvPgogICAgICAgIDxnIGNsYXNzPSJzdW4tYmVhbXMiIHN0cm9rZT0iY3VycmVudENvbG9yIj4KICAgICAgICAgIDxsaW5lIHgxPSIxMiIgeTE9IjEiIHgyPSIxMiIgeTI9IjMiIC8+CiAgICAgICAgICA8bGluZSB4MT0iMTIiIHkxPSIyMSIgeDI9IjEyIiB5Mj0iMjMiIC8+CiAgICAgICAgICA8bGluZSB4MT0iNC4yMiIgeTE9IjQuMjIiIHgyPSI1LjY0IiB5Mj0iNS42NCIgLz4KICAgICAgICAgIDxsaW5lIHgxPSIxOC4zNiIgeTE9IjE4LjM2IiB4Mj0iMTkuNzgiIHkyPSIxOS43OCIgLz4KICAgICAgICAgIDxsaW5lIHgxPSIxIiB5MT0iMTIiIHgyPSIzIiB5Mj0iMTIiIC8+CiAgICAgICAgICA8bGluZSB4MT0iMjEiIHkxPSIxMiIgeDI9IjIzIiB5Mj0iMTIiIC8+CiAgICAgICAgICA8bGluZSB4MT0iNC4yMiIgeTE9IjE5Ljc4IiB4Mj0iNS42NCIgeTI9IjE4LjM2IiAvPgogICAgICAgICAgPGxpbmUgeDE9IjE4LjM2IiB5MT0iNS42NCIgeDI9IjE5Ljc4IiB5Mj0iNC4yMiIgLz4KICAgICAgICAgPC9nPgogICAgICA8L3N2Zz4=';
      icon.setAttribute('src', svg);
      icon.setAttribute('class', "theme-toggle");
    } else {
      //icon.setAttribute('icon', `${iconName}`);
      //console.log('UIIcons[iconName]', iconName, UIIcons[iconName])
      icon.setAttribute('src', UIIcons[iconName]);
    }
    item.appendChild(icon);
    if (text !== '') {
      item.appendChild(document.createTextNode(text));
    }
    return item;
  }
  /*
    private async createMessageListItem(userName: string, text: string, time: string) {
      const item = document.createElement('vaadin-message');
      item.setAttribute('role', 'listitem');
      item.setAttribute('user-name', userName);
      item.setAttribute('time', time);
      item.setAttribute('user-color-index', "6");
      item.style.borderBottom = "1px solid var(--lumo-contrast-10pct)";
      if (text !== '') {
        item.appendChild(document.createTextNode(text));
      }
      return item;
    }

    private createMessageListHeader(count: number) {
      const continer = document.createElement('div');
      const item = document.createElement('h4');
      // item.setAttribute('theme', 'secondary');
      item.appendChild(document.createTextNode(count + ' Yeni Bildirim'));
      // continer.style.display = "flex";
      // continer.style.flexDirection = "column";
      continer.style.borderBottom = "1px solid var(--lumo-contrast-10pct)";
      continer.style.paddingLeft = "var(--lumo-space-m)";
      continer.style.paddingRight = "var(--lumo-space-m)";
      continer.style.paddingTop = "var(--lumo-space-s)";
      continer.style.paddingBottom = "var(--lumo-space-s)";
      continer.style.textAlign = "center";
      continer.appendChild(item);
      return continer;
    }

    private createMessageListReadAll() {
      const continer = document.createElement('div');
      const item = document.createElement('vaadin-button');
      item.setAttribute('theme', 'secondary');
      item.appendChild(document.createTextNode("Temizle"));
      continer.style.display = "flex";
      continer.style.flexDirection = "column";
      continer.style.paddingLeft = "var(--lumo-space-m)";
      continer.style.paddingRight = "var(--lumo-space-m)";
      continer.style.paddingTop = "var(--lumo-space-s)";
      continer.style.paddingBottom = "var(--lumo-space-s)";
      continer.appendChild(item);
      return continer;
    }
  */
  private dialogOpenedChanged(opened: boolean) {
    let t = this;
    //console.log(t.confirmDialog.part.value, opened)
    try {
      t.dialogOpened = opened;
      if (opened) {
        t.lastOpenedDialog = t.confirmDialog.part.value;
      } else {
        if (t.lastOpenedDialogResult === 'confirm') {
          switch (t.lastOpenedDialog) {
            case 'btn-sign-out': {
              //console.log('dialogOpenedChanged:', part);
              t.myElement.classList.add('hidden');
              Notification.show('Oturum sonlandırılıyor...', { position: t.notificationPos, duration: 900, theme: "contrast" });
              setTimeout(() => {
                t.appEvent.emit({ event: 'logout' });
              }, 1000);
              break;
            }
            default: {
              console.log('dialogOpenedChanged:', t.lastOpenedDialog, t);
              break;
            }
          }
        }
        t.contentConfirmDialog.innerHTML = "";
        t.confirmDialog.setAttribute('part', '');
        t.confirmDialog.cancelButtonVisible = true;
        t.confirmDialog.noCloseOnEsc = false;
        t.confirmDialog.header = "";
        t.lastOpenedDialog = '';
        t.lastOpenedDialogResult = '';
      }
    } catch (error) {

    }
  }

  private onDialogCancel(part: string) {
    let t = this;
    t.lastOpenedDialogResult = 'cancel';
    switch (part) {
      case 'btn-sign-out': {
        //console.log('onDialogCancel:', part);
        break;
      }
      default: {
        console.log('onDialogCancel:', part, t);
        break;
      }
    }
  }

  private async onDialogConfirm(part: string) {
    let t = this;
    t.lastOpenedDialogResult = 'confirm';
    switch (part) {
      case 'btn-sign-out': {
        //t.appEvent.emit({ event: 'logout' });
        break;
      }
      default: {
        console.log('onDialogConfirm:', part, t);
        break;
      }
    }
  }

  private getTamplateSideBar() {
    let t = this;
    return (t.itemsSideBar.map((row) => {
      //console.log('getTamplateSideBar', row)
      return (<vaadin-side-nav-item class="unselectable" arg={row} onExpanded-changed={(e: CustomEvent) => { t._sideBarItemChanged(e); }}>
        <vaadin-icon src={UIIcons[row.icon]} slot="prefix"></vaadin-icon>
        {row.title}
        {row.children.map((child) => {
          //console.log('child', child)
          return (<vaadin-side-nav-item slot="children" class={child.class} part={child.part} arg={child}>
            <vaadin-icon src={UIIcons[child.icon]} slot="prefix"></vaadin-icon>
            {child.title}
          </vaadin-side-nav-item>)
        })}
      </vaadin-side-nav-item>)
    }));
  }

  //private weatherDialogRenderer(opened: boolean) {
  //  let t = this;
  //  if (opened) {
  //    const vLayout = document.createElement('vaadin-vertical-layout');
  //    const vLayout2 = document.createElement('vaadin-vertical-layout');
  //    const img = document.createElement('img');
  //    img.setAttribute("src", "https://manevra.gisasgemi.com/gisas/images/map.jpg?id=2");
  //    vLayout2.appendChild(img);
  //    vLayout2.style.alignItems = "stretch";
  //    vLayout.appendChild(vLayout2);
  //    vLayout.setAttribute("theme", "spacing");
  //    vLayout.style.width = "370px";
  //    vLayout.style.maxWidth = "100%";
  //    vLayout.style.alignItems = "stretch";
  //    const overlay = document.body.querySelector("vaadin-dialog-overlay[aria-label='weatherDialog']");
  //    overlay.appendChild(vLayout);
  //    const divOverlay = overlay.shadowRoot.querySelector("div[part='overlay']");
  //    divOverlay.setAttribute("style", "position:absolute;top:50px;right:0px;");
  //  }
  //}

  render() {
    let t = this;
    const theme: string = t.currentTheme.value;
    //if (Build.isDev) {
    //  console.log('Render|', t.myElement.tagName.toLowerCase());
    //}
    //console.log(window.Vaadin.featureFlags.sideNavComponent)
    return (
      <div>
        <vaadin-app-layout
          ref={(el) => t.elAppLayout = el as AppLayout}
          primary-section="drawer"
          drawerOpened={false}
        //onDrawer-opened-changed={() => { t._layoutOverlayChanged(t.elDrawerToggle.ariaExpanded === "true"); }}
        >
          <header slot="drawer" class="header-text">
            <div class="flex flex--col flex--center flex--middle" style={{ height: "100%", width: "100%" }} >
              <div class="flex flex--row flex--center flex--stretch" style={{}}>
                VerimotRT
              </div>
            </div>
          </header>
          <vaadin-scroller slot="drawer" scroll-direction="vertical" tabindex="0" dir="ltr" >
            <vaadin-side-nav dir="ltr" role="navigation" has-children=""
              ref={(el) => t.elSideNav = el as SideNav}>
              {t.getTamplateSideBar()}
            </vaadin-side-nav>
          </vaadin-scroller>
          <footer slot="drawer" class="footer-text">
            <div class="flex flex--row flex--justify" style={{}}>
              <div>v1.0.0</div>
              <div>
                <button //ref={(el) => t.btnTheme = el as HTMLButtonElement}
                  class="theme-toggle"
                  id="theme-toggle" title="Tema Değiştirici açık &amp; koyu" aria-label={theme} aria-live="polite"
                  on-click={() => t._themeToggle()}
                >
                  <svg class="sun-and-moon" aria-hidden="true" width="24" height="24" viewBox="0 0 24 24">
                    <mask class="moon" id="moon-mask">
                      <rect x="0" y="0" width="100%" height="100%" fill="white"></rect>
                      <circle cx="24" cy="10" r="6" fill="black"></circle>
                    </mask>
                    <circle class="sun" cx="12" cy="12" r="6" mask="url(#moon-mask)" fill="currentColor"></circle>
                    <g class="sun-beams" stroke="currentColor">
                      <line x1="12" y1="1" x2="12" y2="3"></line>
                      <line x1="12" y1="21" x2="12" y2="23"></line>
                      <line x1="4.22" y1="4.22" x2="5.64" y2="5.64"></line>
                      <line x1="18.36" y1="18.36" x2="19.78" y2="19.78"></line>
                      <line x1="1" y1="12" x2="3" y2="12"></line>
                      <line x1="21" y1="12" x2="23" y2="12"></line>
                      <line x1="4.22" y1="19.78" x2="5.64" y2="18.36"></line>
                      <line x1="18.36" y1="5.64" x2="19.78" y2="4.22"></line>
                    </g>
                  </svg>
                </button>
              </div>

            </div>
          </footer>
          <vaadin-drawer-toggle slot="navbar touch-optimized"
            ref={(el) => t.elDrawerToggle = el as DrawerToggle}
          ></vaadin-drawer-toggle>
          <div slot="navbar" class="header-navbar" ref={(el) => t.navbarContainer = el as HTMLDivElement}>
            <div class="shortcuts" ref={(el) => t.navbarShortcutsContainer = el as HTMLDivElement}>
              <vaadin-tabs ref={(el) => t.navbarTabs = el as Tabs}
                selected="0" orientation="horizontal" role="tablist" dir="ltr" class="taskbar" theme=""
                onSelected-changed={() => { t._tabItemChanged(); }}
              >
              </vaadin-tabs>
            </div>
            <div class="container-info" ref={(el) => t.navbarInfoContainer = el as HTMLDivElement}>
              <vaadin-menu-bar theme="icon end-aligned tertiary" role="menubar"
                ref={(el) => t.elMenuBar = el as MenuBar}
                onItem-selected={(e) => { t._menuBarItemClicked(e.detail.value); }}
                style={{ marginTop: "8px" }}
              >
                <vaadin-tooltip slot="tooltip"></vaadin-tooltip>
              </vaadin-menu-bar>
            </div>
          </div>
          <div ref={(el) => t.contentContainer = el as HTMLDivElement} class="content-container">
            <div ref={(el) => t.containerMain = el as HTMLDivElement} class="content-main-container">
            </div>
          </div>
        </vaadin-app-layout >
        <vaadin-confirm-dialog
          ref={(el) => t.confirmDialog = el as ConfirmDialog}
          header=""
          cancel-button-visible
          //reject-button-visible
          //reject-text="Discard"
          cancel-text="Hayır"
          cancel-theme="error primary"
          confirm-text="Evet"
          opened={t.dialogOpened}
          no-close-on-outside-click
          onOpened-changed={(e) => { t.dialogOpenedChanged(e.detail.value); }}
          onConfirm={() => { t.onDialogConfirm(t.confirmDialog.part.value); }}
          onCancel={() => { t.onDialogCancel(t.confirmDialog.part.value); }}
        //onReject={() => { console.log('Discarded'); }}
        >
          <div ref={(el) => t.contentConfirmDialog = el as HTMLDivElement}></div>
        </vaadin-confirm-dialog>
      </div>
    );
    /*
    <div ref={(el) => t.containerMain = el as HTMLDivElement} class="content-iframe-container">
              <ui-loading-indicator
                ref={(el) => t.elLoadingIndicator = el as unknown as UILoadingIndicator}
                text='Lüften bekleyiniz...'
                class="content-iframe"
              >
              </ui-loading-indicator>
              <div
                class="weather-indicator-unroro"
              >UNRORO/13:45<br></br>17.00-NE</div>
              <div
                class="weather-indicator-gisas"
              >GISAS/13:45<br></br>17.00-NE</div>
              <div
                class="weather-indicator-gemak"
              >GEMAK/13:45<br></br>17.00-NE</div>
              <iframe
                ref={(el) => t.ifrmMainScreen = el as HTMLIFrameElement}
                class="content-iframe"
                src="https://www.marinetraffic.com/en/ais/embed?zoom=15&centery=40.8455&centerx=29.2770&maptype=1&notation=true&shownames=true&mmsi=0&fleet=&remember=true"
                onLoad={() => {
                  setTimeout(async () => {
                    await t.elLoadingIndicator.setStateMask(true);
                  }, 1000);
                }}
              >
              </iframe>
            </div>
      <vaadin-dialog
          ref={(el) => t.weatherDialog = el as Dialog}
          //headerTitle="deneme"
          id="weatherDialog"
          ariaLabel="weatherDialog"
          //draggable
          modeless
          //resizable
          noCloseOnEsc={true}
          noCloseOnOutsideClick={true}
          opened={false}
          onOpened-changed={(e) => { t.weatherDialogRenderer(e.detail.value); }}
        //headerRenderer={t.dialogHeaderRenderer}
        //renderer={t.dialogRenderer}
        //renderer={() => dialogRenderer(t.dialogRenderer, [])}
        //footerRenderer={t.dialogFooterRenderer}
        >
        </vaadin-dialog>
    */
  }
}
