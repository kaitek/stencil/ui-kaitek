import { Component, Element, Event, EventEmitter, h, Method, Prop } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';
import * as UIIcons from '../ui-icons';
import { IQueryBuilderFilterItem, IQueryBuilderOperatorItem, IQueryBuilderOptions, IQueryBuilderRuleItem, IQueryBuilderRules, QueryBuilderRuleItem, QueryBuilderRules } from '../px-interfaces';
import { Button } from '@vaadin/button';
import '@vaadin/button';
import '@vaadin/checkbox';
import '@vaadin/checkbox-group';
import '@vaadin/combo-box';
import '@vaadin/date-picker';
import '@vaadin/icon';
import '@vaadin/integer-field';
import '@vaadin/item';
import '@vaadin/list-box';
import '@vaadin/multi-select-combo-box';
import '@vaadin/number-field';
import '@vaadin/radio-group';
import '@vaadin/select';
import '@vaadin/text-field';
import { DatePicker } from '@vaadin/date-picker';
@Component({
  tag: 'ui-query-builder',
  styleUrl: 'ui-query-builder.scss',
  shadow: true
})
export class UIQueryBuilder {
  @Element() myElement: HTMLElement;

  @Event({
    eventName: 'appEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) appEvent: EventEmitter;

  @Event({
    eventName: 'rendererEvent',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) rendererEvent: EventEmitter;

  @Event({
    eventName: 'pxQueryBuilderConditionChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxQueryBuilderConditionChange: EventEmitter;

  @Prop() args: any;

  uiFns: UIFunctions = new UIFunctions();

  cntBuilder: HTMLDivElement;
  options: IQueryBuilderOptions;

  isRendered: boolean = false;

  async componentDidLoad() {
    let t = this;
    t.options = t.args?.options;/*|| new QueryBuilderOptions({
      filters: [
        new QueryBuilderFilterItem({
          id: "client"
          , field: "cpd.client"
          , label: "İstasyon"
          , type: "string"
          //, unique: true
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "clientint"
          , field: "cpd.client"
          , label: "İstasyon-Int"
          , type: "integer"
          //, unique: true
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "clientnumber"
          , field: "cpd.client"
          , label: "İstasyon-Number"
          , type: "integer"
          , input: "number"
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "day"
          , field: "cpd.day"
          , label: "Gün"
          , type: "date"
          //, unique: true
          , validation: new QueryBuilderInputValidation({ format: "YYYY-MM-DD" })
          //, input_options: {
          //, plugin: "datepicker"
          //, plugin_config: { format: "yyyy-mm-dd", todayBtn: "linked", todayHighlight: true, autoclose: true, language: "tr" }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "jobrotation"
          , field: "cpd.jobrotation"
          , label: "Vardiya"
          , type: "string"
          , input: "select"
          , input_options: new QueryBuilderInputOptions({
            multiple: true
            , values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
            //, plugin: "selectize"
            //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
          })
          , operators: ["in", "not_in", "is_empty", "is_not_empty", "is_null", "is_not_null"]
        })
        , new QueryBuilderFilterItem({
          id: "jobrotationcheck"
          , field: "cpd.jobrotation"
          , label: "Vardiya-Check"
          , type: "string"
          , input: "checkbox"
          , input_options: new QueryBuilderInputOptions({
            values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
            //, plugin: "selectize"
            //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
          })
          , operators: ["in", "not_in", "is_empty", "is_not_empty", "is_null", "is_not_null"]
        })
        , new QueryBuilderFilterItem({
          id: "jobrotationcombo"
          , field: "cpd.jobrotation"
          , label: "Vardiya-Combo"
          , type: "string"
          , input: "combobox"
          , input_options: new QueryBuilderInputOptions({
            values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
            , path: "id"
            //, plugin: "selectize"
            //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
          })
          , operators: ["in", "not_in", "is_empty", "is_not_empty", "is_null", "is_not_null"]
        })
        , new QueryBuilderFilterItem({
          id: "jobrotationradio"
          , field: "cpd.jobrotation"
          , label: "Vardiya-Radio"
          , type: "string"
          , input: "radio"
          , input_options: new QueryBuilderInputOptions({
            values: [{ "id": "07:30-15:30", "text": "07:30-15:30" }, { "id": "15:30-23:30", "text": "15:30-23:30" }, { "id": "23:30-07:30", "text": "23:30-07:30" }]
            //, plugin: "selectize"
            //, plugin_config: { valueField: "name", labelField: "name", searchField: "name", sortField: "name", options: ' . $jr . ' }
          })
          , operators: ["equal"]
        })
        , new QueryBuilderFilterItem({
          id: "opname"
          , field: "cpd.opname"
          , label: "Operasyon"
          , type: "string"
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
        , new QueryBuilderFilterItem({
          id: "erprefnumber"
          , field: "cpd.erprefnumber"
          , label: "Erp Ref Number"
          , type: "string"
          , input_options: new QueryBuilderInputOptions({
            value_separator: ","
            , size: 30
          })
          , validation: new QueryBuilderInputValidation({ allow_empty_value: false })
          //, description: (rule) => {
          //  if (rule.operator && ['in', 'not_in'].indexOf(rule.operator.type) !== -1) {
          //    return '"içinde" ve "içinde değil" opertörü kullanırken birden fazla değer girmek istediğinizde , kullanabilirsiniz';
          //  }
          //}
        })
      ]
      , rules: [
        new QueryBuilderRules({
          "condition": "AND",
          "rules": [
            new QueryBuilderRuleItem({
              "id": "day",
              "field": "cpd.day",
              "type": "date",
              "input": "date",
              "operator": "between",
              value: "Gün",
              flags: {
                filter_readonly: true,
                no_delete: true
              }
            }),
            new QueryBuilderRuleItem({
              "id": "jobrotation",
              "field": "cpd.jobrotation",
              "type": "string",
              "input": "select",
              "operator": "in",
              "value": "Vardiya"
            }),
            new QueryBuilderRuleItem({
              "id": "jobrotationcheck",
              "field": "cpd.jobrotation",
              "type": "string",
              "input": "checkbox",
              "operator": "in",
              "value": "Vardiya-Check"
            }),
            new QueryBuilderRuleItem({
              "id": "jobrotationradio",
              "field": "cpd.jobrotation",
              "type": "string",
              "input": "radio",
              "operator": "equal",
              "value": "Vardiya-Radio"
            }),
            new QueryBuilderRuleItem({
              "id": "client",
              "field": "cpd.client",
              "type": "string",
              "input": "text",
              "operator": "in",
              "value": "İstasyon"
            }),
            new QueryBuilderRuleItem({
              "id": "clientint",
              "field": "category",
              "type": "number",
              "input": "integer",
              "operator": "equal",
              "value": "İstasyon-Int"
            }),
            new QueryBuilderRuleItem({
              "id": "clientnumber",
              "field": "category",
              "type": "number",
              "input": "integer",
              "operator": "equal",
              "value": "İstasyon-Number"
            }),
            //new QueryBuilderRules({
            //  "condition": "OR",
            //  "rules": [
            //    new QueryBuilderRuleItem({
            //      "id": "client",
            //      "field": "category",
            //      "type": "integer",
            //      "input": "checkbox",
            //      "operator": "in",
            //      "value": [
            //        1,
            //        2
            //      ]
            //    }),
            //    new QueryBuilderRuleItem({
            //      "id": "jobrotation",
            //      "field": "in_stock",
            //      "type": "integer",
            //      "input": "radio",
            //      "operator": "equal",
            //      "value": 0
            //    }),
            //    new QueryBuilderRules({
            //      "condition": "AND",
            //      "rules": [
            //        new QueryBuilderRuleItem({
            //          "id": "opname",
            //          "field": "price",
            //          "type": "double",
            //          "input": "number",
            //          "operator": "less_or_equal",
            //          "value": 200
            //        })
            //      ],
            //      "not": false
            //    })
            //  ],
            //  "not": false
            //}),
            //new QueryBuilderRules({
            //  "condition": "AND",
            //  "rules": [
            //    new QueryBuilderRuleItem({
            //      "id": "erprefnumber",
            //      "field": "in_stock",
            //      "type": "integer",
            //      "input": "radio",
            //      "operator": "equal",
            //      "value": 1
            //    })
            //  ],
            //  "not": true
            //})
          ],
          "not": true,
          "valid": true
        })
      ]
    });*/
    await t.qbRuleParser();
    t.isRendered = true;
  }

  @Method()
  async setButtonStatus(name: string, isDisabled: boolean) {
    let t = this;
    const btnFilter: Button = t.cntBuilder.querySelector("vaadin-button[part=" + name + "]");
    btnFilter.disabled = isDisabled;
  }

  async escapeString(value, additionalEscape) {
    if (typeof value != 'string') {
      return value;
    }

    let escaped = value
      .replace(/[\0\n\r\b\\\'\"]/g, function (s) {
        switch (s) {
          // @formatter:off
          case '\0': return '\\0';
          case '\n': return '\\n';
          case '\r': return '\\r';
          case '\b': return '\\b';
          case '\'': return '\'\'';
          default: return '\\' + s;
          // @formatter:off
        }
      })
      // uglify compliant
      .replace(/\t/g, '\\t')
      .replace(/\x1a/g, '\\Z');

    if (additionalEscape) {
      escaped = escaped
        .replace(new RegExp('[' + additionalEscape + ']', 'g'), function (s) {
          return '\\' + s;
        });
    }

    return escaped;
  }

  async fmt(str, args) {
    if (!Array.isArray(args)) {
      args = Array.prototype.slice.call(arguments, 1);
    }

    return str.replace(/{([0-9]+)}/g, function (_m, i) {
      return args[parseInt(i)];
    });
  }

  //async generateQueryBuilderForm(container: HTMLElement) {
  //  let t=this;
  //  return await t.qbRuleParser( container);
  //}

  async qbRuleParser() {
    let t = this;
    if (t.options.rules.length === 0) {
      t.options.rules = [
        new QueryBuilderRules({
          condition: "AND",
          rules: [
            new QueryBuilderRuleItem({
              "id": "",
              "field": "",
              "type": "",
              "input": "",
              "operator": "",
              "value": ""
            })
          ],
          not: false
        })
      ];
    }
    for (const rule of t.options.rules) {
      await t.qbAddGroup(t.cntBuilder, rule, true);
    }
    return;
  }

  async qbAddGroup(container: HTMLElement, rule: IQueryBuilderRules | IQueryBuilderRuleItem, isFirst: boolean) {
    let t = this;
    //console.log('qbAddGroup', rule, (rule as any).rules.length);
    const buttons: any = [
      { part: 'filter_records', icon: 'fa__filter', text: t.options.lang.filter, theme: 'small primary', hidden: !isFirst }
      , { part: 'filter_clear', icon: 'fa__filter_circle_xmark', text: t.options.lang.clear, theme: 'small primary', hidden: !isFirst }
      , { part: 'add_rule', icon: 'vaadin__plus_circle', text: t.options.lang.add_rule, theme: 'small primary success' }
      , { part: 'add_group', icon: 'vaadin__plus_circle', text: t.options.lang.add_group, theme: 'small primary success' }
      //, { part: 'delete_rule', icon: 'vaadin__minus_circle', text: 'Sil', theme: 'small primary error', hidden: isFirst }
      , { part: 'delete_group', icon: 'vaadin__minus_circle', text: t.options.lang.delete_group, theme: 'small primary error', hidden: isFirst }
    ];
    const gc = document.createElement('div');
    gc.className = "flex flex--col group-container";
    //gc.setAttribute("style", 'padding: 10px;');
    const gh = document.createElement('div');
    gh.className = "flex flex--justify group-header";
    const hleft = document.createElement('div');
    hleft.className = "flex flex--row flex--center flex--stretch";
    hleft.setAttribute("style", 'border: 1px solid #96b7dc;border-radius: 5px;');

    const chkNot = document.createElement('vaadin-checkbox');
    chkNot.label = t.options.lang.NOT;
    chkNot.checked = (rule as IQueryBuilderRules).not;
    chkNot.setAttribute('part', 'chk_not');
    chkNot.setAttribute("style", 'margin-top: 7px;');
    hleft.appendChild(chkNot);

    const btnAnd = document.createElement('vaadin-button');
    btnAnd.addEventListener('click', t.appEvents.bind(t, { type: 'button', element: 'btn_and', container: hleft, el: btnAnd }), { passive: true });
    btnAnd.setAttribute("theme", 'small' + ((rule as IQueryBuilderRules).condition === 'AND' && (rule as any).rules.length > 1 ? ' primary' : ''));
    btnAnd.setAttribute('part', 'btn_and');
    btnAnd.setAttribute("style", 'margin-right: 3px;');
    btnAnd.appendChild(document.createTextNode(t.options.lang.conditions.AND));
    hleft.appendChild(btnAnd);

    const btnOr = document.createElement('vaadin-button');
    btnOr.addEventListener('click', t.appEvents.bind(t, { type: 'button', element: 'btn_or', container: hleft, el: btnOr }), { passive: true });
    btnOr.setAttribute("theme", 'small' + ((rule as IQueryBuilderRules).condition === 'OR' && (rule as any).rules.length > 1 ? ' primary' : ''));
    btnOr.setAttribute('part', 'btn_or');
    btnOr.setAttribute("style", 'margin-right: 3px;');
    btnOr.appendChild(document.createTextNode(t.options.lang.conditions.OR));
    hleft.appendChild(btnOr);

    gh.appendChild(hleft);
    const hright = document.createElement('div');
    hright.className = "flex flex--col flex--center flex--middle";
    const bc = document.createElement('div');
    bc.className = "flex flex--row flex--center flex--stretch";
    for (const item of buttons) {
      if (!item?.hidden) {
        const btn = document.createElement('vaadin-button');
        btn.addEventListener('click', t.appEvents.bind(t, { type: 'button', element: item.part, container: gc, el: btn }), { passive: true });
        btn.setAttribute("theme", item.theme);
        btn.setAttribute('part', item.part);
        btn.setAttribute("style", 'margin-left: 3px;');
        const btnIcon = document.createElement('vaadin-icon');
        btnIcon.setAttribute("slot", "prefix");
        btnIcon.setAttribute("src", UIIcons[item.icon]);
        btnIcon.setAttribute("style", 'width: 16px;');
        btn.appendChild(btnIcon);
        btn.appendChild(document.createTextNode(item.text));
        bc.appendChild(btn);
      }
    }
    hright.appendChild(bc);
    gh.appendChild(hright);
    gc.appendChild(gh);
    const gb = document.createElement('div');
    gb.className = "flex flex--col group-body";
    const rl = document.createElement('div');
    rl.className = "flex flex--col rules-list";
    rl.setAttribute("style", 'padding-left: 15px;');
    gb.appendChild(rl);
    gc.appendChild(gb);
    container.appendChild(gc);
    for (const crule of (rule as IQueryBuilderRules).rules) {
      if (typeof (crule as any).condition !== 'undefined') {
        await t.qbAddGroup(rl, crule, false);
      } else {
        await t.qbAddRule(rl, crule);
      }
    }
  }

  async qbAddRule(container: HTMLElement, rule: IQueryBuilderRuleItem | IQueryBuilderRules) {
    let t = this;
    //console.log('qbAddRule', rule);
    const rc = document.createElement('div');
    rc.className = "flex flex--justify rule-container";

    const rleft = document.createElement('div');
    rleft.className = "flex flex--row flex--center flex--stretch";

    const elField = document.createElement('vaadin-combo-box');
    elField.setAttribute('dataIndex', (rule as IQueryBuilderRuleItem).id);
    let isReadonly: boolean = false;
    let isNoDelete: boolean = false;
    if (typeof (rule as IQueryBuilderRuleItem).flags !== 'undefined') {
      if (typeof ((rule as IQueryBuilderRuleItem).flags as any).filter_readonly !== 'undefined') {
        isReadonly = ((rule as IQueryBuilderRuleItem).flags as any).filter_readonly;
      }
      if (typeof ((rule as IQueryBuilderRuleItem).flags as any).no_delete !== 'undefined') {
        isNoDelete = ((rule as IQueryBuilderRuleItem).flags as any).no_delete;
      }
    }
    if (isReadonly) {
      elField.setAttribute('readonly', "true");
    } else {
      elField.setAttribute('clear-button-visible', "true");
    }
    elField.setAttribute('placeholder', t.options.select_placeholder);
    elField.setAttribute('part', "id");
    elField.setAttribute('required', "true");
    elField.setAttribute('items', JSON.stringify(t.options.filter_combo_items));
    elField.setAttribute("style", 'margin-right: 3px;');
    if (typeof (rule as IQueryBuilderRuleItem).value !== 'undefined' && (rule as IQueryBuilderRuleItem).value !== '') {
      elField.setAttribute("value", (rule as IQueryBuilderRuleItem).value.toString());
    }
    elField.addEventListener('value-changed', t.appEvents.bind(t, { type: 'vaadin-select', element: "id", container: rc, el: elField }), { passive: true });
    rleft.appendChild(elField);

    rc.appendChild(rleft);

    if (!t.isRendered && (rule as IQueryBuilderRuleItem).value.toString() !== '') {
      await t.qbAddOperator(rleft, (rule as IQueryBuilderRuleItem).value.toString(), (rule as IQueryBuilderRuleItem).operator);
    }

    const rright = document.createElement('div');
    rright.className = "flex flex--col flex--center flex--middle";

    if (!isNoDelete) {
      const btn = document.createElement('vaadin-button');
      btn.addEventListener('click', t.appEvents.bind(t, { type: 'button', element: 'delete_rule', container: rc, el: btn }), { passive: true });
      btn.setAttribute("theme", 'small primary error');
      btn.setAttribute('part', 'delete_rule');
      btn.setAttribute("style", 'margin-left: 3px;');
      const btnIcon = document.createElement('vaadin-icon');
      btnIcon.setAttribute("slot", "prefix");
      btnIcon.setAttribute("src", UIIcons['vaadin__minus_circle']);
      btnIcon.setAttribute("style", 'width: 16px;');
      btn.appendChild(btnIcon);
      btn.appendChild(document.createTextNode(t.options.lang.delete_rule));
      rright.appendChild(btn);
    }

    rc.appendChild(rright);

    container.appendChild(rc);
  }

  async qbAddOperator(container: HTMLElement, filterName: string, operator: string) {
    let t = this;
    let filter: IQueryBuilderFilterItem;
    for (const item of t.options.filters) {
      if (item.label === filterName) {
        filter = item;
      }
    }
    let operatorItems: any[] = [];
    let optgroup: string = '-';
    let firstSelectableIndex: number = -1;
    if (typeof filter.operators !== 'undefined' && filter.operators !== null) {
      for (const item of t.options.operators) {
        if (filter.operators.indexOf(item.type) !== -1) {
          if (typeof item.optgroup !== 'undefined' && item.optgroup !== optgroup) {
            if (optgroup !== '-') {
              operatorItems = [...operatorItems, {
                component: 'hr'
              }];
            }
            if (item.optgroup !== null) {
              operatorItems = [...operatorItems, {
                label: item.optgroup,
                value: item.optgroup,
                disabled: true
              }];
            }
            optgroup = item.optgroup;
          }
          if (firstSelectableIndex === -1) {
            firstSelectableIndex = operatorItems.length;
          }
          operatorItems = [...operatorItems, {
            label: t.options.lang.operators[item.type],
            value: item.type,
            nb_inputs: item.nb_inputs,
            multiple: item.multiple
          }];
        }
      }
    } else {
      for (const item of t.options.operators) {
        if (item.apply_to.indexOf(t.options.types[filter.type]) !== -1) {
          if (typeof item.optgroup !== 'undefined' && item.optgroup !== optgroup) {
            if (optgroup !== '-') {
              operatorItems = [...operatorItems, {
                component: 'hr'
              }];
            }
            if (item.optgroup !== null) {
              operatorItems = [...operatorItems, {
                label: item.optgroup,
                value: item.optgroup,
                disabled: true
              }];
            }
            optgroup = item.optgroup;
          }
          if (firstSelectableIndex === -1) {
            firstSelectableIndex = operatorItems.length;
          }
          operatorItems = [...operatorItems, {
            label: t.options.lang.operators[item.type],
            value: item.type,
            nb_inputs: item.nb_inputs,
            multiple: item.multiple
          }];
        }
      }
    }
    //console.log('operator:', operator, operatorItems)
    if (operator !== '') {
      const idx: number = await t.uiFns.getIndex(operatorItems, 'value', operator);
      if (idx !== -1) {
        firstSelectableIndex = idx;
      }
    }
    const currentOperator: any = operatorItems[firstSelectableIndex];
    //console.log('qbAddOperator', filterName, options, filter, currentOperator, firstSelectableIndex);
    if (currentOperator) {
      const elOperator = document.createElement('vaadin-select');
      elOperator.setAttribute('dataIndex', currentOperator.value);
      elOperator.setAttribute('value', currentOperator.value);
      if (operatorItems.length - 1 === firstSelectableIndex) {
        elOperator.setAttribute('readonly', "true");
      }
      //elOperator.setAttribute('clear-button-visible', "true");
      //elOperator.setAttribute('placeholder', options.select_placeholder);
      elOperator.setAttribute('part', "operator");
      elOperator.setAttribute('required', "true");
      elOperator.setAttribute('items', JSON.stringify(operatorItems));
      elOperator.setAttribute("style", 'margin-right: 3px;');
      elOperator.addEventListener('value-changed', t.appEvents.bind(t, { type: 'vaadin-select', element: "operator", container, el: elOperator, filter }), { passive: true });
      container.appendChild(elOperator);
      if (!t.isRendered) {
        await t.qbAddRuleInput(container, filter, currentOperator.value);
      }
    }
  }

  async qbAddRuleInput(container: HTMLElement, filter: IQueryBuilderFilterItem, operator: string) {
    let t = this;
    let currentOperator: IQueryBuilderOperatorItem;
    //console.log('qbAddRuleInput', operator, t.options.operators);
    for (const item of t.options.operators) {
      //console.log('for', item);
      if (item.type === operator) {
        currentOperator = item;
      }
    }
    if (currentOperator.nb_inputs !== 0) {
      //console.log('qbAddRuleInput', filter, currentOperator);
      const key: string = (filter?.input || filter?.type || '');
      switch (key) {
        case 'checkbox': {
          const elRule = document.createElement('vaadin-checkbox-group');
          elRule.setAttribute('dataIndex', filter.id);
          elRule.setAttribute('part', "filtervalue");
          elRule.setAttribute('required', "true");
          for (const row of (filter.input_options.values as any[])) {
            const elItem = document.createElement('vaadin-checkbox');
            elItem.setAttribute('value', row.id);
            elItem.setAttribute('label', row.text);
            elRule.appendChild(elItem);
          }
          container.appendChild(elRule);
          break;
        }
        case 'combobox': {
          const elRule = document.createElement('vaadin-multi-select-combo-box');
          elRule.setAttribute('dataIndex', filter.id);
          elRule.setAttribute('part', "filtervalue");
          elRule.setAttribute('auto-expand-horizontally', "true");
          elRule.setAttribute('auto-expand-vertically', "true");
          elRule.setAttribute('required', "true");
          elRule.setAttribute('item-label-path', "text");
          elRule.setAttribute('item-id-path', "id");
          elRule.setAttribute('items', JSON.stringify(filter.input_options.values));
          container.appendChild(elRule);
          break;
        }
        case 'date': {
          if (currentOperator.nb_inputs >= 1) {
            const elRule1 = document.createElement('vaadin-date-picker');
            elRule1.setAttribute('dataIndex', filter.id + (currentOperator.nb_inputs === 2 ? '|s' : ''));
            elRule1.setAttribute('part', "filtervalue");
            elRule1.setAttribute('auto-open-disabled', "true");
            elRule1.setAttribute('clear-button-visible', "true");
            elRule1.setAttribute('show-week-numbers', "true");
            elRule1.setAttribute('required', "true");
            elRule1.setAttribute('class', "focusable");
            elRule1.setAttribute("style", 'width: 160px;margin-right: 3px;');
            container.appendChild(elRule1);
          }
          if (currentOperator.nb_inputs === 2) {
            const elRule2 = document.createElement('vaadin-date-picker');
            elRule2.setAttribute('part', "filtervalue");
            elRule2.setAttribute('dataIndex', filter.id + (currentOperator.nb_inputs === 2 ? '|f' : ''));
            elRule2.setAttribute('auto-open-disabled', "true");
            elRule2.setAttribute('clear-button-visible', "true");
            elRule2.setAttribute('show-week-numbers', "true");
            elRule2.setAttribute('required', "true");
            elRule2.setAttribute('class', "focusable");
            elRule2.setAttribute("style", 'width: 160px;margin-right: 3px;');
            container.appendChild(elRule2);
          }
          const dp: NodeListOf<DatePicker> = container.querySelectorAll("vaadin-date-picker");
          await t.uiFns.asyncForEach(Array.from(dp), async (item: DatePicker) => {
            item.i18n = {
              ...item.i18n,
              formatDate: t.uiFns.dateFormatIso8601,
              parseDate: t.uiFns.dateParseIso8601,
              firstDayOfWeek: 1,
              monthNames: [
                'Ocak',
                'Şubat',
                'Mart',
                'Nisan',
                'Mayıs',
                'Haziran',
                'Temmuz',
                'Ağustos',
                'Eylül',
                'Ekim',
                'Kasım',
                'Aralık',
              ],
              weekdays: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
              weekdaysShort: ['Paz', 'Pzt', 'Sal', 'Çrş', 'Prş', 'Cum', 'Cmt'],
              today: 'Bugün',
              cancel: 'Vazgeç',
            };
          });
          break;
        }
        case 'integer': {
          if (currentOperator.nb_inputs >= 1) {
            const elRule1 = document.createElement('vaadin-integer-field');
            elRule1.setAttribute('dataIndex', filter.id + (currentOperator.nb_inputs === 2 ? '|s' : ''));
            elRule1.setAttribute('part', "filtervalue");
            elRule1.setAttribute('clear-button-visible', "true");
            elRule1.setAttribute('required', "true");
            elRule1.setAttribute("style", 'margin-right: 3px;');
            container.appendChild(elRule1);
          }
          if (currentOperator.nb_inputs === 2) {
            const elRule2 = document.createElement('vaadin-integer-field');
            elRule2.setAttribute('dataIndex', filter.id + (currentOperator.nb_inputs === 2 ? '|f' : ''));
            elRule2.setAttribute('part', "filtervalue");
            elRule2.setAttribute('clear-button-visible', "true");
            elRule2.setAttribute('required', "true");
            elRule2.setAttribute("style", 'margin-right: 3px;');
            container.appendChild(elRule2);
          }
          break;
        }
        case 'number': {
          if (currentOperator.nb_inputs >= 1) {
            const elRule1 = document.createElement('vaadin-number-field');
            elRule1.setAttribute('dataIndex', filter.id + (currentOperator.nb_inputs === 2 ? '|s' : ''));
            elRule1.setAttribute('part', "filtervalue");
            elRule1.setAttribute('clear-button-visible', "true");
            elRule1.setAttribute('required', "true");
            elRule1.setAttribute("style", 'margin-right: 3px;');
            container.appendChild(elRule1);
          }
          if (currentOperator.nb_inputs === 2) {
            const elRule2 = document.createElement('vaadin-number-field');
            elRule2.setAttribute('dataIndex', filter.id + (currentOperator.nb_inputs === 2 ? '|f' : ''));
            elRule2.setAttribute('part', "filtervalue");
            elRule2.setAttribute('clear-button-visible', "true");
            elRule2.setAttribute('required', "true");
            elRule2.setAttribute("style", 'margin-right: 3px;');
            container.appendChild(elRule2);
          }
          break;
        }
        case 'radio': {
          const elRule = document.createElement('vaadin-radio-group');
          elRule.setAttribute('dataIndex', filter.id);
          elRule.setAttribute('part', "filtervalue");
          elRule.setAttribute('required', "true");
          for (const row of (filter.input_options.values as any[])) {
            const elItem = document.createElement('vaadin-radio-button');
            elItem.setAttribute('value', row.id);
            elItem.setAttribute('label', row.text);
            elRule.appendChild(elItem);
          }
          container.appendChild(elRule);
          break;
        }
        case 'select': {
          const elRule = document.createElement('vaadin-list-box');
          elRule.setAttribute('dataIndex', filter.id);
          elRule.setAttribute('part', "filtervalue");
          elRule.setAttribute('required', "true");
          //elRule.setAttribute('data', JSON.stringify(filter.input_options.values));
          if (typeof filter.input_options !== 'undefined' && typeof filter.input_options.multiple !== 'undefined') {
            elRule.setAttribute('multiple', filter.input_options.multiple.toString());
          }
          for (const row of (filter.input_options.values as any[])) {
            const elItem = document.createElement('vaadin-item');
            elItem.appendChild(document.createTextNode(row.text));
            elRule.appendChild(elItem);
          }
          elRule.setAttribute("style", 'height: 100px;');
          container.appendChild(elRule);
          break;
        }
        case 'string': {
          if (currentOperator.nb_inputs >= 1) {
            const elRule1 = document.createElement('vaadin-text-field');
            elRule1.setAttribute('dataIndex', filter.id + (currentOperator.nb_inputs === 2 ? '|s' : ''));
            elRule1.setAttribute('part', "filtervalue");
            elRule1.setAttribute('clear-button-visible', "true");
            elRule1.setAttribute('required', "true");
            elRule1.setAttribute("style", 'margin-right: 3px;');
            container.appendChild(elRule1);
          }
          if (currentOperator.nb_inputs === 2) {
            const elRule2 = document.createElement('vaadin-text-field');
            elRule2.setAttribute('dataIndex', filter.id + (currentOperator.nb_inputs === 2 ? '|f' : ''));
            elRule2.setAttribute('part', "filtervalue");
            elRule2.setAttribute('clear-button-visible', "true");
            elRule2.setAttribute('required', "true");
            elRule2.setAttribute("style", 'margin-right: 3px;');
            container.appendChild(elRule2);
          }
          break;
        }
        default: {
          console.log('default-qbAddRuleInput', filter);
          break;
        }
      }
    }
  }

  async qbValidateItems() {
    let t = this;
    let isInvalid = false;
    const ruleLists: NodeListOf<ChildNode> = t.cntBuilder.querySelectorAll('div.rules-list');
    await t.uiFns.asyncForEach(ruleLists, async (itemRule: any) => {
      const groupContainer: HTMLDivElement = (itemRule.parentNode.parentNode as HTMLDivElement);
      if (itemRule.childNodes.length === 0) {
        isInvalid = true;
        if (!groupContainer.classList.contains('has-error')) {
          groupContainer.classList.add('has-error');
        }
      } else {
        if (groupContainer.classList.contains('has-error')) {
          groupContainer.classList.remove('has-error');
        }
      }
    });
    const ruleContainers: NodeListOf<ChildNode> = t.cntBuilder.querySelectorAll('div.rule-container');
    await t.uiFns.asyncForEach(ruleContainers, async (itemContainer: any) => {
      if (itemContainer.childNodes[0].childNodes.length === 1) {
        isInvalid = true;
        if (!itemContainer.classList.contains('has-error')) {
          itemContainer.classList.add('has-error');
        }
      } else {
        if (itemContainer.classList.contains('has-error')) {
          itemContainer.classList.remove('has-error');
        }
      }
    });
    const formElements: NodeListOf<ChildNode> = t.cntBuilder.querySelectorAll('[part="filtervalue"]');
    //let formData: any = {};
    await t.uiFns.asyncForEach(formElements, async (item: any) => {
      if (typeof item.getAttribute === 'function') {
        const dataIndex: string = item.getAttribute('dataIndex');
        if (dataIndex !== null && dataIndex !== '') {
          if (typeof item.focus === 'function') {
            item.focus();
          }
          if (typeof item.blur === 'function') {
            item.blur();
          }
          const tagName: string = item.tagName.toLowerCase();
          switch (tagName) {
            case 'vaadin-checkbox-group': {
              if (item.value.length === 0) {
                item.invalid = true;
              } else {
                item.invalid = false;
              }
              break;
            }
            case 'vaadin-list-box': {
              if (item.selectedValues.length === 0) {
                item.invalid = true;
              } else {
                item.invalid = false;
              }
              break;
            }
            case 'vaadin-radio-group': {
              if (item.value === '') {
                item.invalid = true;
              } else {
                item.invalid = false;
              }
              break;
            }
            default:
              break;
          }
          const ruleContainer: HTMLDivElement = (item.parentNode.parentNode as HTMLDivElement);
          if (item.invalid) {
            isInvalid = true;
            if (!ruleContainer.classList.contains('has-error')) {
              ruleContainer.classList.add('has-error');
            }
          } else {
            if (ruleContainer.classList.contains('has-error')) {
              ruleContainer.classList.remove('has-error');
            }
          }
          /*
          if (tagName === "vaadin-integer-field") {
            formData[dataIndex] = parseInt(item.value, 10);
          } else if (tagName === "vaadin-checkbox") {
            formData[dataIndex] = item.checked;
          } else if (tagName === "vaadin-list-box") {
            formData[dataIndex] = [];
            for (const index of item.selectedValues) {
              formData[dataIndex] = [...formData[dataIndex], item.items[index].textContent];
            }
          } else {
            const format: string = item.getAttribute('format');
            if (format === 'money') {
              formData[dataIndex] = item.value.split('.').join('').split(',').join('.');
            } else {
              formData[dataIndex] = item.value;
            }
          }
          */
        }
      }
    });
    //return { isInvalid, data: formData };
    return isInvalid;
  }

  async qbGetSql() {
    let t = this;
    //statement:named,named,false
    let statement: string | boolean = (t.args?.statement || 'named');//question_mark,named,false
    const groupRulesContainer: any = t.cntBuilder.childNodes[0];
    const sqlOperators: any = {
      equal: {
        op: "= ?"
      },
      not_equal: {
        op: "!= ?"
      },
      in: {
        op: "IN(?)",
        sep: ", "
      },
      not_in: {
        op: "NOT IN(?)",
        sep: ", "
      },
      less: {
        op: "< ?"
      },
      less_or_equal: {
        op: "<= ?"
      },
      greater: {
        op: "> ?"
      },
      greater_or_equal: {
        op: ">= ?"
      },
      between: {
        op: "BETWEEN ?",
        sep: " AND "
      },
      not_between: {
        op: "NOT BETWEEN ?",
        sep: " AND "
      },
      begins_with: {
        op: "LIKE(?)",
        mod: "{0}%"
      },
      not_begins_with: {
        op: "NOT LIKE(?)",
        mod: "{0}%"
      },
      contains: {
        op: "LIKE(?)",
        mod: "%{0}%"
      },
      not_contains: {
        op: "NOT LIKE(?)",
        mod: "%{0}%"
      },
      ends_with: {
        op: "LIKE(?)",
        mod: "%{0}"
      },
      not_ends_with: {
        op: "NOT LIKE(?)",
        mod: "%{0}"
      },
      is_empty: {
        op: "= ''"
      },
      is_not_empty: {
        op: "!= ''"
      },
      is_null: {
        op: "IS NULL"
      },
      is_not_null: {
        op: "IS NOT NULL"
      }
    };
    const sqlStatements: any = {
      'question_mark': function () {
        var params = [];
        return {
          add: function (_rule, value) {
            params.push(value);
            return '?';
          },
          run: function () {
            return params;
          }
        };
      },
      'numbered': function (char) {
        if (!char || char.length > 1) char = '$';
        var index = 0;
        var params = [];
        return {
          add: function (_rule, value) {
            params.push(value);
            index++;
            return char + index;
          },
          run: function () {
            return params;
          }
        };
      },
      'named': function (char) {
        if (!char || char.length > 1) char = ':';
        var indexes = {};
        var params = {};
        return {
          add: function (rule, value) {
            if (!indexes[rule.field]) indexes[rule.field] = 1;
            var key = rule.field + '_' + (indexes[rule.field]++);
            params[key] = value;
            return char + key;
          },
          run: function () {
            return params;
          }
        };
      }
    };
    const parse = async (groupContainer: any) => {
      const groupCheck = await groupContainer.querySelector('vaadin-checkbox[part="chk_not"]');
      const isNot: boolean = groupCheck.checked;
      let condition: string = t.options.default_condition;
      const btnAnd: Button = await groupContainer.querySelector('vaadin-button[part="btn_and"]');
      if (btnAnd.getAttribute('theme').indexOf('primary') !== -1) {
        condition = 'AND';
      }
      const btnOr: Button = await groupContainer.querySelector('vaadin-button[part="btn_or"]');
      if (btnOr.getAttribute('theme').indexOf('primary') !== -1) {
        condition = 'OR';
      }
      const groupRules = groupContainer.childNodes[1].childNodes[0];
      if (groupRules.childNodes.length === 0) {
        return '';
      }
      let parts = [];
      //console.log(groupContainer, parts, isNot, condition);
      for (const row of groupRules.childNodes) {
        if ((row as HTMLElement).classList.contains('group-container')) {
          const groupCondition = await parse(row);
          parts.push('(' + groupCondition + ')');
        } else {
          const elId = await row.querySelector('vaadin-combo-box[part="id"]');
          const elOperator = await row.querySelector('vaadin-select[part="operator"]');
          let itemFilter: IQueryBuilderFilterItem;
          const idxFilter: number = await t.uiFns.getIndex(t.options.filters, 'label', elId.value);
          if (idxFilter !== -1) {
            itemFilter = t.options.filters[idxFilter];
          }
          let itemOperator: IQueryBuilderOperatorItem;
          const idxOperator: number = await t.uiFns.getIndex(t.options.operators, 'type', elOperator.value);
          if (idxOperator !== -1) {
            itemOperator = t.options.operators[idxOperator];
          }
          if (itemFilter !== null && itemOperator !== null) {
            const sqlOp = sqlOperators[elOperator.value];
            let value: string = '';
            if (itemOperator.nb_inputs !== 0) {
              let values: any[] = [];
              const key: string = (itemFilter?.input || itemFilter?.type || '');
              switch (key) {
                case 'checkbox': {
                  const elItem = await row.querySelector('vaadin-checkbox-group[dataindex="' + itemFilter.id + '"]');
                  for (const irow of elItem.value) {
                    values = [...values, irow];
                  }
                  break;
                }
                case 'combobox': {
                  //console.log('path:', itemFilter?.input_options?.path)
                  const elItem = await row.querySelector('vaadin-multi-select-combo-box[dataindex="' + itemFilter.id + '"]');
                  for (const irow of elItem.selectedItems) {
                    values = [...values, irow[itemFilter?.input_options?.path || 'text']];
                  }
                  break;
                }
                case 'date': {
                  if (itemOperator.nb_inputs >= 1) {
                    const elItem = await row.querySelector('vaadin-date-picker[dataindex="' + itemFilter.id + (itemOperator.nb_inputs === 2 ? '|s' : '') + '"]');
                    values = [...values, elItem.value];
                  }
                  if (itemOperator.nb_inputs === 2) {
                    const elItem = await row.querySelector('vaadin-date-picker[dataindex="' + itemFilter.id + (itemOperator.nb_inputs === 2 ? '|f' : '') + '"]');
                    values = [...values, elItem.value];
                  }
                  break;
                }
                case 'integer': {
                  if (itemOperator.nb_inputs >= 1) {
                    const elItem = await row.querySelector('vaadin-integer-field[dataindex="' + itemFilter.id + (itemOperator.nb_inputs === 2 ? '|s' : '') + '"]');
                    values = [...values, elItem.value];
                  }
                  if (itemOperator.nb_inputs === 2) {
                    const elItem = await row.querySelector('vaadin-integer-field[dataindex="' + itemFilter.id + (itemOperator.nb_inputs === 2 ? '|f' : '') + '"]');
                    values = [...values, elItem.value];
                  }
                  break;
                }
                case 'number': {
                  if (itemOperator.nb_inputs >= 1) {
                    const elItem = await row.querySelector('vaadin-number-field[dataindex="' + itemFilter.id + (itemOperator.nb_inputs === 2 ? '|s' : '') + '"]');
                    values = [...values, elItem.value];
                  }
                  if (itemOperator.nb_inputs === 2) {
                    const elItem = await row.querySelector('vaadin-number-field[dataindex="' + itemFilter.id + (itemOperator.nb_inputs === 2 ? '|f' : '') + '"]');
                    values = [...values, elItem.value];
                  }
                  break;
                }
                case 'radio': {
                  const elItem = await row.querySelector('vaadin-radio-group[dataindex="' + itemFilter.id + '"]');
                  values = [...values, elItem.value];
                  break;
                }
                case 'select': {
                  const elItem = await row.querySelector('vaadin-list-box[dataindex="' + itemFilter.id + '"]');
                  for (const index of elItem.selectedValues) {
                    values = [...values, elItem.items[index].textContent];
                  }
                  break;
                }
                case 'string': {
                  if (itemOperator.nb_inputs >= 1) {
                    const elItem = await row.querySelector('vaadin-text-field[dataindex="' + itemFilter.id + (itemOperator.nb_inputs === 2 ? '|s' : '') + '"]');
                    values = [...values, elItem.value];
                  }
                  if (itemOperator.nb_inputs === 2) {
                    const elItem = await row.querySelector('vaadin-text-field[dataindex="' + itemFilter.id + (itemOperator.nb_inputs === 2 ? '|f' : '') + '"]');
                    values = [...values, elItem.value];
                  }
                  break;
                }
                default: {
                  console.log('default-parse', itemOperator);
                  break;
                }
              }

              let counter: number = 0;
              for (let itemValue of values) {
                if (counter > 0) {
                  value += sqlOp.sep;
                }
                let boolean_as_integer: boolean = false;
                if (itemFilter.type == 'boolean' && boolean_as_integer) {
                  itemValue = itemValue ? 1 : 0;
                }
                else if (!stmt && itemFilter.type !== 'integer' && itemFilter.type !== 'double' && itemFilter.type !== 'boolean') {
                  itemValue = await t.escapeString(itemValue, sqlOp.escape);
                }
                if (sqlOp.mod) {
                  itemValue = await t.fmt(sqlOp.mod, itemValue);
                }
                if (stmt && typeof stmt.add === 'function') {
                  value += stmt.add(itemFilter, itemValue);
                }
                else {
                  if (typeof itemValue === 'string') {
                    itemValue = '\'' + itemValue + '\'';
                  }
                  value += itemValue;
                }
                counter++;
              }
              //console.log(itemFilter, itemOperator, sqlOp, values, value);
              parts.push(itemFilter.field + ' ' + sqlOp.op.replace('?', value));
            }
          }
        }
      }
      const str = parts.join(' ' + condition + ' ');
      return isNot ? ' NOT ( ' + str + ' ) ' : str;
    };
    let stmt: any;
    if (statement === true) {
      statement = 'question_mark';
    }
    if (typeof statement == 'string') {
      stmt = sqlStatements[statement]();
    }
    //let self = this;
    let sql = await parse(groupRulesContainer);
    //console.log('qbGetSql', {
    //  sql,
    //  params: stmt.run()
    //});
    if (stmt) {
      return {
        sql,
        params: stmt.run()
      };
    } else {
      return {
        sql
      };
    }
  }

  async appEvents(obj: any) {
    let t = this;
    //if (Build.isDev) {
    //  console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
    //}
    if (t.isRendered) {
      switch (obj.type) {
        case 'button': {
          switch (obj.element) {
            case 'btn_and': {
              if (obj.el.theme.indexOf('primary') === -1) {
                const btnPrimary = obj.container.querySelector('vaadin-button[theme*="primary"]');
                if (btnPrimary) {
                  btnPrimary.setAttribute("theme", 'small');
                }
              }
              if (obj.container.parentNode.nextSibling.childNodes[0].childNodes.length > 1) {
                obj.el.setAttribute("theme", 'small primary');
              }
              break;
            }
            case 'btn_or': {
              if (obj.el.theme.indexOf('primary') === -1) {
                const btnPrimary = obj.container.querySelector('vaadin-button[theme*="primary"]');
                if (btnPrimary) {
                  btnPrimary.setAttribute("theme", 'small');
                }
              }
              if (obj.container.parentNode.nextSibling.childNodes[0].childNodes.length > 1) {
                obj.el.setAttribute("theme", 'small primary');
              }
              break;
            }
            case 'add_rule': {
              const rule: IQueryBuilderRuleItem = new QueryBuilderRuleItem({
                "id": "",
                "field": "",
                "type": "",
                "input": "",
                "operator": "",
                "value": ""
              });
              await t.qbAddRule(obj.container.childNodes[1].childNodes[0], rule);
              if (obj.container.childNodes[1].childNodes[0].childNodes.length === 2) {
                const btnPrimary = obj.container.childNodes[0].childNodes[0].querySelector('vaadin-button[theme="small"]');
                if (btnPrimary) {
                  btnPrimary.setAttribute("theme", 'small primary');
                }
              }
              break;
            }
            case 'add_group': {
              const rule: IQueryBuilderRules = new QueryBuilderRules({
                "condition": "AND",
                "rules": [
                  new QueryBuilderRuleItem({
                    "id": "",
                    "field": "",
                    "type": "",
                    "input": "",
                    "operator": "",
                    "value": ""
                  })
                ],
                "not": false
              });
              await t.qbAddGroup(obj.container.childNodes[1].childNodes[0], rule, false);
              if (obj.container.childNodes[1].childNodes[0].childNodes.length === 2) {
                const btnPrimary = obj.container.childNodes[0].childNodes[0].querySelector('vaadin-button[theme="small"]');
                if (btnPrimary) {
                  btnPrimary.setAttribute("theme", 'small primary');
                }
              }
              break;
            }
            case 'delete_rule': {
              if (obj.container.parentNode.childNodes.length <= 2) {
                const btnPrimary = obj.container.parentNode.parentNode.previousSibling.childNodes[0].querySelector('vaadin-button[theme*="primary"]');
                if (btnPrimary) {
                  btnPrimary.setAttribute("theme", 'small');
                }
              }
              obj.container.remove();
              break;
            }
            case 'delete_group': {
              if (obj.container.parentNode.childNodes.length <= 2) {
                const btnPrimary = obj.container.parentNode.parentNode.previousSibling.childNodes[0].querySelector('vaadin-button[theme*="primary"]');
                if (btnPrimary) {
                  btnPrimary.setAttribute("theme", 'small');
                }
              }
              obj.container.remove();
              break;
            }
            case 'filter_clear': {
              t.isRendered = false;
              t.cntBuilder.childNodes[0].remove();
              await t.qbRuleParser();
              t.isRendered = true;
              break;
            }
            case 'filter_records': {
              const isInvalid: boolean = await t.qbValidateItems();
              if (!isInvalid) {
                obj.el.disabled = true;
                const conditions: any = await t.qbGetSql();
                t.pxQueryBuilderConditionChange.emit(conditions);
                //@@info: silinecek
                //console.log(t.myElement.tagName.toLowerCase(), 'conditions:', conditions);
              } else {
                t.rendererEvent.emit({
                  event: 'showNotification'
                  , component: t.myElement.tagName.toLowerCase()
                  , idx: t.args.idx
                  , message: 'Gerekli alanları eksiksiz olarak doldurunuz.'
                });
              }
              break;
            }
            default: {
              console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
              break;
            }
          }
          break;
        }
        case 'vaadin-select': {
          switch (obj.element) {
            case 'id': {
              let parentNode = obj.el.parentNode;
              while (parentNode.childNodes.length > 1) {
                const item = obj.el.parentNode.childNodes[obj.el.parentNode.childNodes.length - 1];
                item.remove();
              }
              if (obj.el.value !== '') {
                //console.log('appEvents-id', obj, obj.el.value);
                await t.qbAddOperator(obj.el.parentNode, obj.el.value, '');
              }
              break;
            }
            case 'operator': {
              let parentNode = obj.el.parentNode;
              while (parentNode.childNodes.length > 2) {
                const item = obj.el.parentNode.childNodes[obj.el.parentNode.childNodes.length - 1];
                item.remove();
              }
              if (obj.el.value !== '') {
                obj.el.setAttribute('dataIndex', obj.el.value);
                obj.el.setAttribute('value', obj.el.value);
                //console.log('appEvents-operator', obj, obj.el.value);
                await t.qbAddRuleInput(obj.el.parentNode, obj.filter, obj.el.value);
              }
              break;
            }
            default: {
              console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
              break;
            }
          }
          break;
        }
        default: {
          console.log('appEvents', t.myElement.tagName.toLowerCase(), obj);
          break;
        }
      }
    }
  }

  render() {
    let t = this;
    return (
      <div class="container">
        <div ref={(el) => t.cntBuilder = el as HTMLDivElement} class="query-builder">

        </div>
        <div id="result" class="hidden">
          <h3>Output</h3>
          <pre></pre>
        </div>
      </div>
    );
  }
}
