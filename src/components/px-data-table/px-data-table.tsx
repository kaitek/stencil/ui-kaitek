import { Build, Component, Element, Event, EventEmitter, h, Method, Prop } from '@stencil/core';
import { UIFunctions } from '../ui-functions/ui-functions';
import { PxToggle } from '../px-toggle/px-toggle';
import { DataTableColumn } from '../px-interfaces';

import '@vaadin/combo-box';
import '@vaadin/date-picker';
import '@vaadin/icon';
//import '@vaadin/icons';
import '@vaadin/number-field';
import '@vaadin/radio-group';
import '@vaadin/text-field';
import * as UIIcons from '../ui-icons';

import type { ComboBox } from '@vaadin/combo-box';
import type { DatePicker } from '@vaadin/date-picker';//DatePickerChangeEvent
import { NumberField } from '@vaadin/number-field';
import type { RadioButton } from '@vaadin/radio-group/vaadin-radio-button';
import { TextField } from '@vaadin/text-field';

import { debounce } from "ts-debounce";
import { Icon } from '@vaadin/icon';

@Component({
  tag: 'px-data-table',
  styleUrl: 'px-data-table.scss',
  shadow: true
})
export class PxDataTable {
  @Element() myElement: HTMLElement;

  @Prop() columns: DataTableColumn[] = [];
  @Prop() data: any[] = [];
  @Prop() clsTable: string = 'table--large';
  @Prop() columnRenderer: string = 'default';
  @Prop() autoClearSelection: boolean = false;
  @Prop() hidden: boolean = false;
  @Prop() selectionMode: 'none' | 'single' | 'multi' | 'cell' = 'none';
  @Prop() isSelectAll: boolean = false;
  @Prop() isReadOnly: boolean = false;
  @Prop() isGroupedRows: boolean = false;
  @Prop() arrGroupFields: string[] = [];
  @Prop() arrConcatFields: any[] = [];
  @Prop() isGroupedSelect: boolean = false;
  @Prop() groupFieldName: string = '';
  @Prop() isEditable: boolean = false;
  @Prop() isFillCell: boolean = false;
  @Prop() calculations: any[] = [];
  @Prop() totals: any[] = [];
  @Prop() isSortable: boolean = false;
  @Prop() isSortMultiple: boolean = false;

  uiFns: UIFunctions = new UIFunctions();

  containerTable: HTMLTableElement;
  selectAllToggle: PxToggle;
  timerClearSelectedRows: any;
  counter: number = 0;
  toggleColumn: any = null;
  isEventListenerReady: boolean = false;

  selectedRows: any[] = [];
  rowModel: any[] = [];// klavye ile gezerken sıradaki elemanı belirlemede kullanılıyor
  rowEvents: string[] = [];
  calculatedRows: any[] = [];
  totaledCols: any[] = [];
  clickTimeout: any;

  @Event({
    eventName: 'pxDataTableRowClick',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxDataTableRowClick: EventEmitter;

  @Event({
    eventName: 'pxDataTableRowDblClick',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxDataTableRowDblClick: EventEmitter;

  @Event({
    eventName: 'pxDataTableClearSelections',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxDataTableClearSelections: EventEmitter;

  @Event({
    eventName: 'pxDataTableCalcChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxDataTableCalcChange: EventEmitter;

  @Event({
    eventName: 'pxDataTableTotalsChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxDataTableTotalsChange: EventEmitter;

  @Event({
    eventName: 'pxDataTableColumnChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxDataTableColumnChange: EventEmitter;

  @Event({
    eventName: 'pxDataTableButtonClick',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxDataTableButtonClick: EventEmitter;

  @Event({
    eventName: 'pxDataTableSortCellClick',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) pxDataTableSortCellClick: EventEmitter;

  async componentWillLoad() {
    //let t = this;
    //if (Build.isDev) {
    //  console.log('CWL|', t.myElement.tagName.toLowerCase());
    //}
  }

  async componentDidLoad() {
    //let t = this;
    //if (Build.isDev) {
    //  console.log('CDL|', t.myElement.tagName.toLowerCase());
    //}
  }

  async initComponent() {
    let t = this;
    //if (Build.isDev) {
    //console.log('initComponent|', t.data);
    //}
    if (t.isSelectAll || t.isGroupedSelect) {
      let selectedGroupName: string = "";
      let selectedGroupRow: any = null;
      for (const row of t.data) {
        if (row.hasOwnProperty('select')) {
          if (row.select) {
            if (!t.isGroupedSelect) {
              await t.rowClicked(row);
            } else {
              if (selectedGroupName === '') {
                selectedGroupName = row[t.groupFieldName];
              }
              if (selectedGroupRow === null) {
                selectedGroupRow = row;
              }
            }
          }
        }
      }
      if (t.isGroupedSelect) {
        // console.log(selectedGroupName,selectedGroupRow)
        const tmpSelectedRows: any[] = t.selectedRows;
        await t.clearSelectedRows();
        t.selectedRows = tmpSelectedRows;
        if (selectedGroupRow !== null) {
          await t.rowClicked(selectedGroupRow);
        }
      }
    }
    if (t.isEditable) {
      const dp: NodeListOf<DatePicker> = t.containerTable.querySelectorAll("vaadin-date-picker");
      await t.uiFns.asyncForEach(dp, async (item: DatePicker) => {
        item.i18n = {
          ...item.i18n,
          formatDate: t.uiFns.dateFormatIso8601,
          parseDate: t.uiFns.dateParseIso8601,
          firstDayOfWeek: 1,
          monthNames: [
            'Ocak',
            'Şubat',
            'Mart',
            'Nisan',
            'Mayıs',
            'Haziran',
            'Temmuz',
            'Ağustos',
            'Eylül',
            'Ekim',
            'Kasım',
            'Aralık',
          ],
          weekdays: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
          weekdaysShort: ['Paz', 'Pzt', 'Sal', 'Çrş', 'Prş', 'Cum', 'Cmt'],
          today: 'Bugün',
          cancel: 'Vazgeç',
        };
      });
      //const icons: NodeListOf<Icon> = t.containerTable.querySelectorAll("vaadin-icon.remove_fill");
      //await t.uiFns.asyncForEach(icons, async (icon: Icon) => {
      //  const svgElement: SVGElement = icon.shadowRoot.querySelector("svg");
      //  svgElement.setAttribute('fill', 'blue')
      //  //console.log(svgElement, svgElement.getAttribute('fill'))
      //  //p.setAttribute('fill', '');
      //});
      t.rowEvents = [];
      const visibleElements: NodeListOf<HTMLElement> = t.containerTable.querySelectorAll("tr.datarow td:not(.hidden) *.focusable:not([readonly])");
      if (visibleElements.length > 0) {
        t.rowModel = [];
        await t.uiFns.asyncForEach(visibleElements, async (item: HTMLElement) => {
          const arrIdentifier: string[] = item.id.split("-");
          const dataIndex: string = arrIdentifier[1];
          const rowIndex: string = arrIdentifier[2];
          const colIndex: string = arrIdentifier[3];
          let radioIndex: string = '0';
          if (arrIdentifier.length > 4) {
            radioIndex = arrIdentifier[4];
          }
          if (parseInt(rowIndex) === 0) {
            t.rowModel = [...t.rowModel, {
              tag: item.tagName.toLowerCase()
              , dataIndex
              , colIndex
              , radioIndex
            }];
          }
          const idxcalc: number = await t.uiFns.getIndex(t.calculations, 'dataIndex', dataIndex);
          if (idxcalc === -1) {
            const handleEventKD = (event: KeyboardEvent) => {
              t._keydown(event, item, dataIndex, rowIndex, colIndex, radioIndex);
            };
            const handleEventB = (event: KeyboardEvent) => {
              t._blur(event, item, dataIndex, rowIndex, colIndex, radioIndex);
            };
            //item.removeEventListener('keydown', handleEvent);
            if (t.rowEvents.indexOf('keydown-' + item.part.value + '-' + item.id) === -1) {
              item.addEventListener('keydown', handleEventKD, true);
              t.rowEvents = [...t.rowEvents, 'keydown-' + item.part.value + '-' + item.id];
            }
            if (t.rowEvents.indexOf('blur-' + item.part.value + '-' + item.id) === -1) {
              item.addEventListener('blur', handleEventB);
              t.rowEvents = [...t.rowEvents, 'blur-' + item.part.value + '-' + item.id];
            }
          }
          const idxc: number = await t.uiFns.getIndex(t.columns, 'dataIndex', dataIndex);
          if (idxc !== -1) {
            const cCol: DataTableColumn = t.columns[idxc];
            //console.log('idxc', rowIndex, dataIndex, idxc, cCol, t.data);
            if (cCol.type === 'money'
              && t.data.length > 0 && t.data[rowIndex][dataIndex] !== ''
              && !((cCol?.style?.display === 'none'))) {
              const colIdentifier: string = "col-" + dataIndex + '-' + rowIndex + "-" + colIndex;
              const calculateElFind: HTMLElement = t.containerTable.querySelector("#" + colIdentifier);
              const strVal: string = await t.float2Money(t.data[rowIndex][dataIndex]);
              //console.log('calculateElFind', dataIndex, t.data[rowIndex][dataIndex], typeof (t.data[rowIndex][dataIndex]), strVal);
              (calculateElFind as TextField).value = strVal;
            }
          }
        });
      }
      const debounced = debounce(
        async () => {
          t.calculate()
        }
        , 50,
        // The maximum time func is allowed to be delayed before it's invoked:
        { maxWait: 100 }
      );
      debounced();
      const focusEl: any = t.rowModel[0];
      if (focusEl) {
        const focusElId: string = "col-" + focusEl.dataIndex + '-' + (t.data.length - 1) + '-' + focusEl.colIndex + (focusEl.radioIndex !== '0' ? ('-' + focusEl.radioIndex) : '');
        const focusElFind: HTMLElement = t.containerTable.querySelector("#" + focusElId);
        if (focusElFind) {
          focusElFind.focus();
        }
        await t.setCellSelect((t.data.length - 1).toString(), focusEl.colIndex);
      }
      t.isEventListenerReady = true;
      //if (Build.isDev) {
      //  console.log(visibleElements, t.columns, t.rowEvents);
      //}
    }
  }

  async calculate() {
    let t = this;
    //console.log('calculate')
    if (t.calculations.length > 0) {
      // if (Build.isDev) {
      //   console.log('blur-calc');
      // }
      t.calculatedRows = [];
      for (const calc of t.calculations) {
        t.calculatedRows = [...t.calculatedRows, { dataIndex: calc.dataIndex, calculated: 0, calculatedFormatted: "" }];
      }
      for (const row of t.data) {
        for (const calc of t.calculatedRows) {
          calc.calculated = calc.calculated + row[calc.dataIndex];
          calc.calculatedFormatted = await t.float2Money(calc.calculated);
        }
      }
      t.pxDataTableCalcChange.emit(t.calculatedRows);
      //console.log('calculatedRows', t.calculatedRows);
    }
    //console.log('blur-calc', t.totals.length);
    if (t.totals.length > 0) {
      t.totaledCols = [];
      for (const total of t.totals) {
        t.totaledCols = [...t.totaledCols, { dataIndex: total.dataIndex, total: 0, totalFormatted: "" }];
      }
      for (const row of t.data) {
        for (const item of t.totaledCols) {
          item.total = item.total + row[item.dataIndex];
          item.totalFormatted = await t.float2Money(item.total);
        }
      }
      //console.log('totaledCols', t.totaledCols);
      t.pxDataTableTotalsChange.emit(t.totaledCols);
    }
  }

  private async _blur(e: KeyboardEvent, item: any, dataIndex: string, rowIndex: string, colIndex: string, radioIndex: string = '0') {
    let t = this;
    if (Build.isDev) {
      console.log('_blur', e, t.rowModel, t.columns, item, dataIndex, rowIndex, colIndex, radioIndex, item.tagName);
    }
    const debounced = debounce(
      async () => {
        t.calculate()
      }
      , 50,
      // The maximum time func is allowed to be delayed before it's invoked:
      { maxWait: 100 }
    );

    if (t.data[rowIndex][dataIndex] !== '') {
      const idx: number = await t.uiFns.getIndex(t.columns, 'dataIndex', dataIndex);
      if (idx !== -1) {
        const column: DataTableColumn = t.columns[idx];
        if (column.type === 'money') {
          (item as TextField).value = await t.float2Money(t.data[rowIndex][dataIndex]);
          //console.log(column, t.data[rowIndex][dataIndex])
        }
      }
      debounced();
    }
  }

  private async _keydown(e: KeyboardEvent, item: any, dataIndex: string, rowIndex: string, colIndex: string, radioIndex: string = '0') {
    let t = this;
    if (Build.isDev) {
      console.log('_keydown', e, t.rowModel, item, dataIndex, rowIndex, colIndex, radioIndex, item.tagName);
    }
    if (e.key === 'ArrowDown') {
      e.preventDefault();
      e.stopPropagation();
      if (parseInt(rowIndex) === t.data.length - 1) {
        //yeni satır ekle
        t.pxDataTableButtonClick.emit({ row: t.data[rowIndex], column: { dataIndex: 'add' } });
      } else {
        //alt satıra geç
        const idx: number = await t.uiFns.getIndex(t.rowModel, 'colIndex', colIndex);
        rowIndex = (parseInt(rowIndex) + 1).toString();
        const focusEl: any = t.rowModel[idx];
        const focusElId: string = "col-" + focusEl.dataIndex + '-' + rowIndex + '-' + focusEl.colIndex + (focusEl.radioIndex !== '0' ? ('-' + focusEl.radioIndex) : '');
        const focusElFind: HTMLElement = t.containerTable.querySelector("#" + focusElId);
        if (focusElFind) {
          focusElFind.focus();
          if (focusEl.tag === 'px-toggle') {
            //focusElFind.focus();
          } else {
            e.preventDefault();
            e.stopPropagation();
          }
          await t.setCellSelect(rowIndex, focusEl.colIndex);
        } else {
          console.log('focusEl not found-2');
        }
      }
      return;
    }
    if (e.key === 'ArrowLeft') {
      e.preventDefault();
      e.stopPropagation();
      item.dispatchEvent(new KeyboardEvent("keydown", {
        key: "Tab",
        keyCode: 9,
        code: "Tab",
        which: 9,
        altKey: false,
        bubbles: true,
        cancelable: true,
        composed: true,
        ctrlKey: false,
        metaKey: false,
        shiftKey: true
      }));
      return;
    }
    if (e.key === 'ArrowUp') {
      e.preventDefault();
      e.stopPropagation();
      if (parseInt(rowIndex) !== 0) {
        //üst satıra geç
        rowIndex = (parseInt(rowIndex) - 1).toString();
        const idx: number = await t.uiFns.getIndex(t.rowModel, 'colIndex', colIndex);
        const focusEl: any = t.rowModel[idx];
        const focusElId: string = "col-" + focusEl.dataIndex + '-' + rowIndex + '-' + focusEl.colIndex + (focusEl.radioIndex !== '0' ? ('-' + focusEl.radioIndex) : '');
        const focusElFind: HTMLElement = t.containerTable.querySelector("#" + focusElId);
        if (focusElFind) {
          focusElFind.focus();
          if (focusEl.tag === 'px-toggle') {
            //focusElFind.focus();
          } else {
            e.preventDefault();
            e.stopPropagation();
          }
          await t.setCellSelect(rowIndex, focusEl.colIndex);
        } else {
          console.log('focusEl not found-4');
        }
      }
      return;
    }
    if (e.key === 'ArrowRight') {
      e.preventDefault();
      e.stopPropagation();
      item.dispatchEvent(new KeyboardEvent("keydown", {
        key: "Tab",
        keyCode: 9,
        code: "Tab",
        which: 9,
        altKey: false,
        bubbles: true,
        cancelable: true,
        composed: true,
        ctrlKey: false,
        metaKey: false,
        shiftKey: false
      }));
      return;
    }
    if (e.key === 'ArrowLeft' || e.key === 'ArrowRight' || e.key === 'Tab') {
      const direction: string = (((e.shiftKey && e.key === 'Tab') || e.key === 'ArrowLeft') ? 'backward' : 'forward');
      item.blur();
      let idx: number = -1;
      idx = await t.uiFns.getIndex(t.rowModel, 'colIndex|radioIndex', colIndex + '|' + radioIndex);
      //console.log('direction:', direction, 'idx:', idx, 'rowIndex:', rowIndex, 'radioIndex:', radioIndex);
      if (idx !== -1) {
        if (direction === 'forward') {
          if (t.rowModel.length - 1 > idx) {
            //yandaki hücre
            const focusEl: any = t.rowModel[idx + 1];
            const focusElId: string = "col-" + focusEl.dataIndex + '-' + rowIndex + '-' + focusEl.colIndex + (focusEl.radioIndex !== '0' ? ('-' + focusEl.radioIndex) : '');
            const focusElFind: HTMLElement = t.containerTable.querySelector("#" + focusElId);
            if (focusElFind) {
              focusElFind.focus();
              //console.log('tabIndex:', focusElFind.tabIndex);
              if (focusEl.tag === 'px-toggle') {
                //focusElFind.focus();
              } else {
                e.preventDefault();
                e.stopPropagation();
              }
              await t.setCellSelect(rowIndex, focusEl.colIndex);
            } else {
              console.log('focusEl not found-1');
            }
          } else {
            //alt satır
            e.preventDefault();
            e.stopPropagation();
            if (parseInt(rowIndex) === t.data.length - 1) {
              //yeni satır ekle
              t.pxDataTableButtonClick.emit({ row: t.data[rowIndex], column: { dataIndex: 'add' } });
            } else {
              //alt satıra geç
              rowIndex = (parseInt(rowIndex) + 1).toString();
              const focusEl: any = t.rowModel[0];
              const focusElId: string = "col-" + focusEl.dataIndex + '-' + rowIndex + '-' + focusEl.colIndex + (focusEl.radioIndex !== '0' ? ('-' + focusEl.radioIndex) : '');
              const focusElFind: HTMLElement = t.containerTable.querySelector("#" + focusElId);
              if (focusElFind) {
                focusElFind.focus();
                if (focusEl.tag === 'px-toggle') {
                  //focusElFind.focus();
                } else {
                  e.preventDefault();
                  e.stopPropagation();
                }
                await t.setCellSelect(rowIndex, focusEl.colIndex);
              } else {
                console.log('focusEl not found-2');
              }
            }
          }
        } else {
          //direction === 'backward'
          if (idx > 0) {
            const focusEl: any = t.rowModel[idx - 1];
            const focusElId: string = "col-" + focusEl.dataIndex + '-' + rowIndex + '-' + focusEl.colIndex + (focusEl.radioIndex !== '0' ? ('-' + focusEl.radioIndex) : '');
            const focusElFind: HTMLElement = t.containerTable.querySelector("#" + focusElId);
            if (focusElFind) {
              focusElFind.focus();
              //console.log('0', focusElFind)
              if (focusEl.tag === 'px-toggle') {
                //if (!focusElFind.hasAttribute('focused')) {
                //console.log('111')
                //focusElFind.focus();
                //focusElFind.setAttribute('focused', '');
                //}
              } else {
                e.preventDefault();
                e.stopPropagation();
              }
              await t.setCellSelect(rowIndex, focusEl.colIndex);
            } else {
              console.log('focusEl not found-3');
            }
          } else {
            if (parseInt(rowIndex) === 0) {
              e.preventDefault();
              e.stopPropagation();
              item.focus();
            } else {
              //üst satıra geç
              rowIndex = (parseInt(rowIndex) - 1).toString();
              const focusEl: any = t.rowModel[t.rowModel.length - 1];
              const focusElId: string = "col-" + focusEl.dataIndex + '-' + rowIndex + '-' + focusEl.colIndex + (focusEl.radioIndex !== '0' ? ('-' + focusEl.radioIndex) : '');
              const focusElFind: HTMLElement = t.containerTable.querySelector("#" + focusElId);
              if (focusElFind) {
                focusElFind.focus();
                if (focusEl.tag === 'px-toggle') {
                  //focusElFind.focus();
                } else {
                  e.preventDefault();
                  e.stopPropagation();
                }
                await t.setCellSelect(rowIndex, focusEl.colIndex);
              } else {
                console.log('focusEl not found-4');
              }
            }
          }
        }
      }
    }
    if (['ArrowDown', 'ArrowLeft', 'ArrowUp', 'ArrowRight', 'Tab', 'Enter'].indexOf(e.key) === -1) {
      const debounced = debounce(
        async () => {
          t.calculate()
        }
        , 50,
        // The maximum time func is allowed to be delayed before it's invoked:
        { maxWait: 100 }
      );
      //console.log('t.data[rowIndex][dataIndex]', t.data[rowIndex][dataIndex]);
      if (t.data[rowIndex][dataIndex] !== '') {
        //const idx: number = await t.uiFns.getIndex(t.columns, 'dataIndex', dataIndex);
        //if (idx !== -1) {
        //  const column: DataTableColumn = t.columns[idx];
        //  if (column.type === 'money') {
        //    (item as TextField).value = await t.float2Money(t.data[rowIndex][dataIndex]);
        //    //console.log(column, t.data[rowIndex][dataIndex])
        //  }
        //}
        debounced();
      }
    }
    return;
  }

  async setCellSelect(rowIndex: string, colIndex: string) {
    let t = this;
    const cSelectedCell: HTMLElement = t.containerTable.querySelector('td.selected');
    if (cSelectedCell) {
      cSelectedCell.classList.remove('selected');
    }
    const selectCell: HTMLElement = t.containerTable.querySelector('td.col-' + rowIndex + '-' + colIndex);
    if (selectCell) {
      selectCell.classList.add('selected');
    }
  }

  //@Method()
  //async addRow(rowData: any) {
  //  let t = this;
  //  t.data = [...t.data, rowData];
  //}
  //
  //@Method()
  //async editRow(id: number, rowData: any) {
  //  let t = this;
  //  let tmpData = t.data;
  //  t.data = [];
  //  if (id && tmpData && tmpData.length > 0) {
  //    for (let i = 0; i < tmpData.length; i++) {
  //      if (tmpData[i]._id_ === id) {
  //        //console.log(rowData);
  //        tmpData[i] = rowData;
  //      }
  //    }
  //  }
  //  t.data = tmpData;
  //}
  //
  //@Method()
  //async deleteRow(id: number) {
  //  let t = this;
  //  let tmpData = t.data;
  //  let tmpDataNew = [];
  //  if (id) {
  //    t.data = [];
  //    for (let i = 0; i < tmpData.length; i++) {
  //      if (tmpData[i]._id_ !== id) {
  //        tmpDataNew.push(tmpData[i]);
  //      }
  //    }
  //    t.data = tmpDataNew;
  //  }
  //}

  //@Method()
  //async setData(data: any) {
  //  let t = this;
  //  t.data = [];
  //  t.data = data;
  //  return;
  //}

  @Method()
  async clearSelectedRows() {
    let t = this;
    //if (Build.isDev) {
    //console.log('clearSelectedRows', t.data);
    //}
    if (t._isSelectable()) {
      let isEmitEvent: boolean = false;
      for (const row of t.data) {
        if (row.select) {
          row.select = false;
          isEmitEvent = true;
          t.pxDataTableRowClick.emit(row);
          if (t.toggleColumn !== null) {
            await t.toggleRowStateChange({ detail: { value: row.select } } as CustomEvent, row, t.toggleColumn);
          }
        }
      }
      if (t.selectedRows.length > 0) {
        isEmitEvent = true;
      }
      t.selectedRows = [];
      const cSelectedRows: NodeListOf<HTMLElement> = t.containerTable.querySelectorAll('tr.selected');
      if (cSelectedRows.length > 0) {
        await cSelectedRows.forEach(async (selectedRow: HTMLElement) => {
          selectedRow.classList.remove('selected');
          const thElements: NodeListOf<HTMLTableCellElement> = selectedRow.querySelectorAll('th');
          await thElements.forEach(async (thElement: HTMLTableCellElement) => {
            thElement.classList.remove('selected');
          });
          if (t.toggleColumn !== null) {
            const elToggle: HTMLPxToggleElement = selectedRow.querySelector('px-toggle[part=' + (selectedRow as any).part + ']:first-of-type');
            //await elToggle.setState(false);
            elToggle.isChecked = false;
          }
        });
      }
      if (t.timerClearSelectedRows !== null) {
        clearTimeout(t.timerClearSelectedRows);
        t.timerClearSelectedRows = null;
      }
      if (t.isSelectAll && t._isMultiSelect()) {
        //setTimeout(async () => {
        await t.checkSelectAllThirdState();
        //}, 500);
      }
      if (isEmitEvent) {
        t.pxDataTableClearSelections.emit();
      }
    }
  }

  @Method()
  async getSelectedRows() {
    let t = this;
    //let ret:any[]=[];
    //for(const row of t.data){
    //  if(row.select){
    //    ret=[...ret,row];
    //  }
    //}
    //t.selectedRows=t.data.filter(item => item.select === true);
    return t.selectedRows;
  }

  @Method()
  async setSelectedCellValue(val: any) {
    let t = this;
    const cSelectedCell: HTMLElement = t.containerTable.querySelector('td.selected');
    const el: any = cSelectedCell.firstChild;
    el.value = val;
    el.invalid = false;
  }

  @Method()
  async setSelectedRow(row: any) {
    let t = this;
    if (t.selectedRows.length > 0) {
      await t.clearSelectedRows();
    }
    if (t._isSelectable()) {
      let parts: string[] = [];
      if (t.isGroupedSelect && t.groupFieldName !== '') {
        if (t?.data?.length > 0) {
          const items: any[] = t.data.filter(item => item[t.groupFieldName] === row[t.groupFieldName]);
          for (const item of items) {
            parts.push(item._part_);
          }
        } else {
          parts.push(row._part_);
        }
      } else {
        if (t?.data?.length > 0) {
          const items: any[] = t.data.filter(item => item.code === row.code);
          for (const item of items) {
            parts.push(item._part_);
          }
          //} else {
          //  parts.push(row._part_);
        }
      }
      //console.log(row, t.data);
      for (const part of parts) {
        const selectRows: NodeListOf<HTMLElement> = t.containerTable.querySelectorAll('tr[part=' + part + ']');
        await selectRows.forEach(async (selectRow: HTMLElement) => {
          const item: any = t.data.filter(item => item._part_ === part);
          const cIdx: number = t.selectedRows.indexOf(item[0]);
          if (selectRow.classList.contains('selected')) {
            if (cIdx !== -1) {
              t.selectedRows.splice(cIdx, 1);
            }
            item[0].select = false;
            selectRow.classList.remove('selected');
            const thElements: NodeListOf<HTMLTableCellElement> = selectRow.querySelectorAll('th');
            await thElements.forEach(async (thElement: HTMLTableCellElement) => {
              thElement.classList.remove('selected');
            });
            if (t.toggleColumn !== null) {
              const elToggle: HTMLPxToggleElement = selectRow.querySelector('px-toggle[part=' + part + '].columnSelect');
              //await elToggle.setState(false);
              elToggle.isChecked = false;
              if (t.toggleColumn !== null) {
                await t.toggleRowStateChange({ detail: { value: elToggle.isChecked } } as CustomEvent, item[0], t.toggleColumn);
              }
            }
          } else {
            if (cIdx === -1) {
              t.selectedRows.push(item[0]);
            }
            item[0].select = true;
            selectRow.classList.add('selected');
            const thElements: NodeListOf<HTMLTableCellElement> = selectRow.querySelectorAll('th');
            await thElements.forEach(async (thElement: HTMLTableCellElement) => {
              thElement.classList.add('selected');
            });
            if (t.toggleColumn !== null) {
              const elToggle: HTMLPxToggleElement = selectRow.querySelector('px-toggle[part=' + part + '].columnSelect');
              //await elToggle.setState(true);
              elToggle.isChecked = true;
              if (t.toggleColumn !== null) {
                await t.toggleRowStateChange({ detail: { value: elToggle.isChecked } } as CustomEvent, item[0], t.toggleColumn);
              }
            }
          }
        });
      }
      await t.checkSelectAllThirdState();
      // if (t.autoClearSelection) {
      //   if (t.timerClearSelectedRows !== null) {
      //     clearTimeout(t.timerClearSelectedRows);
      //     t.timerClearSelectedRows = null;
      //   }
      //   t.timerClearSelectedRows = setTimeout(async () => {
      //     await t.clearSelectedRows();
      //   }, 10000);
      // }
      t.pxDataTableRowClick.emit(row);
    }
  }

  _isMultiSelect() {
    return this._isSelectable() && (this.selectionMode == 'multi' || this.isGroupedSelect);
  }
  _isSelectable() {
    return (this.selectionMode == 'single' || this.selectionMode == 'multi');
  }

  async rowClicked(row: any, trigger: boolean = true) {
    let t = this;
    //if (Build.isDev) {
    //console.log('rowClicked', row, t.selectedRows.length);
    //}
    const isReadOnly: boolean = (t?.isReadOnly || (row?.isReadOnly || false));
    let isRet: boolean = false;
    if (t._isSelectable() && !isReadOnly) {
      if (!t._isMultiSelect() || t.isGroupedSelect) {
        if (!row.select || t.selectedRows.length > 0) {
          if (t.selectionMode === 'single' && t.selectedRows[0] === row) {
            isRet = true;
          }
          await t.clearSelectedRows();
          if (isRet) {
            return;
          }
        }
      }
      let parts: string[] = [];
      if (t.isGroupedSelect && t.groupFieldName !== '') {
        if (t?.data?.length > 0) {
          const items: any[] = t.data.filter(item => item[t.groupFieldName] === row[t.groupFieldName]);
          for (const item of items) {
            parts.push(item._part_);
          }
        } else {
          parts.push(row._part_);
        }
      } else {
        parts.push(row._part_);
      }
      for (const part of parts) {
        const selectRows: NodeListOf<HTMLElement> = t.containerTable.querySelectorAll('tr[part=' + part + ']');
        await selectRows.forEach(async (selectRow: HTMLElement) => {
          const item: any = t.data.filter(item => item._part_ === part);
          if (item.length > 0) {
            const cIdx: number = t.selectedRows.indexOf(item[0]);
            if (selectRow.classList.contains('selected')) {
              if (cIdx !== -1) {
                t.selectedRows.splice(cIdx, 1);
              }
              item[0].select = false;
              selectRow.classList.remove('selected');
              const thElements: NodeListOf<HTMLTableCellElement> = selectRow.querySelectorAll('th');
              await thElements.forEach(async (thElement: HTMLTableCellElement) => {
                thElement.classList.remove('selected');
              });
              if (t.toggleColumn !== null) {
                const elToggle: HTMLPxToggleElement = selectRow.querySelector('px-toggle[part=' + part + '].columnSelect');
                //await elToggle.setState(false);
                elToggle.isChecked = false;
                await t.toggleRowStateChange({ detail: { value: elToggle.isChecked } } as CustomEvent, row, t.toggleColumn);
              }
            } else {
              if (cIdx === -1) {
                t.selectedRows.push(item[0]);
              }
              item[0].select = true;
              selectRow.classList.add('selected');
              const thElements: NodeListOf<HTMLTableCellElement> = selectRow.querySelectorAll('th');
              await thElements.forEach(async (thElement: HTMLTableCellElement) => {
                thElement.classList.add('selected');
              });
              if (t.toggleColumn !== null) {
                const elToggle: HTMLPxToggleElement = selectRow.querySelector('px-toggle[part=' + part + '].columnSelect');
                //await elToggle.setState(true);
                elToggle.isChecked = true;
                await t.toggleRowStateChange({ detail: { value: elToggle.isChecked } } as CustomEvent, row, t.toggleColumn);
              }
            }
          }
        });
      }
      if (t._isMultiSelect() && t.selectedRows.length === 0) {
        await t.pxDataTableClearSelections.emit();
      }
      await t.checkSelectAllThirdState();
      if (t.autoClearSelection) {
        if (t.timerClearSelectedRows !== null) {
          clearTimeout(t.timerClearSelectedRows);
          t.timerClearSelectedRows = null;
        }
        t.timerClearSelectedRows = setTimeout(async () => {
          await t.clearSelectedRows();
        }, 10000);
      }
    }
    //if(Build.isDev){
    //  console.log(t.data);
    //}
    if (!t.isReadOnly && !isReadOnly && trigger) {
      t.pxDataTableRowClick.emit(row);
    }
  }

  async rowDblClicked(row: any) {
    let t = this;
    const isReadOnly: boolean = (t?.isReadOnly || (row?.isReadOnly || false));
    //if (t._isSelectable() && !isReadOnly) {

    //}
    await t.rowClicked(row, false);
    if (!t.isReadOnly && !isReadOnly) {
      t.pxDataTableRowDblClick.emit(row);
    }
  }

  async checkSelectAllThirdState() {
    let t = this;
    if (t.isSelectAll && t._isMultiSelect()) {
      const preSelectedRows: NodeListOf<HTMLElement> = t.containerTable.querySelectorAll('tr.selected');
      if (preSelectedRows.length > 0 || t.selectedRows.length > 0) {
        //await t.selectAllToggle.setState(true);
        t.selectAllToggle.isChecked = true;
        if (preSelectedRows.length === t.data.length || t.selectedRows.length === t.data.length) {
          //await t.selectAllToggle.setThirdState(false);
          t.selectAllToggle.isThirdState = false;
        } else {
          //await t.selectAllToggle.setThirdState(true);
          t.selectAllToggle.isThirdState = true;
        }
      } else {
        //await t.selectAllToggle.setState(false);
        //await t.selectAllToggle.setThirdState(false);
        t.selectAllToggle.isChecked = false;
        t.selectAllToggle.isThirdState = false;
        //await t.pxDataTableClearSelections.emit();
      }
      //console.log('rowClicked-0', t.selectedRows, preSelectedRows.length);
    }
  }

  async cellClicked(rowIndex: string, colIndex: string) {
    let t = this;
    //console.log('cellClicked', rowIndex, colIndex, t.selectionMode);
    if (t.selectionMode === 'cell') {
      return await t.setCellSelect(rowIndex, colIndex);
    }
    //let idx: number = -1;
    //for (let i = 0; i < t.data.length; i++) {
    //  if (idx === -1) {
    //    if (t.data[i]._id_ === row._id_) {
    //      idx = i;
    //      break;
    //    }
    //  }
    //}
    //if (idx !== -1) {
    //  switch (item) {
    //    case 'btn_saglam_minus': {
    //      if (t.data[idx].saglam > 0) {
    //        t.data[idx].saglam--;
    //      }
    //      (e.target as any).parentNode.querySelector('label').innerHTML = t.data[idx].saglam;
    //      break;
    //    }
    //    case 'btn_saglam_plus': {
    //      t.data[idx].saglam++;
    //      (e.target as any).parentNode.querySelector('label').innerHTML = t.data[idx].saglam;
    //      break;
    //    }
    //    case 'btn_burda_minus': {
    //      if (t.data[idx].hurda > 0) {
    //        t.data[idx].hurda--;
    //      }
    //      (e.target as any).parentNode.querySelector('label').innerHTML = t.data[idx].hurda;
    //      break;
    //    }
    //    case 'btn_hurda_plus': {
    //      t.data[idx].hurda++;
    //      (e.target as any).parentNode.querySelector('label').innerHTML = t.data[idx].hurda;
    //      break;
    //    }
    //  }
    //}
  }

  @Method()
  async selectAllStateToggle() {
    let t = this;
    if ((t.isSelectAll && !t.isGroupedSelect) && t._isMultiSelect()) {
      //t.selectAllToggle.setState(!t.selectAllToggle.isChecked, true);
      t.selectAllToggle.isChecked = !t.selectAllToggle.isChecked;
    }
  }

  @Method()
  async selectAllStateSet() {
    let t = this;
    if ((t.isSelectAll && !t.isGroupedSelect) && t._isMultiSelect()) {
      //t.selectAllToggle.setState(true, true);
      t.selectAllToggle.isChecked = true;
    }
  }

  @Method()
  async selectAllStateClear() {
    let t = this;
    if ((t.isSelectAll && !t.isGroupedSelect) && t._isMultiSelect()) {
      //t.selectAllToggle.setState(false, true);
      t.selectAllToggle.isChecked = false;
    }
  }

  async toggleStateChange(e: CustomEvent) {
    let t = this;
    //if (Build.isDev) {
    //  console.log('toggleStateChange', e.detail.value, e);
    //}
    for (const row of t.data) {
      if (row.select !== e.detail.value) {
        row.select = e.detail.value;
        await t.toggleRowStateChange({ detail: { value: row.select } } as CustomEvent, row, t.toggleColumn);
      }
    }
    const selectRows: NodeListOf<HTMLElement> = t.containerTable.querySelectorAll('tbody>tr');
    await selectRows.forEach(async (selectRow: HTMLElement) => {
      if (e.detail.value) {
        if (!selectRow.classList.contains('selected')) {
          selectRow.classList.add('selected');
          const thElements: NodeListOf<HTMLTableCellElement> = selectRow.querySelectorAll('th');
          await thElements.forEach(async (thElement: HTMLTableCellElement) => {
            thElement.classList.add('selected');
          });
          if (t.toggleColumn !== null) {
            const elToggle: HTMLPxToggleElement = selectRow.querySelector('px-toggle[part=' + (selectRow as any).part.value + '].columnSelect');
            //await elToggle.setState(true);
            elToggle.isChecked = true;
          }
        }
      } else {
        if (selectRow.classList.contains('selected')) {
          selectRow.classList.remove('selected');
          const thElements: NodeListOf<HTMLTableCellElement> = selectRow.querySelectorAll('th');
          await thElements.forEach(async (thElement: HTMLTableCellElement) => {
            thElement.classList.remove('selected');
          });
          if (t.toggleColumn !== null) {
            const elToggle: HTMLPxToggleElement = selectRow.querySelector('px-toggle[part=' + (selectRow as any).part.value + '].columnSelect');
            //await elToggle.setState(false);
            elToggle.isChecked = false;
          }
        }
      }
    });
    if (!e.detail.value) {
      await t.clearSelectedRows();
    }
    for (const row of t.data) {
      if (!row.hasOwnProperty('select')) {
        row.select = false;
      }
      if (e.detail.value) {
        row.select = true;
        const cIdx: number = t.selectedRows.indexOf(row);
        if (cIdx === -1) {
          t.selectedRows.push(row);
        }
      } else {
        row.select = false;
      }
      t.pxDataTableRowClick.emit(row);
    }
    await t.checkSelectAllThirdState();
    //console.log('t.selectedRows', t.selectedRows, t.data);
  }

  async iconClick(row: any, column: any) {
    let t = this;
    if (column.dataIndex === 'del') {
      if (t.isEditable) {
        t.isEventListenerReady = false;
      }
      //const idx: number = await t.getIndex(t.data, '_part_', row._part_);
      //for (let i = idx; i < t.data.length; i++) {
      const el: HTMLTableRowElement = t.containerTable.querySelector('tr[part="' + row._part_ + '"]');
      const elements: NodeListOf<HTMLElement> = el.querySelectorAll('[part="' + row._part_ + '"]');
      await elements.forEach(async (element: HTMLElement) => {
        switch (element.tagName) {
          case 'PX-TOGGLE': {
            (element as unknown as PxToggle).isChecked = false;
            break;
          }
          case 'VAADIN-COMBO-BOX': {
            if (element.hasAttribute('has-value')) {
              (element as ComboBox).value = '';
            }
            break;
          }
          case 'VAADIN-DATE-PICKER': {
            if (element.hasAttribute('has-value')) {
              (element as DatePicker).value = '';
            }
            break;
          }
          case 'VAADIN-NUMBER-FIELD': {
            if (element.hasAttribute('has-value')) {
              (element as NumberField).value = '';
            }
            break;
          }
          case 'VAADIN-RADIO-BUTTON': {
            if (element.hasAttribute('has-value')) {
              (element as RadioButton).checked = false;
            }
            break;
          }
          case 'VAADIN-TEXT-FIELD': {
            if (element.hasAttribute('has-value')) {
              (element as TextField).value = '';
            }
            break;
          }
          default: {
            //console.log(element, element.tagName);
          }
        }
      });
      //}
      if (t.isEditable) {
        t.isEventListenerReady = true;
      }
    }
    t.pxDataTableButtonClick.emit({ row, column });
    //console.log('iconClick', row, column, t.data);
  }

  async radioClicked(val: string, row: any, column: any) {
    let t = this;
    //if (Build.isDev) {
    //  console.log('radioClicked', row, column, val);
    //}
    const cInputRadioElements: NodeListOf<HTMLElement> = t.containerTable.querySelectorAll('vaadin-radio-button[part=' + row._part_ + ']');
    await cInputRadioElements.forEach(async (inputRadio: HTMLInputElement) => {
      if (inputRadio.value === val) {
        inputRadio.checked = true;
      } else {
        inputRadio.checked = false;
      }
    });
    row[column.dataIndex] = val;
    //console.log(t.data)
    if (t.isEventListenerReady) {
      t.pxDataTableColumnChange.emit({ row, column, value: val });
    }
  }

  async toggleRowStateChange(e: CustomEvent, row: any, column: any) {
    let t = this;
    //if (Build.isDev) {
    //  console.log('toggleRowStateChange', row, column, e.detail.value);
    //}
    if (t.selectionMode === 'none' || t.isEditable) {
      row[column.dataIndex] = e.detail.value;
      (e.currentTarget as unknown as PxToggle).isChecked = e.detail.value;
      if (t.isEventListenerReady) {
        t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
      }
    }
  }

  async editableFieldChange(e: KeyboardEvent, row: any, column: any) {
    let t = this;
    row[column.dataIndex] = (e.target as any).value;
    //console.log('editableFieldChange', column)
    t.pxDataTableColumnChange.emit({ row, column, value: (e.target as any).value });
  }
  async dropdownChange(e: CustomEvent, row: any, column: any) {
    let t = this;
    row[column.dataIndex] = e.detail.displayValueSelected;
    t.pxDataTableColumnChange.emit({ row, column, value: e.detail.displayValueSelected });
  }

  async float2Money(v: number, dp: number = 2, ds: string = ',', gs: string = '.') {
    let num: number = v;
    const decimalPrecision: number = !isNaN(dp) ? dp : 2;
    if (isNaN(num)) {
      return '';
    }
    const sign: boolean = (num == (num = Math.abs(num)));
    num = Math.floor(num * Math.pow(10, decimalPrecision) + 0.50000000001);
    let cents: string = (num % Math.pow(10, decimalPrecision)).toString();
    let snum: string = Math.floor(num / Math.pow(10, decimalPrecision)).toString();
    while (cents.toString().length < (Math.pow(10, decimalPrecision - 1)).toString().length)
      cents = "0" + cents;
    for (var i = 0, val = Math.floor((snum.length - (1 + i)) / 3); i < val; i++)
      snum = snum.substring(0, snum.length - (4 * i + 3)) + gs + snum.substring(snum.length - (4 * i + 3));
    if (decimalPrecision > 0)
      return (((sign) ? '' : '-') + snum + ds + cents);
    else
      return (((sign) ? '' : '-') + snum);
  }

  async rowCalc(row: any, column: any) {
    let t = this;
    let isColumnCalculated: boolean = false;
    //console.log('rowCalc', row)
    for (const calc of t.calculations) {
      let tmpResult: number = null;
      for (const field of calc.fields) {
        const tmpVal: number = parseFloat(row[field] !== '' ? row[field] : 0);
        if (tmpResult === null) {
          tmpResult = tmpVal;
        } else {
          switch (calc.proc) {
            case '*': {
              tmpResult = tmpResult * tmpVal;
              break;
            }
            case '%': {
              tmpResult = tmpResult * tmpVal / 100;
              break;
            }
            case '-': {
              tmpResult = tmpResult - tmpVal;
              break;
            }
            case '+': {
              tmpResult = tmpResult + tmpVal;
              break;
            }
          }
        }
      }
      if (calc.dataIndex === column.dataIndex) {
        isColumnCalculated = true;
      }
      row[calc.dataIndex] = tmpResult;
      const idx: number = await t.uiFns.getIndex(t.columns, 'dataIndex', calc.dataIndex);
      const changedColumn: DataTableColumn = t.columns[idx];
      t.pxDataTableColumnChange.emit({ row, column: changedColumn, value: row[calc.dataIndex] });
      const idxCol: number = await t.uiFns.getIndex(t.rowModel, 'dataIndex', calc.dataIndex);
      //console.log('idxCol:', idxCol, t.rowModel, 'dataIndex', calc.dataIndex, 'calc:', calc, column);
      const changedRowModel: any = t.rowModel[idxCol];
      const colIndex: string = ((changedColumn?.style?.display === 'none') ? 'hidden' : changedRowModel.colIndex);
      const colIdentifier: string = "col-" + changedColumn.dataIndex + '-' + row.rowIndex + "-" + colIndex;
      const calculateElFind: HTMLElement = t.containerTable.querySelector("#" + colIdentifier);
      //console.log(await t.float2Money(tmpResult));
      (calculateElFind as TextField).value = await t.float2Money(tmpResult);
      //(calculateElFind as NumberField).value = tmpResult.toString();
      //(calculateElFind as NumberField).value = "1.000.000,15";
    }
    if (!isColumnCalculated) {
      //if (column.type === 'money') {
      //  const idx: number = await t.getIndex(t.columns, 'dataIndex', column.dataIndex);
      //  const changedColumn: DataTableColumn = t.columns[idx];
      //  const idxCol: number = await t.getIndex(t.rowModel, 'dataIndex', column.dataIndex);
      //  const changedRowModel: any = t.rowModel[idxCol];
      //  const colIndex: string = (( changedColumn?.style?.display === 'none') ? 'hidden' : changedRowModel.colIndex);
      //  const colIdentifier: string = "col-" + changedColumn.dataIndex + '-' + row.rowIndex + "-" + colIndex;
      //  const calculateElFind: HTMLElement = t.containerTable.querySelector("#" + colIdentifier);
      //  (calculateElFind as NumberField).value = await t.float2Money(row[column.dataIndex]);
      //}
      t.pxDataTableColumnChange.emit({ row, column, value: row[column.dataIndex] });
    }
  }

  @Method()
  async clearSortData() {
    let t = this;
    const sortIcons: NodeListOf<Icon> = t.containerTable.querySelectorAll('.sort-icon');
    await sortIcons.forEach(async (icon: Icon) => {
      icon.className = 'sort-icon';
      icon.src = '';
    });
  }

  async sortCellClicked(col: DataTableColumn) {
    let t = this;
    if (!t.isSortMultiple) {
      const sortIcons: NodeListOf<Icon> = t.containerTable.querySelectorAll('.sort-icon');
      await sortIcons.forEach(async (icon: Icon) => {
        if (!icon.parentElement.classList.contains('th-sort-cell-' + col.dataIndex)) {
          icon.className = 'sort-icon';
          icon.src = '';
        }
      });
    }
    const cSelectedSortCell: HTMLElement = t.containerTable.querySelector('th.th-sort-cell-' + col.dataIndex);
    //console.log('sortCellClicked', col);
    if (cSelectedSortCell) {
      const sortIcon: Icon = cSelectedSortCell.querySelector('vaadin-icon');
      let dir: string = '';
      //console.log('sortIcon', sortIcon.className);
      if (sortIcon.classList.contains('ASC')) {
        dir = 'DESC';
        sortIcon.classList.remove('ASC');
        sortIcon.classList.add(dir);
        sortIcon.src = UIIcons.fa__arrow_up_wide_short;
      } else if (sortIcon.classList.contains('DESC')) {
        dir = '';
        sortIcon.classList.remove('DESC');
        //sortIcon.classList.add(dir);
        //sortIcon.src = UIIcons.fa__arrow_down_short_wide;
        sortIcon.src = '';
      } else {
        dir = 'ASC';
        sortIcon.classList.add(dir);
        sortIcon.src = UIIcons.fa__arrow_down_short_wide;
      }
      t.pxDataTableSortCellClick.emit({ field: ((col?.alias !== '') ? col.alias + '.' : '') + col.fieldName, dir });
    }
  }

  getTemplateHeaderColumn() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('getTemplateHeaderColumn', t.isSortable, t.columns);
    //}
    return t.columns.map((column: DataTableColumn) => {
      if (column?.style?.width) {
        if (column.style.width.indexOf('%') === -1) {
          column.style.minWidth = column.style.width;
        }
      }
      if (column.hasOwnProperty('type') && column.type !== '') {
        switch (column.type) {
          case 'icon': {//editable gridde kullanılıyor
            //console.log(UIIcons[column.options.icon]);
            return (<th style={(column?.style || null)}>
              <vaadin-icon
                class="remove_fill"
                src={(UIIcons[column?.options?.icon] || '')}
              //icon="vaadin:phone"
              ></vaadin-icon>
            </th>);
          }
          case 'toggle': {
            if ((t.isSelectAll && !t.isGroupedSelect) && t._isMultiSelect()) {
              return (<th style={(column?.style || null)}>
                <px-toggle
                  ref={(el) => t.selectAllToggle = el as unknown as PxToggle}
                  onPxToggleCheckedChanged={(res) => { t.toggleStateChange(res); }}
                  part="selectAll"
                  size="regular"
                ></px-toggle>
              </th>);
            } else {
              if (t.isSortable) {
                return (<th class={"thcell th-sort-cell-" + column.dataIndex} style={(column?.style || null)} innerHTML={column.label}
                  on-click={() => t.sortCellClicked(column)}
                ><vaadin-icon
                  class='sort-icon'
                  src={''}
                  style={{ marginLeft: "5px" }}
                ></vaadin-icon></th>);
              } else {
                return (<th class="thcell" style={(column?.style || null)} innerHTML={column.label}></th>);
              }
            }
          }
          default: {
            if (t.isSortable) {
              return (<th class={"thcell th-sort-cell-" + column.dataIndex} style={(column?.style || null)} innerHTML={column.label}
                on-click={() => t.sortCellClicked(column)}
              ><vaadin-icon
                class='sort-icon'
                src={''}
                style={{ marginLeft: "5px" }}
              ></vaadin-icon></th>);
            } else {
              return (<th class="thcell" style={(column?.style || null)} innerHTML={column.label}></th>);
            }
          }
        }
      } else {
        if (t.isSortable) {
          return (<th class={"thcell th-sort-cell-" + column.dataIndex} style={(column?.style || null)} innerHTML={column.label}
            on-click={() => t.sortCellClicked(column)}
          ><vaadin-icon
            class='sort-icon'
            src={''}
            style={{ marginLeft: "5px" }}
          ></vaadin-icon></th>);
        } else {
          return (<th class="thcell" style={(column?.style || null)} innerHTML={column.label}></th>);
        }
      }
    });
  }

  getTemplateHeader() {
    let t = this;
    // filtre varsa fazladan bir satır ile eklenebilir
    return (<tr class="row">
      {t.getTemplateHeaderColumn()}
    </tr>);
  }

  //getTemplateRadio(colIdentifier: string, value: string, label: string, disabled: boolean, row: any, column: DataTableColumn) {
  //  let t = this;
  //  //console.log(row, column, row[column.dataIndex], value, (row[column.dataIndex] === value));
  //  if (row[column.dataIndex] === value) {
  //    return (<vaadin-radio-button id={colIdentifier} class={"focusable " + colIdentifier} part={row._part_} value={value} label={label} disabled={disabled} checked={true} on-click={() => t.radioClicked(value, row, column)}></vaadin-radio-button>);
  //  } else {
  //    return (<vaadin-radio-button id={colIdentifier} class={"focusable " + colIdentifier} part={row._part_} value={value} label={label} disabled={disabled} on-click={() => t.radioClicked(value, row, column)}></vaadin-radio-button>);
  //  }
  //}

  getTemplateBodyColumns(row: any) {
    let t = this;
    let counter: number = 0;
    let counterIndex: number = 0;
    //console.log('getTemplateBodyColumns');
    return t.columns.map((column: DataTableColumn) => {
      const isReadOnly: boolean = (t?.isReadOnly || (row?.isReadOnly || (column?.isReadOnly || (column?.options?.readonly || false))));
      //console.log(column, isReadOnly);
      const colIndex: string = ((column?.style?.display === 'none') ? 'hidden' : (counter++).toString());
      const colIdentifier: string = "col-" + column.dataIndex + '-' + row.rowIndex + "-" + colIndex;
      const rowColIdentifier: string = "tdcell " + colIdentifier + " col-" + row.rowIndex + "-" + colIndex + ((column?.style?.display === 'none') ? ' hidden' : '');
      if (column.hasOwnProperty('type') && column.type !== '' && column.label !== '#') {
        const columnType: string = (column.type === 'type_from_column' ?
          (row[column.dataIndex + '__type'] ? row[column.dataIndex + '__type'] : (row?.cm_row_type || "text")) :
          (column.type == 'type_from_row' ? row.cm_row_type : column.type)
        );
        const kbLayout: string = (column.type === 'type_from_column' ?
          (row[column.dataIndex + '__layout'] ? row[column.dataIndex + '__layout'] : (row?.cm_row_layout || "tr_TR")) :
          (column.type == 'type_from_row' ? row.cm_row_layout : column.kblayout)
        );
        if (t.isFillCell === true && ["#", "+", "-"].indexOf(column.label) === -1) {
          if (typeof column.style === 'undefined') {
            column.style = {};
          }
          column.style.width = '100%';
        }
        switch (columnType) {
          case 'combo': {
            if (column.dataIndex !== 'na') {
              return (<td class={rowColIdentifier} style={(column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)} >
                <vaadin-combo-box
                  id={colIdentifier}
                  part={row._part_}
                  allow-custom-value={(column?.options?.allowCustomValue || false)}
                  label=""
                  helper-text=""
                  items={(column?.options?.data || [])}
                  class={"focusable " + colIdentifier}
                  style={{ width: (column?.style?.width || "130px"), paddingBottom: "0px", paddingTop: "0px" }}
                  value={row[column.dataIndex]}
                  on-value-changed={(e: CustomEvent) => {
                    row[column.dataIndex] = e.detail.value;
                    if (t.isEventListenerReady) {
                      t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
                    }
                    //console.log('combo', row, column, e.detail, t.data);
                  }}
                ></vaadin-combo-box>
              </td>
              );
            }
            break;
          }
          case 'date': {
            return (<td class={rowColIdentifier} style={(column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)}>
              <vaadin-date-picker
                id={colIdentifier}
                part={row._part_}
                label=""
                helper-text=""
                placeholder={(column?.options?.placeholder || '')}
                clear-button-visible={(column?.options?.clearButtonVisible || false)}

                show-week-numbers={(column?.options?.showWeekNumbers || true)}
                auto-open-disabled={(!column?.options?.autoOpen || true)}

                disabled={(column?.options?.disabled || false)}
                readonly={(column?.options?.readonly || false)}

                class={"focusable " + colIdentifier}
                style={{ width: (column?.style?.width || "130px"), paddingBottom: "0px", paddingTop: "0px" }}
                value={row[column.dataIndex]}
                on-value-changed={(e: CustomEvent) => {
                  row[column.dataIndex] = e.detail.value;
                  if (t.isEventListenerReady) {
                    t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
                  }
                  //console.log('date', row, column, e.detail, t.data);
                }}
              ></vaadin-date-picker>
            </td>
            );
            //on-change={(event: DatePickerChangeEvent) => {
            //  console.log(event);
            //}}
          }
          case 'dropdown': {//kullanılmıyor
            if (column.dataIndex !== 'na') {
              const rName: string = "dropdown" + row._part_;
              return (<td class={rowColIdentifier} style={(column?.style || null)} >
                <px-dropdown
                  id={rName}
                  items={column.options.items}
                  sortMode="key"
                  buttonStyle="default"
                  searchMode={false}
                  onPxDropdownChanged={(e) => { t.dropdownChange(e, row, column); }}
                >
                </px-dropdown>
              </td>
              );
            }
          }
          case 'icon': {
            //console.log(UIIcons[column.options.icon]);
            return (<td class={rowColIdentifier} style={(column?.style || null)} >
              <vaadin-icon
                class="remove_fill"
                part={row._part_}
                src={(UIIcons[column?.options?.icon] || '')}
                onClick={() => { t.iconClick(row, column); }}
              ></vaadin-icon>
            </td>
            );
          }
          case 'money': {
            return (<td class={rowColIdentifier} style={(column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)}>
              <vaadin-text-field
                id={colIdentifier}
                part={row._part_}
                label=""
                helper-text=""
                placeholder={(column?.options?.placeholder || '')}
                theme="align-right"
                clear-button-visible={(column?.options?.clearButtonVisible || false)}

                //required={( column?.options?.required || false)} '^(((\d{1,3})(.?\d{1,3})*)|(\d+))(\,\d{2})?$' ^(([0-9]{1,3}\\.)?([0-9]{3}\\.)*[0-9]{1,3})(,[0-9][0-9])?$
                max-length={(column?.options?.maxlength || 50)}
                min-length={(column?.options?.minlength || 0)}
                pattern={(column?.options?.pattern || '^-?[0-9]{1,3}(?:[0-9]*(?:[,][0-9]{2})?|(?:[.][0-9]{3})*(?:[,][0-9]{2})?|(?:[.][0-9]{3})*(?:[,][0-9]{2})?)$')}
                allowed-char-pattern={(column?.options?.allowedCharPattern || '[0-9.,]')}

                disabled={(column?.options?.disabled || false)}
                readonly={(column?.options?.readonly || false)}

                class={"focusable " + colIdentifier}
                style={{ width: (column?.style?.width || "150px"), paddingBottom: "0px", paddingTop: "0px" }}
                value={row[column.dataIndex]}

                //on-validated={(e: CustomEvent) => {
                //  debugger
                //  if (!e.detail.valid) {
                //    (e.currentTarget as TextField).value = '';
                //    row[column.dataIndex] = '';
                //    if (t.isEventListenerReady) {
                //      if (t.calculations.length > 0) {
                //        t.rowCalc(row, column);
                //      } else {
                //        t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
                //      }
                //    }
                //  }
                //  //console.log('money', row, column, e.detail, t.data);
                //}}
                on-input={(e: Event) => {
                  if (e instanceof InputEvent) {
                    if ((e as InputEvent).inputType === 'deleteContentBackward') {
                      row[column.dataIndex] = 0;
                    }
                  } else {
                    if (e.srcElement instanceof HTMLInputElement) {
                      if ((e.srcElement as HTMLInputElement).value === '') {
                        row[column.dataIndex] = 0;
                      }
                    }
                  }

                }}
                on-value-changed={(e: CustomEvent) => {
                  const val: any = (e.detail.value !== '' ? e.detail.value : 0);
                  //const val: any = e.detail.value;
                  const isCalcRequire: boolean = (val === 0 && row[column.dataIndex] !== 0);
                  //if (isCalcRequire) {
                  //  console.log('money-changed', val, row[column.dataIndex], t.isEventListenerReady, isCalcRequire)
                  //}
                  if (val !== '') {
                    const oVal: number = typeof (val) === 'string' ? parseFloat(val.toString().split('.').join('').split(',').join('.')) : val;
                    //console.log(column.dataIndex, val, oVal)
                    row[column.dataIndex] = oVal;
                    if (t.isEventListenerReady) {
                      if (t.calculations.length > 0) {
                        t.rowCalc(row, column);
                      } else {
                        t.pxDataTableColumnChange.emit({ row, column, value: oVal });
                      }
                    }
                    //console.log('text', row, column, row[column.dataIndex], val, t.data);
                  }
                  if (isCalcRequire) {
                    const debounced = debounce(
                      async () => {
                        t.calculate()
                      }
                      , 50,
                      // The maximum time func is allowed to be delayed before it's invoked:
                      { maxWait: 100 }
                    );
                    debounced();
                  }
                }}
              >
                <span slot="prefix">{(column?.options?.symbol || '')}</span>
              </vaadin-text-field>
            </td>
            );
            /*return (<td class={rowColIdentifier} style={( column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)}>
              <vaadin-number-field
                id={colIdentifier}
                part={row._part_}
                label=""
                helper-text=""
                placeholder={( column?.options?.placeholder || '')}
                theme="align-right"
                clear-button-visible={( column?.options?.clearButtonVisible || true)}
                step-buttons-visible={( column?.options?.stepButtonsVisible || false)}
                max={(column?.options?.max || 9999999999)}
                min={(column?.options?.min || -9999999999)}
                step={(column?.options?.step || 1)}

                disabled={(column?.options?.disabled || false)}
                readonly={(column?.options?.readonly || false)}

                class={"focusable " + colIdentifier}
                style={{ width: ( column?.style?.width || "150px"), paddingBottom: "0px", paddingTop: "0px" }}
                width="150px"
                value={row[column.dataIndex]}
                on-validated={(e: CustomEvent) => {
                  if (!e.detail.valid) {
                    (e.currentTarget as NumberField).value = '';
                    row[column.dataIndex] = '';
                    if (t.isEventListenerReady) {
                      if (t.calculations.length > 0) {
                        t.rowCalc(row, column);
                      } else {
                        t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
                      }
                    }
                  } else {
                    console.log('money', row, column, row[column.dataIndex]);
                  }
                }}
                on-value-changed={(e: CustomEvent) => {
                  row[column.dataIndex] = e.detail.value;
                  if (t.isEventListenerReady) {
                    if (t.calculations.length > 0) {
                      t.rowCalc(row, column);
                    } else {
                      t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
                    }
                  }
                  //console.log('money', row, column, e.detail, t.data);
                }}
              >
                <span slot="prefix">{( column?.options?.symbol || '')}</span>
              </vaadin-number-field>
            </td >
            );*/
          }
          case 'number': {
            return (<td class={rowColIdentifier} style={(column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)}>
              <vaadin-number-field
                id={colIdentifier}
                part={row._part_}
                label=""
                helper-text=""
                placeholder={(column?.options?.placeholder || '')}
                theme="align-right"
                clear-button-visible={(column?.options?.clearButtonVisible || true)}
                step-buttons-visible={(column?.options?.stepButtonsVisible || false)}
                max={(column?.options?.max || 9999999999)}
                min={(column?.options?.min || 0)}
                step={(column?.options?.step || 1)}

                disabled={(column?.options?.disabled || false)}
                readonly={(column?.options?.readonly || false)}

                class={"focusable " + colIdentifier}
                style={{ width: (column?.style?.width || "150px"), paddingBottom: "0px", paddingTop: "0px" }}
                width="150px"
                value={row[column.dataIndex]}
                on-input={(e: Event) => {
                  if (e instanceof InputEvent) {
                    if ((e as InputEvent).inputType === 'deleteContentBackward') {
                      row[column.dataIndex] = 0;
                    }
                  } else {
                    if (e.srcElement instanceof HTMLInputElement) {
                      if ((e.srcElement as HTMLInputElement).value === '') {
                        row[column.dataIndex] = 0;
                      }
                    }
                  }
                }}
                on-validated={(e: CustomEvent) => {
                  if (!e.detail.valid) {
                    (e.currentTarget as NumberField).value = '';
                    row[column.dataIndex] = '';
                    if (t.isEventListenerReady) {
                      if (t.calculations.length > 0) {
                        t.rowCalc(row, column);
                      } else {
                        t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
                      }
                    }
                  }
                  //console.log('money', row, column, e.detail, t.data);
                }}
                on-value-changed={(e: CustomEvent) => {
                  const val: any = (e.detail.value !== '' ? e.detail.value : 0);
                  //const val: any = e.detail.value;
                  if (val !== '') {
                    row[column.dataIndex] = parseInt(val);
                    if (t.isEventListenerReady) {
                      if (t.calculations.length > 0) {
                        t.rowCalc(row, column);
                      } else {
                        t.pxDataTableColumnChange.emit({ row, column, value: val });
                      }
                    }
                    //console.log('money', row, column, e.detail, t.data);
                  }
                }}
              >
              </vaadin-number-field>
            </td >
            );
          }
          case 'radio': {
            //{t.getTemplateRadio(colIdentifier + "-1", option1, option1, isReadOnly, row, column)}
            //{t.getTemplateRadio(colIdentifier + "-2", option2, option2, isReadOnly, row, column)}
            if (column.dataIndex !== 'na') {
              const option1: string = (column.options ? (column.options.answertype === 'Evet-Hayır' ? 'Hayır' : (column.options.answertype === 'OK-NOT OK' ? 'NOT OK' : (column.options.answertype === 'Uygun-Uygun Değil' ? 'Uygun Değil' : ''))) : '');
              const option2: string = (column.options ? (column.options.answertype === 'Evet-Hayır' ? 'Evet' : (column.options.answertype === 'OK-NOT OK' ? 'OK' : (column.options.answertype === 'Uygun-Uygun Değil' ? 'Uygun' : ''))) : '');
              return (<td class={rowColIdentifier} style={(column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)}>
                <div class="flex flex--middle" style={{}}>
                  <vaadin-radio-button id={colIdentifier + "-1"} class={"focusable " + colIdentifier + "-1"} part={row._part_} value={option1} label={option1} disabled={isReadOnly} checked={row[column.dataIndex] === option1} on-click={() => t.radioClicked(option1, row, column)}></vaadin-radio-button>
                  <vaadin-radio-button id={colIdentifier + "-2"} class={"focusable " + colIdentifier + "-2"} part={row._part_} value={option2} label={option2} disabled={isReadOnly} checked={row[column.dataIndex] === option2} on-click={() => t.radioClicked(option2, row, column)}></vaadin-radio-button>
                </div>
              </td>
              );
            }
          }
          case 'search': {
            if (column.dataIndex !== 'na') {
              return (<td class={rowColIdentifier} style={(column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)}>
                <vaadin-text-field
                  id={colIdentifier}
                  part={row._part_}
                  label=""
                  helper-text=""
                  placeholder={(column?.options?.placeholder || '')}
                  clear-button-visible={(column?.options?.clearButtonVisible || true)}

                  required={(column?.options?.required || false)}
                  max-length={(column?.options?.maxlength || 50)}
                  min-length={(column?.options?.minlength || 0)}
                  pattern={(column?.options?.pattern || '')}
                  allowed-char-pattern={(column?.options?.allowedCharPattern || '')}

                  disabled={(column?.options?.disabled || false)}
                  readonly={(column?.options?.readonly || false)}

                  class={"focusable " + colIdentifier}
                  style={{ width: (column?.style?.width || "150px"), paddingBottom: "0px", paddingTop: "0px" }}
                  value={row[column.dataIndex]}

                  on-keydown={(e: KeyboardEvent) => {
                    e.stopPropagation();
                    if (e.key === 'Enter') {
                      t.iconClick(row, column);
                    }
                  }}
                  on-value-changed={(e: CustomEvent) => {
                    e.stopPropagation();
                    if (t.isEventListenerReady) {
                      if (e.detail.value !== '') {
                        (e.target as TextField).invalid = true;
                      } else {
                        (e.target as TextField).invalid = false;
                      }
                      row[column.dataIndex] = e.detail.value;
                    }
                    //t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
                    //console.log('search-on-value-changed', row, column, e.detail, e);
                  }}
                >
                  <vaadin-icon
                    slot="prefix"
                    src={UIIcons['fa__magnifying_glass']}
                    onClick={() => { t.iconClick(row, column); }}
                  ></vaadin-icon>
                </vaadin-text-field>
              </td>
              );
            }
          }
          case 'text': {
            if (column.dataIndex !== 'na') {
              const align: string = ((kbLayout === 'numpad' || kbLayout === 'numpadex') ? 'right' : 'left');
              let style: any = { fontSize: "xx-large", height: "60px", textAlign: align };
              if (typeof column.style !== 'undefined' && typeof column.style.maxWidth !== 'undefined') {
                style.maxWidth = column.style.maxWidth;
              }
              return (<td class={rowColIdentifier} style={(column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)}>
                <vaadin-text-field
                  id={colIdentifier}
                  part={row._part_}
                  label=""
                  helper-text=""
                  placeholder={(column?.options?.placeholder || '')}
                  clear-button-visible={(column?.options?.clearButtonVisible || false)}

                  required={(column?.options?.required || false)}
                  max-length={(column?.options?.maxlength || 50)}
                  min-length={(column?.options?.minlength || 0)}
                  pattern={(column?.options?.pattern || '')}
                  allowed-char-pattern={(column?.options?.allowedCharPattern || '')}

                  disabled={(column?.options?.disabled || false)}
                  readonly={(column?.options?.readonly || false)}

                  class={"focusable " + colIdentifier}
                  style={{ width: (column?.style?.width || "150px"), paddingBottom: "0px", paddingTop: "0px" }}
                  value={row[column.dataIndex]}

                  on-value-changed={(e: CustomEvent) => {
                    row[column.dataIndex] = e.detail.value;
                    if (t.isEventListenerReady) {
                      t.pxDataTableColumnChange.emit({ row, column, value: e.detail.value });
                    }
                    //console.log('text', row, column, e.detail, t.data);
                  }}
                >
                </vaadin-text-field>
              </td>
              );
            }
          }
          case 'toggle': {
            if (column.dataIndex !== 'na') {
              const option1: string = (column.options ? (column.options.answertype === 'Evet-Hayır' ? 'Hayır' : (column.options.answertype === 'OK-NOT OK' ? 'NOT OK' : (column.options.answertype === 'Uygun-Uygun Değil' ? 'Uygun Değil' : ''))) : '');
              const option2: string = (column.options ? (column.options.answertype === 'Evet-Hayır' ? 'Evet' : (column.options.answertype === 'OK-NOT OK' ? 'OK' : (column.options.answertype === 'Uygun-Uygun Değil' ? 'Uygun' : ''))) : '');
              if (column.columnSelect === true && t.toggleColumn === null) {
                t.toggleColumn = column;
              }
              const _c_ = (column.columnSelect ? 'columnSelect' : "focusable " + colIdentifier);
              return (<td class={rowColIdentifier} style={(column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)}>
                <px-toggle
                  id={colIdentifier}
                  tabindex={counterIndex++}
                  isChecked={row[column.dataIndex] === true}
                  disabled={isReadOnly}
                  part={row._part_}
                  class={_c_}
                  optionVisible={((option1 !== '' && option2 !== '') ? true : false)}
                  option1={option1}
                  option2={option2}
                  size="regular"
                  onPxToggleCheckedChanged={(res) => {
                    if (t.toggleColumn === null) {
                      t.toggleRowStateChange(res, row, column);
                    }
                  }}
                ></px-toggle>
              </td>);
            } else {
              if (column.style.display === '') {
                if (row[column.dataIndex + '__type']) {
                  const option1: string = 'Hayır';
                  const option2: string = 'Evet';
                  if (column.columnSelect === true && t.toggleColumn === null) {
                    t.toggleColumn = column;
                  }
                  const _c_ = (column.columnSelect ? 'columnSelect' : "focusable " + colIdentifier);
                  return (<td class={rowColIdentifier} style={(column?.style || null)} on-click={() => t.cellClicked(row.rowIndex, colIndex)}>
                    <px-toggle
                      id={colIdentifier}
                      tabindex={counterIndex++}
                      isChecked={row[column.dataIndex]}
                      disabled={row[column.dataIndex + '__disabled']}
                      part={row._part_}
                      class={_c_}
                      optionVisible={((option1 !== '' && option2 !== '') ? true : false)}
                      option1={option1}
                      option2={option2}
                      size="regular"
                      onPxToggleCheckedChanged={(res) => {
                        if (t.toggleColumn === null) {
                          t.toggleRowStateChange(res, row, column);
                        }
                      }}
                    ></px-toggle>
                  </td>);
                } else {
                  return (<td class="tdcell"></td>);
                }
              }
            }
          }
        }
        return;
      } else {
        let cellValue: any = row[column.dataIndex];
        if (column.options && column.options.format && cellValue !== '' && typeof cellValue !== 'undefined') {
          if (column.options.format === 'money') {
            const digits: number = (column?.options?.fractionDigits || 2);
            //console.log('cell:', digits, column.options, cellValue)
            //cellValue = Number((parseFloat(cellValue)).toFixed(digits)).toLocaleString();
            cellValue = new Intl.NumberFormat('tr-TR', { minimumFractionDigits: digits, maximumFractionDigits: digits }).format(cellValue);
          }
          //console.log('cell:', row[column.dataIndex], column.options)
        }
        if (typeof column.style !== 'undefined' && column.style !== null) {
          if (typeof column.style.textAlign !== 'undefined') {
            if (column.style.textAlign === 'right' && typeof column.style.paddingRight === 'undefined') {
              column.style.paddingRight = "5px";
            }
          } else {
            if (typeof column.style.paddingLeft === 'undefined') {
              column.style.paddingLeft = "5px";
            }
          }
        } else {
          column.style = { paddingLeft: "5px" };
        }
        return (<td class={rowColIdentifier} style={(column?.style || null)} innerHTML={cellValue} on-click={() => t.cellClicked(row.rowIndex, (counter - 1).toString())}>
          <input type='hidden' id={(column.dataIndex + '-' + row.rowIndex + '-' + counter)} value='' />
        </td>);
      }
    });
  }

  getTemplateBody() {
    let t = this;
    let counter: number = 0;
    let arrtmpData: any[] = t.data;
    let arrData: any[] = [];
    //if (Build.isDev) {
    //console.log('getTemplateBody|', t.myElement.tagName.toLowerCase(), t.data);
    //}
    if (t.isGroupedRows) {
      let tmpGroups: string[] = [];
      for (const row of arrtmpData) {
        let groupValue: string = '';
        let arrGroupValue: string[] = [];
        for (const item of t.arrGroupFields) {
          arrGroupValue.push(row[item]);
        }
        groupValue = arrGroupValue.join('|');
        if (tmpGroups.indexOf(groupValue) === -1) {
          tmpGroups.push(groupValue);
          for (const concatField of t.arrConcatFields) {
            row[concatField.afterFieldName] = row[concatField.beforeFieldName];
          }
          arrData.push(row);
        } else {
          for (const rowCurrent of arrData) {
            let arrTmpGroupValue: string[] = [];
            for (const item of t.arrGroupFields) {
              arrTmpGroupValue.push(rowCurrent[item]);
            }
            if (groupValue === arrTmpGroupValue.join('|')) {
              for (const concatField of t.arrConcatFields) {
                rowCurrent[concatField.afterFieldName] = rowCurrent[concatField.afterFieldName] + ' / ' + row[concatField.beforeFieldName];
              }
            }
          }
        }
      }
    } else {
      for (const row of arrtmpData) {
        for (const concatField of t.arrConcatFields) {
          row[concatField.afterFieldName] = row[concatField.beforeFieldName];
        }
      }
      arrData = arrtmpData;
    }
    if (arrData && arrData.length > 0) {
      let currentRowClass: string = 'even-row ';
      const objects: any = arrData.map((row: any) => {
        if (typeof row._id_ === 'undefined') {
          row._id_ = t.counter++;
        }
        const cPart = "DataTableRow-" + row._id_;
        row._part_ = cPart;
        row.rowIndex = counter++
        if (t.columnRenderer === 'default') {
          const rowCls: string = "datarow " + currentRowClass + (row?.rowCls || "") + " row-" + row.rowIndex;
          if (currentRowClass === 'even-row ') {
            currentRowClass = 'odd-row ';
          } else {
            currentRowClass = 'even-row ';
          }
          return (<tr class={rowCls} part={cPart} on-click={(e) => {
            if (t.clickTimeout !== null) {
              clearTimeout(t.clickTimeout);
              t.clickTimeout = null;
            }
            t.clickTimeout = setTimeout(function () {
              if (e.detail === 1) {
                t.rowClicked(row);
              }
            }, 200);
            //if (e.detail === 1) {
            //  t.rowClicked(row);
            //}
          }} on-dblclick={() => {
            if (t.clickTimeout !== null) {
              clearTimeout(t.clickTimeout);
              t.clickTimeout = null;
            }
            t.rowDblClicked(row);
          }
          }>
            {t.getTemplateBodyColumns(row)}
          </tr>
          );
        }
      });
      setTimeout(() => {
        t.initComponent();
      }, 0);
      return objects;
    }
  }

  render() {
    let t = this;
    //if (Build.isDev) {
    //  console.log('Render|', t.myElement.tagName.toLowerCase(), t.containerTable);
    //}
    if (t.isEditable) {
      t.isEventListenerReady = false;
      //if (typeof t.containerTable !== 'undefined') {
      //  console.log(t.containerTable.tBodies[0].innerHTML)
      //}
    }
    const cls = "table " + t.clsTable;
    return (
      <table ref={(el) => t.containerTable = el as HTMLTableElement} class={cls} hidden={t.hidden} cellPadding={0} cellspacing={0}>
        <thead>
          {t.getTemplateHeader()}
        </thead>
        <tbody>
          {t.getTemplateBody()}
        </tbody>
      </table>
    );
  }
}
