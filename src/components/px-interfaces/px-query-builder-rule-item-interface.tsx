export interface IQueryBuilderRuleItem {
  id: string;
  operator: string;
  field?: string;
  type?: string;
  input?: string;
  value?: string | number | any[];
  flags?: object;
}
