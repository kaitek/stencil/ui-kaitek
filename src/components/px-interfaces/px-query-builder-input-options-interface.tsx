export interface IQueryBuilderInputOptions {
  optgroup?: string | object; //Optional id of an <optgroup> in the filters dropdown.
  values?: any[] | object; // Required for radio and checkbox inputs. Generally needed for select inputs.
  value_separator?: string;// Used the split and join the value when a text input is used with an operator allowing multiple values (between for example).
  default_value?: any;
  input_event?: string; // 'change'	Space separated list of DOM events which the builder should listen to detect value changes.
  size?: number; // Only for text and textarea inputs: horizontal size of the input.
  rows?: number;// Only for textarea inputs: vertical size of the input.
  multiple?: boolean;// false Only for select inputs: accept multiple values.
  placeholder?: string;// Only for text and textarea inputs: placeholder to display inside the input.
  vertical?: boolean;// false Only for radio and checkbox inputs: display inputs vertically on not horizontally.
  path?: string;//retun value object path
}
