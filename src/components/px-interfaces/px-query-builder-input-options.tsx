import { IQueryBuilderInputOptions } from "./px-query-builder-input-options-interface";

export class QueryBuilderInputOptions {
  optgroup?: string | object; //Optional id of an <optgroup> in the filters dropdown.
  values?: any[] | object; // Required for radio and checkbox inputs. Generally needed for select inputs.
  value_separator?: string;// Used the split and join the value when a text input is used with an operator allowing multiple values (between for example).
  default_value?: any;
  input_event?: string; // 'change'	Space separated list of DOM events which the builder should listen to detect value changes.
  size?: number; // Only for text and textarea inputs: horizontal size of the input.
  rows?: number;// Only for textarea inputs: vertical size of the input.
  multiple?: boolean;// false Only for select inputs: accept multiple values.
  placeholder?: string;// Only for text and textarea inputs: placeholder to display inside the input.
  vertical?: boolean;// false Only for radio and checkbox inputs: display inputs vertically on not horizontally.
  path?: string;//retun value object path
  constructor(obj: IQueryBuilderInputOptions) {
    this.optgroup = (obj.optgroup ? obj.optgroup : null);
    this.values = (obj.values ? obj.values : null);
    this.value_separator = (obj.value_separator ? obj.value_separator : null);
    this.default_value = (obj.default_value ? obj.default_value : null);
    this.input_event = (obj.input_event ? obj.input_event : null);
    this.size = (obj.size ? obj.size : null);
    this.rows = (obj.rows ? obj.rows : null);
    this.multiple = (obj.multiple ? obj.multiple : null);
    this.placeholder = (obj.placeholder ? obj.placeholder : null);
    this.vertical = (obj.vertical ? obj.vertical : null);
    this.path = (obj.path ? obj.path : 'text');
  }
}
