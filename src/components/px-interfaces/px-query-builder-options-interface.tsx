import { IQueryBuilderFilterItem } from "./px-query-builder-filter-item-interface";
import { IQueryBuilderOperatorItem } from "./px-query-builder-operator-item-interface";
import { IQueryBuilderRules } from "./px-query-builder-rules-interface";

export interface IQueryBuilderOptions {
  //lang_code?: string;
  lang?: any;
  filters: IQueryBuilderFilterItem[];
  filter_combo_items?: string[];
  rules?: IQueryBuilderRules[];
  default_filter?: string;// null The id of the default filter for any new rule.
  sort_filters?: boolean;// false
  allow_groups?: boolean;// true	Number of allowed nested groups. true for no limit.
  allow_empty?: boolean;// false No error will be thrown is the builder is entirely empty.
  display_errors?: boolean;// true	When an error occurs on a rule, display an icon  with a tooltip explaining the error.
  conditions?: string[];// ['AND', 'OR']	Array of available group conditions. Use the lang option to change the label.
  default_condition?: string;// 'AND'	Default active condition.
  inputs_separator?: string;// ' , '	Piece of HTML used to separate multiple inputs (for between operator).
  display_empty_filter?: boolean;// true	Add an empty option with select_placeholder string to the filter dropdowns. If the empty filter is disabled and no default_filter is defined, the first filter will be loaded when adding a rule.
  select_placeholder?: string;// '------'	Label of the "no filter" option.
  operators?: IQueryBuilderOperatorItem[];
  types?: any;
}
