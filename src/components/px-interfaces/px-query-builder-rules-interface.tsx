import { IQueryBuilderRuleItem } from "./px-query-builder-rule-item-interface";

export interface IQueryBuilderRules {
  condition: string;// 'AND', 'OR'
  rules?: (IQueryBuilderRules | IQueryBuilderRuleItem)[];
  not?: boolean;
  valid?: boolean;
}
