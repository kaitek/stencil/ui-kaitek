import { IDataTableColumn } from "./px-data-table-column-interface";
//import { IDataTableColumnFilter } from "./px-data-table-column-filter-interface";
import { DataTableColumnFilter } from "./px-data-table-column-filter";

export class DataTableColumn {
  label: string;
  dataIndex: string;
  fieldName?: string;
  alias?: string;
  style?: any;
  kblayout?: string;
  type?: string;
  columnSelect?: boolean;
  isReadOnly?: boolean;
  options?: any
  filter?: DataTableColumnFilter
  constructor(obj: IDataTableColumn) {
    this.label = obj.label;
    this.dataIndex = obj.dataIndex;
    this.fieldName = (obj.fieldName ? obj.fieldName : obj.dataIndex);
    this.alias = (obj.alias ? obj.alias : '');
    this.style = (obj.style ? obj.style : null);
    this.kblayout = (obj.kblayout ? obj.kblayout : 'tr_TR');
    this.type = (obj.type ? obj.type : '');
    this.columnSelect = (obj.columnSelect ? obj.columnSelect : false);
    this.isReadOnly = (obj.isReadOnly ? obj.isReadOnly : false);
    this.options = (obj.options ? obj.options : false);
    this.filter = (typeof obj.filter === 'object' ? obj.filter : null);
  }
}
