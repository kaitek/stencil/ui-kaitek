import { IDataTableColumnFilter } from "./px-data-table-column-filter-interface";

export class DataTableColumnFilter {
  field?: string;
  type?: string;
  details?: any;
  constructor(obj: IDataTableColumnFilter) {
    this.field = (obj.field ? obj.field : 'text');
    this.type = (obj.type ? obj.type : 'single');
    this.details = (obj.details ? obj.details : null);
  }
}
