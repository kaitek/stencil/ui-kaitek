import { IQueryBuilderInputOptions } from "./px-query-builder-input-options-interface";
import { IQueryBuilderInputValidation } from "./px-query-builder-input-validation-interface";

export interface IQueryBuilderFilterItem {
  id: string;// Unique identifier of the filter.
  type: string;
  field?: string;// =id Field used by the filter, multiple filters can use the same field.
  label?: string;// =field Label used to display the filter. It can be simple string or a map for localization.
  input?: string; // Type of the field. Available types are date, text, number, textarea, radio, checkbox and select.
  unique?: boolean;
  input_options?: IQueryBuilderInputOptions;

  validation?: IQueryBuilderInputValidation;// Object of options for rule validation.
  operators?: string[];// Array of operators types to use for this filter. If empty the filter will use all applicable operators.
  default_operator?: string;// operators[0] Type code of the default operator for this filter.
  data?: object;// Additional data not used by QueryBuilder but that will be added to the output rules object. Use this to store any functional data you need.

  valueGetter?: any;// Function used to get the input(s) value. If provided the default function is not run. It takes 1 parameter: rule the Rule object
  valueSetter?: any;// Function used to set the input(s) value. If provided the default function is not run. It takes 2 parameters: rule the Rule object value
}
