import { IQueryBuilderRuleItem } from "./px-query-builder-rule-item-interface";

export class QueryBuilderRuleItem {
  id: string;
  operator: string;
  field?: string;
  type?: string;
  input?: string;
  value?: string | number | any[];
  flags?: object;
  constructor(obj: IQueryBuilderRuleItem) {
    this.id = obj.id;
    this.operator = obj.operator;
    this.field = (obj.field ? obj.field : '');
    this.type = (obj.type ? obj.type : '');
    this.input = (obj.input ? obj.input : '');
    this.value = (obj.value ? obj.value : '');
    this.flags = (obj.flags ? obj.flags : {});
  }
}
