import { IQueryBuilderOperatorItem } from "./px-query-builder-operator-item-interface";

export class QueryBuilderOperatorItem {
  type: string;
  optgroup: string;
  nb_inputs: number;
  multiple: boolean;
  apply_to: string[];
  constructor(obj: IQueryBuilderOperatorItem) {
    this.type = obj.type;
    this.optgroup = (obj.optgroup ? obj.optgroup : null);
    this.nb_inputs = (obj.nb_inputs ? obj.nb_inputs : 0);
    this.multiple = (obj.multiple ? obj.multiple : false);
    this.apply_to = (obj.apply_to ? obj.apply_to : []);
  }
}
