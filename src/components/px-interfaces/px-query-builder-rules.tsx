import { IQueryBuilderRuleItem } from "./px-query-builder-rule-item-interface";
import { IQueryBuilderRules } from "./px-query-builder-rules-interface";

export class QueryBuilderRules {
  condition: string;// 'AND', 'OR'
  rules?: (IQueryBuilderRules | IQueryBuilderRuleItem)[];
  not?: boolean;
  valid?: boolean;
  constructor(obj: IQueryBuilderRules) {
    this.condition = obj.condition;
    this.rules = (obj.rules ? obj.rules : null);
    this.not = (obj.not ? obj.not : false);
    this.valid = (obj.valid ? obj.valid : false);
  }
}
