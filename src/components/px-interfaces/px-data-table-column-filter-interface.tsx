export interface IDataTableColumnFilter {
  field?: string;
  type?: string;
  details?: any;
}
