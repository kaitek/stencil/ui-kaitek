import { IDataTableColumnFilter } from "./px-data-table-column-filter-interface";
export interface IDataTableColumn {
  label: string;
  dataIndex: string;
  fieldName?: any;
  alias?: any;
  style?: any;
  type?: string;
  kblayout?: string;
  columnSelect?: boolean;
  isReadOnly?: boolean;
  options?: any;
  filter?: IDataTableColumnFilter;
}
