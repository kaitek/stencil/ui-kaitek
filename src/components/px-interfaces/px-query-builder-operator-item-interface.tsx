export interface IQueryBuilderOperatorItem {
  type: string;
  optgroup?: string;
  nb_inputs: number;
  multiple: boolean;
  apply_to: string[];
}
