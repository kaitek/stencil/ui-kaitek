import { IQueryBuilderFilterItem } from "./px-query-builder-filter-item-interface";
import { IQueryBuilderInputOptions } from "./px-query-builder-input-options-interface";
import { IQueryBuilderInputValidation } from "./px-query-builder-input-validation-interface";

export class QueryBuilderFilterItem {
  id: string;// Unique identifier of the filter.
  type: string;
  field?: string;// =id Field used by the filter, multiple filters can use the same field.
  label?: string;// =field Label used to display the filter. It can be simple string or a map for localization.
  input?: string; // Type of the field. Available types are date, text, number, textarea, radio, checkbox and select.
  unique?: boolean;
  input_options?: IQueryBuilderInputOptions;

  validation?: IQueryBuilderInputValidation;// Object of options for rule validation.
  operators?: string[];// Array of operators types to use for this filter. If empty the filter will use all applicable operators.
  default_operator?: string;// operators[0] Type code of the default operator for this filter.
  data?: object;// Additional data not used by QueryBuilder but that will be added to the output rules object. Use this to store any functional data you need.

  valueGetter?: any;// Function used to get the input(s) value. If provided the default function is not run. It takes 1 parameter: rule the Rule object
  valueSetter?: any;// Function used to set the input(s) value. If provided the default function is not run. It takes 2 parameters: rule the Rule object value
  constructor(obj: IQueryBuilderFilterItem) {
    this.id = obj.id;
    this.type = obj.type;
    this.field = (obj.field ? obj.field : obj.id);
    this.label = (obj.label ? obj.label : obj.field);
    this.input = (obj.input ? obj.input : obj.type);
    this.unique = (obj.unique ? obj.unique : false);
    this.input_options = (obj.input_options ? obj.input_options : null);
    this.validation = (obj.validation ? obj.validation : null);
    this.operators = (obj.operators ? obj.operators : null);
    this.default_operator = (obj.default_operator ? obj.default_operator : (obj.operators ? obj.operators[0] : null));
    this.data = (obj.data ? obj.data : null);
    this.valueGetter = (obj.valueGetter ? obj.valueGetter : null);
    this.valueSetter = (obj.valueSetter ? obj.valueSetter : null);
  }
}
