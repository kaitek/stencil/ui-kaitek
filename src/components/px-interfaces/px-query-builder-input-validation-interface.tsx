export interface IQueryBuilderInputValidation {
  format?: string; // For date, time, datetime: a valid MomentJS string format For string: a regular expression (plain or RegExp object)
  max?: string | number;// For integer, double: maximum value For date, time, datetime: maximum value, respecting format For string: maximum length
  min?: string | number; // For integer, double: minimum value For date, time, datetime: minimum value, respecting format For string: minimum length
  step?: number; // For integer, double: step value (for double you should always provide this value in order to pass the browser validation on <number> inputs). any is allowed to bypass browser validation.
  messages?: object; // Custom error messages for the standard validations, keys are validation names.
  allow_empty_value?: boolean;// true to allow empty user input for this filter
  callback?: object;// A function used to perform the validation. If provided, the default validation will not be performed. It must returns true if the value is valid or an error string otherwise. It takes 2 parameters:value rule the Rule object
}
