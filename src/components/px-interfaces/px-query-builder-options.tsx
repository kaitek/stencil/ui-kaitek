import { IQueryBuilderFilterItem } from "./px-query-builder-filter-item-interface";
import { QueryBuilderOperatorItem } from "./px-query-builder-operator-item";
import { IQueryBuilderOperatorItem } from "./px-query-builder-operator-item-interface";
import { IQueryBuilderOptions } from "./px-query-builder-options-interface";
import { IQueryBuilderRules } from "./px-query-builder-rules-interface";

export class QueryBuilderOptions {
  //lang_code: string;
  lang?: any;
  filters: IQueryBuilderFilterItem[];
  filter_combo_items?: string[];
  rules?: IQueryBuilderRules[];
  default_filter?: string;// null The id of the default filter for any new rule.
  sort_filters?: boolean;// false
  allow_groups?: boolean;// true	Number of allowed nested groups. true for no limit.
  allow_empty?: boolean;// false No error will be thrown is the builder is entirely empty.
  display_errors?: boolean;// true	When an error occurs on a rule, display an icon  with a tooltip explaining the error.
  conditions?: string[];// ['AND', 'OR']	Array of available group conditions. Use the lang option to change the label.
  default_condition?: string;// 'AND'	Default active condition.
  inputs_separator?: string;// ' , '	Piece of HTML used to separate multiple inputs (for between operator).
  display_empty_filter?: boolean;// true	Add an empty option with select_placeholder string to the filter dropdowns. If the empty filter is disabled and no default_filter is defined, the first filter will be loaded when adding a rule.
  select_placeholder?: string;// '------'	Label of the "no filter" option.
  operators?: IQueryBuilderOperatorItem[];
  types?: any = {
    string: "string",
    integer: "number",
    double: "number",
    date: "datetime",
    time: "datetime",
    datetime: "datetime",
    boolean: "boolean"
  };
  constructor(obj: IQueryBuilderOptions) {
    const baseOperators: IQueryBuilderOperatorItem[] = [
      new QueryBuilderOperatorItem({ type: 'equal', optgroup: 'basit', nb_inputs: 1, multiple: !1, apply_to: ["string", "number", "datetime", "boolean"] }),
      new QueryBuilderOperatorItem({ type: 'not_equal', optgroup: 'basit', nb_inputs: 1, multiple: !1, apply_to: ["string", "number", "datetime", "boolean"] }),
      new QueryBuilderOperatorItem({ type: 'in', optgroup: 'basit', nb_inputs: 1, multiple: !0, apply_to: ["string", "number", "datetime"] }),
      new QueryBuilderOperatorItem({ type: 'not_in', optgroup: 'basit', nb_inputs: 1, multiple: !0, apply_to: ["string", "number", "datetime"] }),
      new QueryBuilderOperatorItem({ type: 'less', optgroup: 'sayı', nb_inputs: 1, multiple: !1, apply_to: ["number", "datetime"] }),
      new QueryBuilderOperatorItem({ type: 'less_or_equal', optgroup: 'sayı', nb_inputs: 1, multiple: !1, apply_to: ["number", "datetime"] }),
      new QueryBuilderOperatorItem({ type: 'greater', optgroup: 'sayı', nb_inputs: 1, multiple: !1, apply_to: ["number", "datetime"] }),
      new QueryBuilderOperatorItem({ type: 'greater_or_equal', optgroup: 'sayı', nb_inputs: 1, multiple: !1, apply_to: ["number", "datetime"] }),
      new QueryBuilderOperatorItem({ type: 'between', optgroup: 'sayı', nb_inputs: 2, multiple: !1, apply_to: ["number", "datetime"] }),
      new QueryBuilderOperatorItem({ type: 'not_between', optgroup: 'sayı', nb_inputs: 2, multiple: !1, apply_to: ["number", "datetime"] }),
      new QueryBuilderOperatorItem({ type: 'begins_with', optgroup: 'karakter', nb_inputs: 1, multiple: !1, apply_to: ["string"] }),
      new QueryBuilderOperatorItem({ type: 'not_begins_with', optgroup: 'karakter', nb_inputs: 1, multiple: !1, apply_to: ["string"] }),
      new QueryBuilderOperatorItem({ type: 'contains', optgroup: 'karakter', nb_inputs: 1, multiple: !1, apply_to: ["string"] }),
      new QueryBuilderOperatorItem({ type: 'not_contains', optgroup: 'karakter', nb_inputs: 1, multiple: !1, apply_to: ["string"] }),
      new QueryBuilderOperatorItem({ type: 'ends_with', optgroup: 'karakter', nb_inputs: 1, multiple: !1, apply_to: ["string"] }),
      new QueryBuilderOperatorItem({ type: 'not_ends_with', optgroup: 'karakter', nb_inputs: 1, multiple: !1, apply_to: ["string"] }),
      new QueryBuilderOperatorItem({ type: 'is_empty', nb_inputs: 0, multiple: !1, apply_to: ["string"] }),
      new QueryBuilderOperatorItem({ type: 'is_not_empty', nb_inputs: 0, multiple: !1, apply_to: ["string"] }),
      new QueryBuilderOperatorItem({ type: 'is_null', nb_inputs: 0, multiple: !1, apply_to: ["string", "number", "datetime", "boolean"] }),
      new QueryBuilderOperatorItem({ type: 'is_not_null', nb_inputs: 0, multiple: !1, apply_to: ["string", "number", "datetime", "boolean"] })
    ];
    //this.lang_code = (obj.lang_code ? obj.lang_code : 'tr');
    this.lang = {
      "__locale": "Turkish (tr)",
      "add_rule": "Kural",
      "add_group": "Grup",
      "clear": "Temizle",
      "delete_rule": "Sil",
      "delete_group": "Sil",
      "filter": "Filtrele",
      "conditions": {
        "AND": "VE",
        "OR": "VEYA"
      },
      "operators": {
        "equal": "eşit",
        "not_equal": "eşit değil",
        "in": "içinde",
        "not_in": "içinde değil",
        "less": "küçük",
        "less_or_equal": "küçük veya eşit",
        "greater": "büyük",
        "greater_or_equal": "büyük veya eşit",
        "between": "arasında",
        "not_between": "arasında değil",
        "begins_with": "ile başlayan",
        "not_begins_with": "ile başlamayan",
        "contains": "içeren",
        "not_contains": "içermeyen",
        "ends_with": "ile biten",
        "not_ends_with": "ile bitmeyen",
        "is_empty": "boş ise",
        "is_not_empty": "boş değil ise",
        "is_null": "yok ise",
        "is_not_null": "var ise"
      },
      "errors": {
        "no_filter": "Bir filtre seçili değil",
        "empty_group": "Grup bir eleman içermiyor",
        "radio_empty": "Seçim yapılmalı",
        "checkbox_empty": "Seçim yapılmalı",
        "select_empty": "Seçim yapılmalı",
        "string_empty": "Bir metin girilmeli",
        "string_exceed_min_length": "En az {0} karakter girilmeli",
        "string_exceed_max_length": "En fazla {0} karakter girilebilir",
        "string_invalid_format": "Uyumsuz format ({0})",
        "number_nan": "Sayı değil",
        "number_not_integer": "Tam sayı değil",
        "number_not_double": "Ondalıklı sayı değil",
        "number_exceed_min": "Sayı {0}'den/dan daha büyük olmalı",
        "number_exceed_max": "Sayı {0}'den/dan daha küçük olmalı",
        "number_wrong_step": "{0} veya katı olmalı",
        "number_between_invalid": "Geçersiz değerler, {0} değeri {1} değerinden büyük",
        "datetime_empty": "Tarih Seçilmemiş",
        "datetime_invalid": "Uygun olmayan tarih formatı ({0})",
        "datetime_exceed_min": "{0} Tarihinden daha sonrası olmalı.",
        "datetime_exceed_max": "{0} Tarihinden daha öncesi olmalı.",
        "datetime_between_invalid": "Geçersiz değerler, {0} değeri {1} değerinden büyük",
        "boolean_not_valid": "Değer Doğru/Yanlış(bool) olmalı",
        "operator_not_multiple": "Operatör \"{1}\" birden fazla değer kabul etmiyor"
      },
      "invert": "Ters Çevir",
      "NOT": "DEĞİL"
    };
    this.filters = obj.filters;
    this.filter_combo_items = [];
    for (const item of this.filters) {
      this.filter_combo_items = [...this.filter_combo_items, item.label];
    }
    this.filter_combo_items.sort();
    this.rules = (obj.rules ? obj.rules : []);
    this.default_filter = (obj.default_filter ? obj.default_filter : null);
    this.sort_filters = (obj.sort_filters ? obj.sort_filters : false);
    this.allow_groups = (obj.allow_groups ? obj.allow_groups : true);
    this.allow_empty = (obj.allow_empty ? obj.allow_empty : false);
    this.display_errors = (obj.display_errors ? obj.display_errors : true);
    this.conditions = (obj.conditions ? obj.conditions : ['AND', 'OR']);
    this.default_condition = (obj.default_condition ? obj.default_condition : 'AND');
    this.inputs_separator = (obj.inputs_separator ? obj.inputs_separator : ' , ');
    this.display_empty_filter = (obj.display_empty_filter ? obj.display_empty_filter : true);
    this.select_placeholder = (obj.select_placeholder ? obj.select_placeholder : '------');
    this.operators = (obj.operators ? obj.operators : baseOperators);
  }
}
