// import '@vaadin/vaadin-lumo-styles/all-imports.js';
export default async () => {
  /**
   * The code to be executed should be placed within a default function that is
   * exported by the global script. Ensure all of the code in the global script
   * is wrapped in the function() that is exported.
   */
  if (typeof window !== "undefined") {
    (async () => {
      //await import("@vaadin/side-nav/enable.js");//v24.1
      const allImports = await import(
        "@vaadin/vaadin-lumo-styles/all-imports.js"
      );

      const style = document.createElement("style");
      Object.values(allImports).forEach((css) => {
        style.append(document.createTextNode(css.toString()));
      });
      document.head.append(style);
    })();
  }
};
