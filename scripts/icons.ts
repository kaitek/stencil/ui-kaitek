import * as fs from 'fs';
import { globby } from 'globby';
import * as path from "path";
const createCopyright = () => {
  return `/**
 * @license
 * Copyright (c) 2013 - ${new Date().getFullYear()} Kaitek Ltd.
 * This program is available under Apache License Version 2.0, available at https://www.kaitek.com.tr/license/
 */`;
}
(async () => {
  const basePath = path.join(process.cwd(), 'src', 'components', 'ui-icons');
  const basePath2 = basePath.split('\\').join('/');
  // console.log(basePath)
  if (fs.existsSync(path.join(basePath, 'index.ts'))) {
    fs.unlinkSync(path.join(basePath, 'index.ts'));
  }
  let imports: string = ``;
  let exports: string = ``;
  const paths = (await globby([basePath2 + '/**/*'])).sort();
  // console.log(paths);
  for (const item of paths) {
    const relativePath = item.split(basePath2)[1];
    const iconName = relativePath.slice(1).split('.svg').join('').split('/').join('__').split('-').join('_');
    //if (relativePath === '/vaadin/star.svg') {
    const content = fs.readFileSync(item, 'utf-8');
    if (content.indexOf('<path fill="#444"') !== -1) {
      const new_content = content.split('<path fill="#444"').join('<path');
      //console.log(new_content);
      fs.writeFileSync(item, new_content);
    }
    //}
    //console.log(iconName);
    // imports += `\n  export * as ${iconName} from '.${relativePath}';`;
    imports += `\nimport ${iconName} from '.${relativePath}';`;
    exports += `\n  ` + (exports === '' ? '' : ',') + `${iconName}`;
  }
  const content = `${createCopyright()}
  ${imports}
  \nexport {${exports}};
  `;
  fs.writeFileSync(path.join(basePath, 'index.ts'), content)
  console.log('icons-finished');
})();
